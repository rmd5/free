const fetch = require("node-fetch");

let postcode = "EH9";
let count = 1;
let numberCount = 0;
let average = 0;
let numRooms = 1;

call();

function call() {
    fetch("https://www.rightmove.co.uk/house-prices/" + postcode + ".html?soldIn=1&page=" + count)
        .then(response => {
            return response.text();
        })
        .then(props => {
            let firstSplit = props.search("window.__PRELOADED_STATE__ = ") + 29;
            let splitOne = props.slice(firstSplit, props.length);
            let secondSplit = splitOne.search("</script>");
            let splitTwo = splitOne.slice(0, secondSplit);
            let result = JSON.parse(splitTwo);
            let properties = result.results.properties;
            console.log(result);
            // console.log("\n" + count + "\n");
            for(let i = 0; i < properties.length; i++){
                numRoomsResult = properties[i].bedrooms;
                if(numRoomsResult == numRooms){
                    numberCount++;
                    let price = properties[i].transactions[0].displayPrice;
                    price = price.slice(1, price.length);
                    price = price.replace(/,/g, "")
                    price = parseInt(price);
                    // console.log(price);
                    average += price;
                }
            }
            if(properties.length == 25){
                count++;
                call();
            } else {
                // console.log("\n" + average / numberCount);
            }
        });
}