import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

class Photoshoot extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			booking: null,
			booked: false
		};
	}

	nextPage = () => {
		window.location.href = "https://free.co.uk/dashboard"
	}

	prevPage = () => {
		if(this.state.booked === true){
			this.setState({
				booked: false,
				booking: null
			});
		} else {
			document.getElementById("leaveModal").style.display = "block";
		}
	}

	leavePage = (e) => {
		if(e.currentTarget.value === "yes"){
			window.location.href = "https://free.co.uk/dashboard";
		} else {
			document.getElementById("leaveModal").style.display = "none";
		}
	}

	accordion = (e) => {
		if (document.getElementById(e.currentTarget.value)) {
			if (document.getElementById(e.currentTarget.value).style.maxHeight === document.getElementById(e.currentTarget.value).scrollHeight + "px") {
				document.getElementById(e.currentTarget.value).style.maxHeight = "0px";
				if (document.getElementById("moreOptions")) {
					document.getElementById("moreOptions").innerHTML = "View later appointments";
				}
			} else {
				document.getElementById(e.currentTarget.value).style.maxHeight = document.getElementById(e.currentTarget.value).scrollHeight + "px";
				if (document.getElementById("moreOptions")) {
					document.getElementById("moreOptions").innerHTML = "View less appointments";
				}
			}
		}
	}

	bookAppointment = (e) => {
		let booking = e.currentTarget.value;
		this.setState({
			booking: booking,
			booked: true
		});
	}

	render() {
		let set = "";
		if(this.state.booking !== null){
			let index = this.state.booking.indexOf("-");
			let day = this.state.booking.slice(0, index);
			let slice = this.state.booking.slice(index + 1, this.state.booking.length);

			index = slice.indexOf("-");
			let date = slice.slice(0,index);
			slice = slice.slice(index + 1, slice.length);

			let time = slice;

			set = <div className="bookingTime"><span className="timeRosette">{time}</span><br/>{day} {date}</div>
		}

		return (
			<div>
				<div className="navbar">
					<button className="back" onClick={this.prevPage}>	
						<img className="backArrow" src={require("./img/icon-arrow-back.svg")}></img>	
						<div className="backText">		
							Back	
						</div>
					</button>
					<img className="logo" src={require("./img/logo.svg")}></img>
				</div>
				<div className="page" style={{display: this.state.booked === false ? "block" : "none"}}>
					<div className="pageHeading">
						Select an appointment for your free Splento photoshoot
					</div>
					<div className="pageContent">
						Please make sure the property is clean and presentable for the appointment,
						the photographer will meet you there.
					</div>
					<button className="radio" value="Monday-27th-2:00pm" onClick={this.bookAppointment}>Monday 27th <span className="btnTime">2:00pm</span></button><br/>
					<button className="radio" value="Monday-27th-4:00pm" onClick={this.bookAppointment}>Monday 27th <span className="btnTime">4:00pm</span></button><br/>
					<button className="radio" value="Tuesday-28th-9:00am" onClick={this.bookAppointment}>Tuesday 28th <span className="btnTime">9:00am</span></button><br/>
					<button className="radio" value="Tuesday-28th-1:00pm" onClick={this.bookAppointment}>Tuesday 28th <span className="btnTime">1:00pm</span></button><br/>
					<div className="accordion" id="photoAccordion">
						<button className="radio" value="Tuesday-28th-2:00pm" onClick={this.bookAppointment}>Tuesday 28th <span className="btnTime">3:00pm</span></button><br/>
						<button className="radio" value="Wednesday-29th-10:00am" onClick={this.bookAppointment}>Wednesday 29th <span className="btnTime">10:00am</span></button><br/>
						<button className="radio" value="Wednesday-29th-5:00pm" onClick={this.bookAppointment}>Wednesday 29th <span className="btnTime">5:00pm</span></button><br/>
					</div>
					<button id="moreOptions" className="radio" value="photoAccordion" onClick={this.accordion}>View later appointments</button>
					<div id="leaveModal" className="modal">
						<div className="modalContent">
							<div className="pageHeading">
								Go back to the dashboard?
							</div>
							<button className="radio" value="yes" onClick={this.leavePage}>Yes</button><br/>
							<button className="radio" value="no" onClick={this.leavePage}>No</button>
						</div>
					</div>
				</div>

				<div className="page" style={{display: this.state.booked === false ? "none" : "block"}}>
					<div className="pageHeading">
						You're all booked!
					</div>
					<div className="pageContent">
						Your Splento photographer will meet you at the property.
					</div>
					<img className="rosette" src={require("./img/rosette-frame.svg")}></img>
					{set}
					<button className="capBtn" onClick={this.nextPage}>View my dashboard</button>
				</div>
			</div>
		);
	}
}

ReactDOM.render(
	<React.StrictMode>
		<Photoshoot />
	</React.StrictMode>,
	document.getElementById('root')
);
