function accordion(accordion, x) {
    if(document.getElementById(accordion)){
        let acc = document.getElementById(accordion);
        if(acc.style.maxHeight === "0" || acc.style.maxHeight === "0px"){
            acc.style.maxHeight = acc.scrollHeight + "px";
            if(document.getElementById(x)){
                document.getElementById(x).style.transform = "rotate(45deg)";
            }
        } else {
            acc.style.maxHeight = "0";
            if(document.getElementById(x)){
                document.getElementById(x).style.transform = "rotate(0deg)";
            }
        }
    }
}