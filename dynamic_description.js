function getRandom(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

// let localStorageData = localStorage.getItem("freeDate");
let localStorageData = 0;
let propDescription = "";

if (localStorageData !== null) {

  let propTypeDescription = "";
  let propFloorDescription = "";
  let propLastDecoratedDescription = "";
  let propBuiltDescription = "";
  let propBedsDescription = "";
  let propBathsDescription = "";
  let propOutdoorSpaceDescription = "";
  let propParkingTypeDescription = "";
  let propSizeDescription = "";
  let propHasLiftDescription = "";
  let propHeatingTypeDescription = "";
  let propAskingPriceDescription = "";
  let propTenureDescription = "";
  let propLeaseYearsDescription = "";
  let propExtraCostsDescription = "";

  //----------------------------------------------------------------------------------------------//

  // let propType = localStorageData.propType;
  // let propFloor = localStorageData.propFloor;
  // let propLastDecorated = localStorageData.propLastDecorated;
  // let propBuilt = localStorageData.propBuilt;
  // let propBeds = localStorageData.propBeds;
  // let propBaths = localStorageData.propBaths;
  // let propOutdoorSpace = localStorageData.propOutdoorSpace;
  // let propParkingType = localStorageData.propParkingType;
  // let propSize = localStorageData.propSize;
  // let propHasLift = localStorageData.propHasLift;
  // let propHeatingType = localStorageData.propHeatingType;
  // let propAskingPrice = localStorageData.propAskingPrice;
  // let propTenure = localStorageData.propTenure;
  // let propLeaseYears = localStorageData.propLeaseYears;
  // let propExtraCosts = localStorageData.propExtraCosts;
  // let descriptionTone = localStorageData.descriptionTone;

  // //----------------------------------------------------------------------------------------------//

  // //----------------------------------------------------------------------------------------------//

  // detached, semi, terraced, bungalo, flat
  let propTypeArray = ["detached-house", "semi-detached-house", "terraced-house", "bungalow", "flat"];
  let propRandom = getRandom(0, 5);
  let propType = propTypeArray[4];

  //basement, groundFloor, 1stFloor, 2ndFloor, higher
  let propFloorArray = ["basement", "ground-floor", "first-floor", "second-floor", "higher"];
  propRandom = getRandom(0, 5);
  let propFloor = propFloorArray[2];

  //thisYear, year2019, year2018, pre2018
  let propLastDecoratedArray = ["thisYear", "year2019", "year2018", "pre2018"];
  propRandom = getRandom(0, 4);
  let propLastDecorated = propLastDecoratedArray[propRandom];

  //brandNew, last10Years, between1950and2000,
  let propBuiltArray = ["brandNew", "last20Years", "between1950and2000", "between1900and1950", "pre1900"];
  propRandom = getRandom(0, 5);
  let propBuilt = propBuiltArray[3];

  //studio, 1, 2, 3, 4
  // let propBedsArray = [0, 1, 2, 3, 4];
  propRandom = getRandom(0, 10);
  let propBeds = propRandom;

  //1, 2, 3
  // let propBathsArray = [1, 2, 3];
  propRandom = getRandom(1, 10);
  let propBaths = propRandom;

  //privateGarden, patio, balcony, no-outside
  let propOutdoorSpaceArray = ["private-garden", "patio", "terrace", "no-outside"];
  propRandom = getRandom(0, 4);
  let propOutdoorSpace = propOutdoorSpaceArray[propRandom];

  //garage, driveway, permitRequired
  let propParkingTypeArray = ["garage", "driveway", "permit", "on-street", "off-street", "garage-and-driveway", "allocated"];
  propRandom = getRandom(0, 7);
  let propParkingType = propParkingTypeArray[propRandom];

  let propSize = getRandom(20, 100);

  //true, false
  let propHasLiftArray = ["higher-with-lift", "higher-no-lift"];
  propRandom = getRandom(0, 2);
  let propHasLift = propHasLiftArray[propRandom];

  //electric, gas, oil, other
  let propHeatingTypeArray = ["electric", "gas", "oil", "exchanger"];
  propRandom = getRandom(0, 4);
  let propHeatingType = propHeatingTypeArray[3];

  let propAskingPrice = getRandom(10000, 200000).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');

  //leasehold, freehold
  let propTenureArray = ["leasehold", "freehold", "flying-freehold", "share-of-freehold"];
  propRandom = getRandom(0, 4);
  let propTenure = propTenureArray[propRandom];

  let propLeaseYears = getRandom(80, 1000);

  let propExtraCosts = getRandom(0, 100);

  //0 = plain, 1 = normal, 2 = agent
  let descriptionTone = getRandom(0, 2);

  //----------------------------------------------------------------------------------------------//

  if (descriptionTone === 0) {
    console.log("Plain");
  } else if (descriptionTone === 1) {
    console.log("Normal");
  } else if (descriptionTone === 2) {
    console.log("Agent");
  }

  let randomiser = getRandom(0, 3);

  switch (descriptionTone) {

    // --------------------------------------------Plain---------------------------------------------------------






    case 0:

      if (randomiser === 0) {
        switch (propType) {
          case "detached-house":
            propTypeDescription = "detached house";
            break;
          case "semi-detached-house":
            propTypeDescription = "semi-detached house";
            break;
          case "terraced-house":
            propTypeDescription = "terraced house";
            break;
          case "bungalow":
            propTypeDescription = "bungalow";
            break;
          case "flat":
            propTypeDescription = "apartment";
            break;
        }

        propDescription = propTypeDescription;

        if (propType === "flat") {
          switch (propFloor) {
            case "basement":
              propFloorDescription = "basement";
              propDescription = propFloorDescription + " " + propTypeDescription;
              break;
            case "ground-floor":
              propFloorDescription = "on the ground floor";
              propDescription += " " + propFloorDescription;
              break;
            case "first-floor":
              propFloorDescription = "on the first floor";
              propDescription += " " + propFloorDescription;
              break;
            case "second-floor":
              propFloorDescription = "on the second floor";
              propDescription += " " + propFloorDescription;
              break;
            case "higher":
              propFloorDescription = "on an upper floor";
              propDescription += " " + propFloorDescription;
              break;
            default:
              propFloorDescription = "";
              propDescription += propFloorDescription;
              break;
          }
        }

        let aAn = "A ";
        if (propType === "flat" && propFloor != "basement") {
          aAn = "An ";
        }

        switch (propLastDecorated) {
          case "thisYear":
            propLastDecoratedDescription = ", decorated this year";
            propDescription = aAn + propDescription + propLastDecoratedDescription + " and ";
            break;
          case "year2019":
            propLastDecoratedDescription = ", decorated last year";
            propDescription = aAn + propDescription + propLastDecoratedDescription + " and ";
            break;
          case "year2018":
            propLastDecoratedDescription = ", last decorated in 2018";
            propDescription = aAn + propDescription + propLastDecoratedDescription + " and ";
            break;
          case "pre2018":
            propLastDecoratedDescription = ", last decorated before 2018";
            propDescription = aAn + propDescription + propLastDecoratedDescription + " and ";
            break;
        }

        switch (propBuilt) {
          case "brandNew":
            propBuiltDescription = "built this year. ";
            propDescription += propBuiltDescription;
            break;
          case "last20Years":
            propBuiltDescription = "built in the last 20 years. ";
            propDescription += propBuiltDescription;
            break;
          case "between1950and2000":
            propBuiltDescription = "built between 1950 and 2000. ";
            propDescription += propBuiltDescription;
            break;
          case "between1900and1950":
            propBuiltDescription = "built between 1900 and 1950. ";
            propDescription += propBuiltDescription;
            break;
          case "pre1900":
            propBuiltDescription = "built before 1900. ";
            propDescription += propBuiltDescription;
            break;
        }

        switch (propBeds) {
          case 0:
            propBedsDescription = "It has a studio bedroom"
            propDescription += propBedsDescription;
            break;
          case 1:
            propBedsDescription = "It has one bedroom"
            propDescription += propBedsDescription;
            break;
          case 2:
            propBedsDescription = "It has two bedrooms"
            propDescription += propBedsDescription;
            break;
          case 3:
            propBedsDescription = "It has three bedrooms"
            propDescription += propBedsDescription;
            break;
          case 4:
            propBedsDescription = "It has four bedrooms"
            propDescription += propBedsDescription;
            break;
          case 5:
            propBedsDescription = "It has five bedrooms"
            propDescription += propBedsDescription;
            break;
          case 6:
            propBedsDescription = "It has six bedrooms"
            propDescription += propBedsDescription;
            break;
          case 7:
            propBedsDescription = "It has seven bedrooms"
            propDescription += propBedsDescription;
            break;
          case 8:
            propBedsDescription = "It has eight bedrooms"
            propDescription += propBedsDescription;
            break;
          case 9:
            propBedsDescription = "It has nine bedrooms"
            propDescription += propBedsDescription;
            break;
          default:
            propBedsDescription = "It has many bedrooms"
            propDescription += propBedsDescription;
            break;
        }

        switch (propBaths) {
          case 1:
            propBathsDescription = " and one bathroom"
            propDescription += propBathsDescription;
            break;
          case 2:
            propBathsDescription = " and two bathrooms"
            propDescription += propBathsDescription;
            break;
          case 3:
            propBathsDescription = " and three bathrooms"
            propDescription += propBathsDescription;
            break;
          default:
            let numberBaths = "many";
            if (propBaths === 4) {
              numberBaths = "four";
            } else if (propBaths === 5) {
              numberBaths = "five";
            } else if (propBaths === 6) {
              numberBaths = "six";
            } else if (propBaths === 7) {
              numberBaths = "seven";
            } else if (propBaths === 8) {
              numberBaths = "eight";
            } else if (propBaths === 9) {
              numberBaths = "nine";
            }
            propBathsDescription = " and " + numberBaths + " bathrooms"
            propDescription += propBathsDescription;
            break;
        }

        propDescription += ".";

        switch (propOutdoorSpace) {
          case "private-garden":
            propOutdoorSpaceDescription = " It has a private garden"
            propDescription += propOutdoorSpaceDescription;
            break;
          case "patio":
            propOutdoorSpaceDescription = " It has a patio area"
            propDescription += propOutdoorSpaceDescription;
            break;
          case "terrace":
            propOutdoorSpaceDescription = " It has a terrace"
            propDescription += propOutdoorSpaceDescription;
            break;
          case "no-outside":
            propOutdoorSpaceDescription = " It is located near public parks and gardens"
            propDescription += propOutdoorSpaceDescription;
            break;
        }

        switch (propParkingType) {
          case "garage":
            propParkingTypeDescription = " and a private garage."
            propDescription += propParkingTypeDescription;
            break;
          case "driveway":
            propParkingTypeDescription = " and a driveway."
            if (propOutdoorSpace === "no-outside") {
              propParkingTypeDescription = " and has a driveway."
            }
            propDescription += propParkingTypeDescription;
            break;
          case "permit":
            propParkingTypeDescription = " and permit required street parking."
            if (propOutdoorSpace === "no-outside") {
              propParkingTypeDescription = " and has permit required street parking."
            }
            propDescription += propParkingTypeDescription;
            break;
          case "on-street":
            propParkingTypeDescription = " and free street parking."
            if (propOutdoorSpace === "no-outside") {
              propParkingTypeDescription = " and has free street parking."
            }
            propDescription += propParkingTypeDescription;
            break;
          case "off-street":
            propParkingTypeDescription = " and off street parking."
            if (propOutdoorSpace === "no-outside") {
              propParkingTypeDescription = " and has off street parking."
            }
            propDescription += propParkingTypeDescription;
            break;
          case "garage-and-driveway":
            propParkingTypeDescription = " and both a garage and a driveway."
            if (propOutdoorSpace === "no-outside") {
              propParkingTypeDescription = " and has both a garage and a driveway."
            }
            propDescription += propParkingTypeDescription;
            break;
          case "allocated":
            propParkingTypeDescription = " and allocated parking."
            if (propOutdoorSpace === "no-outside") {
              propParkingTypeDescription = " and has allocated parking."
            }
            propDescription += propParkingTypeDescription;
            break;
        }

        propSizeDescription = " Covering " + propSize + " square metres";
        propDescription += propSizeDescription;

        if (propHasLift === "higher-with-lift") {
          propHasLiftDescription = ", with a lift";
          propDescription += propHasLiftDescription;
        }

        switch (propHeatingType) {
          case "electric":
            propHeatingTypeDescription = " and electric heating"
            if (propHasLift === "higher-no-lift") {
              propHeatingTypeDescription = ", and with electric heating";
            }
            propDescription += propHeatingTypeDescription;
            break;
          case "gas":
            propHeatingTypeDescription = " and gas central heating";
            if (propHasLift === "higher-no-lift") {
              propHeatingTypeDescription = ", and with gas central heating";
            }
            propDescription += propHeatingTypeDescription;
            break;
          case "oil":
            propHeatingTypeDescription = " and oil fired central heating";
            if (propHasLift === "higher-with-lift") {
              propHeatingTypeDescription = " and with oil fired central heating";
            }
            propDescription += propHeatingTypeDescription;
            break;
          default:
            propHeatingTypeDescription = " and a heat exchanger";
            if (propHasLift === "higher-no-lift") {
              propHeatingTypeDescription = ", and with a heat exchanger";
            }
            propDescription += propHeatingTypeDescription;
            break;
        }

        propAskingPriceDescription = ", this house is on the market for a price of £" + propAskingPrice + ".";
        propDescription += propAskingPriceDescription;

        switch (propTenure) {
          case "freehold":
            propTenureDescription = " It is a freehold property";
            propDescription += propTenureDescription;
            break;
          case "leasehold":
            propTenureDescription = " It is a leasehold property";
            propDescription += propTenureDescription;
            break;
          case "flying-freehold":
            propTenureDescription = " It is a flying freehold property";
            propDescription += propTenureDescription;
            break;
          case "share-of-freehold":
            propTenureDescription = " It is a share of freehold property";
            propDescription += propTenureDescription;
            break;
        }

        if (propTenure === "leasehold") {
          propLeaseYearsDescription = " with " + propLeaseYears + " years left on the lease";
          propDescription += propLeaseYearsDescription;
        }

        if (propExtraCosts > 0) {
          propExtraCostsDescription = " with additional costs of £" + propExtraCosts + " per month.";
          if (propTenure === "leasehold") {
            propExtraCostsDescription = " and additional costs of £" + propExtraCosts + " per month.";
          }
        } else {
          propExtraCostsDescription = " with no additional costs.";
          if (propTenure === "leasehold") {
            propExtraCostsDescription = " and no additional costs.";
          }
        }
        propDescription += propExtraCostsDescription;






      } else if (randomiser === 1) {

        switch (propBeds) {
          case 0:
            propBedsDescription = "A studio ";
            propDescription += propBedsDescription;
            break;
          case 1:
            propBedsDescription = "A one bedroom ";
            propDescription += propBedsDescription;
            break;
          case 2:
            propBedsDescription = "A two bedroom ";
            propDescription += propBedsDescription;
            break;
          case 3:
            propBedsDescription = "A three bedroom ";
            propDescription += propBedsDescription;
            break;
          case 4:
            propBedsDescription = "A four bedroom ";
            propDescription += propBedsDescription;
            break;
          case 5:
            propBedsDescription = "A five bedroom "
            propDescription += propBedsDescription;
            break;
          case 6:
            propBedsDescription = "A six bedroom "
            propDescription += propBedsDescription;
            break;
          case 7:
            propBedsDescription = "A seven bedroom "
            propDescription += propBedsDescription;
            break;
          case 8:
            propBedsDescription = "An eight bedroom "
            propDescription += propBedsDescription;
            break;
          case 9:
            propBedsDescription = "A nine bedroom "
            propDescription += propBedsDescription;
            break;
          default:
            propBedsDescription = "A multiple bedroom ";
            propDescription += propBedsDescription;
            break;
        }

        switch (propType) {
          case "detached-house":
            propTypeDescription = "detached house";
            break;
          case "semi-detached-house":
            propTypeDescription = "semi-detached house";
            break;
          case "terraced-house":
            propTypeDescription = "terraced house";
            break;
          case "bungalow":
            propTypeDescription = "bungalow";
            break;
          case "flat":
            propTypeDescription = "apartment";
            break;
        }

        propDescription = propBedsDescription + propTypeDescription;

        switch (propBaths) {
          case 1:
            propBathsDescription = " with one bathroom"
            propDescription += propBathsDescription;
            break;
          case 2:
            propBathsDescription = " with two bathrooms"
            propDescription += propBathsDescription;
            break;
          case 3:
            propBathsDescription = " with three bathrooms"
            propDescription += propBathsDescription;
            break;
          default:
            let numberBaths = "many";
            if (propBaths === 4) {
              numberBaths = "four";
            } else if (propBaths === 5) {
              numberBaths = "five";
            } else if (propBaths === 6) {
              numberBaths = "six";
            } else if (propBaths === 7) {
              numberBaths = "seven";
            } else if (propBaths === 8) {
              numberBaths = "eight";
            } else if (propBaths === 9) {
              numberBaths = "nine";
            }
            propBathsDescription = " with " + numberBaths + " bathrooms"
            propDescription += propBathsDescription;
            break;
        }

        if (propType === "flat") {
          switch (propFloor) {
            case "basement":
              propFloorDescription = "basement";
              propDescription = propBedsDescription + propFloorDescription + " " + propTypeDescription + propBathsDescription + ".";
              break;
            case "ground-floor":
              propFloorDescription = "on the ground floor.";
              propDescription += " " + propFloorDescription;
              break;
            case "first-floor":
              propFloorDescription = "on the first floor.";
              propDescription += " " + propFloorDescription;
              break;
            case "second-floor":
              propFloorDescription = "on the second floor.";
              propDescription += " " + propFloorDescription;
              break;
            case "higher":
              propFloorDescription = "on an upper floor.";
              propDescription += " " + propFloorDescription;
              break;
            default:
              propFloorDescription = ".";
              propDescription += propFloorDescription;
              break;
          }
        } else {
          propDescription += ".";
        }

        switch (propBuilt) {
          case "brandNew":
            propBuiltDescription = " Built this year ";
            propDescription += propBuiltDescription;
            break;
          case "last20Years":
            propBuiltDescription = " Built in the last 20 years ";
            propDescription += propBuiltDescription;
            break;
          case "between1950and2000":
            propBuiltDescription = " Built between 1950 and 2000 ";
            propDescription += propBuiltDescription;
            break;
          case "between1900and1950":
            propBuiltDescription = " Built between 1900 and 1950 ";
            propDescription += propBuiltDescription;
            break;
          case "pre1900":
            propBuiltDescription = " Built before 1900 ";
            propDescription += propBuiltDescription;
            break;
        }

        switch (propLastDecorated) {
          case "thisYear":
            propLastDecoratedDescription = "and decorated this year,";
            propDescription = propDescription + propLastDecoratedDescription;
            break;
          case "year2019":
            propLastDecoratedDescription = "and decorated last year,";
            propDescription = propDescription + propLastDecoratedDescription;
            break;
          case "year2018":
            propLastDecoratedDescription = "and last decorated in 2018,";
            propDescription = propDescription + propLastDecoratedDescription;
            break;
          case "pre2018":
            propLastDecoratedDescription = "and last decorated before 2018,";
            propDescription = propDescription + propLastDecoratedDescription;
            break;
        }

        switch (propOutdoorSpace) {
          case "private-garden":
            propOutdoorSpaceDescription = " it has a private garden"
            propDescription += propOutdoorSpaceDescription;
            break;
          case "patio":
            propOutdoorSpaceDescription = " it has a patio area"
            propDescription += propOutdoorSpaceDescription;
            break;
          case "terrace":
            propOutdoorSpaceDescription = " it has a terrace"
            propDescription += propOutdoorSpaceDescription;
            break;
          case "no-outside":
            propOutdoorSpaceDescription = " it is located near public parks and gardens"
            propDescription += propOutdoorSpaceDescription;
            break;
        }

        switch (propParkingType) {
          case "garage":
            propParkingTypeDescription = " and a private garage."
            propDescription += propParkingTypeDescription;
            break;
          case "driveway":
            propParkingTypeDescription = " and a driveway."
            if (propOutdoorSpace === "no-outside") {
              propParkingTypeDescription = " and has a driveway."
            }
            propDescription += propParkingTypeDescription;
            break;
          case "permit":
            propParkingTypeDescription = " and permit required street parking."
            if (propOutdoorSpace === "no-outside") {
              propParkingTypeDescription = " and has permit required street parking."
            }
            propDescription += propParkingTypeDescription;
            break;
          case "on-street":
            propParkingTypeDescription = " and free street parking."
            if (propOutdoorSpace === "no-outside") {
              propParkingTypeDescription = " and has free street parking."
            }
            propDescription += propParkingTypeDescription;
            break;
          case "off-street":
            propParkingTypeDescription = " and off street parking."
            if (propOutdoorSpace === "no-outside") {
              propParkingTypeDescription = " and has off street parking."
            }
            propDescription += propParkingTypeDescription;
            break;
          case "garage-and-driveway":
            propParkingTypeDescription = " and both a garage and a driveway."
            if (propOutdoorSpace === "no-outside") {
              propParkingTypeDescription = " and has both a garage and a driveway."
            }
            propDescription += propParkingTypeDescription;
            break;
          case "allocated":
            propParkingTypeDescription = " and allocated parking."
            if (propOutdoorSpace === "no-outside") {
              propParkingTypeDescription = " and has allocated parking."
            }
            propDescription += propParkingTypeDescription;
            break;
        }

        propAskingPriceDescription = " On the market for £" + propAskingPrice + " ";
        propDescription += propAskingPriceDescription;

        if (propExtraCosts > 0) {
          propExtraCostsDescription = "and with additional costs of £" + propExtraCosts + " per month,";
        } else {
          propExtraCostsDescription = "and with no additional costs,";
        }
        propDescription += propExtraCostsDescription;

        propSizeDescription = " this property offers " + propSize + " square metres";
        propDescription += propSizeDescription;

        if (propHasLift === "higher-with-lift") {
          propHasLiftDescription = ", a lift";
          propDescription += propHasLiftDescription;
        }

        switch (propHeatingType) {
          case "electric":
            propHeatingTypeDescription = " and electric heating."
            propDescription += propHeatingTypeDescription;
            break;
          case "gas":
            propHeatingTypeDescription = " and gas central heating.";
            propDescription += propHeatingTypeDescription;
            break;
          case "oil":
            propHeatingTypeDescription = " and oil fired central heating.";
            propDescription += propHeatingTypeDescription;
            break;
          default:
            propHeatingTypeDescription = " and a heat exchanger.";
            propDescription += propHeatingTypeDescription;
            break;
        }

        switch (propTenure) {
          case "freehold":
            propTenureDescription = " It is a freehold property.";
            propDescription += propTenureDescription;
            break;
          case "leasehold":
            propTenureDescription = " It is a leasehold property";
            propDescription += propTenureDescription;
            break;
          case "flying-freehold":
            propTenureDescription = " It is a flying freehold property.";
            propDescription += propTenureDescription;
            break;
          case "share-of-freehold":
            propTenureDescription = " It is a share of freehold property.";
            propDescription += propTenureDescription;
            break;
        }

        if (propTenure === "leasehold") {
          propLeaseYearsDescription = " with " + propLeaseYears + " years left on the lease.";
          propDescription += propLeaseYearsDescription;
        }






      } else if (randomiser === 2) {
        switch (propBeds) {
          case 0:
            propBedsDescription = "A studio ";
            propDescription += propBedsDescription;
            break;
          case 1:
            propBedsDescription = "A one bedroom ";
            propDescription += propBedsDescription;
            break;
          case 2:
            propBedsDescription = "A two bedroom ";
            propDescription += propBedsDescription;
            break;
          case 3:
            propBedsDescription = "A three bedroom ";
            propDescription += propBedsDescription;
            break;
          case 4:
            propBedsDescription = "A four bedroom ";
            propDescription += propBedsDescription;
            break;
          case 5:
            propBedsDescription = "A five bedroom "
            propDescription += propBedsDescription;
            break;
          case 6:
            propBedsDescription = "A six bedroom "
            propDescription += propBedsDescription;
            break;
          case 7:
            propBedsDescription = "A seven bedroom "
            propDescription += propBedsDescription;
            break;
          case 8:
            propBedsDescription = "An eight bedroom "
            propDescription += propBedsDescription;
            break;
          case 9:
            propBedsDescription = "A nine bedroom "
            propDescription += propBedsDescription;
            break;
          default:
            propBedsDescription = "A multi-bedroom ";
            propDescription += propBedsDescription;
            break;
        }

        switch (propType) {
          case "detached-house":
            propTypeDescription = "detached house";
            break;
          case "semi-detached-house":
            propTypeDescription = "semi-detached house";
            break;
          case "terraced-house":
            propTypeDescription = "terraced house";
            break;
          case "bungalow":
            propTypeDescription = "bungalow";
            break;
          case "flat":
            propTypeDescription = "apartment";
            break;
        }

        propDescription = propBedsDescription + propTypeDescription;

        propAskingPriceDescription = " on the market for £" + propAskingPrice + ".";
        propDescription += propAskingPriceDescription;

        if (propType === "flat") {
          switch (propFloor) {
            case "basement":
              propFloorDescription = "basement";
              propDescription = propBedsDescription + propFloorDescription + " " + propTypeDescription + propAskingPriceDescription;
              break;
            case "ground-floor":
              propFloorDescription = "On the ground floor";
              propDescription += " " + propFloorDescription;
              break;
            case "first-floor":
              propFloorDescription = "On the first floor";
              propDescription += " " + propFloorDescription;
              break;
            case "second-floor":
              propFloorDescription = "On the second floor";
              propDescription += " " + propFloorDescription;
              break;
            case "higher":
              propFloorDescription = "On an upper floor";
              propDescription += " " + propFloorDescription;
              break;
          }
        }

        if (propType === "flat" && propFloor === "basement") {
          switch (propBaths) {
            case 1:
              propBathsDescription = " With one bathroom, "
              propDescription += propBathsDescription;
              break;
            case 2:
              propBathsDescription = " With two bathrooms, "
              propDescription += propBathsDescription;
              break;
            case 3:
              propBathsDescription = " With three bathrooms, "
              propDescription += propBathsDescription;
              break;
            default:
              let numberBaths = "many";
              if (propBaths === 4) {
                numberBaths = "four";
              } else if (propBaths === 5) {
                numberBaths = "five";
              } else if (propBaths === 6) {
                numberBaths = "six";
              } else if (propBaths === 7) {
                numberBaths = "seven";
              } else if (propBaths === 8) {
                numberBaths = "eight";
              } else if (propBaths === 9) {
                numberBaths = "nine";
              }
              propBathsDescription = " With " + numberBaths + " bathrooms, "
              propDescription += propBathsDescription;
              break;
          }
        } else if (propType === "flat") {
          switch (propBaths) {
            case 1:
              propBathsDescription = " with one bathroom, "
              propDescription += propBathsDescription;
              break;
            case 2:
              propBathsDescription = " with two bathrooms, "
              propDescription += propBathsDescription;
              break;
            case 3:
              propBathsDescription = " with three bathrooms, "
              propDescription += propBathsDescription;
              break;
            default:
              let numberBaths = "many";
              if (propBaths === 4) {
                numberBaths = "four";
              } else if (propBaths === 5) {
                numberBaths = "five";
              } else if (propBaths === 6) {
                numberBaths = "six";
              } else if (propBaths === 7) {
                numberBaths = "seven";
              } else if (propBaths === 8) {
                numberBaths = "eight";
              } else if (propBaths === 9) {
                numberBaths = "nine";
              }
              propBathsDescription = " with " + numberBaths + " bathrooms, "
              propDescription += propBathsDescription;
              break;
          }
        } else {
          switch (propBaths) {
            case 1:
              propBathsDescription = " With one bathroom, ";
              propDescription += propBathsDescription;
              break;
            case 2:
              propBathsDescription = " With two bathrooms, ";
              propDescription += propBathsDescription;
              break;
            case 3:
              propBathsDescription = " With three bathrooms, ";
              propDescription += propBathsDescription;
              break;
            default:
              let numberBaths = "many";
              if (propBaths === 4) {
                numberBaths = "four";
              } else if (propBaths === 5) {
                numberBaths = "five";
              } else if (propBaths === 6) {
                numberBaths = "six";
              } else if (propBaths === 7) {
                numberBaths = "seven";
              } else if (propBaths === 8) {
                numberBaths = "eight";
              } else if (propBaths === 9) {
                numberBaths = "nine";
              }
              propBathsDescription = " With " + numberBaths + " bathrooms, ";
              propDescription += propBathsDescription;
              break;
          }
        }

        switch (propOutdoorSpace) {
          case "private-garden":
            propOutdoorSpaceDescription = "a private garden";
            propDescription += propOutdoorSpaceDescription;
            break;
          case "patio":
            propOutdoorSpaceDescription = "a patio area";
            propDescription += propOutdoorSpaceDescription;
            break;
          case "terrace":
            propOutdoorSpaceDescription = "a terrace";
            propDescription += propOutdoorSpaceDescription;
            break;
          case "no-outside":
            propOutdoorSpaceDescription = "nearby parks and gardens";
            propDescription += propOutdoorSpaceDescription;
            break;
        }

        propSizeDescription = " and " + propSize + " square metres";
        propDescription += propSizeDescription;

        switch (propLastDecorated) {
          case "thisYear":
            propLastDecoratedDescription = ", the property was last decorated this year.";
            propDescription = propDescription + propLastDecoratedDescription;
            break;
          case "year2019":
            propLastDecoratedDescription = ", the property was decorated last year.";
            propDescription = propDescription + propLastDecoratedDescription;
            break;
          case "year2018":
            propLastDecoratedDescription = ", the property was last decorated in 2018.";
            propDescription = propDescription + propLastDecoratedDescription;
            break;
          case "pre2018":
            propLastDecoratedDescription = ", the property was last decorated before 2018.";
            propDescription = propDescription + propLastDecoratedDescription;
            break;
        }

        switch (propBuilt) {
          case "brandNew":
            propBuiltDescription = " Built this year,";
            propDescription += propBuiltDescription;
            break;
          case "last20Years":
            propBuiltDescription = " Built in the last 20 years,";
            propDescription += propBuiltDescription;
            break;
          case "between1950and2000":
            propBuiltDescription = " Built between 1950 and 2000,";
            propDescription += propBuiltDescription;
            break;
          case "between1900and1950":
            propBuiltDescription = " Built between 1900 and 1950,";
            propDescription += propBuiltDescription;
            break;
          case "pre1900":
            propBuiltDescription = " Built before 1900,";
            propDescription += propBuiltDescription;
            break;
        }

        switch (propParkingType) {
          case "garage":
            propParkingTypeDescription = " it has a private garage";
            propDescription += propParkingTypeDescription;
            break;
          case "driveway":
            propParkingTypeDescription = " it has a driveway";
            propDescription += propParkingTypeDescription;
            break;
          case "permit":
            propParkingTypeDescription = " it has permit required street parking";
            propDescription += propParkingTypeDescription;
            break;
          case "on-street":
            propParkingTypeDescription = " it has free street parking";
            propDescription += propParkingTypeDescription;
            break;
          case "off-street":
            propParkingTypeDescription = " it has off street parking";
            propDescription += propParkingTypeDescription;
            break;
          case "garage-and-driveway":
            propParkingTypeDescription = " it has both a garage and a driveway";
            propDescription += propParkingTypeDescription;
            break;
          case "allocated":
            propParkingTypeDescription = " it has allocated parking";
            propDescription += propParkingTypeDescription;
            break;
        }

        if (propHasLift === "higher-with-lift") {
          propHasLiftDescription = ", a lift,";
          propDescription += propHasLiftDescription;
        }

        switch (propHeatingType) {
          case "electric":
            propHeatingTypeDescription = " and electric heating."
            propDescription += propHeatingTypeDescription;
            break;
          case "gas":
            propHeatingTypeDescription = " and gas central heating.";
            propDescription += propHeatingTypeDescription;
            break;
          case "oil":
            propHeatingTypeDescription = " and oil fired central heating.";
            propDescription += propHeatingTypeDescription;
            break;
          default:
            propHeatingTypeDescription = " and aheat exchanger.";
            propDescription += propHeatingTypeDescription;
            break;
        }

        if (propExtraCosts > 0) {
          propExtraCostsDescription = " It comes with additional costs of £" + propExtraCosts + " per month";
        } else {
          propExtraCostsDescription = " It comes with no additional costs";
        }
        propDescription += propExtraCostsDescription;

        switch (propTenure) {
          case "freehold":
            propTenureDescription = " and is a freehold property.";
            propDescription += propTenureDescription;
            break;
          case "leasehold":
            propTenureDescription = " and is a leasehold property";
            propDescription += propTenureDescription;
            break;
          case "flying-freehold":
            propTenureDescription = " and is a flying freehold property.";
            propDescription += propTenureDescription;
            break;
          case "share-of-freehold":
            propTenureDescription = " and is a share of freehold property.";
            propDescription += propTenureDescription;
            break;
        }

        if (propTenure === "leasehold") {
          propLeaseYearsDescription = " with " + propLeaseYears + " years left on the lease.";
          propDescription += propLeaseYearsDescription;
        }
      }

      break;

    // --------------------------------------------\ Plain---------------------------------------------------------







    case 1:

      // --------------------------------------------Agent---------------------------------------------------------

      if (randomiser === 0) {
        switch (propType) {
          case "detached-house":
            propTypeDescription = "detached house";
            break;
          case "semi-detached-house":
            propTypeDescription = "semi-detached house";
            break;
          case "terraced-house":
            propTypeDescription = "terraced house";
            break;
          case "bungalow":
            propTypeDescription = "bungalow";
            break;
          case "flat":
            propTypeDescription = "apartment";
            break;
        }

        propDescription = propTypeDescription;

        if (propType === "flat") {
          switch (propFloor) {
            case "basement":
              propFloorDescription = "basement";
              propDescription = propFloorDescription + " " + propTypeDescription;
              break;
            case "ground-floor":
              propFloorDescription = "on the ground floor";
              propDescription += " " + propFloorDescription;
              break;
            case "first-floor":
              propFloorDescription = "on the first floor";
              propDescription += " " + propFloorDescription;
              break;
            case "second-floor":
              propFloorDescription = "on the second floor";
              propDescription += " " + propFloorDescription;
              break;
            case "higher":
              propFloorDescription = "on an upper floor";
              propDescription += " " + propFloorDescription;
              break;
            default:
              propFloorDescription = "";
              propDescription += propFloorDescription;
              break;
          }
        }

        switch (propLastDecorated) {
          case "thisYear":
            propLastDecoratedDescription = "A newly and tastefully decorated";
            propDescription = propLastDecoratedDescription + " " + propDescription + ", ";
            break;
          case "year2019":
            propLastDecoratedDescription = "A recently and tastefully decorated";
            propDescription = propLastDecoratedDescription + " " + propDescription + ", ";
            break;
          case "year2018":
            propLastDecoratedDescription = ", last decorated in 2018 in a tasteful manner,";
            propDescription = "A beautiful " + propDescription + propLastDecoratedDescription + " and ";
            break;
          case "pre2018":
            propLastDecoratedDescription = ", last decorated before 2018 in a tasteful manner,";
            propDescription = "A beautiful " + propDescription + propLastDecoratedDescription + " and ";
            break;
        }

        switch (propBuilt) {
          case "brandNew":
            propBuiltDescription = "newly built in a modern and contemporary style. ";
            propDescription += propBuiltDescription;
            break;
          case "last20Years":
            propBuiltDescription = "built in the last 20 years with style and elegance in mind. ";
            propDescription += propBuiltDescription;
            break;
          case "between1950and2000":
            propBuiltDescription = "built between 1950 and 2000 with plenty of charm and character. ";
            propDescription += propBuiltDescription;
            break;
          case "between1900and1950":
            propBuiltDescription = "built between 1900 and 1950 with plenty of charm and character. ";
            propDescription += propBuiltDescription;
            break;
          case "pre1900":
            propBuiltDescription = "built before 1900 with stunning period craftsmanship. ";
            propDescription += propBuiltDescription;
            break;
        }

        if (propFloor != "basement" && propFloor != "groundFloor") {
          propHasLiftDescription = "Unfortunately the property doesn't have a lift, but the stairs will keep you fit and healthy! ";
          if (propHasLift === "higher-with-lift") {
            propHasLiftDescription = "Don't worry about the stairs though, the property comes with a lift! ";
            propDescription += propHasLiftDescription;
          }
        }

        switch (propBeds) {
          case 0:
            propBedsDescription = "With a brilliant studio bedroom"
            propDescription += propBedsDescription;
            break;
          case 1:
            propBedsDescription = "With a single, lovely bedroom, perfect for a couple,"
            propDescription += propBedsDescription;
            break;
          case 2:
            propBedsDescription = "With two lovely bedrooms, perfect if you're hoping to have guests to stay,"
            propDescription += propBedsDescription;
            break;
          case 3:
            propBedsDescription = "With three lovely bedrooms, perfect if you're looking for a family home,"
            propDescription += propBedsDescription;
            break;
          case 4:
            propBedsDescription = "With four lovely bedrooms, perfect for a large family,"
            propDescription += propBedsDescription;
            break;
          case 5:
            propBedsDescription = "With five lovely bedrooms, perfect for a large family,"
            propDescription += propBedsDescription;
            break;
          case 6:
            propBedsDescription = "With six lovely bedrooms, perfect for a large family,"
            propDescription += propBedsDescription;
            break;
          case 7:
            propBedsDescription = "With seven lovely bedrooms, perfect for a large family,"
            propDescription += propBedsDescription;
            break;
          case 8:
            propBedsDescription = "With eight lovely bedrooms, perfect for a large family,"
            propDescription += propBedsDescription;
            break;
          case 9:
            propBedsDescription = "With nine lovely bedrooms, perfect for a large family,"
            propDescription += propBedsDescription;
            break;
          default:
            propBedsDescription = "With many lovely bedrooms, perfect for a large family,"
            propDescription += propBedsDescription;
            break;
        }

        switch (propBaths) {
          case 1:
            propBathsDescription = " and one luxurious bathroom"
            propDescription += propBathsDescription;
            break;
          case 2:
            propBathsDescription = " and two luxurious bathrooms"
            propDescription += propBathsDescription;
            break;
          case 3:
            propBathsDescription = " and three luxurious bathrooms"
            propDescription += propBathsDescription;
            break;
          default:
            let numberBaths = "many";
            if (propBaths === 4) {
              numberBaths = "four";
            } else if (propBaths === 5) {
              numberBaths = "five";
            } else if (propBaths === 6) {
              numberBaths = "six";
            } else if (propBaths === 7) {
              numberBaths = "seven";
            } else if (propBaths === 8) {
              numberBaths = "eight";
            } else if (propBaths === 9) {
              numberBaths = "nine";
            }
            propBathsDescription = " and " + numberBaths + " luxurious bathrooms"
            propDescription += propBathsDescription;
            break;
        }

        propDescription += ", this could be the dream house for you.";

        switch (propOutdoorSpace) {
          case "private-garden":
            propOutdoorSpaceDescription = " It has a stunning private garden that would be great for a dog,"
            propDescription += propOutdoorSpaceDescription;
            break;
          case "patio":
            propOutdoorSpaceDescription = " It has a stunning patio area that's great for summer barbeques"
            propDescription += propOutdoorSpaceDescription;
            break;
          case "terrace":
            propOutdoorSpaceDescription = " It has a stunning terrace with an incredible view"
            propDescription += propOutdoorSpaceDescription;
            break;
          case "no-outside":
            propOutdoorSpaceDescription = " It is located near stunning parks and gardens"
            propDescription += propOutdoorSpaceDescription;
            break;
        }

        switch (propParkingType) {
          case "garage":
            propParkingTypeDescription = " and boasts a private garage where you can store your car or bike, or even convert into your own workshop or art studio!";
            propDescription += propParkingTypeDescription;
            break;
          case "driveway":
            propParkingTypeDescription = " and has a fully accessible, private driveway.";
            propDescription += propParkingTypeDescription;
            break;
          case "permit":
            propParkingTypeDescription = " and has permit required street parking with an abundance of spaces.";
            propDescription += propParkingTypeDescription;
            break;
          case "on-street":
            propParkingTypeDescription = " and has free street parking with an abundance of spaces.";
            propDescription += propParkingTypeDescription;
            break;
          case "off-street":
            propParkingTypeDescription = " and has off street parking with an abundance of spaces.";
            propDescription += propParkingTypeDescription;
            break;
          case "garage-and-driveway":
            propParkingTypeDescription = " and boasts both a driveway and a garage where you can store your car or bike, or even convert into your own workshop or art studio!";
            propDescription += propParkingTypeDescription;
            break;
          case "allocated":
            propParkingTypeDescription = " and has personal, allocated parking, so you won't have to search out a parking spot.";
            propDescription += propParkingTypeDescription;
            break;
        }

        if (propSize < 60) {
          let propSizeDescription = " An extremely cozy property with an overall footprint of " + propSize + " square metres";
          propDescription += propSizeDescription;
        } else {
          let propSizeDescription = " An extremely spacious property with an overall footprint of " + propSize + " square metres";
          propDescription += propSizeDescription;
        }

        switch (propHeatingType) {
          case "electric":
            propHeatingTypeDescription = " and efficient, warming electric heating";
            propDescription += propHeatingTypeDescription;
            break;
          case "gas":
            propHeatingTypeDescription = " and efficient, warming gas central heating";
            propDescription += propHeatingTypeDescription;
            break;
          case "oil":
            propHeatingTypeDescription = " and efficient, warming oil fired central heating";
            propDescription += propHeatingTypeDescription;
            break;
          default:
            propHeatingTypeDescription = " and a warm, maintainable heat exchanger";
            propDescription += propHeatingTypeDescription;
            break;
        }

        propAskingPriceDescription = ", this house is on the market for an absolute bargain of £" + propAskingPrice + ", get it before it's gone!";
        propDescription += propAskingPriceDescription;

        switch (propTenure) {
          case "freehold":
            propTenureDescription = " This is a freehold property, so no need to worry about lease renewals,";
            propDescription += propTenureDescription;
            break;
          case "leasehold":
            propTenureDescription = " This is a leasehold property";
            propDescription += propTenureDescription;
            break;
          case "flying-freehold":
            propTenureDescription = " This is a flying freehold property";
            propDescription += propTenureDescription;
            break;
          case "share-of-freehold":
            propTenureDescription = " This is a share of freehold property";
            propDescription += propTenureDescription;
            break;
        }

        if (propTenure === "leasehold") {
          propLeaseYearsDescription = " with " + propLeaseYears + " years left on the lease, plenty of time before renewal is necessary,";
          propDescription += propLeaseYearsDescription;
        }

        if (propExtraCosts > 0) {
          propExtraCostsDescription = " with additional costs of only £" + propExtraCosts + " per month.";
          if (propTenure === "leasehold") {
            propExtraCostsDescription = " and additional costs of only £" + propExtraCosts + " per month.";
          }
        } else {
          propExtraCostsDescription = " with no additional costs!";
          if (propTenure === "leasehold") {
            propExtraCostsDescription = " and no additional costs!";
          }
        }
        propDescription += propExtraCostsDescription;

        propDescription += " Don't miss out, book a viewing today!"





      } else if (randomiser === 1) {
        switch (propBeds) {
          case 0:
            propBedsDescription = "A beautiful studio ";
            propDescription += propBedsDescription;
            break;
          case 1:
            propBedsDescription = "A beautiful, one bedroom ";
            propDescription += propBedsDescription;
            break;
          case 2:
            propBedsDescription = "A beautiful, two bedroom ";
            propDescription += propBedsDescription;
            break;
          case 3:
            propBedsDescription = "A beautiful, three bedroom ";
            propDescription += propBedsDescription;
            break;
          case 4:
            propBedsDescription = "A beautiful, four bedroom ";
            propDescription += propBedsDescription;
            break;
          case 5:
            propBedsDescription = "A beautiful, five bedroom "
            propDescription += propBedsDescription;
            break;
          case 6:
            propBedsDescription = "A beautiful, six bedroom "
            propDescription += propBedsDescription;
            break;
          case 7:
            propBedsDescription = "A beautiful, seven bedroom "
            propDescription += propBedsDescription;
            break;
          case 8:
            propBedsDescription = "A beautiful, eight bedroom "
            propDescription += propBedsDescription;
            break;
          case 9:
            propBedsDescription = "A beautiful, nine bedroom "
            propDescription += propBedsDescription;
            break;
          default:
            propBedsDescription = "A beautiful, multi-bedroom ";
            propDescription += propBedsDescription;
            break;
        }

        switch (propType) {
          case "detached-house":
            propTypeDescription = "detached house";
            break;
          case "semi-detached-house":
            propTypeDescription = "semi-detached house";
            break;
          case "terraced-house":
            propTypeDescription = "terraced house";
            break;
          case "bungalow":
            propTypeDescription = "bungalow";
            break;
          case "flat":
            propTypeDescription = "apartment";
            break;
        }

        propDescription = propBedsDescription + propTypeDescription;

        switch (propBaths) {
          case 1:
            propBathsDescription = " with a lovely bathroom"
            propDescription += propBathsDescription;
            break;
          case 2:
            propBathsDescription = " with two lovely bathrooms"
            propDescription += propBathsDescription;
            break;
          case 3:
            propBathsDescription = " with three lovely bathrooms"
            propDescription += propBathsDescription;
            break;
          default:
            let numberBaths = "many";
            if (propBaths === 4) {
              numberBaths = "four";
            } else if (propBaths === 5) {
              numberBaths = "five";
            } else if (propBaths === 6) {
              numberBaths = "six";
            } else if (propBaths === 7) {
              numberBaths = "seven";
            } else if (propBaths === 8) {
              numberBaths = "eight";
            } else if (propBaths === 9) {
              numberBaths = "nine";
            }
            propBathsDescription = " with " + numberBaths + " lovely bathrooms"
            propDescription += propBathsDescription;
            break;
        }

        if (propType === "flat") {
          switch (propFloor) {
            case "basement":
              propFloorDescription = "basement";
              propDescription = propBedsDescription + propFloorDescription + " " + propTypeDescription;
              break;
            case "ground-floor":
              propFloorDescription = "on the ground floor";
              propDescription += " " + propFloorDescription;
              break;
            case "first-floor":
              propFloorDescription = "on the first floor";
              propDescription += " " + propFloorDescription;
              break;
            case "second-floor":
              propFloorDescription = "on the second floor";
              propDescription += " " + propFloorDescription;
              break;
            case "higher":
              propFloorDescription = "on an upper floor";
              propDescription += " " + propFloorDescription;
              break;
            default:
              propFloorDescription = ".";
              propDescription += propFloorDescription;
              break;
          }
        }

        if (propBeds > 1) {
          propDescription += ", perfect for a family.";
        } else {
          propDescription += ", perfect as a first property or for a couple.";
        }

        switch (propBuilt) {
          case "brandNew":
            propBuiltDescription = " Built this year in a modern and contemporary style, ";
            propDescription += propBuiltDescription;
            break;
          case "last20Years":
            propBuiltDescription = " Built in the last 20 years with style and elegance in mind ";
            propDescription += propBuiltDescription;
            break;
          case "between1950and2000":
            propBuiltDescription = " Built between 1950 and 2000, with plenty of charm and character, ";
            propDescription += propBuiltDescription;
            break;
          case "between1900and1950":
            propBuiltDescription = " Built between 1900 and 1950, with plenty of charm and character, ";
            propDescription += propBuiltDescription;
            break;
          case "pre1900":
            propBuiltDescription = " Built before 1900 with stunning period craftsmanship, ";
            propDescription += propBuiltDescription;
            break;
        }

        switch (propLastDecorated) {
          case "thisYear":
            propLastDecoratedDescription = "and decorated this year in a tasteful manner,";
            propDescription = propDescription + propLastDecoratedDescription;
            break;
          case "year2019":
            propLastDecoratedDescription = "and decorated last year in a tasteful manner,";
            propDescription = propDescription + propLastDecoratedDescription;
            break;
          case "year2018":
            propLastDecoratedDescription = "and last decorated in 2018 in a tasteful manner,";
            propDescription = propDescription + propLastDecoratedDescription;
            break;
          case "pre2018":
            propLastDecoratedDescription = "and last decorated before 2018 in a tasteful manner,";
            propDescription = propDescription + propLastDecoratedDescription;
            break;
        }

        switch (propOutdoorSpace) {
          case "private-garden":
            propOutdoorSpaceDescription = " it has a stunning private garden that would be great for a dog,"
            propDescription += propOutdoorSpaceDescription;
            break;
          case "patio":
            propOutdoorSpaceDescription = " it has a stunning patio area that would be great for summer barbeques,"
            propDescription += propOutdoorSpaceDescription;
            break;
          case "terrace":
            propOutdoorSpaceDescription = " it has a stunning terrace with a great view,"
            propDescription += propOutdoorSpaceDescription;
            break;
          case "no-outside":
            propOutdoorSpaceDescription = " it is located near stunning public parks and gardens"
            propDescription += propOutdoorSpaceDescription;
            break;
        }

        switch (propParkingType) {
          case "garage":
            propParkingTypeDescription = " and boasts a private garage where you can store your car or bike, or even convert into your own workshop or art studio!";
            propDescription += propParkingTypeDescription;
            break;
          case "driveway":
            propParkingTypeDescription = " and a fully accessible, private driveway.";
            if (propOutdoorSpace === "no-outside") {
              propParkingTypeDescription = " and has a fully accessible, private driveway.";
            }
            propDescription += propParkingTypeDescription;
            break;
          case "permit":
            propParkingTypeDescription = " and permit required street parking with an abundance of spaces on offer.";
            if (propOutdoorSpace === "no-outside") {
              propParkingTypeDescription = " and has permit required street parking with an abundance of spaces on offer.";
            }
            propDescription += propParkingTypeDescription;
            break;
          case "on-street":
            propParkingTypeDescription = " and free street parking with an abundance of spaces on offer.";
            if (propOutdoorSpace === "no-outside") {
              propParkingTypeDescription = " and has free street parking with an abundance of spaces on offer.";
            }
            propDescription += propParkingTypeDescription;
            break;
          case "off-street":
            propParkingTypeDescription = " and off street parking with an abundance of space.";
            if (propOutdoorSpace === "no-outside") {
              propParkingTypeDescription = " and has off street parking with an abundance of space.";
            }
            propDescription += propParkingTypeDescription;
            break;
          case "garage-and-driveway":
            propParkingTypeDescription = " and boasts both a garage and a driveway where you can store your car or bike, or even convert into your own workshop or art studio!";
            propDescription += propParkingTypeDescription;
            break;
          case "allocated":
            propParkingTypeDescription = " and peronal, allocated parking, so no wasted time searching for a parking space.";
            if (propOutdoorSpace === "no-outside") {
              propParkingTypeDescription = " and has personal, allocated parking, so no wasted time searching for a parking space.";
            }
            propDescription += propParkingTypeDescription;
            break;
        }

        propAskingPriceDescription = " On the market for a bargain of £" + propAskingPrice + " ";
        propDescription += propAskingPriceDescription;

        if (propExtraCosts > 0) {
          propExtraCostsDescription = "and with additional costs of only £" + propExtraCosts + " per month,";
        } else {
          propExtraCostsDescription = "and with no additional costs,";
        }
        propDescription += propExtraCostsDescription;

        if (propSize < 60) {
          let propSizeDescription = " this cozy property offers an overall footprint of " + propSize + " square metres";
          propDescription += propSizeDescription;
        } else {
          let propSizeDescription = " this spacious property offers an overall footprint of  " + propSize + " square metres";
          propDescription += propSizeDescription;
        }

        if (propHasLift === "higher-with-lift") {
          propHasLiftDescription = ", a functional lift";
          propDescription += propHasLiftDescription;
        }

        switch (propHeatingType) {
          case "electric":
            propHeatingTypeDescription = " and warm, comforting electric heating."
            propDescription += propHeatingTypeDescription;
            break;
          case "gas":
            propHeatingTypeDescription = " and warm, comforting gas central heating.";
            propDescription += propHeatingTypeDescription;
            break;
          case "oil":
            propHeatingTypeDescription = " and warm, comforting oil fired central heating.";
            propDescription += propHeatingTypeDescription;
            break;
          default:
            propHeatingTypeDescription = " and a warm, maintainable heat exchanger.";
            propDescription += propHeatingTypeDescription;
            break;
        }

        propDescription += " Get it before it's gone!";

        switch (propTenure) {
          case "freehold":
            propTenureDescription = " It is a freehold property, so no need to worry about lease renewals.";
            propDescription += propTenureDescription;
            break;
          case "leasehold":
            propTenureDescription = " It is a leasehold property";
            propDescription += propTenureDescription;
            break;
          case "flying-freehold":
            propTenureDescription = " It is a flying freehold property.";
            propDescription += propTenureDescription;
            break;
          case "share-of-freehold":
            propTenureDescription = " It is a share of freehold property.";
            propDescription += propTenureDescription;
            break;
        }

        if (propTenure === "leasehold") {
          propLeaseYearsDescription = " with " + propLeaseYears + " years left on the lease.";
          propDescription += propLeaseYearsDescription;
        }

        propDescription += " Don't miss out, book a viewing today!";





      } else if (randomiser === 2) {
        switch (propBeds) {
          case 0:
            propBedsDescription = "A beautiful studio ";
            propDescription += propBedsDescription;
            break;
          case 1:
            propBedsDescription = "A beautiful, one bedroom ";
            propDescription += propBedsDescription;
            break;
          case 2:
            propBedsDescription = "A beautiful, two bedroom ";
            propDescription += propBedsDescription;
            break;
          case 3:
            propBedsDescription = "A beautiful, three bedroom ";
            propDescription += propBedsDescription;
            break;
          case 4:
            propBedsDescription = "A beautiful, four bedroom ";
            propDescription += propBedsDescription;
            break;
          case 5:
            propBedsDescription = "A beautiful, five bedroom "
            propDescription += propBedsDescription;
            break;
          case 6:
            propBedsDescription = "A beautiful, six bedroom "
            propDescription += propBedsDescription;
            break;
          case 7:
            propBedsDescription = "A beautiful, seven bedroom "
            propDescription += propBedsDescription;
            break;
          case 8:
            propBedsDescription = "A beautiful, eight bedroom "
            propDescription += propBedsDescription;
            break;
          case 9:
            propBedsDescription = "A beautiful, nine bedroom "
            propDescription += propBedsDescription;
            break;
          default:
            propBedsDescription = "A beautiful multi-bedroom ";
            propDescription += propBedsDescription;
            break;
        }

        switch (propType) {
          case "detached-house":
            propTypeDescription = "detached house";
            break;
          case "semi-detached-house":
            propTypeDescription = "semi-detached house";
            break;
          case "terraced-house":
            propTypeDescription = "terraced house";
            break;
          case "bungalow":
            propTypeDescription = "bungalow";
            break;
          case "flat":
            propTypeDescription = "apartment";
            break;
        }

        propDescription = propBedsDescription + propTypeDescription;

        propAskingPriceDescription = " on the market for an absolute bargain of £" + propAskingPrice;
        propDescription += propAskingPriceDescription;

        if (propBeds > 1) {
          propDescription += ", this property is perfect for a family, don't miss out!";
        } else {
          propDescription += ", this property is perfect as a first home or for a couple, don't miss out!";
        }

        if (propType === "flat") {
          switch (propFloor) {
            case "basement":
              propFloorDescription = "basement";
              propDescription = propBedsDescription + propFloorDescription + " " + propTypeDescription + ".";
              break;
            case "ground-floor":
              propFloorDescription = "On the ground floor";
              propDescription += " " + propFloorDescription;
              break;
            case "first-floor":
              propFloorDescription = "On the first floor";
              propDescription += " " + propFloorDescription;
              break;
            case "second-floor":
              propFloorDescription = "On the second floor";
              propDescription += " " + propFloorDescription;
              break;
            case "higher":
              propFloorDescription = "On an upper floor";
              propDescription += " " + propFloorDescription;
              break;
          }
        }

        if (propType === "flat" && propFloor === "basement") {
          switch (propBaths) {
            case 1:
              propBathsDescription = " with one luxurious bathroom, "
              propDescription += propBathsDescription;
              break;
            case 2:
              propBathsDescription = " with two luxurious bathrooms, "
              propDescription += propBathsDescription;
              break;
            case 3:
              propBathsDescription = " with three luxurious bathrooms, "
              propDescription += propBathsDescription;
              break;
            default:
              let numberBaths = "many";
              if (propBaths === 4) {
                numberBaths = "four";
              } else if (propBaths === 5) {
                numberBaths = "five";
              } else if (propBaths === 6) {
                numberBaths = "six";
              } else if (propBaths === 7) {
                numberBaths = "seven";
              } else if (propBaths === 8) {
                numberBaths = "eight";
              } else if (propBaths === 9) {
                numberBaths = "nine";
              }
              propBathsDescription = " with " + numberBaths + " luxurious bathrooms"
              propDescription += propBathsDescription;
              break;
          }
        } else if (propType === "flat") {
          switch (propBaths) {
            case 1:
              propBathsDescription = " with one luxurious bathroom, "
              propDescription += propBathsDescription;
              break;
            case 2:
              propBathsDescription = " with two luxurious bathrooms, "
              propDescription += propBathsDescription;
              break;
            case 3:
              propBathsDescription = " with three luxurious bathrooms, "
              propDescription += propBathsDescription;
              break;
            default:
              let numberBaths = "many";
              if (propBaths === 4) {
                numberBaths = "four";
              } else if (propBaths === 5) {
                numberBaths = "five";
              } else if (propBaths === 6) {
                numberBaths = "six";
              } else if (propBaths === 7) {
                numberBaths = "seven";
              } else if (propBaths === 8) {
                numberBaths = "eight";
              } else if (propBaths === 9) {
                numberBaths = "nine";
              }
              propBathsDescription = " with " + numberBaths + " luxurious bathrooms"
              propDescription += propBathsDescription;
              break;
          }
        } else {
          switch (propBaths) {
            case 1:
              propBathsDescription = " With one luxurious bathroom, "
              propDescription += propBathsDescription;
              break;
            case 2:
              propBathsDescription = " With two luxurious bathrooms, "
              propDescription += propBathsDescription;
              break;
            case 3:
              propBathsDescription = " With three luxurious bathrooms, "
              propDescription += propBathsDescription;
              break;
            default:
              let numberBaths = "many";
              if (propBaths === 4) {
                numberBaths = "four";
              } else if (propBaths === 5) {
                numberBaths = "five";
              } else if (propBaths === 6) {
                numberBaths = "six";
              } else if (propBaths === 7) {
                numberBaths = "seven";
              } else if (propBaths === 8) {
                numberBaths = "eight";
              } else if (propBaths === 9) {
                numberBaths = "nine";
              }
              propBathsDescription = " With " + numberBaths + " luxurious bathrooms"
              propDescription += propBathsDescription;
              break;
          }
        }

        switch (propOutdoorSpace) {
          case "private-garden":
            propOutdoorSpaceDescription = "a stunning private garden that would be perfect for a dog,"
            propDescription += propOutdoorSpaceDescription;
            break;
          case "patio":
            propOutdoorSpaceDescription = "a stunning patio area that would be perfect for summer barbeques,"
            propDescription += propOutdoorSpaceDescription;
            break;
          case "terrace":
            propOutdoorSpaceDescription = "a stunning terrace with an incredible view"
            propDescription += propOutdoorSpaceDescription;
            break;
          case "no-outside":
            propOutdoorSpaceDescription = "stunning nearby parks and gardens,"
            propDescription += propOutdoorSpaceDescription;
            break;
        }

        if (propSize < 60) {
          propSizeDescription = " and an extremely cozy footprint of " + propSize + " square metres";
          propDescription += propSizeDescription;
        } else {
          propSizeDescription = " and an extremely spacious footprint of " + propSize + " square metres";
          propDescription += propSizeDescription;
        }

        propDescription += ", this could be the house for you!"

        switch (propLastDecorated) {
          case "thisYear":
            propLastDecoratedDescription = " The property was last decorated this year in a modern and contemporary manner, ";
            propDescription = propDescription + propLastDecoratedDescription;
            break;
          case "year2019":
            propLastDecoratedDescription = " The property was decorated last year in a modern and contemporary manner, ";
            propDescription = propDescription + propLastDecoratedDescription;
            break;
          case "year2018":
            propLastDecoratedDescription = " The property was last decorated in 2018 in a stylish and tastefull manner, ";
            propDescription = propDescription + propLastDecoratedDescription;
            break;
          case "pre2018":
            propLastDecoratedDescription = " The property was last decorated before 2018 in a stylish and tastefull manner, ";
            propDescription = propDescription + propLastDecoratedDescription;
            break;
        }

        switch (propBuilt) {
          case "brandNew":
            propBuiltDescription = "and built this year with elegance in mind.";
            propDescription += propBuiltDescription;
            break;
          case "last20Years":
            propBuiltDescription = "and built in the last 20 years with elegance in mind.";
            propDescription += propBuiltDescription;
            break;
          case "between1950and2000":
            propBuiltDescription = "and built between 1950 and 2000 with elegance in mind.";
            propDescription += propBuiltDescription;
            break;
          case "between1900and1950":
            propBuiltDescription = "and built between 1900 and 1950 with elegance in mind.";
            propDescription += propBuiltDescription;
            break;
          case "pre1900":
            propBuiltDescription = "and built before 1900 with stunning period craftsmanship.";
            propDescription += propBuiltDescription;
            break;
        }

        switch (propParkingType) {
          case "garage":
            propParkingTypeDescription = " It boasts a private garage where you can store your car or bike, or even convert into your own workshop or art studio,";
            propDescription += propParkingTypeDescription;
            break;
          case "driveway":
            propParkingTypeDescription = " It has a fully accessible, private driveway";
            propDescription += propParkingTypeDescription;
            break;
          case "permit":
            propParkingTypeDescription = " It has permit required street parking with a huge choice of spaces";
            propDescription += propParkingTypeDescription;
            break;
          case "on-street":
            propParkingTypeDescription = " It has free street parking with a huge choice of spaces";
            propDescription += propParkingTypeDescription;
            break;
          case "off-street":
            propParkingTypeDescription = " It has off street parking with a huge choice of spaces";
            propDescription += propParkingTypeDescription;
            break;
          case "garage-and-driveway":
            propParkingTypeDescription = " It boasts both a garage and a driveway where you can store your car or bike, or even convert into your own workshop or art studio";
            propDescription += propParkingTypeDescription;
            break;
          case "allocated":
            propParkingTypeDescription = " It has personal, allocated parking, so there is no time wasted searching out a space";
            propDescription += propParkingTypeDescription;
            break;
        }

        if (propParkingType !== "garage") {
          propDescription += ",";
        }

        if (propHasLift === "higher-with-lift") {
          propHasLiftDescription = " a functional lift,";
          propDescription += propHasLiftDescription;
        }

        switch (propHeatingType) {
          case "electric":
            propHeatingTypeDescription = " and warm, comforting electric heating."
            propDescription += propHeatingTypeDescription;
            break;
          case "gas":
            propHeatingTypeDescription = " and warm, comforting gas central heating.";
            propDescription += propHeatingTypeDescription;
            break;
          case "oil":
            propHeatingTypeDescription = " and warm, comforting oil fired central heating.";
            propDescription += propHeatingTypeDescription;
            break;
          default:
            propHeatingTypeDescription = " and a warm, maintainable heat exchanger.";
            propDescription += propHeatingTypeDescription;
            break;
        }

        if (propExtraCosts > 0) {
          propExtraCostsDescription = " It comes with additional costs of only £" + propExtraCosts + " per month";
        } else {
          propExtraCostsDescription = " It comes with no additional costs";
        }
        propDescription += propExtraCostsDescription;

        switch (propTenure) {
          case "freehold":
            propTenureDescription = " and is a freehold property, so no need to worry about lease renewals.";
            propDescription += propTenureDescription;
            break;
          case "leasehold":
            propTenureDescription = " and is a leasehold property";
            propDescription += propTenureDescription;
            break;
          case "flying-freehold":
            propTenureDescription = " and is a flying freehold property.";
            propDescription += propTenureDescription;
            break;
          case "share-of-freehold":
            propTenureDescription = " and is a share of freehold property.";
            propDescription += propTenureDescription;
            break;
        }

        if (propTenure === "leasehold") {
          propLeaseYearsDescription = " with " + propLeaseYears + " years left on the lease.";
          propDescription += propLeaseYearsDescription;
        }

        propDescription += " Don't miss out, book a viewing today!";
      }


      break;

    default:

      console.log("Unknown Error");
      break;
  }
}

let descriptionGenerator = propDescription;

console.log(descriptionGenerator);