import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import "scroll-behavior-polyfill";

class LandingPage extends React.Component {
	constructor(props){
		super(props);
		this.state = {

		};
	}

	accordion(accordion, x) {
		if(document.getElementById(accordion)){
			let acc = document.getElementById(accordion);
			if(acc.style.maxHeight === "0" || acc.style.maxHeight === "0px"){
				acc.style.maxHeight = acc.scrollHeight + "px";
				if(document.getElementById(x)){
					document.getElementById(x).style.transform = "rotate(45deg)";
				}
			} else {
				acc.style.maxHeight = "0";
				if(document.getElementById(x)){
					document.getElementById(x).style.transform = "rotate(0deg)";
				}
			}
		}
	}

	render() {
		return(
			<div id="pageContent">
				<div className="topBlock">
					<img className="logo" src={require("./img/free-logo.svg")}></img>

					<div className="circleContainer">
						<div className="backgroundText">
							The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull**** The fees
							The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull**** The fees
							The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull**** The fees
							The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull**** The fees
						</div>
						<img className="rosette wobbleCircle" src={require("./img/rosette-green.svg")}></img>
						<div className="rosetteText">
							Everything<br/>must go
						</div>
					</div>
					
					<div className="topHeader">
						<div className="heading">
							You're the best person to sell your own home
						</div>
						Advertise your home on rightmove in under 5 minutes. For Free.<br/>

						{/* <a href="http://free.co.uk/listing" target="_blank"> */}
						<a href="http://free.co.uk/construction" target="_blank">
							<button className="whiteBtn">Sell your home</button>
						</a>
					</div>
				</div>

				<div className="mortgagePrinciple">
					<div className="quoteBox">
						<div className="quoteContent">
							"I feel like I'm not getting all the options... but when I see a mortgage broker I feel like 
							I know more than them"
						</div>
						<div className="quoteAuthor">
							Donna, Cheam
						</div>
					</div>

					<div className="quoteResponse">
						Donna, we hear you
					</div>
					<div className="quoteResponseExplanation">
						<div className="explanationHeading">
							There is a better way, and it's Free.
						</div>
						<div className="quoteResponseContent">
							We don't just help you to sell, we can also help you buy your new home. With just a few details,
							we can show you what 47 lenders will offer you as a mortgage in principle (and we'll give you free
							expert advice too).
						</div>
						<div className="explanationHeading">
							Get your Free mortgage in principle today
						</div>
					</div>

					{/* <a href="http://free.co.uk/mortgage/wizard" target="_blank"> */}
					<a href="http://free.co.uk/construction" target="_blank">
						<button style={{fontSize: "18px", textTransform: "none", letterSpacing: "0"}} className="blackBtn">How much can you borrow?</button>
					</a>
				</div>

				<div className="cuppaBox">
					<div className="cuppaHeading">
						QUICKER THAN <img className="tvIcon" src={require("./img/icon-tv.svg")}></img> A CUPPA
					</div>
					<table>
						<tbody>
							<tr>
								<td style={{width: "55%"}}>
									<iframe className="rayVideo" src="https://www.youtube.com/embed/pw77mdvt6A8"></iframe>
								</td>
								<td style={{textAlign: "left"}}>
									<div className="explanationHeading">
										What's free & faster than making a cuppa?
									</div>
									<div className="cuppaContentTable">
										Yep. Honestly, it's that easy to advertise your property on Rightmove. We're so confident
										we made it a challenge.
									</div>
									<div className="explanationHeading">
										Ready to advertise your home for Free?
									</div>

									{/* <a href="http://free.co.uk/mortgage/wizard" target="_blank"> */}
									<a href="http://free.co.uk/construction" target="_blank">
										<button className="whiteBtn">Sell your home</button>
									</a>
								</td>
							</tr>
						</tbody>
					</table>

					<div className="altTable">
						<iframe className="rayVideo" src="https://www.youtube.com/embed/pw77mdvt6A8"></iframe>

						<div className="explanationHeading">
							What's free & faster than making a cuppa?
						</div>
						<div className="cuppaContent" style={{marginBottom: "10px"}}>
							Yep. Honestly, it's that easy to advertise your property on Rightmove. We're so confident
							we made it a challenge.
						</div>
						<div className="explanationHeading">
							Ready to advertise your home for Free?
						</div>

						{/* <a href="http://free.co.uk/listing" target="_blank"> */}
						<a href="http://free.co.uk/construction" target="_blank">
							<button className="whiteBtn">Get Started</button>
						</a>
					</div>
				</div>

				<div className="catchBox">
					<div className="catchHeading">
						So, what's the catch?
					</div>
					<div className="explanationHeading">
						We don't make money helping you sell your house
					</div>
					<div className="catchContent">
						There's no hook - we make money selling mortgages to the 20 buyers enquiring to book a viewing.
						<br/>
						<img className="rosette fishImage" src={require("./img/rosette-fish.svg")}></img>
						<br/>
						(Whilst there's no obligation for you to use Free for a mortgage, it'd be awesome if you did.)
					</div>
				</div>

				<div className="storyBox">
					<div className="cuppaHeading">
						SIMPLE IS <img className="tvIcon" src={require("./img/icon-tv.svg")}></img> ALWAYS BETTER
					</div>
					<table>
						<tbody>
							<tr>
								<td style={{width: "55%"}}>
									<iframe className="rayVideo" src="https://www.youtube.com/embed/4PhiNDzVGnY"></iframe>
								</td>
								<td style={{textAlign: "left"}}>
									<div className="explanationHeading">
										What's the story?
									</div>
									<div className="cuppaContentTable">
										Listen to our CEO and founder Ray Rafiq tell you the tale about what Free really means.
									</div>
									<div className="youtubeLinkDiv">
										<a href="https://www.youtube.com/channel/UCTHBn84d5gx92jRB6xKyL6A" target="_blank" className="youtubeLink">
											Check out our videos
										</a>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
					<div className="altTable">
						<iframe className="rayVideo" src="https://www.youtube.com/embed/4PhiNDzVGnY"></iframe>
						<div className="explanationHeading">
							What's the story?
						</div>
						<div className="cuppaContent">
							Listen to our CEO and founder Ray Rafiq tell you the tale about what Free really means.
						</div>
						<div className="youtubeLinkDiv">
							<a href="https://www.youtube.com/channel/UCTHBn84d5gx92jRB6xKyL6A" target="_blank" className="youtubeLink">
								Check out our videos
							</a>
						</div>
					</div>
				</div>

				<div className="jobBox">
					<div className="jobHeading">
						Everything for Free
					</div>
					<div className="explanationHeading">
						Interested in joining the good fight?
					</div>
					<div className="jobContent">
						We don't "fill roles". We attract amazing people and help them work on things they are excited about.
						<br/>
						If you're the best at what you do, or young and hungry, we'd love to speak to you.
					</div>
					<div className="emailLinkDiv">
						<a className="emailLink" href="mailto:born@free.co.uk">born@free.co.uk</a>
					</div>
				</div>

				<hr/>

				<div className="otherServices">
					<div className="jobHeading">
						From selling your home to buying a new one
					</div>
					<div className="jobContent">
						Free's got you covered every step of the way.
					</div>
				</div>

				<div className="accordion">
					<div className="accordionHeading"  onClick={() => this.accordion("sellingAccordion", "sellingX")}>
						Sell your home
						<div className="x">
							<img src={require("./img/accordion-cross.svg")}  id="sellingX"></img>
						</div>
					</div>
					
					<div style={{maxHeight: "0px"}} className="accordionContent" id="sellingAccordion">
						Get a professional advert for your home on Rightmove in minutes, with a ton of free advice and powerful tools (we'll even help you manage your viewings for Free!)
						<br/>
						<div className="accordionLinkDiv">
							{/* <a className="accordionLink" href="http://free.co.uk/listing" target="_blank"> */}
							<a className="accordionLink" href="http://free.co.uk/construction" target="_blank">
								Advertise your home
							</a>
						</div>
					</div>
				</div>
				<div className="accordion" >
					<div className="accordionHeading" onClick={() => this.accordion("mortgageAccordion", "mortgageX")}>
						Get a mortgage in principle
						<div className="x">
							<img src={require("./img/accordion-cross.svg")}  id="mortgageX"></img>
						</div>
					</div>
					
					<div style={{maxHeight: "0px"}} className="accordionContent" id="mortgageAccordion">
						With a few details, get an accurate amount to borrow from 47 mortgage providers — so you can confidently find a home knowing how much you can afford - and so your offers get considered seriously.
						<br/>
						<div className="accordionLinkDiv">
							{/* <a className="accordionLink" href="http://free.co.uk/mortgage/wizard" target="_blank"> */}
							<a className="accordionLink" href="http://free.co.uk/construction" target="_blank">
								Find out how much you can borrow
							</a>
						</div>
					</div>
				</div>
				<div className="accordion" >
					<div className="accordionHeading" onClick={() => this.accordion("applyAccordion", "applyX")}>
						Apply for a mortgage
						<div className="x">
							<img src={require("./img/accordion-cross.svg")}  id="applyX"></img>
						</div>
					</div>
					
					<div style={{maxHeight: "0px"}} className="accordionContent" id="applyAccordion">
						Once you’ve found your new home and decided which mortgage is best for you, we’ll help you step-by-step through the application process. Our regulated experts are on hand to answer any questions and speed you to buying your home.
						<div className="paddingBottom"></div>
					</div>
				</div>
				<div className="accordion" >
					<div className="accordionHeading" onClick={() => this.accordion("insuranceAccordion", "insuranceX")}>
						Protect your new home
						<div className="x">
							<img src={require("./img/accordion-cross.svg")} id="insuranceX"></img>
						</div>
					</div>
					
					<div style={{maxHeight: "0px"}} className="accordionContent" id="insuranceAccordion">
						Once you've had your offer accepted, it's important to make sure your new home is protected – we want you and your family to enjoy your new home worry-free. Our experts are here to help you navigate the life insurance products that provide peace of mind.
						<div className="paddingBottom"></div>
					</div>
				</div>

				<div className="bottom">
					<div className="mention">
						Did we mention it's free?
					</div>

					<hr/>

					<a href="https://www.linkedin.com/company/freecouk/" target="_blank">
						<img className="socialIcon" src={require("./img/social-linkedin.svg")}></img>
					</a>

					<a href="https://www.youtube.com/channel/UCTHBn84d5gx92jRB6xKyL6A" target="_blank">
						<img className="socialIcon" src={require("./img/social-youtube.svg")}></img>
					</a>

					<div className="legal">
						Free free free ltd is a registered company No.0123456789. Copyright &copy; 2019-2020 Free free free
						ltd. All rights reserved.
					</div>
				</div>
			</div>
		)
	}
}

ReactDOM.render(
    <LandingPage />,
    document.getElementById('root')
);