import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import Valuator from './valuation';

class Dashboard extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			listing: null,
			postarea: null,
			slug: null,
			token: null
		};
	}

	componentDidMount() {
		localStorage.setItem("previous", "dashboard");

		var url = new URL(window.location.href);
		var slug = url.searchParams.get("slug");

		let user = JSON.parse(localStorage.getItem("user"));
		if(user !== undefined && user !== null){
			let token = user.token;

			this.setState({
				token: token,
				slug: slug
			}, () => {

				var myHeaders = new Headers();
				myHeaders.append("Content-Type", "application/json");
				myHeaders.append("X-Requested-With", "XMLHttpRequest");
				myHeaders.append("Authorization", "Token " + token);

				var requestOptions = {
					method: 'GET',
					headers: myHeaders,
					redirect: 'follow'
				};

				fetch("https://freeco-api.herokuapp.com/api/listings/" + slug, requestOptions)
					.then(response => response.json())
					.then(result => {
						this.setState({
							listing: result.listing
						}, () => {
							console.log(this.state.listing);
							this.getPostcode();
						});
					})
					.catch(error => {
						console.log('error', error);
						window.location.href = "https://free.co.uk/login";
					});
				});
			} else {
				window.location.href = "https://free.co.uk/login";
			}
	}

	getPostcode() {
		let postcode = this.state.listing.addPostcode;
		let postarea = postcode.slice(0,postcode.length - 3);

		this.setState({
			postarea: postarea
		});
	}

	accordion(accordion, x) {
		if (document.getElementById(accordion)) {
			let acc = document.getElementById(accordion);
			if (acc.style.maxHeight === "0" || acc.style.maxHeight === "0px") {
				if (accordion === "valuationAccordion") {
					acc.style.maxHeight = "1500px";
					acc.style.minHeight = "300px";
				} else {
					acc.style.maxHeight = acc.scrollHeight + "px";
				}

				if (document.getElementById(x)) {
					document.getElementById(x).style.transform = "rotate(45deg)";
				}
			} else {
				acc.style.maxHeight = "0";
				acc.style.minHeight = "0";
				if (document.getElementById(x)) {
					document.getElementById(x).style.transform = "rotate(0deg)";
				}
			}
		}
	}

	publish = () => {
		if(this.state.listing !== null){
			var myHeaders = new Headers();
			console.log(this.state.token);
			myHeaders.append("Content-Type", "application/json");
			myHeaders.append("X-Requested-With", "XMLHttpRequest");
			myHeaders.append("Authorization", "Token " + this.state.token);

			var raw = JSON.stringify({"listing":{"isPublished":true}});
			if(this.state.listing.isPublished === true){
				raw = JSON.stringify({"listing":{"isPublished":false}});
			}

			var requestOptions = {
				method: 'PUT',
				headers: myHeaders,
				body: raw,
				redirect: 'follow'
			};

			fetch("https://freeco-api.herokuapp.com/api/listings/" + this.state.slug, requestOptions)
			.then(response => response.text())
			.then(result => {
				let data = JSON.parse(result);
				console.log(data);
				this.setState({
					listing: data.listing
				}, () => {
					console.log(this.state.listing);
				});
			})
			.catch(error => console.log('error', error));
		}
	}

	render() {
		let address = "";
		let price = "";
		if(this.state.listing !== null){
			address = this.state.listing.addLine1;
			price = this.state.listing.propAskingPrice.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
		}

		let checked = "";
		if(this.state.listing !== null){
			if(this.state.listing.isPublished){
				checked = "checked";
			}
		}

		return (
			<div className="listing-flow">
				<div className="navbar">
					<button className="back" onClick={this.prevPage}>
						<img className="backArrow" src={require("./img/icon-arrow-back.svg")}></img>
						<div className="backText">
							Back
						</div>
					</button>
					<img className="logo" src={require("./img/logo.svg")}></img>
				</div>
				<div className="page">
					<a href="https://www.youtube.com/channel/UCTHBn84d5gx92jRB6xKyL6A" target="_blank">
						<div className="blackBar">
							Get some <span className="colorText">Free</span> guidance
							<span className="floatX">+</span>
						</div>
					</a>
					<div className="photoUpload">

						<div className="boxHeading">
							<div className="pageHeading">
								Selling your home
							</div>
							<div className="pageContent">
								{address}
							</div>
							<div className="dashboardPrice">
								£{price}
							</div>
						</div>

						<img className="uploadedPhoto dashboardPhoto" src={require("./img/free-logo.svg")} alt="Exterior image"></img>
						<a href="https://zoopla.com" target="_blank">
							<button className="adBtn">
								Zoopla <img className="rightArrow" src={require("./img/icon-arrow-forward.svg")}></img>
							</button>
						</a><br />
						<a href="https://homeshare.com" target="_blank">
							<button className="adBtn">
								homeshare <img className="rightArrow" src={require("./img/icon-arrow-forward.svg")}></img>
							</button>
						</a><br />
						<a href="https://primelocation.com" target="_blank">
							<button className="adBtn">
								PrimeLocation <img className="rightArrow" src={require("./img/icon-arrow-forward.svg")}></img>
							</button>
						</a><br />
						<a href="https://gumtree.com" target="_blank">
							<button className="adBtn">
								Gumtree <img className="rightArrow" src={require("./img/icon-arrow-forward.svg")}></img>
							</button>
						</a><br />
						<a href="https://rightmove.com" target="_blank">
							<button disabled="disabled" className="adBtn">
								Rightmove <img className="rightArrow" src={require("./img/icon-arrow-forward.svg")}></img>
							</button>
						</a>
						<button className="publishBtn adBtn mB">
							Publish advert
							<label className="switch">
								<input type="checkbox" checked={checked}></input>
								<span className="slider round sliderSwitch" onClick={this.publish}></span>
							</label>
						</button>
					</div>

					<div className="photoUpload">
						<a href="https://free.co.uk/availability">
							<button className="adBtn">
								Your availability <img className="rightArrow" src={require("./img/icon-arrow-forward.svg")}></img>
							</button>
						</a>

						<a href="" target="_blank">
							<button className="adBtn">
								Your viewings <img className="rightArrow" src={require("./img/icon-arrow-forward.svg")}></img>
							</button>
						</a>

						<a href="" target="_blank">
							<button className="adBtn">
								Your offers <img className="rightArrow" src={require("./img/icon-arrow-forward.svg")}></img>
							</button>
						</a>

						<div className="pageContent" style={{ marginTop: "20px" }}>
							<strong>Improve your ad's performance</strong><br />
							Click on a rating to learn how:
						</div>
					</div>
				</div>
				<div class="accordion dashboardAccordion">
					<div class="accordionHeading" onClick={() => this.accordion("qualityAccordion", "qualityX")}>
						Quality
						<div class="x">
							<img src={require("./img/accordion-cross.svg")} id="qualityX"></img>
						</div>
					</div>

					<div style={{ maxHeight: "0px" }} class="accordionContent" id="qualityAccordion">
						Waiting...
						<div className="paddingBottom"></div>
					</div>
				</div>

				<div class="accordion dashboardAccordion">
					<div class="accordionHeading" onClick={() => this.accordion("visibilityAccordion", "visibilityX")}>
						Visibility
						<div class="x">
							<img src={require("./img/accordion-cross.svg")} id="visibilityX"></img>
						</div>
					</div>

					<div style={{ maxHeight: "0px" }} class="accordionContent" id="visibilityAccordion">
						Waiting...
						<div className="paddingBottom"></div>
					</div>
				</div>

				<div class="accordion dashboardAccordion">
					<div class="accordionHeading" onClick={() => this.accordion("valuationAccordion", "valuationX")}>
						Valuation
						<div class="x">
							<img src={require("./img/accordion-cross.svg")} id="valuationX"></img>
						</div>
					</div>

					<div style={{ maxHeight: "0px" }} class="accordionContent" id="valuationAccordion">
						<Valuator postcode={this.state.postarea}/>
						<div className="paddingBottom"></div>
					</div>
				</div>

				<div className="whatNext">
					<div className="pageHeading whatNextHeading">
						What's next?
					</div>

					<div className="whiteLine">
						<div className="whiteDot">

						</div>
					</div>

					<div className="pageContent">
						<strong>Get a mortgage-in-principle in 5 minutes</strong><br />
						A no-commitment peek into what lenders are prepared to offer you, giving you a clear idea
						of a budget while looking for your new home.
					</div>

					<a href="https://beta.free.co.uk/mortgage/wizard" target="_blank">
						<button className="capBtn colorBtn">Find your budget</button>
					</a>
				</div>
			</div>
		);
	}
}

ReactDOM.render(
	<React.StrictMode>
		<Dashboard />
	</React.StrictMode>,
	document.getElementById('root')
);
