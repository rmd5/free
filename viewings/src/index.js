import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
require ("datejs");

class Viewings extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			bookings: null,
			viewingSections: null,
			viewingSectionsPast: null,
			slug: null,
			specific: null,
			token: null,
			timeframe: "future"
		};
	}

	prevPage = () => {
		this.setState({
			specific: null
		});
	}

	componentDidMount() {
		var url = new URL(window.location.href);
		var slug = url.searchParams.get("slug");

		// let user = JSON.parse(localStorage.getItem("user"));
		// let token = user.token;
		let token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmMTgyZjFjM2VmYWRkMzgwMDBkZDdjMyIsImV4cCI6MTYwMTI4OTk5OSwiaWF0IjoxNTk2MTA1OTk5fQ.a6nJFKR8Wzd_w6P1CU7OodMFjZZucr4EPNE7uKSbm-w";

		this.setState({
			slug: slug,
			token: token
		});

		var myHeaders = new Headers();
		myHeaders.append("Content-Type", "application/json");
		myHeaders.append("X-Requested-With", "XMLHttpRequest");
		myHeaders.append("Authorization", "Token " + token);

		var requestOptions = {
			method: 'GET',
			headers: myHeaders,
			redirect: 'follow'
		};

		fetch("https://freeco-api.herokuapp.com/api/listings/" + slug + "/viewings", requestOptions)
		.then(response => response.json())
		.then(result => {
			console.log(result.viewings);
			this.setState({
				bookings: result.viewings
			}, () => {
				this.sortData();
			});
		})
		.catch(error => console.log('error', error));		
	}

	sortData() {
		let bookings = this.state.bookings.sort(function(a, b) {
			return (a.date < b.date) ? -1 : ((a.date > b.date) ? 1 : 0);
		});

		this.setState({
			bookings: bookings
		});

		let vCardArr = [];
		let vCardArrPast = [];
		let date = "";
		for(let i = 0, size = bookings.length; i < size; i++){
			let yearSlice = parseInt(bookings[i].date.slice(0,4));
			let monthSlice = parseInt(bookings[i].date.slice(5,7));
			let daySlice = parseInt(bookings[i].date.slice(8,10));
			let timeSlice = bookings[i].date.slice(11,16);

			let vCard = "";
			let formatDate = Date.today().set({year: yearSlice, month: monthSlice-1, day: daySlice}).toString("dddd dS MMMM");
			
			let status = bookings[i].status;
			let statusText = "";
			if(status === "pending"){
				statusText = <div>Review this request</div>;
			} else if(status === "confirmed"){
				statusText = <div style={{color: "green", fontWeight: "900"}}>Confirmed</div>;
			} else if(status === "seller_declined"){
				statusText = <div style={{color: "#999", fontWeight: "900"}}>Cancelled</div>;
			}
			
			if(Date.compare(Date.today().setTimeToNow(), Date.parse(bookings[i].date)) === -1){
				if(date !== formatDate){
					date = formatDate;

					vCard =
						<div>
							<div className="pageContent viewingHeading">
								{formatDate}
							</div>
							<div className="viewingBtn" onClick={() => this.goToBooking(i)}>
								<div className="viewingBtnName" style={{textDecoration: status === "seller_declined" ? "line-through" : "none"}}>
									<img style={{marginBottom: "-3px", display: status==="pending"?"inline-block":"none"}} src={require("./img/icon-exclamation.svg")}></img> {timeSlice} with {bookings[i].name}
								</div>
								<img className="viewingBtnArrow" src={require("./img/icon-arrow-forward.svg")}></img>
								<div className="viewingBtnStatus">
									{statusText}
								</div>
							</div>
						</div>;
				} else {
					vCard =
						<div>
							<div className="viewingBtn" onClick={() => this.goToBooking(i)}>
								<div className="viewingBtnName" style={{textDecoration: status === "seller_declined" ? "line-through" : "none"}}>
									<img style={{marginBottom: "-3px", display: status==="pending"?"inline-block":"none"}} src={require("./img/icon-exclamation.svg")}></img> {timeSlice} with {bookings[i].name}
								</div>
								<img className="viewingBtnArrow" src={require("./img/icon-arrow-forward.svg")}></img>
								<div className="viewingBtnStatus">
									{statusText}
								</div>
							</div>
						</div>;
				}

				vCardArr.push(vCard);
			} else {
				if(date !== formatDate){
					date = formatDate;

					vCard =
						<div>
							<div className="pageContent viewingHeading">
								{formatDate}
							</div>
							<div className="viewingBtn" onClick={() => this.goToBooking(i)}>
								<div className="viewingBtnName" style={{textDecoration: status === "seller_declined" ? "line-through" : "none"}}>
									<img style={{marginBottom: "-3px", display: status==="pending"?"inline-block":"none"}} src={require("./img/icon-exclamation.svg")}></img> {timeSlice} with {bookings[i].name}
								</div>
								<img className="viewingBtnArrow" src={require("./img/icon-arrow-forward.svg")}></img>
								<div className="viewingBtnStatus">
									Star Rating
								</div>
							</div>
						</div>;
				} else {
					vCard =
						<div>
							<div className="viewingBtn" onClick={() => this.goToBooking(i)}>
								<div className="viewingBtnName" style={{textDecoration: status === "seller_declined" ? "line-through" : "none"}}>
									<img style={{marginBottom: "-3px", display: status==="pending"?"inline-block":"none"}} src={require("./img/icon-exclamation.svg")}></img> {timeSlice} with {bookings[i].name}
								</div>
								<img className="viewingBtnArrow" src={require("./img/icon-arrow-forward.svg")}></img>
								<div className="viewingBtnStatus">
									Star Rating
								</div>
							</div>
						</div>;
				}

				vCardArrPast.push(vCard);
			}
			// let vCard = 
			// 	<div>
			// 		<div className="pageContent">
			// 			{Date.parse(bookings[i].date)}
			// 		</div>
			// 	</div>;
			this.setState({
				viewingSections: vCardArr,
				viewingSectionsPast: vCardArrPast
			});
		}
	}

	goToBooking = (num) => {
		console.log(num);
		this.setState({
			specific: num
		});
	}

	confirmBooking = (e) => {
		let booking = e.currentTarget.value;

		console.log(this.state.token);

		var myHeaders = new Headers();
		myHeaders.append("Content-Type", "application/json");
		myHeaders.append("X-Requested-With", "XMLHttpRequest");
		myHeaders.append("Authorization", "Token " + this.state.token);

		var raw = JSON.stringify({"viewing":{"status":"confirmed"}});

		var requestOptions = {
			method: 'PUT',
			headers: myHeaders,
			body: raw,
			redirect: 'follow'
		};

		fetch("https://freeco-api.herokuapp.com/api/listings/" + this.state.slug + "/viewings/" + booking, requestOptions)
		.then(response => response.text())
		.then(result => {
			console.log(result);
			this.componentDidMount();
		})
		.catch(error => console.log('error', error));
	}

	declineBooking(booking) {
		console.log(this.state.token);

		var myHeaders = new Headers();
		myHeaders.append("Content-Type", "application/json");
		myHeaders.append("X-Requested-With", "XMLHttpRequest");
		myHeaders.append("Authorization", "Token " + this.state.token);

		var raw = JSON.stringify({"viewing":{"status":"seller_declined"}});

		var requestOptions = {
			method: 'PUT',
			headers: myHeaders,
			body: raw,
			redirect: 'follow'
		};

		fetch("https://freeco-api.herokuapp.com/api/listings/" + this.state.slug + "/viewings/" + booking, requestOptions)
		.then(response => response.text())
		.then(result => {
			console.log(result);
			this.componentDidMount();
		})
		.catch(error => console.log('error', error));
	}

	switchTimeframe = () => {
		if(this.state.timeframe === "future"){
			document.getElementById("timeframeBtn").innerHTML = "View upcoming viewings";
			this.setState({
				timeframe: "past"
			});
		} else {
			document.getElementById("timeframeBtn").innerHTML = "View previous viewings";
			this.setState({
				timeframe: "future"
			});
		}
	}

	render() {

		let main = 
			<div>
				<div className="pageHeading" style={{textAlign: "left"}}>
					Your viewings
				</div>
				<div style={{display: this.state.timeframe === "future" ? "block" : "none"}}>{this.state.viewingSections}</div>
				<div style={{display: this.state.timeframe === "past" ? "block" : "none"}}>{this.state.viewingSectionsPast}</div>
				<div className="pageContent" style={{textAlign: "left", marginTop: "20px"}}>
					Viewers of your advert are only able to book viewings where you have set your availability.
					<br/><br/>
					<a href={"https://free.co.uk/availability/?slug=" + this.state.slug}>
						<span className="link">
							Edit your availability
						</span>
					</a>
				</div>
				<div className="viewingPastBtn">
					<span id="timeframeBtn" onClick={this.switchTimeframe} className="link" style={{float: "right"}}>
						View previous viewings
					</span>
				</div>
			</div>;

		if(this.state.specific !== null){
			let bookings = this.state.bookings;
			let sp = this.state.specific;

			let yearSlice = parseInt(bookings[sp].date.slice(0,4));
			let monthSlice = parseInt(bookings[sp].date.slice(5,7));
			let daySlice = parseInt(bookings[sp].date.slice(8,10));
			let timeSlice = bookings[sp].date.slice(11,16);
			let formatDate = Date.today().set({year: yearSlice, month: monthSlice, day: daySlice}).toString("dddd dS MMMM");
			
			let status = bookings[sp].status;
			let text =
				<div style={{color: "red"}}>
					Please respond promptly to their request
				</div>;
			if(status === "confirmed"){
			text =
				<div style={{color: "green"}}>
					Confirmed
				</div>;
			} else if(status === "seller_declined"){
			text =
				<div style={{color: "#999"}}>
					Cancelled
				</div>;
			}

			let count = 0;
			for(let i = 0, size = bookings.length; i < size; i++){
				if(bookings[i].mobile === bookings[sp].mobile){
					count++;
				}
			}

			let countText = Date.today().set({day: 4}).toString("dS");

			if(bookings[sp].status === "pending"){
				main =
					<div>
						<div className="pageContent" style={{textAlign: "left", marginTop: "20px", fontWeight: "900"}}>
							{formatDate}
							<span style={{float: "right", fontSize: "21px"}}>
								{timeSlice}
							</span>
						</div>
						<div className="pageHeading" style={{textAlign: "left", fontSize: "36px"}}>
							{bookings[sp].name}
						</div>
						<div className="pageContent" style={{textAlign: "left", fontWeight: "900"}}>
							{text}
						</div>
						<div className="pageContent" style={{textAlign: "left"}}>
							<i>This looks like {bookings[sp].name}'s {countText} viewing of the property</i>
						</div>
						<div className="viewingPastBtn">
							<span onClick={() => this.declineBooking(bookings[sp]._id)} style={{float: "left", marginTop: "17px", fontSize: "15px"}} className="link">Decline</span>
							<button onClick={this.confirmBooking} value={bookings[sp]._id} style={{float:"right", margin:"0",fontSize: "21px"}} className="capBtn">Confirm</button>
						</div>
					</div>;
			} else if(bookings[sp].status === "confirmed"){
				main =
					<div>
						<div className="pageContent" style={{textAlign: "left", marginTop: "20px", fontWeight: "900"}}>
							{formatDate}
							<span style={{float: "right", fontSize: "21px"}}>
								{timeSlice}
							</span>
						</div>
						<div className="pageHeading" style={{textAlign: "left", fontSize: "36px"}}>
							{bookings[sp].name}
						</div>
						<div className="pageContent" style={{textAlign: "left", fontWeight: "900"}}>
							{text}
						</div>
						<div className="pageContent" style={{textAlign: "left"}}>
							<i>This looks like {bookings[sp].name}'s {countText} viewing of the property</i>
						</div>
						<span onClick={() => this.declineBooking(bookings[sp]._id)} style={{float: "left", marginTop: "17px", fontSize: "15px"}} className="link">Cancel the booking</span>
					</div>;
			} else if(bookings[sp].status === "seller_declined"){
				let yearUpdateSlice = parseInt(bookings[sp].updatedAt.slice(0,4));
				let monthUpdateSlice = parseInt(bookings[sp].updatedAt.slice(5,7));
				let dayUpdateSlice = parseInt(bookings[sp].updatedAt.slice(8,10));
				let timeHourUpdateSlice = parseInt(bookings[sp].updatedAt.slice(11,13)) + 1;
				let timeMinUpdateSlice = parseInt(bookings[sp].updatedAt.slice(14,16));
				console.log(bookings[sp]);
				console.log(bookings[sp].date.slice(14,16));

				let formatUpdateDate = Date.today().set({year: yearUpdateSlice, month: monthUpdateSlice, day: dayUpdateSlice, hour: timeHourUpdateSlice, minute: timeMinUpdateSlice}).toString("h:mmtt on dddd dS MMMM yyyy");

				main =
					<div>
						<div className="pageContent" style={{textAlign: "left", marginTop: "20px", fontWeight: "900"}}>
							{formatDate}
							<span style={{float: "right", fontSize: "21px", textDecoration: "line-through"}}>
								{timeSlice}
							</span>
						</div>
						<div className="pageHeading" style={{textAlign: "left", fontSize: "36px", textDecoration: "line-through"}}>
							{bookings[sp].name}
						</div>
						<div className="pageContent" style={{textAlign: "left", fontWeight: "900"}}>
							{text}
						</div>
						<div className="pageContent" style={{textAlign: "left"}}>
							<i>This looks like {bookings[sp].name}'s {countText} viewing of the property</i>
						</div>
						<div className="pageContent" style={{textAlign: "left"}}>
							You cancelled the booking at {formatUpdateDate}
						</div>
						<div className="pageContent" style={{textAlign: "left"}}>
							{bookings[sp].name} was sent an SMS informing them that the booking was cancelled
						</div>
					</div>;
			}
		}

		return (
			<div className="listing-flow">
				<div className="navbar">
					<button className="back" onClick={this.prevPage}>
						<img className="backArrow" src={require("./img/icon-arrow-back.svg")}></img>
						<div className="backText">
							Back
						</div>
					</button>
					<img className="logo" src={require("./img/logo.svg")}></img>
				</div>
				<div className="page">
					<a href="https://www.youtube.com/channel/UCTHBn84d5gx92jRB6xKyL6A" target="_blank">
						<div className="blackBar">
							Get some <span className="colorText">Free</span> guidance
							<span className="floatX">+</span>
						</div>
					</a>

					<div className="content" style={{padding: "20px"}}>
						{main}
					</div>
				</div>
			</div>
		)
	}
}

ReactDOM.render(
	<React.StrictMode>
		<Viewings />
	</React.StrictMode>,
	document.getElementById('root')
);
