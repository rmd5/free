import React from 'react';
import { Route, Switch} from 'react-router-dom';

import Listing from './components/listing';
import Availability from './components/availability';
import Calculator from './components/calculator';
import Optimiser from './components/optimiser';
import Valuator from './components/valuation';
import Dashboard from './components/dashboard';
import LandingPage from './components/landing_page';
import PhotoUpload from './components/photo_upload';
import Photoshoot from './components/photoshoot';
import SelectListing from './components/select_listing';
import Login from './components/login';
import Viewings from './components/viewings';
import BookViewing from './components/book_viewing';
import Publish from './components/publish';

function App() {
	return (
		<div className="App">
			<Switch>
				<Route exact path="/" component={LandingPage}></Route>
				<Route path="/listing" component={Listing}></Route>
				<Route path="/photo-upload" component={PhotoUpload}></Route>
				<Route path="/photoshoot" component={Photoshoot}></Route>
				<Route path="/select-listing" component={SelectListing}></Route>
				<Route path="/login" component={Login}></Route>
				<Route path="/viewings" component={Viewings}></Route>
				<Route path="/book-viewing" component={BookViewing}></Route>
				<Route path="/availability" component={Availability}></Route>
				<Route path="/calculator" component={Calculator}></Route>
				<Route path="/optimiser" component={Optimiser}></Route>
				<Route path="/valuation" component={Valuator}></Route>
				<Route path="/dashboard" component={Dashboard}></Route>
				<Route path="/publish" component={Publish}></Route>
			</Switch>
		</div>
	);
}

export default App;