import React from 'react';
import { Route, Switch} from 'react-router-dom';

import Thanks from './components/thanks';

import Listing from './components/listing-flow/listing';
import Availability from './components/listing-flow/availability';
import Dashboard from './components/listing-flow/dashboard';
import LandingPage from './components/landing_page';
import PhotoUpload from './components/listing-flow/photo_upload';
import Photoshoot from './components/listing-flow/photoshoot';
import SelectListing from './components/listing-flow/select_listing';
import Login from './components/listing-flow/login';
import Viewings from './components/listing-flow/viewings';
import BookViewing from './components/listing-flow/book_viewing';
import Publish from './components/listing-flow/publish';
import EPC from './components/listing-flow/epc';

import Entry from './components/mortgage-wizard/entry';
import Details from './components/mortgage-wizard/details';

import Calculator from './components/tools/calculator';
import Optimiser2 from './components/tools/optimiser2';
import Valuator2 from './components/tools/valuation2';
import Chart from './components/tools/chart';
import Optimiser3 from './components/tools/optimiser3';

function App() {
	return (
		<div>
			<Switch>
				<Route exact path="/" component={LandingPage}></Route>
				<Route path="/thanks" component={Thanks}></Route>

				<Route path="/listing" component={Listing}></Route>
				<Route path="/photo-upload" component={PhotoUpload}></Route>
				<Route path="/photoshoot" component={Photoshoot}></Route>
				<Route path="/select-listing" component={SelectListing}></Route>
				<Route path="/login" component={Login}></Route>
				<Route path="/viewings" component={Viewings}></Route>
				<Route path="/book-viewing" component={BookViewing}></Route>
				<Route path="/availability" component={Availability}></Route>
				<Route path="/dashboard" component={Dashboard}></Route>
				<Route path="/publish" component={Publish}></Route>
				<Route path="/EPC" component={EPC}></Route>

				<Route exact path="/mortgage/wizard" component={Entry}></Route>
				<Route path="/mortgage/wizard/details" component={Details}></Route>

				<Route path="/calculator" component={Calculator}></Route>
				<Route path="/optimiser" component={Optimiser2}></Route>
				<Route path="/optimiser2" component={Optimiser3}></Route>
				<Route path="/valuation" component={Valuator2}></Route>
				<Route path="/chart" component={Chart}></Route>
			</Switch>
		</div>
	);
}

export default App;