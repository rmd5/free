import React from 'react';

class Entry extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            page: 1,
            title: null,
            firstName: null,
            lastName: null,
            phonenumber: "+447399266184",
            numberOfApplicants: null,
            nationality: null,
            residencyStatus: null,
            employment1: null,
            employmentType1: null,
            annualIncome1: null,
            yearsInBusiness: null,
            netProfit1920: null,
            netProfit1819: null,
            shareOfCompany: null,
            remuneration1920: null,
            remuneration1819: null,
            opProfit1920: null,
            opProfit1819: null,
            dailyRate: null,
            deductionsOnPayslip: null,
            additionalIncome: null,
            monthlyLoanRepayments: null,
            monthlyLoanPaymentReduction: null,
            loanRemainingBalance: null,
            creditBalance: null,
            creditBalanceRepaid: null,
            adverseCredit: null,
            budget: null,
            deposit: null,
            dob: "30051998",
        };
    }

    nextPage = () => {
        let page = this.state.page;
        if(page === 5.6){
            page = 6;
        } else if(page === 5.3){
            page = 5.6;
        } else if(page === 5 && this.state.nationality === "OTHER"){
            page = 5.3;
        } else if(page === 9 || page === 13 || page === 21){
            page = 23;
        } else if(page === 3 && this.state.numberOfApplicants === "3"){
            page = 3.5;
        } else if(page === 7){
            if(this.state.employment1 === "SELF_EMPLOYED"){
                page = 10;
            } else if(this.state.employment1 === "COMPANY_DIRECTOR"){
                page = 14;
            } else if(this.state.employment1 === "CONTRACTOR"){
                page = 22;
            } else {
                page += 1;
            }
        } else if(page === 30){
            if(this.state.adverseCredit === "YES"){
                page = 30.5;
            } else {
                page = 31;
            }
        } else if(page === 30.5){
            page = 31;
        } else {
            page += 1;
        }

        this.setState({
            page: page
        }, () => {
            console.log(this.state.page);
        });
    }

    prevPage = () => {
        let page = this.state.page;
        if(page === 1){
            document.getElementById("leaveModal").style.display = "block";
        } else {
            if(page === 3.5){
                page = 3;
            } else if(page === 5.6){
                page = 5.3;
            } else if(page === 5.3){
                page = 5;
            } else if(page === 6 && this.state.nationality === "OTHER"){
                page = 5.6;
            } else if(page === 10){
                page = 7;
            } else if(page === 14){
                page = 7;
            } else if(page === 22){
                page = 7;
            } else if(page === 23){
                if(this.state.employment1 === "EMPLOYED"){
                    page = 9;
                } else if(this.state.employment1 === "SELF_EMPLOYED"){
                    page = 13;
                } else if(this.state.employment1 === "COMPANY_DIRECTOR"){
                    page = 21;
                } else if(this.state.employment1 === "CONTRACTOR"){
                    page = 22;
                }
            } else if(page === 30.5){
                page = 30;
            } else {
                page -= 1;
            }

            this.setState({
                page: page
            });
        }
    }

    manualCalc(e, update){
        let value = e.currentTarget.value.replace(/,/g, "");
        value = value.replace(/\//g, "");
        value = value.replace(/ /g, "");
        value = value.trim();
        value = parseInt(value);

		if(value >= 0 || isNaN(value) === true){
			if(isNaN(value) === true){
				value = "";
			} else {
                value = value.toString();
                if(this.state.dob !== null){
                    if(update === "dob" && value.length > 8){
                        value = value.slice(0,8);
                    }
                }
			}
			this.setState({
				[update]: value
			});
		}
    }

    calc(e, update){
        let input = e.currentTarget.value;
		let current = this.state[update];
		if (input !== "backspace") {
            if(current !== null){
                if(update === "dob" && current.toString().length === 8){
                    this.setState({
                        [update]: current
                    });
                } else {
                    let value = input;
                    if (current !== null) {
                        value = current.toString() + input;
                    }
    
                    this.setState({
                        [update]: value
                    });
                }
            } else {
                let value = input;
                if (current !== null) {
                    value = current.toString() + input;
                }

                this.setState({
                    [update]: value
                });
            }
		} else if (input === "backspace") {
            if(current !== null){
                let value = current.toString().slice(0, current.toString().length - 1);
                this.setState({
                    [update]: value
                });
            }
		}
    }

    updateDetail(e, update){
        this.setState({
            [update]: e.currentTarget.value
        }, () => {
            this.nextPage();
        });
    }

    updateDetailNoPageTurn(e, update){
        this.setState({
            [update]: e.currentTarget.value
        });
    }

    leavePage = (e) => {
		let decision = e.currentTarget.value;
		if(decision === "yes"){
			window.location.href = "/";
		} else {
			document.getElementById("leaveModal").style.display = "none";
		}
    }

    accordion = (e) => {
		if (document.getElementById(e.currentTarget.value)) {
			if (document.getElementById(e.currentTarget.value).style.maxHeight === document.getElementById(e.currentTarget.value).scrollHeight + "px") {
				document.getElementById(e.currentTarget.value).style.maxHeight = "0px";
				if (document.getElementById("moreDependents1")) {
					document.getElementById("moreDependents1").innerHTML = "More options";
				}
			} else {
				document.getElementById(e.currentTarget.value).style.maxHeight = document.getElementById(e.currentTarget.value).scrollHeight + "px";
				if (document.getElementById("moreDependents1")) {
					document.getElementById("moreDependents1").innerHTML = "Less options";
				}
			}
		}
    }

    callback(reason) {
        let title = this.state.title;
        let firstName = this.state.firstName;
        let lastName = this.state.lastName;
        let phonenumber = this.state.phonenumber;

        if(this.state.nationality === "OTHER"){
            reason = reason + ". Residency status: " + this.state.residencyStatus;
        }

        var raw = {"title": title, "name": firstName, "surname": lastName, "phone": phonenumber, "reason": reason};
            
        var requestOptions = {
            method: 'POST',
            body: JSON.stringify(raw),
            redirect: 'follow'
        };
        
        fetch("https://hooks.zapier.com/hooks/catch/7885656/ozueoq2", requestOptions)
            .then(response => response.text())
            .then(result => console.log(result))
            .catch(error => console.log('error', error));
    }

    render() {
        let displayPage = null;
        switch(this.state.page){
            case 1:
                displayPage = 
                    <div>
                        <div className="pageHeading" style={{fontSize: "42px", lineHeight: "48px", marginBottom: "100px"}}>
                            Mortgages as easy as booking that viewing
                        </div>
                        <div className="pageContent">
                            Get a mortgage-in-principle in just 5 minutes.
                        </div>
                        <div className="pageContent">
                            For free. Obviously.
                        </div>
                        <button onClick={this.nextPage} className="capBtn">Go</button>
                        <div id="leaveModal" className="modal">
							<div className="modalContent">
								<div className="pageHeading">
									Go back to Free.co.uk?
								</div>
								<button className="radio" value="yes" onClick={this.leavePage}>Yes</button><br/>
								<button className="radio" value="no" onClick={this.leavePage}>No</button>
							</div>
						</div>
                    </div>; 
                break;

            case 2:
                let dis2 = "";
                if(this.state.firstName === null || this.state.lastName === null || this.state.title === null){
                    dis2 = "disabled";
                }
                displayPage = 
                    <div>
                        <div className="pageHeading">
                            What's your name?
                        </div>
                        <div className="pageContent">
							Some personal details first. Don't worry, we won't share these!
						</div>
                        <input className="titleInput" type="text" value={this.state.title} placeholder="Title" onChange={(e) => this.updateDetailNoPageTurn(e,"title")}></input>
						<br />
						<input className="lastNameInput" type="text" value={this.state.firstName} placeholder="First name" onChange={(e) => this.updateDetailNoPageTurn(e,"firstName")}></input>
						<br />
						<input className="lastNameInput" type="text" value={this.state.lastName} placeholder="Last name" onChange={(e) => this.updateDetailNoPageTurn(e,"lastName")}></input>
						<br />
						<button className="capBtn" disabled={dis2} onClick={this.nextPage}>Next</button>
                    </div>
                break;

            case 3:
                displayPage =
                    <div>
                        <div className="pageHeading">
                            Who else will be on the application?
                        </div>
                        <div className="pageContent">
                            Buying a house with a combined income can get you access to better offers.
                        </div>
                        <button onClick={(e) => this.updateDetail(e,"numberOfApplicants")} className="radio" value="1">Just me<img style={{ visibility: this.state.numberOfApplicants === "1" ? "visible" : "hidden" }} className="tick" src={require("../img/icon-tick.svg")}></img></button>
                        <button onClick={(e) => this.updateDetail(e,"numberOfApplicants")} className="radio" value="2">Me &amp; someone else<img style={{ visibility: this.state.numberOfApplicants === "2" ? "visible" : "hidden" }} className="tick" src={require("../img/icon-tick.svg")}></img></button>
                        <button onClick={(e) => this.updateDetail(e,"numberOfApplicants")} className="radio" value="3">More than two of us<img style={{ visibility: this.state.numberOfApplicants === "3" ? "visible" : "hidden" }} className="tick" src={require("../img/icon-tick.svg")}></img></button>
                    </div>
                break;

            case 3.5:
                displayPage =
                    <div>
                        <div className="pageHeading">
                            Mortgages for more than two people require futher information
                        </div>
                        <div className="pageContent">
                            This isn't a problem - just a situation for which we'll need a few more details.
                        </div>
                        <div className="callbackBox">
                            <div className="callbackHeading">
                                We want to help
                            </div>
                            <div className="pageContent">
                                One of our advisors will give you a call to discuss your options and ensure we get you accurate results.
                            </div>
                            <button className="capBtn" onClick={() => this.callback("More than two people")} style={{margin: "0"}}>Get a callback</button>
                        </div>
                    </div>
                break;

            case 4:
                let date = this.state.dob;
                let disableDOBBtn = "disabled";
                if(date !== null){
                    if(date.length === 8){
                        disableDOBBtn = "";
                    }

                    if(date.length > 4){
                        let dateDay = date.slice(0,2);
                        let dateMonth = date.slice(2,4);
                        let dateYear = date.slice(4,8);
                        date = dateDay + " / " + dateMonth + " / " + dateYear;
                    }

                    if(date.length > 2 && date.length <= 4){
                        let dateDay = date.slice(0,2);
                        let dateMonth = date.slice(2,4);
                        date = dateDay + " / " + dateMonth;
                    }
                }
                displayPage =
                    <div>
                        <div className="pageHeading">
                            When's your birthday?
                        </div>
                        <div className="pageContent">
                            We won't send you a birthday card, but this helps us gauge the length of mortgages
                            lenders can offer you.
                            <br/><br/>
                            (Okay, we might send you a card).
                        </div>
                        <input style={{width: "276px"}} type="text" placeholder="DD / MM / YYYY" value={date} className="leaseInput" onChange={(e) => this.manualCalc(e, "dob")}></input>
                        <br/>
                        <div className="calculator">
                            <button className="calc-btn" value="1" onClick={(e) => this.calc(e, "dob")}>1</button>
                            <button className="calc-btn" value="2" onClick={(e) => this.calc(e, "dob")}>2</button>
                            <button className="calc-btn" value="3" onClick={(e) => this.calc(e, "dob")}>3</button>
                            <button className="calc-btn" value="4" onClick={(e) => this.calc(e, "dob")}>4</button>
                            <button className="calc-btn" value="5" onClick={(e) => this.calc(e, "dob")}>5</button>
                            <button className="calc-btn" value="6" onClick={(e) => this.calc(e, "dob")}>6</button>
                            <button className="calc-btn" value="7" onClick={(e) => this.calc(e, "dob")}>7</button>
                            <button className="calc-btn" value="8" onClick={(e) => this.calc(e, "dob")}>8</button>
                            <button className="calc-btn" value="9" onClick={(e) => this.calc(e, "dob")}>9</button>
                            <button disabled="disabled" className="calc-btn dot-calc-btn" value="." onClick={(e) => this.calc(e, "dob")}>.</button>
                            <button className="calc-btn" value="0" onClick={(e) => this.calc(e, "dob")}>0</button>
                            <button className="calc-btn" value="backspace" onClick={(e) => this.calc(e, "dob")}><img value="backspace" src={require("../img/icon-keypad-delete.svg")}></img></button>
                        </div>
                        <br/>
                        <button disabled={disableDOBBtn} onClick={this.nextPage} clasName="capBtn">Next</button>
                    </div>
                break;

            case 5:
                displayPage =
                    <div>
                        <div className="pageHeading">
                            Are all of the applicants UK nationals?
                        </div>
                        <div className="pageContent">
                            There's a slightly different process involved for non-UK citizens when applying for a mortgage.
                        </div>
                        <button onClick={(e) => this.updateDetail(e,"nationality")} className="radio" value="UK">Yes<img style={{ visibility: this.state.nationality === "UK" ? "visible" : "hidden" }} className="tick" src={require("../img/icon-tick.svg")}></img></button><br/>
                        <button onClick={(e) => this.updateDetail(e,"nationality")} className="radio" value="OTHER">No<img style={{ visibility: this.state.nationality === "OTHER" ? "visible" : "hidden" }} className="tick" src={require("../img/icon-tick.svg")}></img></button>
                    </div>
                break;

            case 5.3:
                displayPage =
                    <div>
                        <div className="pageHeading">
                            What's your current residency status?
                        </div>
                        <div className="pageContent">
                            Mortgage providers may require some further information, depending on your residency.
                        </div>
                        <button onClick={(e) => this.updateDetail(e,"residencyStatus")} className="radio" value="INDEFINITE_LEAVE_TO_REMAIN">Indefinite leave to remain<img style={{ visibility: this.state.residencyStatus === "INDEFINITE_LEAVE_TO_REMAIN" ? "visible" : "hidden" }} className="tick" src={require("../img/icon-tick.svg")}></img></button><br/>
                        <button onClick={(e) => this.updateDetail(e,"residencyStatus")} className="radio" value="MORE_THAN_TWO_YEARS_ON_VISA">More than two years on visa<img style={{ visibility: this.state.residencyStatus === "MORE_THAN_TWO_YEARS_ON_VISA" ? "visible" : "hidden" }} className="tick" src={require("../img/icon-tick.svg")}></img></button><br/>
                        <button onClick={(e) => this.updateDetail(e,"residencyStatus")} className="radio" value="LESS_THAN_TWO_YEARS_ON_VISA">Less than two years on visa<img style={{ visibility: this.state.residencyStatus === "LESS_THAN_TWO_YEARS_ON_VISA" ? "visible" : "hidden" }} className="tick" src={require("../img/icon-tick.svg")}></img></button><br/>
                    </div>
                break;

            case 5.6:
                displayPage =
                    <div>
                        <div className="pageHeading">
                            Your visa status may affect what you can borrow
                        </div>
                        <div className="callbackBox">
                            <div className="callbackHeading">
                                We want to help
                            </div>
                            <div className="pageContent">
                                One of our advisers will give you a call to review different options over the phone, to ensure
                                we get you accurate results.
                            </div>
                            <button className="capBtn" onClick={() => this.callback("Not all applicants are UK citizens")} style={{margin: "0"}}>Get a callback</button>
                        </div>
                        <div className="pageContent" style={{marginTop: "20px"}}>
                            It's still important to complete the form to get some provisional results, we can help with the rest!
                            <br/><br/>
                            <span className="link" onClick={this.nextPage}>Complete the form</span>
                        </div>
                    </div>
                break;

            case 6:
                displayPage =
                    <div>
                        <div className="pageHeading">
                            How many dependents do you have?
                        </div>
                        <div className="pageContent">
                            This includes any children or financially dependent adults over the age of 17 (Please don't include any other applicants!)
                        </div>
                        <button onClick={(e) => this.updateDetail(e,"dependents1")} className="radio" value="0">None<img style={{ visibility: this.state.dependents1 === "0" ? "visible" : "hidden" }} className="tick" src={require("../img/icon-tick.svg")}></img></button><br/>
                        <button onClick={(e) => this.updateDetail(e,"dependents1")} className="radio" value="1">1<img style={{ visibility: this.state.dependents1 === "1" ? "visible" : "hidden" }} className="tick" src={require("../img/icon-tick.svg")}></img></button><br/>
                        <button onClick={(e) => this.updateDetail(e,"dependents1")} className="radio" value="2">2<img style={{ visibility: this.state.dependents1 === "2" ? "visible" : "hidden" }} className="tick" src={require("../img/icon-tick.svg")}></img></button><br/>
                        <button onClick={(e) => this.updateDetail(e,"dependents1")} className="radio" value="3">3<img style={{ visibility: this.state.dependents1 === "3" ? "visible" : "hidden" }} className="tick" src={require("../img/icon-tick.svg")}></img></button><br/>
                        <div className="accordion" id="dependents1Accordion" style={{overflow: "hidden"}}>
                            <button onClick={(e) => this.updateDetail(e,"dependents1")} className="radio" value="4">4<img style={{ visibility: this.state.dependents1 === "4" ? "visible" : "hidden" }} className="tick" src={require("../img/icon-tick.svg")}></img></button><br/>
                            <button onClick={(e) => this.updateDetail(e,"dependents1")} className="radio" value="5">5<img style={{ visibility: this.state.dependents1 === "5" ? "visible" : "hidden" }} className="tick" src={require("../img/icon-tick.svg")}></img></button><br/>
                            <button onClick={(e) => this.updateDetail(e,"dependents1")} className="radio" value="6">6<img style={{ visibility: this.state.dependents1 === "6" ? "visible" : "hidden" }} className="tick" src={require("../img/icon-tick.svg")}></img></button><br/>
                            <button onClick={(e) => this.updateDetail(e,"dependents1")} className="radio" value="7">7<img style={{ visibility: this.state.dependents1 === "7" ? "visible" : "hidden" }} className="tick" src={require("../img/icon-tick.svg")}></img></button><br/>
                            <button onClick={(e) => this.updateDetail(e,"dependents1")} className="radio" value="8">8<img style={{ visibility: this.state.dependents1 === "8" ? "visible" : "hidden" }} className="tick" src={require("../img/icon-tick.svg")}></img></button><br/>
                        </div>
                        <button id="moreDependents1" className="radio" value="dependents1Accordion" onClick={this.accordion}>More options</button>
                    </div>
                break;

            case 7:
                displayPage =
                    <div>
                        <div className="pageHeading">
                            What's your current employment status?
                        </div>
                        <div className="pageContent">
                            This will have an impact on the other financial details we'll need.
                        </div>
                        <button onClick={(e) => this.updateDetail(e,"employment1")} className="radio" value="EMPLOYED">Employed<img style={{ visibility: this.state.employment1 === "EMPLOYED" ? "visible" : "hidden" }} className="tick" src={require("../img/icon-tick.svg")}></img></button><br/>
                        <button onClick={(e) => this.updateDetail(e,"employment1")} className="radio" value="SELF_EMPLOYED">Self employed<img style={{ visibility: this.state.employment1 === "SELF_EMPLOYED" ? "visible" : "hidden" }} className="tick" src={require("../img/icon-tick.svg")}></img></button><br/>
                        <button onClick={(e) => this.updateDetail(e,"employment1")} className="radio" value="COMPANY_DIRECTOR">Company director<img style={{ visibility: this.state.employment1 === "COMPANY_DIRECTOR" ? "visible" : "hidden" }} className="tick" src={require("../img/icon-tick.svg")}></img></button><br/>
                        <button onClick={(e) => this.updateDetail(e,"employment1")} className="radio" value="CONTRACTOR">Contractor<img style={{ visibility: this.state.employment1 === "CONTRACTOR" ? "visible" : "hidden" }} className="tick" src={require("../img/icon-tick.svg")}></img></button>
                    </div>
                break;

            case 8:
                displayPage =
                    <div>
                        <div className="pageHeading">
                            How are you employed?
                        </div>
                        <div className="pageContent">
                            This helps lenders understand what they can lend you.
                        </div>
                        <button onClick={(e) => this.updateDetail(e,"employmentType1")} className="radio" value="PERMANENT_CONTRACT">Permanent contract<img style={{ visibility: this.state.employmentType1 === "PERMANENT_CONTRACT" ? "visible" : "hidden" }} className="tick" src={require("../img/icon-tick.svg")}></img></button><br/>
                        <button onClick={(e) => this.updateDetail(e,"employmentType1")} className="radio" value="FIXED_TERM_CONTRACT">Fixed term contract<img style={{ visibility: this.state.employmentType1 === "FIXED_TERM_CONTRACT" ? "visible" : "hidden" }} className="tick" src={require("../img/icon-tick.svg")}></img></button><br/>
                        <button onClick={(e) => this.updateDetail(e,"employmentType1")} className="radio" value="SUBCONTRACTOR_FIXED_TERM">Subcontractor (fixed term)<img style={{ visibility: this.state.employmentType1 === "SUBCONTRACTOR_FIXED_TERM" ? "visible" : "hidden" }} className="tick" src={require("../img/icon-tick.svg")}></img></button><br/>
                        <button onClick={(e) => this.updateDetail(e,"employmentType1")} className="radio" value="SUBCONTRACTOR_OPEN_ENDED">Subcontractor (open ended)<img style={{ visibility: this.state.employmentType1 === "SUBCONTRACTOR_OPEN_ENDED" ? "visible" : "hidden" }} className="tick" src={require("../img/icon-tick.svg")}></img></button><br/>
                        <button onClick={(e) => this.updateDetail(e,"employmentType1")} className="radio" value="TEMPORARY_CONTRACT">Temporary contract<img style={{ visibility: this.state.employmentType1 === "TEMPORARY_CONTRACT" ? "visible" : "hidden" }} className="tick" src={require("../img/icon-tick.svg")}></img></button>
                    </div>
                break;

            case 9:
                displayPage =
                    <div>
                        <div className="pageHeading">
                            What is your annual income?
                        </div>
                        <div className="pageContent">
                            This is before tax, and excluding any bonuses, commission or overtime.
                        </div>
                        <span className="input-label-pound input-label">£</span>
                        <input type="text" placeholder="" value={this.state.annualIncome1 !== null ? this.state.annualIncome1.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') : null} className="leaseInput priceInput" onChange={(e) => this.manualCalc(e, "annualIncome1")}></input>
                        <br/>
                        <div className="calculator">
                            <button className="calc-btn" value="1" onClick={(e) => this.calc(e, "annualIncome1")}>1</button>
                            <button className="calc-btn" value="2" onClick={(e) => this.calc(e, "annualIncome1")}>2</button>
                            <button className="calc-btn" value="3" onClick={(e) => this.calc(e, "annualIncome1")}>3</button>
                            <button className="calc-btn" value="4" onClick={(e) => this.calc(e, "annualIncome1")}>4</button>
                            <button className="calc-btn" value="5" onClick={(e) => this.calc(e, "annualIncome1")}>5</button>
                            <button className="calc-btn" value="6" onClick={(e) => this.calc(e, "annualIncome1")}>6</button>
                            <button className="calc-btn" value="7" onClick={(e) => this.calc(e, "annualIncome1")}>7</button>
                            <button className="calc-btn" value="8" onClick={(e) => this.calc(e, "annualIncome1")}>8</button>
                            <button className="calc-btn" value="9" onClick={(e) => this.calc(e, "annualIncome1")}>9</button>
                            <button disabled="disabled" className="calc-btn dot-calc-btn" value="." onClick={(e) => this.calc(e, "annualIncome1")}>.</button>
                            <button className="calc-btn" value="0" onClick={(e) => this.calc(e, "annualIncome1")}>0</button>
                            <button className="calc-btn" value="backspace" onClick={(e) => this.calc(e, "annualIncome1")}><img value="backspace" src={require("../img/icon-keypad-delete.svg")}></img></button>
                        </div>
                        <br/>
                        <button onClick={this.nextPage} clasName="capBtn">Next</button>
                    </div>
                break;

            case 10:
                displayPage =
                    <div>
                        <div className="pageHeading">
                            How are you self employed?
                        </div>
                        <button onClick={(e) => this.updateDetail(e,"employmentType1")} className="radio" value="AS_A_SOLE_TRADER">As a sole trader<img style={{ visibility: this.state.employmentType1 === "AS_A_SOLE_TRADER" ? "visible" : "hidden" }} className="tick" src={require("../img/icon-tick.svg")}></img></button><br/>
                        <button onClick={(e) => this.updateDetail(e,"employmentType1")} className="radio" value="IN_A_PARTNERSHIP">In a partnership<img style={{ visibility: this.state.employmentType1 === "IN_A_PARTNERSHIP" ? "visible" : "hidden" }} className="tick" src={require("../img/icon-tick.svg")}></img></button>
                    </div>
                break;

            case 11:
                displayPage =
                    <div>
                        <div className="pageHeading">
                            How many years have you been in business?
                        </div>
                        <input type="text" value={this.state.yearsInBusiness} className="leaseInput" onChange={(e) => this.manualCalc(e, "yearsInBusiness")}></input>
						<span className="input-label">years</span><br />
                        <br/>
                        <div className="calculator">
                            <button className="calc-btn" value="1" onClick={(e) => this.calc(e, "yearsInBusiness")}>1</button>
                            <button className="calc-btn" value="2" onClick={(e) => this.calc(e, "yearsInBusiness")}>2</button>
                            <button className="calc-btn" value="3" onClick={(e) => this.calc(e, "yearsInBusiness")}>3</button>
                            <button className="calc-btn" value="4" onClick={(e) => this.calc(e, "yearsInBusiness")}>4</button>
                            <button className="calc-btn" value="5" onClick={(e) => this.calc(e, "yearsInBusiness")}>5</button>
                            <button className="calc-btn" value="6" onClick={(e) => this.calc(e, "yearsInBusiness")}>6</button>
                            <button className="calc-btn" value="7" onClick={(e) => this.calc(e, "yearsInBusiness")}>7</button>
                            <button className="calc-btn" value="8" onClick={(e) => this.calc(e, "yearsInBusiness")}>8</button>
                            <button className="calc-btn" value="9" onClick={(e) => this.calc(e, "yearsInBusiness")}>9</button>
                            <button disabled="disabled" className="calc-btn dot-calc-btn" value="." onClick={(e) => this.calc(e, "yearsInBusiness")}>.</button>
                            <button className="calc-btn" value="0" onClick={(e) => this.calc(e, "yearsInBusiness")}>0</button>
                            <button className="calc-btn" value="backspace" onClick={(e) => this.calc(e, "yearsInBusiness")}><img value="backspace" src={require("../img/icon-keypad-delete.svg")}></img></button>
                        </div>
                        <br/>
                        <button onClick={this.nextPage} clasName="capBtn">Next</button>
                    </div>
                break;

            case 12:
                displayPage =
                    <div>
                        <div className="pageHeading">
                            What was your annual net-profit for 2019-2020?
                        </div>
                        <div className="pageContent">
                            This is any after all business expenses before tax, for the 2019-2020 tax year.
                        </div>
                        <span className="input-label-pound input-label">£</span>
                        <input type="text" placeholder="" value={this.state.netProfit1920 !== null ? this.state.netProfit1920.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') : ""} className="leaseInput priceInput" onChange={(e) => this.manualCalc(e, "netProfit1920")}></input>
                        <br/>
                        <div className="calculator">
                            <button className="calc-btn" value="1" onClick={(e) => this.calc(e, "netProfit1920")}>1</button>
                            <button className="calc-btn" value="2" onClick={(e) => this.calc(e, "netProfit1920")}>2</button>
                            <button className="calc-btn" value="3" onClick={(e) => this.calc(e, "netProfit1920")}>3</button>
                            <button className="calc-btn" value="4" onClick={(e) => this.calc(e, "netProfit1920")}>4</button>
                            <button className="calc-btn" value="5" onClick={(e) => this.calc(e, "netProfit1920")}>5</button>
                            <button className="calc-btn" value="6" onClick={(e) => this.calc(e, "netProfit1920")}>6</button>
                            <button className="calc-btn" value="7" onClick={(e) => this.calc(e, "netProfit1920")}>7</button>
                            <button className="calc-btn" value="8" onClick={(e) => this.calc(e, "netProfit1920")}>8</button>
                            <button className="calc-btn" value="9" onClick={(e) => this.calc(e, "netProfit1920")}>9</button>
                            <button disabled="disabled" className="calc-btn dot-calc-btn" value="." onClick={(e) => this.calc(e, "netProfit1920")}>.</button>
                            <button className="calc-btn" value="0" onClick={(e) => this.calc(e, "netProfit1920")}>0</button>
                            <button className="calc-btn" value="backspace" onClick={(e) => this.calc(e, "netProfit1920")}><img value="backspace" src={require("../img/icon-keypad-delete.svg")}></img></button>
                        </div>
                        <br/>
                        <button onClick={this.nextPage} clasName="capBtn">Next</button>
                    </div>
                break;

            case 13:
                displayPage =
                    <div>
                        <div className="pageHeading">
                            What was your annual net-profit for 2018-2019?
                        </div>
                        <div className="pageContent">
                            This is any after all business expenses before tax, for the 2018-2019 tax year.
                        </div>
                        <span className="input-label-pound input-label">£</span>
                        <input type="text" placeholder="" value={this.state.netProfit1819 !== null ? this.state.netProfit1819.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') : ""} className="leaseInput priceInput" onChange={(e) => this.manualCalc(e, "netProfit1819")}></input>
                        <br/>
                        <div className="calculator">
                            <button className="calc-btn" value="1" onClick={(e) => this.calc(e, "netProfit1819")}>1</button>
                            <button className="calc-btn" value="2" onClick={(e) => this.calc(e, "netProfit1819")}>2</button>
                            <button className="calc-btn" value="3" onClick={(e) => this.calc(e, "netProfit1819")}>3</button>
                            <button className="calc-btn" value="4" onClick={(e) => this.calc(e, "netProfit1819")}>4</button>
                            <button className="calc-btn" value="5" onClick={(e) => this.calc(e, "netProfit1819")}>5</button>
                            <button className="calc-btn" value="6" onClick={(e) => this.calc(e, "netProfit1819")}>6</button>
                            <button className="calc-btn" value="7" onClick={(e) => this.calc(e, "netProfit1819")}>7</button>
                            <button className="calc-btn" value="8" onClick={(e) => this.calc(e, "netProfit1819")}>8</button>
                            <button className="calc-btn" value="9" onClick={(e) => this.calc(e, "netProfit1819")}>9</button>
                            <button disabled="disabled" className="calc-btn dot-calc-btn" value="." onClick={(e) => this.calc(e, "netProfit1819")}>.</button>
                            <button className="calc-btn" value="0" onClick={(e) => this.calc(e, "netProfit1819")}>0</button>
                            <button className="calc-btn" value="backspace" onClick={(e) => this.calc(e, "netProfit1819")}><img value="backspace" src={require("../img/icon-keypad-delete.svg")}></img></button>
                        </div>
                        <br/>
                        <button onClick={this.nextPage} clasName="capBtn">Next</button>
                    </div>
                break;

            case 14:
                displayPage =
                    <div>
                        <div className="pageHeading">
                            What's your shareholding of the company?
                        </div>
                        <div className="pageContent">
                            This helps lenders understand what they can offer to lend you.
                        </div>
                        <button onClick={(e) => this.updateDetail(e,"employmentType1")} className="radio" value="LESS_THAN_20%">Less than 20%<img style={{ visibility: this.state.employmentType1 === "LESS_THAN_20%" ? "visible" : "hidden" }} className="tick" src={require("../img/icon-tick.svg")}></img></button><br/>
                        <button onClick={(e) => this.updateDetail(e,"employmentType1")} className="radio" value="MORE_THAN_20%">More than 20%<img style={{ visibility: this.state.employmentType1 === "MORE_THAN_20%" ? "visible" : "hidden" }} className="tick" src={require("../img/icon-tick.svg")}></img></button>
                    </div>
                break;

            case 15:
                displayPage =
                    <div>
                        <div className="pageHeading">
                            How many years have you been in business?
                        </div>
                        <input type="text" value={this.state.yearsInBusiness} className="leaseInput" onChange={(e) => this.manualCalc(e, "yearsInBusiness")}></input>
						<span className="input-label">years</span><br />
                        <br/>
                        <div className="calculator">
                            <button className="calc-btn" value="1" onClick={(e) => this.calc(e, "yearsInBusiness")}>1</button>
                            <button className="calc-btn" value="2" onClick={(e) => this.calc(e, "yearsInBusiness")}>2</button>
                            <button className="calc-btn" value="3" onClick={(e) => this.calc(e, "yearsInBusiness")}>3</button>
                            <button className="calc-btn" value="4" onClick={(e) => this.calc(e, "yearsInBusiness")}>4</button>
                            <button className="calc-btn" value="5" onClick={(e) => this.calc(e, "yearsInBusiness")}>5</button>
                            <button className="calc-btn" value="6" onClick={(e) => this.calc(e, "yearsInBusiness")}>6</button>
                            <button className="calc-btn" value="7" onClick={(e) => this.calc(e, "yearsInBusiness")}>7</button>
                            <button className="calc-btn" value="8" onClick={(e) => this.calc(e, "yearsInBusiness")}>8</button>
                            <button className="calc-btn" value="9" onClick={(e) => this.calc(e, "yearsInBusiness")}>9</button>
                            <button disabled="disabled" className="calc-btn dot-calc-btn" value="." onClick={(e) => this.calc(e, "yearsInBusiness")}>.</button>
                            <button className="calc-btn" value="0" onClick={(e) => this.calc(e, "yearsInBusiness")}>0</button>
                            <button className="calc-btn" value="backspace" onClick={(e) => this.calc(e, "yearsInBusiness")}><img value="backspace" src={require("../img/icon-keypad-delete.svg")}></img></button>
                        </div>
                        <br/>
                        <button onClick={this.nextPage} clasName="capBtn">Next</button>
                    </div>
                break;

            case 16:
                displayPage =
                    <div>
                        <div className="pageHeading">
                            What was your annual remuneration for 2019-2020?
                        </div>
                        <div className="pageContent">
                            This is before tax, and excluding any bonuses, commission or overtime, for the 2019-2020 tax year.
                        </div>
                        <span className="input-label-pound input-label">£</span>
                        <input type="text" placeholder="" value={this.state.remuneration1920 !== null ? this.state.remuneration1920.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') : ""} className="leaseInput priceInput" onChange={(e) => this.manualCalc(e, "remuneration1920")}></input>
                        <br/>
                        <div className="calculator">
                            <button className="calc-btn" value="1" onClick={(e) => this.calc(e, "remuneration1920")}>1</button>
                            <button className="calc-btn" value="2" onClick={(e) => this.calc(e, "remuneration1920")}>2</button>
                            <button className="calc-btn" value="3" onClick={(e) => this.calc(e, "remuneration1920")}>3</button>
                            <button className="calc-btn" value="4" onClick={(e) => this.calc(e, "remuneration1920")}>4</button>
                            <button className="calc-btn" value="5" onClick={(e) => this.calc(e, "remuneration1920")}>5</button>
                            <button className="calc-btn" value="6" onClick={(e) => this.calc(e, "remuneration1920")}>6</button>
                            <button className="calc-btn" value="7" onClick={(e) => this.calc(e, "remuneration1920")}>7</button>
                            <button className="calc-btn" value="8" onClick={(e) => this.calc(e, "remuneration1920")}>8</button>
                            <button className="calc-btn" value="9" onClick={(e) => this.calc(e, "remuneration1920")}>9</button>
                            <button disabled="disabled" className="calc-btn dot-calc-btn" value="." onClick={(e) => this.calc(e, "remuneration1920")}>.</button>
                            <button className="calc-btn" value="0" onClick={(e) => this.calc(e, "remuneration1920")}>0</button>
                            <button className="calc-btn" value="backspace" onClick={(e) => this.calc(e, "remuneration1920")}><img value="backspace" src={require("../img/icon-keypad-delete.svg")}></img></button>
                        </div>
                        <br/>
                        <button onClick={this.nextPage} clasName="capBtn">Next</button>
                    </div>
                break;

            case 17:
                displayPage =
                    <div>
                        <div className="pageHeading">
                            What was your annual remuneration for 2018-2019?
                        </div>
                        <div className="pageContent">
                            This is before tax, and excluding any bonuses, commission or overtime, for the 2018-2019 tax year.
                        </div>
                        <span className="input-label-pound input-label">£</span>
                        <input type="text" placeholder="" value={this.state.remuneration1819 !== null ? this.state.remuneration1819.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') : ""} className="leaseInput priceInput" onChange={(e) => this.manualCalc(e, "remuneration1819")}></input>
                        <br/>
                        <div className="calculator">
                            <button className="calc-btn" value="1" onClick={(e) => this.calc(e, "remuneration1819")}>1</button>
                            <button className="calc-btn" value="2" onClick={(e) => this.calc(e, "remuneration1819")}>2</button>
                            <button className="calc-btn" value="3" onClick={(e) => this.calc(e, "remuneration1819")}>3</button>
                            <button className="calc-btn" value="4" onClick={(e) => this.calc(e, "remuneration1819")}>4</button>
                            <button className="calc-btn" value="5" onClick={(e) => this.calc(e, "remuneration1819")}>5</button>
                            <button className="calc-btn" value="6" onClick={(e) => this.calc(e, "remuneration1819")}>6</button>
                            <button className="calc-btn" value="7" onClick={(e) => this.calc(e, "remuneration1819")}>7</button>
                            <button className="calc-btn" value="8" onClick={(e) => this.calc(e, "remuneration1819")}>8</button>
                            <button className="calc-btn" value="9" onClick={(e) => this.calc(e, "remuneration1819")}>9</button>
                            <button disabled="disabled" className="calc-btn dot-calc-btn" value="." onClick={(e) => this.calc(e, "remuneration1819")}>.</button>
                            <button className="calc-btn" value="0" onClick={(e) => this.calc(e, "remuneration1819")}>0</button>
                            <button className="calc-btn" value="backspace" onClick={(e) => this.calc(e, "remuneration1819")}><img value="backspace" src={require("../img/icon-keypad-delete.svg")}></img></button>
                        </div>
                        <br/>
                        <button onClick={this.nextPage} clasName="capBtn">Next</button>
                    </div>
                break;

            case 18:
                displayPage =
                    <div>
                        <div className="pageHeading">
                            What was your operating profit for 2019-2020?
                        </div>
                        <div className="pageContent">
                            This is any after business expenses before tax, for the 2019-2020 tax year.
                        </div>
                        <span className="input-label-pound input-label">£</span>
                        <input type="text" placeholder="" value={this.state.opProfit1920 !== null ? this.state.opProfit1920.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') : ""} className="leaseInput priceInput" onChange={(e) => this.manualCalc(e, "opProfit1920")}></input>
                        <br/>
                        <div className="calculator">
                            <button className="calc-btn" value="1" onClick={(e) => this.calc(e, "opProfit1920")}>1</button>
                            <button className="calc-btn" value="2" onClick={(e) => this.calc(e, "opProfit1920")}>2</button>
                            <button className="calc-btn" value="3" onClick={(e) => this.calc(e, "opProfit1920")}>3</button>
                            <button className="calc-btn" value="4" onClick={(e) => this.calc(e, "opProfit1920")}>4</button>
                            <button className="calc-btn" value="5" onClick={(e) => this.calc(e, "opProfit1920")}>5</button>
                            <button className="calc-btn" value="6" onClick={(e) => this.calc(e, "opProfit1920")}>6</button>
                            <button className="calc-btn" value="7" onClick={(e) => this.calc(e, "opProfit1920")}>7</button>
                            <button className="calc-btn" value="8" onClick={(e) => this.calc(e, "opProfit1920")}>8</button>
                            <button className="calc-btn" value="9" onClick={(e) => this.calc(e, "opProfit1920")}>9</button>
                            <button disabled="disabled" className="calc-btn dot-calc-btn" value="." onClick={(e) => this.calc(e, "opProfit1920")}>.</button>
                            <button className="calc-btn" value="0" onClick={(e) => this.calc(e, "opProfit1920")}>0</button>
                            <button className="calc-btn" value="backspace" onClick={(e) => this.calc(e, "opProfit1920")}><img value="backspace" src={require("../img/icon-keypad-delete.svg")}></img></button>
                        </div>
                        <br/>
                        <button onClick={this.nextPage} clasName="capBtn">Next</button>
                    </div>
                break;

            case 19:
                displayPage =
                    <div>
                        <div className="pageHeading">
                            What was your operating profit for 2018-2019?
                        </div>
                        <div className="pageContent">
                            This is any after business expenses before tax, for the 2018-2019 tax year.
                        </div>
                        <span className="input-label-pound input-label">£</span>
                        <input type="text" placeholder="" value={this.state.opProfit1819 !== null ? this.state.opProfit1819.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') : ""} className="leaseInput priceInput" onChange={(e) => this.manualCalc(e, "opProfit1819")}></input>
                        <br/>
                        <div className="calculator">
                            <button className="calc-btn" value="1" onClick={(e) => this.calc(e, "opProfit1819")}>1</button>
                            <button className="calc-btn" value="2" onClick={(e) => this.calc(e, "opProfit1819")}>2</button>
                            <button className="calc-btn" value="3" onClick={(e) => this.calc(e, "opProfit1819")}>3</button>
                            <button className="calc-btn" value="4" onClick={(e) => this.calc(e, "opProfit1819")}>4</button>
                            <button className="calc-btn" value="5" onClick={(e) => this.calc(e, "opProfit1819")}>5</button>
                            <button className="calc-btn" value="6" onClick={(e) => this.calc(e, "opProfit1819")}>6</button>
                            <button className="calc-btn" value="7" onClick={(e) => this.calc(e, "opProfit1819")}>7</button>
                            <button className="calc-btn" value="8" onClick={(e) => this.calc(e, "opProfit1819")}>8</button>
                            <button className="calc-btn" value="9" onClick={(e) => this.calc(e, "opProfit1819")}>9</button>
                            <button disabled="disabled" className="calc-btn dot-calc-btn" value="." onClick={(e) => this.calc(e, "opProfit1819")}>.</button>
                            <button className="calc-btn" value="0" onClick={(e) => this.calc(e, "opProfit1819")}>0</button>
                            <button className="calc-btn" value="backspace" onClick={(e) => this.calc(e, "opProfit1819")}><img value="backspace" src={require("../img/icon-keypad-delete.svg")}></img></button>
                        </div>
                        <br/>
                        <button onClick={this.nextPage} clasName="capBtn">Next</button>
                    </div>
                break;

            case 20:
                displayPage =
                    <div>
                        <div className="pageHeading">
                            What was your  before dividends for 2019-2020?
                        </div>
                        <div className="pageContent">
                            This is any after business expenses before tax, for the 2019-2020 tax year.
                        </div>
                        <span className="input-label-pound input-label">£</span>
                        <input type="text" placeholder="" value={this.state.netProfit1920 !== null ? this.state.netProfit1920.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') : ""} className="leaseInput priceInput" onChange={(e) => this.manualCalc(e, "netProfit1920")}></input>
                        <br/>
                        <div className="calculator">
                            <button className="calc-btn" value="1" onClick={(e) => this.calc(e, "netProfit1920")}>1</button>
                            <button className="calc-btn" value="2" onClick={(e) => this.calc(e, "netProfit1920")}>2</button>
                            <button className="calc-btn" value="3" onClick={(e) => this.calc(e, "netProfit1920")}>3</button>
                            <button className="calc-btn" value="4" onClick={(e) => this.calc(e, "netProfit1920")}>4</button>
                            <button className="calc-btn" value="5" onClick={(e) => this.calc(e, "netProfit1920")}>5</button>
                            <button className="calc-btn" value="6" onClick={(e) => this.calc(e, "netProfit1920")}>6</button>
                            <button className="calc-btn" value="7" onClick={(e) => this.calc(e, "netProfit1920")}>7</button>
                            <button className="calc-btn" value="8" onClick={(e) => this.calc(e, "netProfit1920")}>8</button>
                            <button className="calc-btn" value="9" onClick={(e) => this.calc(e, "netProfit1920")}>9</button>
                            <button disabled="disabled" className="calc-btn dot-calc-btn" value="." onClick={(e) => this.calc(e, "netProfit1920")}>.</button>
                            <button className="calc-btn" value="0" onClick={(e) => this.calc(e, "netProfit1920")}>0</button>
                            <button className="calc-btn" value="backspace" onClick={(e) => this.calc(e, "netProfit1920")}><img value="backspace" src={require("../img/icon-keypad-delete.svg")}></img></button>
                        </div>
                        <br/>
                        <button onClick={this.nextPage} clasName="capBtn">Next</button>
                    </div>
                break;

            case 21:
                displayPage =
                    <div>
                        <div className="pageHeading">
                            What was your  before dividends for 2018-2019?
                        </div>
                        <div className="pageContent">
                            This is any after business expenses before tax, for the 2018-2019 tax year.
                        </div>
                        <span className="input-label-pound input-label">£</span>
                        <input type="text" placeholder="" value={this.state.netProfit1819 !== null ? this.state.netProfit1819.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') : ""} className="leaseInput priceInput" onChange={(e) => this.manualCalc(e, "netProfit1819")}></input>
                        <br/>
                        <div className="calculator">
                            <button className="calc-btn" value="1" onClick={(e) => this.calc(e, "netProfit1819")}>1</button>
                            <button className="calc-btn" value="2" onClick={(e) => this.calc(e, "netProfit1819")}>2</button>
                            <button className="calc-btn" value="3" onClick={(e) => this.calc(e, "netProfit1819")}>3</button>
                            <button className="calc-btn" value="4" onClick={(e) => this.calc(e, "netProfit1819")}>4</button>
                            <button className="calc-btn" value="5" onClick={(e) => this.calc(e, "netProfit1819")}>5</button>
                            <button className="calc-btn" value="6" onClick={(e) => this.calc(e, "netProfit1819")}>6</button>
                            <button className="calc-btn" value="7" onClick={(e) => this.calc(e, "netProfit1819")}>7</button>
                            <button className="calc-btn" value="8" onClick={(e) => this.calc(e, "netProfit1819")}>8</button>
                            <button className="calc-btn" value="9" onClick={(e) => this.calc(e, "netProfit1819")}>9</button>
                            <button disabled="disabled" className="calc-btn dot-calc-btn" value="." onClick={(e) => this.calc(e, "netProfit1819")}>.</button>
                            <button className="calc-btn" value="0" onClick={(e) => this.calc(e, "netProfit1819")}>0</button>
                            <button className="calc-btn" value="backspace" onClick={(e) => this.calc(e, "netProfit1819")}><img value="backspace" src={require("../img/icon-keypad-delete.svg")}></img></button>
                        </div>
                        <br/>
                        <button onClick={this.nextPage} clasName="capBtn">Next</button>
                    </div>
                break;

            case 22:
                displayPage =
                    <div>
                        <div className="pageHeading">
                            What is your daily rate?
                        </div>
                        <div className="pageContent">
                            This helps lenders understand what they can offer to lend you.
                        </div>
                        <span className="input-label-pound input-label">£</span>
                        <input type="text" placeholder="" value={this.state.dailyRate !== null ? this.state.dailyRate.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') : ""} className="leaseInput priceInput" onChange={(e) => this.manualCalc(e, "dailyRate")}></input>
                        <br/>
                        <div className="calculator">
                            <button className="calc-btn" value="1" onClick={(e) => this.calc(e, "dailyRate")}>1</button>
                            <button className="calc-btn" value="2" onClick={(e) => this.calc(e, "dailyRate")}>2</button>
                            <button className="calc-btn" value="3" onClick={(e) => this.calc(e, "dailyRate")}>3</button>
                            <button className="calc-btn" value="4" onClick={(e) => this.calc(e, "dailyRate")}>4</button>
                            <button className="calc-btn" value="5" onClick={(e) => this.calc(e, "dailyRate")}>5</button>
                            <button className="calc-btn" value="6" onClick={(e) => this.calc(e, "dailyRate")}>6</button>
                            <button className="calc-btn" value="7" onClick={(e) => this.calc(e, "dailyRate")}>7</button>
                            <button className="calc-btn" value="8" onClick={(e) => this.calc(e, "dailyRate")}>8</button>
                            <button className="calc-btn" value="9" onClick={(e) => this.calc(e, "dailyRate")}>9</button>
                            <button disabled="disabled" className="calc-btn dot-calc-btn" value="." onClick={(e) => this.calc(e, "dailyRate")}>.</button>
                            <button className="calc-btn" value="0" onClick={(e) => this.calc(e, "dailyRate")}>0</button>
                            <button className="calc-btn" value="backspace" onClick={(e) => this.calc(e, "dailyRate")}><img value="backspace" src={require("../img/icon-keypad-delete.svg")}></img></button>
                        </div>
                        <br/>
                        <button onClick={this.nextPage} clasName="capBtn">Next</button>
                    </div>
                break;

            case 23:
                displayPage =
                    <div>
                        <div className="pageHeading">
                            Are there any deductions included on your payslip?
                        </div>
                        <div className="pageContent">
                            This includes monthly childcare vouchers, student loan repayments, season ticket loan repayments, or other deductions after tax.
                        </div>
                        <span className="input-label-pound input-label">£</span>
                        <input type="text" placeholder="" value={this.state.deductionsOnPayslip !== null ? this.state.deductionsOnPayslip.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') : ""} className="leaseInput priceInput" onChange={(e) => this.manualCalc(e, "deductionsOnPayslip")}></input>
                        <br/>
                        <div className="calculator">
                            <button className="calc-btn" value="1" onClick={(e) => this.calc(e, "deductionsOnPayslip")}>1</button>
                            <button className="calc-btn" value="2" onClick={(e) => this.calc(e, "deductionsOnPayslip")}>2</button>
                            <button className="calc-btn" value="3" onClick={(e) => this.calc(e, "deductionsOnPayslip")}>3</button>
                            <button className="calc-btn" value="4" onClick={(e) => this.calc(e, "deductionsOnPayslip")}>4</button>
                            <button className="calc-btn" value="5" onClick={(e) => this.calc(e, "deductionsOnPayslip")}>5</button>
                            <button className="calc-btn" value="6" onClick={(e) => this.calc(e, "deductionsOnPayslip")}>6</button>
                            <button className="calc-btn" value="7" onClick={(e) => this.calc(e, "deductionsOnPayslip")}>7</button>
                            <button className="calc-btn" value="8" onClick={(e) => this.calc(e, "deductionsOnPayslip")}>8</button>
                            <button className="calc-btn" value="9" onClick={(e) => this.calc(e, "deductionsOnPayslip")}>9</button>
                            <button disabled="disabled" className="calc-btn dot-calc-btn" value="." onClick={(e) => this.calc(e, "deductionsOnPayslip")}>.</button>
                            <button className="calc-btn" value="0" onClick={(e) => this.calc(e, "deductionsOnPayslip")}>0</button>
                            <button className="calc-btn" value="backspace" onClick={(e) => this.calc(e, "deductionsOnPayslip")}><img value="backspace" src={require("../img/icon-keypad-delete.svg")}></img></button>
                        </div>
                        <br/>
                        <button onClick={this.nextPage} clasName="capBtn">Next</button>
                    </div>
                break;

            case 24:
                displayPage =
                    <div>
                        <div className="pageHeading">
                            Do you have any additional income?
                        </div>
                        <div className="pageContent">
                            This includes car allowances, a pension, benefits, or from rent or a second job.
                        </div>
                        <span className="input-label-pound input-label">£</span>
                        <input type="text" placeholder="" value={this.state.additionalIncome !== null ? this.state.additionalIncome.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') : ""} className="leaseInput priceInput" onChange={(e) => this.manualCalc(e, "additionalIncome")}></input>
                        <br/>
                        <div className="calculator">
                            <button className="calc-btn" value="1" onClick={(e) => this.calc(e, "additionalIncome")}>1</button>
                            <button className="calc-btn" value="2" onClick={(e) => this.calc(e, "additionalIncome")}>2</button>
                            <button className="calc-btn" value="3" onClick={(e) => this.calc(e, "additionalIncome")}>3</button>
                            <button className="calc-btn" value="4" onClick={(e) => this.calc(e, "additionalIncome")}>4</button>
                            <button className="calc-btn" value="5" onClick={(e) => this.calc(e, "additionalIncome")}>5</button>
                            <button className="calc-btn" value="6" onClick={(e) => this.calc(e, "additionalIncome")}>6</button>
                            <button className="calc-btn" value="7" onClick={(e) => this.calc(e, "additionalIncome")}>7</button>
                            <button className="calc-btn" value="8" onClick={(e) => this.calc(e, "additionalIncome")}>8</button>
                            <button className="calc-btn" value="9" onClick={(e) => this.calc(e, "additionalIncome")}>9</button>
                            <button disabled="disabled" className="calc-btn dot-calc-btn" value="." onClick={(e) => this.calc(e, "additionalIncome")}>.</button>
                            <button className="calc-btn" value="0" onClick={(e) => this.calc(e, "additionalIncome")}>0</button>
                            <button className="calc-btn" value="backspace" onClick={(e) => this.calc(e, "additionalIncome")}><img value="backspace" src={require("../img/icon-keypad-delete.svg")}></img></button>
                        </div>
                        <br/>
                        <button onClick={this.nextPage} clasName="capBtn">Next</button>
                    </div>
                break;

            case 25:
                displayPage =
                    <div>
                        <div className="pageHeading">
                            Do you have any monthly loan repayments?
                        </div>
                        <div className="pageContent">
                            This should include any regular loans or pension payments.
                        </div>
                        <span className="input-label-pound input-label">£</span>
                        <input type="text" placeholder="" value={this.state.monthlyLoanRepayments !== null ? this.state.monthlyLoanRepayments.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') : ""} className="leaseInput priceInput" onChange={(e) => this.manualCalc(e, "monthlyLoanRepayments")}></input>
                        <br/>
                        <div className="calculator">
                            <button className="calc-btn" value="1" onClick={(e) => this.calc(e, "monthlyLoanRepayments")}>1</button>
                            <button className="calc-btn" value="2" onClick={(e) => this.calc(e, "monthlyLoanRepayments")}>2</button>
                            <button className="calc-btn" value="3" onClick={(e) => this.calc(e, "monthlyLoanRepayments")}>3</button>
                            <button className="calc-btn" value="4" onClick={(e) => this.calc(e, "monthlyLoanRepayments")}>4</button>
                            <button className="calc-btn" value="5" onClick={(e) => this.calc(e, "monthlyLoanRepayments")}>5</button>
                            <button className="calc-btn" value="6" onClick={(e) => this.calc(e, "monthlyLoanRepayments")}>6</button>
                            <button className="calc-btn" value="7" onClick={(e) => this.calc(e, "monthlyLoanRepayments")}>7</button>
                            <button className="calc-btn" value="8" onClick={(e) => this.calc(e, "monthlyLoanRepayments")}>8</button>
                            <button className="calc-btn" value="9" onClick={(e) => this.calc(e, "monthlyLoanRepayments")}>9</button>
                            <button disabled="disabled" className="calc-btn dot-calc-btn" value="." onClick={(e) => this.calc(e, "monthlyLoanRepayments")}>.</button>
                            <button className="calc-btn" value="0" onClick={(e) => this.calc(e, "monthlyLoanRepayments")}>0</button>
                            <button className="calc-btn" value="backspace" onClick={(e) => this.calc(e, "monthlyLoanRepayments")}><img value="backspace" src={require("../img/icon-keypad-delete.svg")}></img></button>
                        </div>
                        <br/>
                        <button onClick={this.nextPage} clasName="capBtn">Next</button>
                    </div>
                break;

            case 26:
                displayPage =
                    <div>
                        <div className="pageHeading">
                            How much will your monthly loan payments be reduced?
                        </div>
                        <div className="pageContent">
                            This should include any regular loans or pension payments.
                        </div>
                        <span className="input-label-pound input-label">£</span>
                        <input type="text" placeholder="" value={this.state.monthlyLoanPaymentReduction !== null ? this.state.monthlyLoanPaymentReduction.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') : ""} className="leaseInput priceInput" onChange={(e) => this.manualCalc(e, "monthlyLoanPaymentReduction")}></input>
                        <br/>
                        <div className="calculator">
                            <button className="calc-btn" value="1" onClick={(e) => this.calc(e, "monthlyLoanPaymentReduction")}>1</button>
                            <button className="calc-btn" value="2" onClick={(e) => this.calc(e, "monthlyLoanPaymentReduction")}>2</button>
                            <button className="calc-btn" value="3" onClick={(e) => this.calc(e, "monthlyLoanPaymentReduction")}>3</button>
                            <button className="calc-btn" value="4" onClick={(e) => this.calc(e, "monthlyLoanPaymentReduction")}>4</button>
                            <button className="calc-btn" value="5" onClick={(e) => this.calc(e, "monthlyLoanPaymentReduction")}>5</button>
                            <button className="calc-btn" value="6" onClick={(e) => this.calc(e, "monthlyLoanPaymentReduction")}>6</button>
                            <button className="calc-btn" value="7" onClick={(e) => this.calc(e, "monthlyLoanPaymentReduction")}>7</button>
                            <button className="calc-btn" value="8" onClick={(e) => this.calc(e, "monthlyLoanPaymentReduction")}>8</button>
                            <button className="calc-btn" value="9" onClick={(e) => this.calc(e, "monthlyLoanPaymentReduction")}>9</button>
                            <button disabled="disabled" className="calc-btn dot-calc-btn" value="." onClick={(e) => this.calc(e, "monthlyLoanPaymentReduction")}>.</button>
                            <button className="calc-btn" value="0" onClick={(e) => this.calc(e, "monthlyLoanPaymentReduction")}>0</button>
                            <button className="calc-btn" value="backspace" onClick={(e) => this.calc(e, "monthlyLoanPaymentReduction")}><img value="backspace" src={require("../img/icon-keypad-delete.svg")}></img></button>
                        </div>
                        <br/>
                        <button onClick={this.nextPage} clasName="capBtn">Next</button>
                    </div>
                break;

            case 27:
                displayPage =
                    <div>
                        <div className="pageHeading">
                            What will the remaining balance be on your loans?
                        </div>
                        <div className="pageContent">
                            This is the outstanding balance when your mortgage begins.
                        </div>
                        <span className="input-label-pound input-label">£</span>
                        <input type="text" placeholder="" value={this.state.loanRemainingBalance !== null ? this.state.loanRemainingBalance.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') : ""} className="leaseInput priceInput" onChange={(e) => this.manualCalc(e, "loanRemainingBalance")}></input>
                        <br/>
                        <div className="calculator">
                            <button className="calc-btn" value="1" onClick={(e) => this.calc(e, "loanRemainingBalance")}>1</button>
                            <button className="calc-btn" value="2" onClick={(e) => this.calc(e, "loanRemainingBalance")}>2</button>
                            <button className="calc-btn" value="3" onClick={(e) => this.calc(e, "loanRemainingBalance")}>3</button>
                            <button className="calc-btn" value="4" onClick={(e) => this.calc(e, "loanRemainingBalance")}>4</button>
                            <button className="calc-btn" value="5" onClick={(e) => this.calc(e, "loanRemainingBalance")}>5</button>
                            <button className="calc-btn" value="6" onClick={(e) => this.calc(e, "loanRemainingBalance")}>6</button>
                            <button className="calc-btn" value="7" onClick={(e) => this.calc(e, "loanRemainingBalance")}>7</button>
                            <button className="calc-btn" value="8" onClick={(e) => this.calc(e, "loanRemainingBalance")}>8</button>
                            <button className="calc-btn" value="9" onClick={(e) => this.calc(e, "loanRemainingBalance")}>9</button>
                            <button disabled="disabled" className="calc-btn dot-calc-btn" value="." onClick={(e) => this.calc(e, "loanRemainingBalance")}>.</button>
                            <button className="calc-btn" value="0" onClick={(e) => this.calc(e, "loanRemainingBalance")}>0</button>
                            <button className="calc-btn" value="backspace" onClick={(e) => this.calc(e, "loanRemainingBalance")}><img value="backspace" src={require("../img/icon-keypad-delete.svg")}></img></button>
                        </div>
                        <br/>
                        <button onClick={this.nextPage} clasName="capBtn">Next</button>
                    </div>
                break;

            case 28:
                displayPage =
                    <div>
                        <div className="pageHeading">
                            What's your current credit card balance?
                        </div>
                        <div className="pageContent">
                            How much credit card debt will you have when your new mortgage starts?
                        </div>
                        <span className="input-label-pound input-label">£</span>
                        <input type="text" placeholder="" value={this.state.creditBalance !== null ? this.state.creditBalance.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') : ""} className="leaseInput priceInput" onChange={(e) => this.manualCalc(e, "creditBalance")}></input>
                        <br/>
                        <div className="calculator">
                            <button className="calc-btn" value="1" onClick={(e) => this.calc(e, "creditBalance")}>1</button>
                            <button className="calc-btn" value="2" onClick={(e) => this.calc(e, "creditBalance")}>2</button>
                            <button className="calc-btn" value="3" onClick={(e) => this.calc(e, "creditBalance")}>3</button>
                            <button className="calc-btn" value="4" onClick={(e) => this.calc(e, "creditBalance")}>4</button>
                            <button className="calc-btn" value="5" onClick={(e) => this.calc(e, "creditBalance")}>5</button>
                            <button className="calc-btn" value="6" onClick={(e) => this.calc(e, "creditBalance")}>6</button>
                            <button className="calc-btn" value="7" onClick={(e) => this.calc(e, "creditBalance")}>7</button>
                            <button className="calc-btn" value="8" onClick={(e) => this.calc(e, "creditBalance")}>8</button>
                            <button className="calc-btn" value="9" onClick={(e) => this.calc(e, "creditBalance")}>9</button>
                            <button disabled="disabled" className="calc-btn dot-calc-btn" value="." onClick={(e) => this.calc(e, "creditBalance")}>.</button>
                            <button className="calc-btn" value="0" onClick={(e) => this.calc(e, "creditBalance")}>0</button>
                            <button className="calc-btn" value="backspace" onClick={(e) => this.calc(e, "creditBalance")}><img value="backspace" src={require("../img/icon-keypad-delete.svg")}></img></button>
                        </div>
                        <br/>
                        <button onClick={this.nextPage} clasName="capBtn">Next</button>
                    </div>
                break;

            case 29:
                displayPage =
                    <div>
                        <div className="pageHeading">
                            How much of your credit card balance will be repaid?
                        </div>
                        <div className="pageContent">
                            Reducing any outstanding balance before your mortgage begins will improve what you're able to borrow.
                        </div>
                        <span className="input-label-pound input-label">£</span>
                        <input type="text" placeholder="" value={this.state.creditBalanceRepaid !== null ? this.state.creditBalanceRepaid.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') : ""} className="leaseInput priceInput" onChange={(e) => this.manualCalc(e, "creditBalanceRepaid")}></input>
                        <br/>
                        <div className="calculator">
                            <button className="calc-btn" value="1" onClick={(e) => this.calc(e, "creditBalanceRepaid")}>1</button>
                            <button className="calc-btn" value="2" onClick={(e) => this.calc(e, "creditBalanceRepaid")}>2</button>
                            <button className="calc-btn" value="3" onClick={(e) => this.calc(e, "creditBalanceRepaid")}>3</button>
                            <button className="calc-btn" value="4" onClick={(e) => this.calc(e, "creditBalanceRepaid")}>4</button>
                            <button className="calc-btn" value="5" onClick={(e) => this.calc(e, "creditBalanceRepaid")}>5</button>
                            <button className="calc-btn" value="6" onClick={(e) => this.calc(e, "creditBalanceRepaid")}>6</button>
                            <button className="calc-btn" value="7" onClick={(e) => this.calc(e, "creditBalanceRepaid")}>7</button>
                            <button className="calc-btn" value="8" onClick={(e) => this.calc(e, "creditBalanceRepaid")}>8</button>
                            <button className="calc-btn" value="9" onClick={(e) => this.calc(e, "creditBalanceRepaid")}>9</button>
                            <button disabled="disabled" className="calc-btn dot-calc-btn" value="." onClick={(e) => this.calc(e, "creditBalanceRepaid")}>.</button>
                            <button className="calc-btn" value="0" onClick={(e) => this.calc(e, "creditBalanceRepaid")}>0</button>
                            <button className="calc-btn" value="backspace" onClick={(e) => this.calc(e, "creditBalanceRepaid")}><img value="backspace" src={require("../img/icon-keypad-delete.svg")}></img></button>
                        </div>
                        <br/>
                        <button onClick={this.nextPage} clasName="capBtn">Next</button>
                    </div>
                break;

            case 30:
                displayPage =
                    <div>
                        <div className="pageHeading">
                            Do you have any adverse credit?
                        </div>
                        <div className="pageContent">
                            This refers to any negative payment information that would appear on your credit rating (such as defaulted loans, bankruptcy, missed payments and CCJ's, or mortgage repossession).
                        </div>
                        <button onClick={(e) => this.updateDetail(e,"adverseCredit")} className="radio" value="YES">Yes<img style={{ visibility: this.state.adverseCredit === "YES" ? "visible" : "hidden" }} className="tick" src={require("../img/icon-tick.svg")}></img></button><br/>
                        <button onClick={(e) => this.updateDetail(e,"adverseCredit")} className="radio" value="NO">No<img style={{ visibility: this.state.adverseCredit === "NO" ? "visible" : "hidden" }} className="tick" src={require("../img/icon-tick.svg")}></img></button>
                    </div>
                break;

            case 30.5:
                displayPage =
                    <div>
                        <div className="pageHeading">
                            Having adverse credit can affect what you can borrow
                        </div>
                        <div className="pageContent">
                            Lenders will often use these as indicators to refuse certain mortgage products.
                        </div>
                        <div className="callbackBox">
                            <div className="callbackHeading">
                                We want to help
                            </div>
                            <div className="pageContent">
                                One of our advisers will give you a call to review different options over the phone, to ensure
                                we get you accurate results.
                            </div>
                            <button className="capBtn" onClick={() => this.callback("Applicant with adverse credit")} style={{margin: "0"}}>Get a callback</button>
                        </div>
                        <div className="pageContent" style={{marginTop: "20px"}}>
                            It's still important to complete the form to get some provisional results, we can help with the rest!
                            <br/><br/>
                            <span className="link" onClick={this.nextPage}>Complete the form</span>
                        </div>
                    </div>
                break;

            case 31:
                displayPage =
                    <div>
                        <div className="pageHeading">
                            What's the budget for your new home?
                        </div>
                        <div className="pageContent">
                            How much are you planning on spending on your new home?
                        </div>
                        <span className="input-label-pound input-label">£</span>
                        <input type="text" placeholder="" value={this.state.budget !== null ? this.state.budget.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') : ""} className="leaseInput priceInput" onChange={(e) => this.manualCalc(e, "budget")}></input>
                        <br/>
                        <div className="calculator">
                            <button className="calc-btn" value="1" onClick={(e) => this.calc(e, "budget")}>1</button>
                            <button className="calc-btn" value="2" onClick={(e) => this.calc(e, "budget")}>2</button>
                            <button className="calc-btn" value="3" onClick={(e) => this.calc(e, "budget")}>3</button>
                            <button className="calc-btn" value="4" onClick={(e) => this.calc(e, "budget")}>4</button>
                            <button className="calc-btn" value="5" onClick={(e) => this.calc(e, "budget")}>5</button>
                            <button className="calc-btn" value="6" onClick={(e) => this.calc(e, "budget")}>6</button>
                            <button className="calc-btn" value="7" onClick={(e) => this.calc(e, "budget")}>7</button>
                            <button className="calc-btn" value="8" onClick={(e) => this.calc(e, "budget")}>8</button>
                            <button className="calc-btn" value="9" onClick={(e) => this.calc(e, "budget")}>9</button>
                            <button disabled="disabled" className="calc-btn dot-calc-btn" value="." onClick={(e) => this.calc(e, "budget")}>.</button>
                            <button className="calc-btn" value="0" onClick={(e) => this.calc(e, "budget")}>0</button>
                            <button className="calc-btn" value="backspace" onClick={(e) => this.calc(e, "budget")}><img value="backspace" src={require("../img/icon-keypad-delete.svg")}></img></button>
                        </div>
                        <br/>
                        <button onClick={this.nextPage} clasName="capBtn">Next</button>
                    </div>
                break;

            case 32:
                displayPage =
                    <div>
                        <div className="pageHeading">
                            How much do you have for the deposit?
                        </div>
                        <div className="pageContent">
                            A larger deposit can get you access to better deals (and for budget of £{this.state.budget.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}, the minimum will be £{(this.state.budget / 10).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')})
                        </div>
                        <span className="input-label-pound input-label">£</span>
                        <input type="text" placeholder="" value={this.state.deposit !== null ? this.state.deposit.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') : ""} className="leaseInput priceInput" onChange={(e) => this.manualCalc(e, "deposit")}></input>
                        <br/>
                        <div className="calculator">
                            <button className="calc-btn" value="1" onClick={(e) => this.calc(e, "deposit")}>1</button>
                            <button className="calc-btn" value="2" onClick={(e) => this.calc(e, "deposit")}>2</button>
                            <button className="calc-btn" value="3" onClick={(e) => this.calc(e, "deposit")}>3</button>
                            <button className="calc-btn" value="4" onClick={(e) => this.calc(e, "deposit")}>4</button>
                            <button className="calc-btn" value="5" onClick={(e) => this.calc(e, "deposit")}>5</button>
                            <button className="calc-btn" value="6" onClick={(e) => this.calc(e, "deposit")}>6</button>
                            <button className="calc-btn" value="7" onClick={(e) => this.calc(e, "deposit")}>7</button>
                            <button className="calc-btn" value="8" onClick={(e) => this.calc(e, "deposit")}>8</button>
                            <button className="calc-btn" value="9" onClick={(e) => this.calc(e, "deposit")}>9</button>
                            <button disabled="disabled" className="calc-btn dot-calc-btn" value="." onClick={(e) => this.calc(e, "deposit")}>.</button>
                            <button className="calc-btn" value="0" onClick={(e) => this.calc(e, "deposit")}>0</button>
                            <button className="calc-btn" value="backspace" onClick={(e) => this.calc(e, "deposit")}><img value="backspace" src={require("../img/icon-keypad-delete.svg")}></img></button>
                        </div>
                        <br/>
                        <button onClick={this.nextPage} clasName="capBtn">Next</button>
                    </div>
                break;

            case 33:
                let dateEnd = this.state.dob;
                let dateDayEnd = parseInt(dateEnd.slice(0,2));
                let dateMonthEnd = parseInt(dateEnd.slice(2,4));
                let dateYearEnd = parseInt(dateEnd.slice(4,8));

                let formatDateEnd = Date.today().set({year: dateYearEnd, month: dateMonthEnd - 1, day: dateDayEnd}).toString("dS MMMM yyyy")

                displayPage =
                    <div>
                        <div className="pageHeading">
                            Confirm your details
                        </div>
                        <div className="pageContent">
                            Make sure your details are correct to get an accurate response from 47 lenders.
                        </div>
                        <table className="detailTable">
                            <tbody style={{backgroundColor: "#f0f0f0"}}>
                                <tr style={{borderTop: "2px solid black"}}>
                                    <td>Your details</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style={{fontWeight: "400"}}>Name</td>
                                    <td>{this.state.firstName} {this.state.lastName}</td>
                                </tr>
                                <tr>
                                    <td style={{fontWeight: "400"}}>Birthday</td>
                                    <td>{formatDateEnd}</td>
                                </tr>
                                <tr>
                                    <td style={{fontWeight: "400"}}>Nationality</td>
                                    <td>{this.state.nationality === "UK" ? "United Kingdom" : "Other"}</td>
                                </tr>
                                <tr>
                                    <td style={{fontWeight: "400"}}>Employment</td>
                                    <td>{this.state.employment1}</td>
                                </tr>
                                <tr>
                                    <td style={{fontWeight: "400"}}>Contract</td>
                                    <td>{this.state.employmentType1}</td>
                                </tr>

                                <tr style={{borderTop: "2px solid black"}}>
                                    <td>Other applicants</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style={{fontWeight: "400"}}></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Add an applicant</td>
                                </tr>

                                <tr style={{borderTop: "2px solid black"}}>
                                    <td>Your dependants</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style={{fontWeight: "400"}}></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Add a dependant</td>
                                </tr>

                                <tr style={{borderTop: "2px solid black"}}>
                                    <td>Your income</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style={{fontWeight: "400"}}>Annual income</td>
                                    <td>£{this.state.annualIncome1 === null ? 0 : this.state.annualIncome1.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}</td>
                                </tr>
                                <tr>
                                    <td style={{fontWeight: "400"}}>Payslip deductions</td>
                                    <td>£{this.state.deductionsOnPayslip === null ? 0 : this.state.deductionsOnPayslip.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}</td>
                                </tr>
                                <tr>
                                    <td style={{fontWeight: "400"}}>Additional income</td>
                                    <td>£{this.state.additionalIncome === null ? 0 : this.state.additionalIncome.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}</td>
                                </tr>

                                <tr style={{borderTop: "2px solid black"}}>
                                    <td>Loans</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style={{fontWeight: "400"}}>Monthly repayments</td>
                                    <td>£{this.state.monthlyLoanRepayments === null ? 0 : this.state.monthlyLoanRepayments.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}</td>
                                </tr>
                                <tr>
                                    <td style={{fontWeight: "400"}}>Reduced repayments</td>
                                    <td>£{this.state.monthlyLoanPaymentReduction === null ? 0 : this.state.monthlyLoanPaymentReduction.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}</td>
                                </tr>
                                <tr>
                                    <td style={{fontWeight: "400"}}>Remaining balance</td>
                                    <td>£{this.state.loanRemainingBalance === null ? 0 : this.state.loanRemainingBalance.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}</td>
                                </tr>

                                <tr style={{borderTop: "2px solid black"}}>
                                    <td>Credit cards</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style={{fontWeight: "400"}}>Current balance</td>
                                    <td>£{this.state.creditBalance === null ? 0 : this.state.creditBalance.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}</td>
                                </tr>
                                <tr>
                                    <td style={{fontWeight: "400"}}>Balance repaid</td>
                                    <td>£{this.state.creditBalanceRepaid === null ? 0 : this.state.creditBalanceRepaid.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}</td>
                                </tr>

                                <tr style={{borderTop: "2px solid black"}}>
                                    <td>Adverse credit</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style={{fontWeight: "400"}}>{this.state.adverseCredit === "YES" ? "Yes" : "None"}</td>
                                    <td></td>
                                </tr>

                                <tr style={{borderTop: "2px solid black"}}>
                                    <td>Your budget</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style={{fontWeight: "400"}}>Budget for a new home</td>
                                    <td>£{this.state.budget === null ? 0 : this.state.budget.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}</td>
                                </tr>
                                <tr>
                                    <td style={{fontWeight: "400"}}>Deposit</td>
                                    <td>£{this.state.deposit === null ? 0 : this.state.deposit.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}</td>
                                </tr>
                            </tbody>
                        </table>
                        <button onClick={this.nextPage} clasName="capBtn">Confirm details</button>
                    </div>
                break;
        }





        return (
            <div className="listing-flow">
                <div className="navbar">
					<button className="back" onClick={this.prevPage}>	
						<img className="backArrow" src={require("../img/icon-arrow-back.svg")}></img>	
						<div className="backText">		
							Back	
						</div>
					</button>
					<img className="logo" src={require("../img/logo.svg")}></img>
				</div>

                <div className="page">
                    {displayPage}
                </div>
            </div>
        );
    }
}

export default Entry;