import React from 'react';
// import "scroll-behavior-polyfill";

class LandingPage extends React.Component {
	constructor(props){
		super(props);
		this.state = {

		};
	}

	accordion(accordion, x) {
		if(document.getElementById(accordion)){
			let acc = document.getElementById(accordion);
			if(acc.style.maxHeight === "0" || acc.style.maxHeight === "0px"){
				acc.style.maxHeight = acc.scrollHeight + "px";
				if(document.getElementById(x)){
					document.getElementById(x).style.transform = "rotate(45deg)";
				}
			} else {
				acc.style.maxHeight = "0";
				if(document.getElementById(x)){
					document.getElementById(x).style.transform = "rotate(0deg)";
				}
			}
		}
	}

	render() {
		return(
			<div id="landingPage">
				<div className="topBlock">
					<img className="logo" src={require("./img/free-logo.svg")}></img>

					<div className="circleContainer">
						<div className="backgroundText">
							The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The
							fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The
							complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The
							bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The
							fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The
							complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The
							bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The
							fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The
							complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The
							bull**** The fees
							The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The
							fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The
							complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The
							bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The
							fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The
							complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The
							bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The
							fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The
							complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The
							bull**** The fees
							The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The
							fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The
							complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The
							bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The
							fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The
							complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The
							bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The
							fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The
							complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The
							bull**** The fees
							The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The
							fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The
							complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The
							bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The
							fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The
							complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The
							bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The
							fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The
							complexity&nbsp;&nbsp;The bull****&nbsp;&nbsp;The fees&nbsp;&nbsp;The complexity&nbsp;&nbsp;The
							bull**** The fees
						</div>
						<img className="rosette wobbleCircle" src={require("./img/rosette-green.svg")}></img>
						<div className="rosetteText">
							Everything<br />must go
						</div>
					</div>

					<div className="topHeader">
						<div className="heading">
							Selling your home has never been easier
						</div>
						At Free, we believe full service estate agency doesn’t have to cost you a penny.<br />

							<a href="/listing" >
								<button className="whiteBtn">Sell your home for Free</button>
							</a>
						<br/>

						<div style={{marginTop: "-30px", marginBottom: "30px"}}>
							Already have a listing? <a style={{borderBottom: "1px solid #f0f0f0"}} className="link" href="/login">Login here</a>
						</div>
					</div>
				</div>

				<div className="mortgagePrinciple">
					<div className="quoteBox">
						<div className="quoteContent">
							"I feel like I'm not getting all the options... but when I see a mortgage broker I feel like
							I know more than them"
						</div>
						<div className="quoteAuthor">
							Donna, Cheam
						</div>
					</div>

					<div className="quoteResponse">
						Donna, we hear you
					</div>
					<div className="quoteResponseExplanation">
						<div className="explanationHeading">
							There is a better way, and it's Free.
						</div>
						<div className="quoteResponseContent">
							We don't just help you to sell, we can also help you buy your new home. With just a few details,
							we can show you what 47 lenders will offer you as a mortgage in principle (and we'll give you free
							expert advice too).
						</div>
						<div className="explanationHeading">
							Get your Free mortgage in principle today
						</div>
					</div>

						<a href="https://beta.free.co.uk/mortgage/wizard" >
							<button style={{fontSize: "18px", textTransform: "none", letterSpacing: "0"}}
								className="blackBtn">How much can you borrow?</button>
						</a>
				</div>

				<div className="cuppaBox">
					<div className="cuppaHeading">
						FREE <img className="tvIcon" src={require("./img/icon-tv.svg")}></img> LOVE
					</div>
					<table>
						<tbody>
							<tr>
								<td style={{width: "55%"}}>
									<iframe className="rayVideo" src="https://www.youtube.com/embed/AUiM_9cv-kg"></iframe>
								</td>
								<td style={{textAlign: "left"}}>
									<div className="explanationHeading">
										We empower people to sell their own home
									</div>
									<div className="cuppaContentTable">
										And we love the fact that it's Free so much that we made a song about it.
									</div>
									<div className="explanationHeading">
										Ready to sell your home for Free?
									</div>

										<a href="/listing" >
											<button className="whiteBtn">Sell your home</button>
										</a>
								</td>
							</tr>
						</tbody>
					</table>

					<div className="altTable">
						<iframe className="rayVideo" src="https://www.youtube.com/embed/AUiM_9cv-kg"></iframe>

						<div className="explanationHeading">
							We empower people to sell their own home
						</div>
						<div className="cuppaContent" style={{marginBottom: "10px"}}>
							And we love the fact that it's Free so much that we made a song about it.
						</div>
						<div className="explanationHeading">
							Ready to advertise your home for Free?
						</div>

							<a href="/listing" >
								<button className="whiteBtn">Get Started</button>
							</a>
					</div>
				</div>

				<div className="catchBox">
					<div className="catchHeading">
						So, what's the catch?
					</div>
					<div className="explanationHeading">
						We don't make money helping you sell your house
					</div>
					<div className="catchContent">
						There's no hook - we make money selling mortgages to the 20 buyers enquiring to book a viewing.
						<br />
						<img className="rosette fishImage" src={require("./img/rosette-fish.svg")}></img>
						<br />
						(Whilst there's no obligation for you to use Free for a mortgage, it'd be awesome if you did.)
					</div>
				</div>

				<div className="storyBox">
					<div className="cuppaHeading">
						SIMPLE IS <img className="tvIcon" src={require("./img/icon-tv.svg")}></img> ALWAYS BETTER
					</div>
					<table>
						<tbody>
							<tr>
								<td style={{width: "55%"}}>
									<iframe className="rayVideo" src="https://www.youtube.com/embed/4PhiNDzVGnY"></iframe>
								</td>
								<td style={{textAlign: "left"}}>
									<div className="explanationHeading">
										What's the story?
									</div>
									<div className="cuppaContentTable">
										Listen to our CEO and founder Ray Rafiq tell you the tale about what Free really means.
									</div>
									<div className="youtubeLinkDiv">
										<a href="https://www.youtube.com/channel/UCTHBn84d5gx92jRB6xKyL6A" 
											className="youtubeLink">
											Check out our videos
										</a>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
					<div className="altTable">
						<iframe className="rayVideo" src="https://www.youtube.com/embed/4PhiNDzVGnY"></iframe>
						<div className="explanationHeading">
							What's the story?
						</div>
						<div className="cuppaContent">
							Listen to our CEO and founder Ray Rafiq tell you the tale about what Free really means.
						</div>
						<div className="youtubeLinkDiv">
							<a href="https://www.youtube.com/channel/UCTHBn84d5gx92jRB6xKyL6A" 
								className="youtubeLink">
								Check out our videos
							</a>
						</div>
					</div>
				</div>

				<div className="jobBox">
					<div className="jobHeading">
						Everything for Free
					</div>
					<div className="explanationHeading">
						Interested in joining the good fight?
					</div>
					<div className="jobContent">
						We don't "fill roles". We attract amazing people and help them work on things they are excited about.
						<br />
						If you're the best at what you do, or young and hungry, we'd love to speak to you.
					</div>
					<div className="emailLinkDiv">
						<a className="emailLink" href="mailto:born@free.co.uk">born@free.co.uk</a>
					</div>
				</div>

				<hr />

				<div className="otherServices">
					<div className="jobHeading">
						From selling your home to buying a new one
					</div>
					<div className="jobContent">
						Free's got you covered every step of the way.
					</div>
				</div>

				<div className="accordion">
					<div className="accordionHeading" onClick={() => this.accordion('homeAccordion', 'homeX')}>
						Tell us about your home
						<div className="x">
							<img src={require("./img/accordion-cross.svg")} id="homeX"></img>
						</div>
					</div>

					<div style={{maxHeight: "0px"}} className="accordionContent" id="homeAccordion">
						Fill us in on your property details in a matter of minutes, and receive a ton of free advice and powerful tools immediately.
						<div className="accordionLinkDiv">
								<a className="accordionLink" href="/listing" >
									Advertise your home
								</a>
						</div>
					</div>
				</div>

				<div className="accordion">
					<div className="accordionHeading" onClick={() => this.accordion('helpAccordion', 'helpX')}>
						We’ll help you sell it better
						<div className="x">
							<img src={require("./img/accordion-cross.svg")} id="helpX"></img>
						</div>
					</div>

					<div style={{maxHeight: "0px"}} className="accordionContent" id="helpAccordion">
						We’ll send around a photographer to capture the best parts of your property and double check what you’ve told us about your home. Since our services are free and there’s no commitments, we’ll let you have these photos even if you decide to go elsewhere!
						<div className="paddingBottom"></div>
					</div>
				</div>

				<div className="accordion">
					<div className="accordionHeading" onClick={() => this.accordion('marketAccordion', 'marketX')}>
						We’ll get you on the market
						<div className="x">
							<img src={require("./img/accordion-cross.svg")} id="marketX"></img>
						</div>
					</div>

					<div style={{maxHeight: "0px"}} className="accordionContent" id="marketAccordion">
						With a spruced up listing ready to go, we’ll put your property on the market and capture as much interest as possible. And when people call to arrange a viewing, we’ll get buyers booked into your calendar for free!
						<div className="paddingBottom"></div>
					</div>
				</div>

				<div className="accordion">
					<div className="accordionHeading" onClick={() => this.accordion('mortgageAccordion', 'mortgageX')}>
						Get a mortgage in principle
						<div className="x">
							<img src={require("./img/accordion-cross.svg")} id="mortgageX"></img>
						</div>
					</div>

					<div style={{maxHeight: "0px"}} className="accordionContent" id="mortgageAccordion">
						With a few details, get an accurate amount to borrow from 47 mortgage providers — so you can confidently
						find a home knowing how much you can afford - and so your offers get considered seriously.
						<br />
						<div className="accordionLinkDiv">
								<a className="accordionLink" href="https://beta.free.co.uk/mortgage/wizard" >
									Find out how much you can borrow
								</a>
						</div>
					</div>
				</div>
				<div className="accordion">
					<div className="accordionHeading" onClick={() => this.accordion('applyAccordion', 'applyX')}>
						Apply for a mortgage
						<div className="x">
							<img src={require("./img/accordion-cross.svg")} id="applyX"></img>
						</div>
					</div>

					<div style={{maxHeight: "0px"}} className="accordionContent" id="applyAccordion">
						Once you’ve found your new home and decided which mortgage is best for you, we’ll help you step-by-step
						through the application process. Our regulated experts are on hand to answer any questions and speed you
						to buying your home.
						<div className="paddingBottom"></div>
					</div>
				</div>
				<div className="accordion">
					<div className="accordionHeading" onClick={() => this.accordion('insuranceAccordion', 'insuranceX')}>
						Protect your new home
						<div className="x">
							<img src={require("./img/accordion-cross.svg")} id="insuranceX"></img>
						</div>
					</div>

					<div style={{maxHeight: "0px"}} className="accordionContent" id="insuranceAccordion">
						Once you've had your offer accepted, it's important to make sure your new home is protected – we want
						you and your family to enjoy your new home worry-free. Our experts are here to help you navigate the
						life insurance products that provide peace of mind.
						<div className="paddingBottom"></div>
					</div>
				</div>

				<div className="bottom">
					<div className="mention">
						Did we mention it's free?
					</div>

					<hr />

					<a href="https://www.linkedin.com/company/freecouk/" >
						<img className="socialIcon" src={require("./img/social-linkedin.svg")}></img>
					</a>

					<a href="https://www.youtube.com/channel/UCTHBn84d5gx92jRB6xKyL6A" >
						<img className="socialIcon" src={require("./img/social-youtube.svg")}></img>
					</a>

					<div className="legal">
						Free free free ltd is a registered company No.12429135. Copyright &copy; 2019-2020 Free free free
						ltd. All rights reserved.
					</div>
				</div>
			</div>
		);
	}
}

export default LandingPage;