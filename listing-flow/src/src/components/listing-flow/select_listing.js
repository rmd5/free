import React from 'react';

class SelectListing extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			listings: [],
			cards: [],
			token: null
		};
	}

	componentDidMount() {
		let user = JSON.parse(localStorage.getItem("user"));
		if(user !== undefined && user !== null){
			let token = user.token;

			this.setState({
				token: token
			}, () => {

				var myHeaders = new Headers();
				myHeaders.append("Content-Type", "application/json");
				myHeaders.append("X-Requested-With", "XMLHttpRequest");
				myHeaders.append("Authorization", "Token " + token);

				var requestOptions = {
					method: 'GET',
					headers: myHeaders,
					redirect: 'follow'
				};

				fetch("https://freeco-api.herokuapp.com/api/listings", requestOptions)
					.then(response => response.json())
					.then(result => {
						console.log(result);
						this.setState({
							listings: result.listings
						}, () => {
							console.log(this.state.listings);
							this.sortListings();
						});
					})
					.catch(error => {
						console.log('error', error);
						window.location.href = "/login";
					});
				});
			} else {
				window.location.href = "/login";
			}
	}

	sortListings() {
		let listings = this.state.listings;
		let cards = [];

		for(let i = 0, size = listings.length; i < size; i++){
			// let yearSlice = parseInt(listings[i].createdAt.slice(0,4));
			// let monthSlice = parseInt(listings[i].createdAt.slice(5,7));
			// let daySlice = parseInt(listings[i].createdAt.slice(8,10));
			// let dateListed = Date.today().set({year: yearSlice, month: monthSlice, day: daySlice}).toString("dddd dS MMMM");

			let source = "";
			if(listings[i].images[0] !== undefined){
				source = listings[i].images[0].thumbUrl;
			}

			let card =
				<div className="propCard" onClick={() => this.chooseSlug(listings[i].slug)}>
					<div className="propCardImg">
						<img style={{maxWidth: "250px"}} src={source}></img>
					</div>
					<div className="propAddress" style={{textAlign: "left"}}>
						{listings[i].addLine1}<br/>
						{listings[i].addCounty}<br/>
						{listings[i].addPostcode}<br/>
						{/* {dateListed} */}
						{/* <div onClick={(e) => this.deleteListing(e, i)}>
							Delete
						</div> */}
					</div>

					<div id={"modal" + i} className="modal">
						<div className="modalContent">
							<div className="pageHeading">
								Delete {listings[i].addLine1}?
							</div>
							<div className="pageContent">
								This can't be undone
							</div>
							<button className="radio" value="yes" onClick={(e) => this.removeListing(e, i, "yes", listings[i].slug)}>Yes</button><br/>
							<button className="radio" value="no" onClick={(e) => this.removeListing(e, i, "no", listings[i].slug)}>No</button>
						</div>
					</div>
				</div>;

			cards.push(card);
		}

		this.setState({
			cards: cards
		});
	}

	logout = () => {
		localStorage.clear();
		window.location.href = "/";
	}

	chooseSlug(slug){
		window.location.href = "/dashboard/?slug=" + slug;
	}

	prevPage = () => {
		document.getElementById("leaveModal").style.display = "block";
	}

	deleteListing(e, i){
		e.stopPropagation();
		document.getElementById("modal" + i).style.display = "block";
	}

	removeListing(e, i, des, slug) {
		e.stopPropagation();
		if(des === "yes"){
			var myHeaders = new Headers();
			myHeaders.append("Content-Type", "application/json");
			myHeaders.append("X-Requested-With", "XMLHttpRequest");
			myHeaders.append("Authorization", "Token " + this.state.token);

			var requestOptions = {
				method: 'DELETE',
				headers: myHeaders,
				redirect: 'follow'
			};

			fetch("https://freeco-api.herokuapp.com/api/listings/" + slug, requestOptions)
			.then(response => response.text())
			.then(result => {
				console.log(result);
				this.componentDidMount();
			})
			.catch(error => console.log('error', error));
		} else {
			document.getElementById("modal" + i).style.display = "none";
		}
	}

	leavePage = (e) => {
		let des = e.currentTarget.value;
		if(des === "yes"){
			window.location.href = "/";
		} else {
			document.getElementById("leaveModal").style.display = "none";
		}
	}

	render() {
		return (
			<div className="listing-flow">
				<div className="navbar">
					<button className="back" onClick={this.prevPage}>
						<img className="backArrow" src={require("../img/icon-arrow-back.svg")}></img>
						<div className="backText">
							Back
						</div>
					</button>
					<img className="logo" src={require("../img/logo.svg")}></img>
					<button className="back" onClick={this.logout}>
						<div className="backText">
							Logout
						</div>
					</button>
				</div>

				<div className="page">
					<div className="pageHeading">
						Select the property you would like to view
					</div>
					{this.state.cards}
				</div>

				<div id="leaveModal" className="modal">
					<div className="modalContent">
						<div className="pageHeading">
							Go back to Free.co.uk?
						</div>
						<button className="radio" value="yes" onClick={this.leavePage}>Yes</button><br/>
						<button className="radio" value="no" onClick={this.leavePage}>No</button>
					</div>
				</div>
			</div>
		);
	}
}

export default SelectListing;