import React from 'react';

import Valuator from '../tools/valuation';
import Optimiser from '../tools/optimiser';
require('datejs');

class Dashboard extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			listing: null,
			postarea: null,
			slug: null,
			token: null,
			scroll: 0,
			descriptionOriginal: null,
			descriptionModified: null,
			descriptionSaved: null
		};
	}

	componentDidMount() {
		localStorage.setItem("previous", "dashboard");

		var url = new URL(window.location.href);
		var slug = url.searchParams.get("slug");

		let user = JSON.parse(localStorage.getItem("user"));
		if(user !== undefined && user !== null){
			if(!user.listings.includes(slug)){
				window.location.href = "/book-viewing/?slug=" + slug;
			}
			let token = user.token;

			this.setState({
				token: token,
				slug: slug
			}, () => {

				var myHeaders = new Headers();
				myHeaders.append("Content-Type", "application/json");
				myHeaders.append("X-Requested-With", "XMLHttpRequest");
				myHeaders.append("Authorization", "Token " + token);

				var requestOptions = {
					method: 'GET',
					headers: myHeaders,
					redirect: 'follow'
				};

				fetch("https://freeco-api.herokuapp.com/api/listings/" + slug, requestOptions)
					.then(response => response.json())
					.then(result => {
						this.setState({
							listing: result.listing
						}, () => {
							this.setState({
								descriptionOriginal: this.state.listing.description,
								descriptionModified: this.state.listing.description
							}, () => {
								this.cancelEditDescription();
							});
							this.getPostcode();
						});
					})
					.catch(error => {
						console.log('error', error);
						window.location.href = "/book-viewing/?slug=" + slug;
					});
				});
		} else {
			window.location.href = "/book-viewing/?slug=" + slug;
		}
	}

	getPostcode() {
		let postcode = this.state.listing.addPostcode;
		let postarea = postcode.slice(0,postcode.length - 3);

		this.setState({
			postarea: postarea
		});
	}

	prevPage = () => {
		let user = localStorage.getItem("user");

		if(user !== null && user !== undefined){
			user = JSON.parse(user);
			let arr = user.listings;

			if(arr.length > 1){
				window.location.href = "/select-listing";
			} else {
				window.location.href = "/";
			}
		}
	}

	accordion(accordion, x) {
		if (document.getElementById(accordion)) {
			let acc = document.getElementById(accordion);
			if (acc.style.maxHeight === "0" || acc.style.maxHeight === "0px") {
				acc.style.maxHeight = "1500px";
				acc.style.minHeight = "300px";

				if (document.getElementById(x)) {
					document.getElementById(x).style.transform = "rotate(45deg)";
				}
			} else {
				acc.style.maxHeight = "0";
				acc.style.minHeight = "0";
				if (document.getElementById(x)) {
					document.getElementById(x).style.transform = "rotate(0deg)";
				}
			}
		}
	}

	logout = () => {
		localStorage.clear();
		window.location.href = "/";
	}

	slideLeft = () => {
		let to = parseInt(this.state.scroll) - 1;
		if(to < 0){
			to = 0;
		}

		this.setState({
			scroll: to
		}, () => {
			if(document.getElementById("qualityImage" + this.state.scroll)){
				document.getElementById("qualityImage" + this.state.scroll).scrollIntoView({ behavior: "smooth", block: "nearest", inline: "nearest" });
			}
		})
	}

	slideRight = () => {
		let to = parseInt(this.state.scroll) + 1;
		if(to >= this.state.listing.images.length){
			to = this.state.listing.images.length - 1;
		}

		this.setState({
			scroll: to
		}, () => {
			if(document.getElementById("qualityImage" + this.state.scroll)){
				document.getElementById("qualityImage" + this.state.scroll).scrollIntoView({ behavior: "smooth", block: "nearest", inline: "nearest" });
			}
		})
	}

	updateDescription = (e) => {
		this.setState({
			descriptionModified: e.currentTarget.value
		});
	}

	cancelEditDescription = () => {
		this.setState({
			descriptionModified: this.state.descriptionOriginal
		}, () => {
			document.getElementById("cancel").style.display = "none";
			document.getElementById("update").style.display = "initial";
			document.getElementById("save").style.display = "none";
			document.getElementById("editor").style.display = "none";
			document.getElementById("description").style.display = "inline";
		});
	}

	startEditor = () => {
		document.getElementById("editor").style.display = "block";
		document.getElementById("cancel").style.display = "initial";
		document.getElementById("update").style.display = "none";
		document.getElementById("save").style.display = "initial";
		document.getElementById("description").style.display = "none";
	}

	saveEdit = () => {
		this.setState({
			descriptionOriginal: this.state.descriptionModified
		}, () => {
			document.getElementById("cancel").style.display = "none";
			document.getElementById("update").style.display = "initial";
			document.getElementById("save").style.display = "none";
			document.getElementById("editor").style.display = "none";
			document.getElementById("description").style.display = "initial";
		});
	}

	render() {
		let address = "";
		let price = "";
		if(this.state.listing !== null){
			address = this.state.listing.addLine1;
			price = this.state.listing.propAskingPrice.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
		}

		let checked = "";
		if(this.state.listing !== null){
			if(this.state.listing.isPublished){
				checked = "checked";
			}
		}

		let mainImg = <img className="uploadedPhoto dashboardPhoto" src={require("../img/free-logo.svg")} alt="Exterior image"></img>;
		let imgDisplay = 1;
		if(this.state.listing !== null){
			if(this.state.listing.images[0] !== undefined){
				mainImg = <img className="uploadedPhoto dashboardPhoto" src={this.state.listing.images[0].url} alt="Exterior image"></img>;
				imgDisplay = 2;
			}
		}

		let imageArr = [];
		let display = "none";
		if(this.state.listing !== null){
			for(let i = 0, size = this.state.listing.images.length; i < size; i++){
				imageArr.push(<img id={"qualityImage" + i} style={{width: (100 / size) + "%"}} className="qualityImage" src={this.state.listing.images[i].url}></img>);
				display = "block";
			}
		}

		return (
			<div className="listing-flow">
				<div className="navbar">
					<button className="back" onClick={this.prevPage}>
						<img className="backArrow" src={require("../img/icon-arrow-back.svg")}></img>
						<div className="backText">
							Back
						</div>
					</button>
					<img className="logo" src={require("../img/logo.svg")}></img>
					<button className="back" onClick={this.logout}>
						<div className="backText">
							Logout
						</div>
					</button>
				</div>
				<div className="page">
					<a href="https://www.youtube.com/channel/UCTHBn84d5gx92jRB6xKyL6A" target="_blank">
						<div className="blackBar">
							Get some <span className="colorText">Free</span> guidance
							<span className="floatX">+</span>
						</div>
					</a>
					<div className="photoUpload">

						<div className="boxHeading">
							<div className="pageHeading">
								Selling your home
							</div>
							<div className="pageContent">
								{address}
							</div>
							<div className="dashboardPrice">
								£{price}
							</div>
						</div>

						{mainImg}

						<a href={"/photo-upload/?slug=" + this.state.slug} className="link">
							{imgDisplay === 2 ? "Update your photos" : "Add photos"}
						</a>

						<a href={"/publish/?slug=" + this.state.slug}>
							<button className="adBtn" style={{marginTop: "10px"}}>
								Publish my advert <img className="rightArrow" src={require("../img/icon-arrow-forward.svg")}></img>
							</button>
						</a>

						<a href={"/availability/?slug=" + this.state.slug}>
							<button className="adBtn">
								Your availability <img className="rightArrow" src={require("../img/icon-arrow-forward.svg")}></img>
							</button>
						</a>

						<a href={"/viewings/?slug=" + this.state.slug}>
							<button className="adBtn">
								Your viewings <div className="viewingNumber"><strong>{this.state.listing !== null ? this.state.listing.viewings.length : null}</strong></div><img className="rightArrow" src={require("../img/icon-arrow-forward.svg")}></img>
							</button>
						</a>

						<a href="">
							<button disabled="disabled" className="adBtn">
								Your offers <img className="rightArrow" src={require("../img/icon-arrow-forward.svg")}></img>
							</button>
						</a>

						<div className="pageContent" style={{ marginTop: "20px" }}>
							<strong>Improve your ad's performance</strong><br />
							Click on a rating to learn how:
						</div>
					</div>
				</div>
				<div className="accordion dashboardAccordion">
					<div className="accordionHeading" onClick={() => this.accordion("qualityAccordion", "qualityX")}>
						Quality
						<div className="x">
							<img src={require("../img/accordion-cross.svg")} id="qualityX"></img>
						</div>
					</div>

					<div style={{ maxHeight: "0px", textAlign: "left"}} className="accordionContent" id="qualityAccordion">
						<div style={{display: display}}>
							<div className="qualityImagesContainer">
								<div className="qualityImagesExtension" style={{width: imageArr.length + "00%"}}>
									{imageArr}
								</div>
							</div>

							<div style={{textAlign: "center", border: "1px solid black", borderTop: "none"}}>
								<button style={{float: "left", margin: "0", "borderRadius": "0", dislay: "inline-block", border: "none", borderRight: "1px solid black"}} onClick={this.slideLeft}><i class="fas fa-angle-left"></i></button>
								<button style={{float: "right", margin: "0", "borderRadius": "0", dislay: "inline-block", border: "none", borderLeft: "1px solid black"}} onClick={this.slideRight}><i class="fas fa-angle-right"></i></button>
								<button className="imageNumberBox" style={{cursor: "auto", fontSize: "15px", textTransform: "initial", margin: "0", border: "none", "borderRadius": "0", dislay: "inline-block"}}>{this.state.scroll + 1} out of {imageArr.length} photos</button>
							</div>
						</div>

						<br/>

						<a href={"/photo-upload/?slug=" + this.state.slug} className="link">{display === "block" ? "Update your photos" : "Add photos"}</a>
						<br/><br/>
						<strong>{this.state.listing !== null ? this.state.listing.propBeds : null} bedroom house for sale</strong>
						<br/>
						<div id="description" className="pageContent" style={{textAlign: "left"}}>
							{this.state.descriptionOriginal !== null ? this.state.descriptionOriginal : null}
							<br/><br/>
						</div>
						<textarea style={{width: "calc(100% - 6px)"}} id="editor" onChange={this.updateDescription} value={this.state.descriptionModified}></textarea>
						<span onClick={this.startEditor} id="update" className="link">Update your description</span>
						<span onClick={this.saveEdit} id="save" className="link">Save changes</span>
						<span id="cancel" onClick={this.cancelEditDescription} className="link" style={{float: "right"}}>Cancel changes</span>
						<div className="paddingBottom"></div>
					</div>
				</div>

				<div className="accordion dashboardAccordion">
					<div className="accordionHeading" onClick={() => this.accordion("visibilityAccordion", "visibilityX")}>
						Visibility
						<div className="x">
							<img src={require("../img/accordion-cross.svg")} id="visibilityX"></img>
						</div>
					</div>

					<div style={{ maxHeight: "0px" }} className="accordionContent" id="visibilityAccordion">
						{this.state.listing !== null && this.state.postarea !== null ? <Optimiser numRooms={this.state.listing.propBeds} postcode={this.state.postarea} price={this.state.listing.propAskingPrice}/> : null}
						<div className="paddingBottom"></div>
					</div>
				</div>

				<div className="accordion dashboardAccordion">
					<div className="accordionHeading" onClick={() => this.accordion("valuationAccordion", "valuationX")}>
						Valuation
						<div className="x">
							<img src={require("../img/accordion-cross.svg")} id="valuationX"></img>
						</div>
					</div>

					<div style={{ maxHeight: "0px" }} className="accordionContent" id="valuationAccordion">
						{this.state.postarea !== null ? <Valuator postcode={this.state.postarea}/> : null}
						<div className="paddingBottom"></div>
					</div>
				</div>

				<div className="whatNext">
					<div className="pageHeading whatNextHeading">
						What's next?
					</div>

					<div className="whiteLine">
						<div className="whiteDot">

						</div>
					</div>

					<div className="pageContent">
						<strong>Get a mortgage-in-principle in 5 minutes</strong><br />
						A no-commitment peek into what lenders are prepared to offer you, giving you a clear idea
						of a budget while looking for your new home.
					</div>

					<a href="https://beta.free.co.uk/mortgage/wizard" target="_blank">
						<button className="capBtn colorBtn">Find your budget</button>
					</a>
				</div>
			</div>
		);
	}
}

export default Dashboard;
