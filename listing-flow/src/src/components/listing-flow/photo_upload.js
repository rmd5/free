import React from 'react';

class PhotoUpload extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			page: 2,
			availableSetting: null,
			availability: {
				"mon": {"am": false,"pm": false,"eve": false},
				"tue": {"am": false,"pm": false,"eve": false},
				"wed": {"am": false,"pm": false,"eve": false},
				"thurs": {"am": false,"pm": false,"eve": false},
				"fri": {"am": false,"pm": false,"eve": false},
				"sat": {"am": false,"pm": false,"eve": false},
				"sun": {"am": false,"pm": false,"eve": false}
			},
			sheltering: null,
			photos: {
			},
			nBaths: 10,
			nBeds: 10,
			listing: null,
			token: null,
			slug: null,
			preventBtn: false,
			ids: {
			},
		};
	}

	componentDidMount() {

		let user = JSON.parse(localStorage.getItem("user"));
		if(user !== null && user !== undefined){
			let token = user.token;

			var url = new URL(window.location.href);
			var slug = url.searchParams.get("slug");

			this.setState({
				token: token,
				slug: slug
			});

			var myHeaders = new Headers();
			myHeaders.append("Content-Type", "application/json");
			myHeaders.append("X-Requested-With", "XMLHttpRequest");
			myHeaders.append("Authorization", "Token " + token);

			var requestOptions = {
				method: 'GET',
				headers: myHeaders,
				redirect: 'follow'
			};

			console.log(token);

			fetch("https://freeco-api.herokuapp.com/api/listings", requestOptions)
			.then(response => response.json())
			.then(result => {
				console.log(result);
				let arr = [];

				let nBaths = 0;
				let nBeds = 0;

				for(let i = 0; i < result.listings.length; i++){
					arr.push(result.listings[i].slug);
					if(result.listings[i].slug === slug){
						nBaths = result.listings[i].propBaths;
						nBeds = result.listings[i].propBeds;
					}
				}

				let index = arr.indexOf(slug.toString());
				console.log(index);

				if(!arr.includes(slug.toString())){
					window.location.href = "/listing";
				}

				let images = result.listings[index].images;

				let photos = this.state.photos;
				let ids = this.state.ids;
				for(let i = 0, size = images.length; i < size; i++){
					console.log()
					if(photos[result.listings[index].images[i].room] === undefined){
						photos[result.listings[index].images[i].room] = result.listings[index].images[i].url;
						ids[result.listings[index].images[i].room] = result.listings[index].images[i].id;
					}
				}

				this.setState({
					nBaths: nBaths,
					nBeds: nBeds,
					photos: photos,
					ids: ids
				}, () => {
					console.log(this.state.photos);
				});
			})
			.catch(error => console.log('error', error));
		} else {
			window.location.href = "/login";
		}
	}

	nextPage = () => {
		let page = this.state.page;
		if(page === 2){
			window.location.href = "/dashboard/?slug=" + this.state.slug;
		} else {
			if(page === 1.5){
				page = 2;
			} else {
				page += 1;
			}
	
			this.setState({
				page: page
			});
		}
	}

	prevPage = () => {
		let page = this.state.page;
		if(page === 1.5){
			page = 1;
		} else if(page === 2 && this.state.availableSetting === "Specify"){
			page = 1.5;
		} else if(page === 1){
			document.getElementById("leaveModal").style.display = "block";
			page = 1;
		} else {
			page -= 1;
		}

		this.setState({
			page: page
		});
	}

	updateAvailability = (e) => {
		let time = e.currentTarget.value;
		let page = 2;
		let availability = this.state.availability;

		if(time === "Anytime"){
			availability = {
				"mon": {"am": true,"pm": true,"eve": true},
				"tue": {"am": true,"pm": true,"eve": true},
				"wed": {"am": true,"pm": true,"eve": true},
				"thurs": {"am": true,"pm": true,"eve": true},
				"fri": {"am": true,"pm": true,"eve": true},
				"sat": {"am": true,"pm": true,"eve": true},
				"sun": {"am": true,"pm": true,"eve": true}
			};
		} else if(time === "Weekends"){
			availability = {
				"mon": {"am": false,"pm": false,"eve": false},
				"tue": {"am": false,"pm": false,"eve": false},
				"wed": {"am": false,"pm": false,"eve": false},
				"thurs": {"am": false,"pm": false,"eve": false},
				"fri": {"am": false,"pm": false,"eve": false},
				"sat": {"am": true,"pm": true,"eve": true},
				"sun": {"am": true,"pm": true,"eve": true}
			};
		} else if(time === "Evenings"){
			availability = {
				"mon": {"am": false,"pm": false,"eve": true},
				"tue": {"am": false,"pm": false,"eve": true},
				"wed": {"am": false,"pm": false,"eve": true},
				"thurs": {"am": false,"pm": false,"eve": true},
				"fri": {"am": false,"pm": false,"eve": true},
				"sat": {"am": false,"pm": false,"eve": true},
				"sun": {"am": false,"pm": false,"eve": true},
			};
		} else if(time === "Evenings-and-weekends"){
			availability = {
				"mon": {"am": false,"pm": false,"eve": true},
				"tue": {"am": false,"pm": false,"eve": true},
				"wed": {"am": false,"pm": false,"eve": true},
				"thurs": {"am": false,"pm": false,"eve": true},
				"fri": {"am": false,"pm": false,"eve": true},
				"sat": {"am": true,"pm": true,"eve": true},
				"sun": {"am": true,"pm": true,"eve": true},
			};
		} else if(time === "Specify"){
			page = 1.5;
		}

		this.setState({
			page: page,
			availability: availability,
			availableSetting: time
		});
	}

	updateSheltering = (e) => {
		let decision = e.currentTarget.value;
		this.setState({
			sheltering: decision,
			page: 2
		});
	}

	clickCheck = (e) => {
		let x = e.currentTarget.value;
		let av = this.state.availability;
		if(e.currentTarget.value.toString().slice(e.currentTarget.value.length - 4, e.currentTarget.value.length) === "-img"){
			document.getElementById(e.currentTarget.value).style.display = "none";
			document.getElementById(e.currentTarget.value.toString().slice(0,e.currentTarget.value.length - 4)).style.display = "block";
			if(x.slice(0,3) === "mon"){
				if(x.slice(4, x.length - 4) === "am"){av.mon.am = false;} 
				else if(x.slice(4, x.length - 4) === "pm"){av.mon.pm = false;} 
				else if(x.slice(4, x.length - 4) === "eve"){av.mon.eve = false;}
			} else if(x.slice(0,3) === "tue"){
				if(x.slice(4, x.length - 4) === "am"){av.tue.am = false;} 
				else if(x.slice(4, x.length - 4) === "pm"){av.tue.pm = false;} 
				else if(x.slice(4, x.length - 4) === "eve"){av.tue.eve = false;}
			} else if(x.slice(0,3) === "wed"){
				if(x.slice(4, x.length - 4) === "am"){av.wed.am = false;} 
				else if(x.slice(4, x.length - 4) === "pm"){av.wed.pm = false;} 
				else if(x.slice(4, x.length - 4) === "eve"){av.wed.eve = false;}
			} else if(x.slice(0,3) === "thu"){
				if(x.slice(4, x.length - 4) === "am"){av.thurs.am = false;} 
				else if(x.slice(4, x.length - 4) === "pm"){av.thurs.pm = false;} 
				else if(x.slice(4, x.length - 4) === "eve"){av.thurs.eve = false;}
			} else if(x.slice(0,3) === "fri"){
				if(x.slice(4, x.length - 4) === "am"){av.fri.am = false;} 
				else if(x.slice(4, x.length - 4) === "pm"){av.fri.pm = false;} 
				else if(x.slice(4, x.length - 4) === "eve"){av.fri.eve = false;}
			} else if(x.slice(0,3) === "sat"){
				if(x.slice(4, x.length - 4) === "am"){av.sat.am = false;} 
				else if(x.slice(4, x.length - 4) === "pm"){av.sat.pm = false;} 
				else if(x.slice(4, x.length - 4) === "eve"){av.sat.eve = false;}
			} else if(x.slice(0,3) === "sun"){
				if(x.slice(4, x.length - 4) === "am"){av.sun.am = false;} 
				else if(x.slice(4, x.length - 4) === "pm"){av.sun.pm = false;} 
				else if(x.slice(4, x.length - 4) === "eve"){av.sun.eve = false;}
			}
		} else {
			document.getElementById(e.currentTarget.value).style.display = "none";
			document.getElementById(e.currentTarget.value + "-img").style.display = "block";
			if(x.slice(0,3) === "mon"){
				if(x.slice(4, x.length) === "am"){av.mon.am = true;} 
				else if(x.slice(4, x.length) === "pm"){av.mon.pm = true;} 
				else if(x.slice(4, x.length) === "eve"){av.mon.eve = true;}
			} else if(x.slice(0,3) === "tue"){
				if(x.slice(4, x.length) === "am"){av.tue.am = true;} 
				else if(x.slice(4, x.length) === "pm"){av.tue.pm = true;} 
				else if(x.slice(4, x.length) === "eve"){av.tue.eve = true;}
			} else if(x.slice(0,3) === "wed"){
				if(x.slice(4, x.length) === "am"){av.wed.am = true;} 
				else if(x.slice(4, x.length) === "pm"){av.wed.pm = true;} 
				else if(x.slice(4, x.length) === "eve"){av.wed.eve = true;}
			} else if(x.slice(0,3) === "thu"){
				if(x.slice(4, x.length) === "am"){av.thurs.am = true;} 
				else if(x.slice(4, x.length) === "pm"){av.thurs.pm = true;} 
				else if(x.slice(4, x.length) === "eve"){av.thurs.eve = true;}
			} else if(x.slice(0,3) === "fri"){
				if(x.slice(4, x.length) === "am"){av.fri.am = true;} 
				else if(x.slice(4, x.length) === "pm"){av.fri.pm = true;} 
				else if(x.slice(4, x.length) === "eve"){av.fri.eve = true;}
			} else if(x.slice(0,3) === "sat"){
				if(x.slice(4, x.length) === "am"){av.sat.am = true;} 
				else if(x.slice(4, x.length) === "pm"){av.sat.pm = true;} 
				else if(x.slice(4, x.length) === "eve"){av.sat.eve = true;}
			} else if(x.slice(0,3) === "sun"){
				if(x.slice(4, x.length) === "am"){av.sun.am = true;} 
				else if(x.slice(4, x.length) === "pm"){av.sun.pm = true;} 
				else if(x.slice(4, x.length) === "eve"){av.sun.eve = true;}
			}
		}

		this.setState({
			availability: av
		});
	}

	uploadPhoto(e, room) {
		if(e.currentTarget.files && e.currentTarget.files[0]){
			this.setState({
				preventBtn: true
			});
			let img = e.currentTarget.files[0];
			let photos = this.state.photos;
			let image = URL.createObjectURL(img);
			console.log(img);
			photos[room] = image;
			this.setState({
				photos: photos
			});

			let token = this.state.token;

			var myHeaders = new Headers();
			myHeaders.append("authorization", "Token " + token);
			myHeaders.append("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36");
			// myHeaders.append("Content-Type", "multipart/form-data");
			myHeaders.append("Accept", "*/*");

			var formdata = new FormData();
			formdata.append("file", e.currentTarget.files[0]);
			formdata.append("room", room);

			console.log(...formdata);

			var requestOptions = {
				method: 'POST',
				headers: myHeaders,
				body: formdata,
				redirect: 'follow'
			};

			fetch("https://freeco-api.herokuapp.com/api/listings/" + this.state.slug + "/photos", requestOptions)
			.then(response => response.text())
			.then(result => {
				this.setState({
					preventBtn: false
				});
			})
			.catch(error => console.log('error', error));
		}
	}

	deletePhoto(room) {
		this.setState({
			preventBtn: true
		});

		let token = this.state.token;

		var myHeaders = new Headers();
		myHeaders.append("authorization", "Token " + token);
		myHeaders.append("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36");
		// myHeaders.append("Content-Type", "multipart/form-data");
		myHeaders.append("Accept", "*/*");

		var requestOptions = {
			method: 'DELETE',
			headers: myHeaders,
			redirect: 'follow'
		};

		fetch("https://freeco-api.herokuapp.com/api/listings/" + this.state.slug + "/photos/" + this.state.ids[room], requestOptions)
		.then(response => response.text())
		.then(result => {
			let photos = this.state.photos;
			delete photos[room];
			this.setState({
				preventBtn: false,
				photos: photos
			});
			console.log(result);
		})
		.catch(error => console.log('error', error));
	}

	leavePage = (e) => {
		let decision = e.currentTarget.value;
		if(decision === "yes"){
      if(localStorage.getItem("previous") === "dashboard"){
        window.location.href = "/dashboard/?slug=" + this.state.slug;
      } else {
        localStorage.setItem("prevoius", "listing");
        window.location.href = "/availability";
      }
		} else {
			document.getElementById("leaveModal").style.display = "none";
		}
	}

	goToPhotoshoot = () => {
		window.location.href = "/photoshoot";
	}

	render() {
    let displayPage = null;
    let previousPage = "availability page";
    if(localStorage.getItem("previous") === "dashboard"){
      previousPage = "dashboard";
    }
		switch(this.state.page){
			case 1:
				displayPage =
					<div>
						<div className="pageHeading">
							Are you sheltering in place due to Coronavirus?
						</div>
						<div className="pageContent">
							If so, let us know and we can get your advert on Rightmove with a short video call.
						</div>
						<button className="radio" value="Yes" onClick={this.updateSheltering}>Yes</button><br />
						<button className="radio" value="No" onClick={this.updateSheltering}>No</button>
            <div id="leaveModal" className="modal">
							<div className="modalContent">
								<div className="pageHeading">
									Go back to the {previousPage}?
								</div>
								<button className="radio" value="yes" onClick={this.leavePage}>Yes</button><br/>
								<button className="radio" value="no" onClick={this.leavePage}>No</button>
							</div>
						</div>
					</div>
				break;

			case 2:
				displayPage =
					<div>
						<div className="pageHeading">
							Add photos to your advert
						</div>
						<div className="pageContent">
							Adverts with photos receive up to 200% more enquiries from potential buyers.
							<br/><br/>
							This does not have to be done now, you can add photos at a later date.
							If you wish to skip this stage, 
							&nbsp;<span className="link" onClick={this.nextPage}>click here</span>
						</div>
						{/* <div className="blackBar" onClick={this.goToPhotoshoot}>
							Get <span className="colorText">professional photos</span> in just 24hrs
							<span className="floatX">+</span>
						</div> */}
						<div className="photoUploads">
							<div className="photoUpload">
								<div className="photoHeading">
									Exterior of the property
								</div>
								<div className="photoSpace">
									<img src={this.state.photos.exterior} className="uploadedPhoto"></img>
								</div>
								{/* <div className="photoMagicBar">
									Free photo magic
								</div> */}
								<br/>
								
								{
									this.state.photos.exterior === undefined
									?
									<div>
										<label for="exterior-upload" className="replacePhoto">
											Choose a photo
										</label>
										<input onChange={(e) => this.uploadPhoto(e, "exterior")} accept=".png, .jpg, .jpeg, .PNG, .JPG, JPEG" className="fileUpload" id="exterior-upload" type="file"/>
									</div>
									:
									<label onClick={() => this.deletePhoto("exterior")} className="replacePhoto">Remove</label>
								}
							</div>

							<div className="photoUpload">
								<div className="photoHeading">
									Master bedroom
								</div>
								<div className="photoSpace">
									<img src={this.state.photos.masterBedroom} className="uploadedPhoto"></img>
								</div>
								{/* <div className="photoMagicBar">
									Free photo magic
								</div> */}
								<br/>
								{
									this.state.photos.masterBedroom === undefined
									?
									<div>
										<label for="master-bedroom-upload" className="replacePhoto">
											Choose a photo
										</label>
										<input onChange={(e) => this.uploadPhoto(e, "masterBedroom")} className="fileUpload" id="master-bedroom-upload" type="file"/>
									</div>
									:
									<label onClick={() => this.deletePhoto("masterBedroom")} className="replacePhoto">Remove</label>
								}
								
							</div>

							<div className="photoUpload" style={{display: this.state.nBeds < 2 ? "none" : "block"}}>
								<div className="photoHeading">
									2nd bedroom
								</div>
								<div className="photoSpace">
									<img src={this.state.photos.bedroom2} className="uploadedPhoto"></img>
								</div>
								{/* <div className="photoMagicBar">
									Free photo magic
								</div> */}
								<br/>
								{
									this.state.photos.bedroom2 === undefined
									?
									<div>
										<label for="second-bedroom-upload" className="replacePhoto">
											Choose a photo
										</label>
										<input onChange={(e) => this.uploadPhoto(e, "bedroom2")} className="fileUpload" id="second-bedroom-upload" type="file"/>
									</div>
									:
									<label onClick={() => this.deletePhoto("bedroom2")} className="replacePhoto">Remove</label>
								}
							</div>

							<div className="photoUpload" style={{display: this.state.nBeds < 3 ? "none" : "block"}}>
								<div className="photoHeading">
									3rd bedroom
								</div>
								<div className="photoSpace">
									<img src={this.state.photos.bedroom3} className="uploadedPhoto"></img>
								</div>
								{/* <div className="photoMagicBar">
									Free photo magic
								</div> */}
								<br/>
								{
									this.state.photos.bedroom3 === undefined
									?
									<div>
										<label for="third-bedroom-upload" className="replacePhoto">
											Choose a photo
										</label>
										<input onChange={(e) => this.uploadPhoto(e, "bedroom3")} className="fileUpload" id="third-bedroom-upload" type="file"/>
									</div>
									:
									<label onClick={() => this.deletePhoto("bedroom3")} className="replacePhoto">Remove</label>
								}
							</div>

							<div className="photoUpload" style={{display: this.state.nBeds < 4 ? "none" : "block"}}>
								<div className="photoHeading">
									4th bedroom
								</div>
								<div className="photoSpace">
									<img src={this.state.photos.bedroom4} className="uploadedPhoto"></img>
								</div>
								{/* <div className="photoMagicBar">
									Free photo magic
								</div> */}
								<br/>
								{
									this.state.photos.bedroom4 === undefined
									?
									<div>
										<label for="fourth-bedroom-upload" className="replacePhoto">
											Choose a photo
										</label>
										<input onChange={(e) => this.uploadPhoto(e, "bedroom4")} className="fileUpload" id="fourth-bedroom-upload" type="file"/>
									</div>
									:
									<label onClick={() => this.deletePhoto("bedroom4")} className="replacePhoto">Remove</label>
								}
							</div>

							<div className="photoUpload" style={{display: this.state.nBeds < 5 ? "none" : "block"}}>
								<div className="photoHeading">
									5th bedroom
								</div>
								<div className="photoSpace">
									<img src={this.state.photos.bedroom5} className="uploadedPhoto"></img>
								</div>
								{/* <div className="photoMagicBar">
									Free photo magic
								</div> */}
								<br/>
								{
									this.state.photos.bedroom5 === undefined
									?
									<div>
										<label for="fifth-bedroom-upload" className="replacePhoto">
											Choose a photo
										</label>
										<input onChange={(e) => this.uploadPhoto(e, "bedroom5")} className="fileUpload" id="fifth-bedroom-upload" type="file"/>
									</div>
									:
									<label onClick={() => this.deletePhoto("bedroom5")} className="replacePhoto">Remove</label>
								}
							</div>

							<div className="photoUpload" style={{display: this.state.nBeds < 6 ? "none" : "block"}}>
								<div className="photoHeading">
									6th bedroom
								</div>
								<div className="photoSpace">
									<img src={this.state.photos.bedroom6} className="uploadedPhoto"></img>
								</div>
								{/* <div className="photoMagicBar">
									Free photo magic
								</div> */}
								<br/>
								{
									this.state.photos.bedroom6 === undefined
									?
									<div>
										<label for="sixth-bedroom-upload" className="replacePhoto">
											Choose a photo
										</label>
										<input onChange={(e) => this.uploadPhoto(e, "bedroom6")} className="fileUpload" id="sixth-bedroom-upload" type="file"/>
									</div>
									:
									<label onClick={() => this.deletePhoto("bedroom6")} className="replacePhoto">Remove</label>
								}
							</div>

							<div className="photoUpload" style={{display: this.state.nBeds < 7 ? "none" : "block"}}>
								<div className="photoHeading">
									7th bedroom
								</div>
								<div className="photoSpace">
									<img src={this.state.photos.bedroom7} className="uploadedPhoto"></img>
								</div>
								{/* <div className="photoMagicBar">
									Free photo magic
								</div> */}
								<br/>
								{
									this.state.photos.bedroom7 === undefined
									?
									<div>
										<label for="seventh-bedroom-upload" className="replacePhoto">
											Choose a photo
										</label>
										<input onChange={(e) => this.uploadPhoto(e, "bedroom7")} className="fileUpload" id="seventh-bedroom-upload" type="file"/>
									</div>
									:
									<label onClick={() => this.deletePhoto("bedroom7")} className="replacePhoto">Remove</label>
								}
							</div>

							<div className="photoUpload" style={{display: this.state.nBeds < 8 ? "none" : "block"}}>
								<div className="photoHeading">
									8th bedroom
								</div>
								<div className="photoSpace">
									<img src={this.state.photos.bedroom8} className="uploadedPhoto"></img>
								</div>
								{/* <div className="photoMagicBar">
									Free photo magic
								</div> */}
								<br/>
								{
									this.state.photos.bedroom8 === undefined
									?
									<div>
										<label for="eighth-bedroom-upload" className="replacePhoto">
											Choose a photo
										</label>
										<input onChange={(e) => this.uploadPhoto(e, "bedroom8")} className="fileUpload" id="eighth-bedroom-upload" type="file"/>
									</div>
									:
									<label onClick={() => this.deletePhoto("bedroom8")} className="replacePhoto">Remove</label>
								}
							</div>

							<div className="photoUpload" style={{display: this.state.nBeds < 9 ? "none" : "block"}}>
								<div className="photoHeading">
									9th bedroom
								</div>
								<div className="photoSpace">
									<img src={this.state.photos.bedroom9} className="uploadedPhoto"></img>
								</div>
								{/* <div className="photoMagicBar">
									Free photo magic
								</div> */}
								<br/>
								{
									this.state.photos.bedroom9 === undefined
									?
									<div>
										<label for="ninth-bedroom-upload" className="replacePhoto">
											Choose a photo
										</label>
										<input onChange={(e) => this.uploadPhoto(e, "bedroom9")} className="fileUpload" id="ninth-bedroom-upload" type="file"/>
									</div>
									:
									<label onClick={() => this.deletePhoto("bedroom9")} className="replacePhoto">Remove</label>
								}
							</div>

							<div className="photoUpload" style={{display: this.state.nBaths < 1 ? "none" : "block"}}>
								<div className="photoHeading">
									Bathroom 1
								</div>
								<div className="photoSpace">
									<img src={this.state.photos.bathroom1} className="uploadedPhoto"></img>
								</div>
								{/* <div className="photoMagicBar">
									Free photo magic
								</div> */}
								<br/>
								{
									this.state.photos.bathroom1 === undefined
									?
									<div>
										<label for="bathroom1-upload" className="replacePhoto">
											Choose a photo
										</label>
										<input onChange={(e) => this.uploadPhoto(e, "bathroom1")} className="fileUpload" id="bathroom1-upload" type="file"/>
									</div>
									:
									<label onClick={() => this.deletePhoto("bathroom1")} className="replacePhoto">Remove</label>
								}
							</div>

							<div className="photoUpload" style={{display: this.state.nBaths < 2 ? "none" : "block"}}>
								<div className="photoHeading">
									Bathroom 2
								</div>
								<div className="photoSpace">
									<img src={this.state.photos.bathroom2} className="uploadedPhoto"></img>
								</div>
								{/* <div className="photoMagicBar">
									Free photo magic
								</div> */}
								<br/>
								{
									this.state.photos.bathroom2 === undefined
									?
									<div>
										<label for="bathroom2-upload" className="replacePhoto">
											Choose a photo
										</label>
										<input onChange={(e) => this.uploadPhoto(e, "bathroom2")} className="fileUpload" id="bathroom2-upload" type="file"/>
									</div>
									:
									<label onClick={() => this.deletePhoto("bathroom2")} className="replacePhoto">Remove</label>
								}
							</div>

							<div className="photoUpload" style={{display: this.state.nBaths < 3 ? "none" : "block"}}>
								<div className="photoHeading">
									Bathroom 3
								</div>
								<div className="photoSpace">
									<img src={this.state.photos.bathroom3} className="uploadedPhoto"></img>
								</div>
								{/* <div className="photoMagicBar">
									Free photo magic
								</div> */}
								<br/>
								{
									this.state.photos.bathroom3 === undefined
									?
									<div>
										<label for="bathroom3-upload" className="replacePhoto">
											Choose a photo
										</label>
										<input onChange={(e) => this.uploadPhoto(e, "bathroom3")} className="fileUpload" id="bathroom3-upload" type="file"/>
									</div>
									:
									<label onClick={() => this.deletePhoto("bathroom3")} className="replacePhoto">Remove</label>
								}
							</div>

							<div className="photoUpload" style={{display: this.state.nBaths < 4 ? "none" : "block"}}>
								<div className="photoHeading">
									Bathroom 4
								</div>
								<div className="photoSpace">
									<img src={this.state.photos.bathroom4} className="uploadedPhoto"></img>
								</div>
								{/* <div className="photoMagicBar">
									Free photo magic
								</div> */}
								<br/>
								{
									this.state.photos.bathroom4 === undefined
									?
									<div>
										<label for="bathroom4-upload" className="replacePhoto">
											Choose a photo
										</label>
										<input onChange={(e) => this.uploadPhoto(e, "bathroom4")} className="fileUpload" id="bathroom4-upload" type="file"/>
									</div>
									:
									<label onClick={() => this.deletePhoto("bathroom4")} className="replacePhoto">Remove</label>
								}
							</div>

							<div className="photoUpload" style={{display: this.state.nBaths < 5 ? "none" : "block"}}>
								<div className="photoHeading">
									Bathroom 5
								</div>
								<div className="photoSpace">
									<img src={this.state.photos.bathroom5} className="uploadedPhoto"></img>
								</div>
								{/* <div className="photoMagicBar">
									Free photo magic
								</div> */}
								<br/>
								{
									this.state.photos.bathroom5 === undefined
									?
									<div>
										<label for="bathroom5-upload" className="replacePhoto">
											Choose a photo
										</label>
										<input onChange={(e) => this.uploadPhoto(e, "bathroom5")} className="fileUpload" id="bathroom5-upload" type="file"/>
									</div>
									:
									<label onClick={() => this.deletePhoto("bathroom5")} className="replacePhoto">Remove</label>
								}
							</div>

							<div className="photoUpload" style={{display: this.state.nBaths < 6 ? "none" : "block"}}>
								<div className="photoHeading">
									Bathroom 6
								</div>
								<div className="photoSpace">
									<img src={this.state.photos.bathroom6} className="uploadedPhoto"></img>
								</div>
								{/* <div className="photoMagicBar">
									Free photo magic
								</div> */}
								<br/>
								{
									this.state.photos.bathroom6 === undefined
									?
									<div>
										<label for="bathroom6-upload" className="replacePhoto">
											Choose a photo
										</label>
										<input onChange={(e) => this.uploadPhoto(e, "bathroom6")} className="fileUpload" id="bathroom6-upload" type="file"/>
									</div>
									:
									<label onClick={() => this.deletePhoto("bathroom6")} className="replacePhoto">Remove</label>
								}
							</div>

							<div className="photoUpload" style={{display: this.state.nBaths < 7 ? "none" : "block"}}>
								<div className="photoHeading">
									Bathroom 7
								</div>
								<div className="photoSpace">
									<img src={this.state.photos.bathroom7} className="uploadedPhoto"></img>
								</div>
								{/* <div className="photoMagicBar">
									Free photo magic
								</div> */}
								<br/>
								{
									this.state.photos.bathroom7 === undefined
									?
									<div>
										<label for="bathroom7-upload" className="replacePhoto">
											Choose a photo
										</label>
										<input onChange={(e) => this.uploadPhoto(e, "bathroom7")} className="fileUpload" id="bathroom7-upload" type="file"/>
									</div>
									:
									<label onClick={() => this.deletePhoto("bathroom7")} className="replacePhoto">Remove</label>
								}
							</div>

							<div className="photoUpload" style={{display: this.state.nBaths < 8 ? "none" : "block"}}>
								<div className="photoHeading">
									Bathroom 8
								</div>
								<div className="photoSpace">
									<img src={this.state.photos.bathroom8} className="uploadedPhoto"></img>
								</div>
								{/* <div className="photoMagicBar">
									Free photo magic
								</div> */}
								<br/>
								{
									this.state.photos.bathroom8 === undefined
									?
									<div>
										<label for="bathroom8-upload" className="replacePhoto">
											Choose a photo
										</label>
										<input onChange={(e) => this.uploadPhoto(e, "bathroom8")} className="fileUpload" id="bathroom8-upload" type="file"/>
									</div>
									:
									<label onClick={() => this.deletePhoto("bathroom8")} className="replacePhoto">Remove</label>
								}
							</div>

							<div className="photoUpload" style={{display: this.state.nBaths < 9 ? "none" : "block"}}>
								<div className="photoHeading">
									Bathroom 9
								</div>
								<div className="photoSpace">
									<img src={this.state.photos.bathroom9} className="uploadedPhoto"></img>
								</div>
								{/* <div className="photoMagicBar">
									Free photo magic
								</div> */}
								<br/>
								{
									this.state.photos.bathroom9 === undefined
									?
									<div>
										<label for="bathroom9-upload" className="replacePhoto">
											Choose a photo
										</label>
										<input onChange={(e) => this.uploadPhoto(e, "bathroom9")} className="fileUpload" id="bathroom9-upload" type="file"/>
									</div>
									:
									<label onClick={() => this.deletePhoto("bathroom9")} className="replacePhoto">Remove</label>
								}
							</div>

							<div className="photoUpload">
								<div className="photoHeading">
									Kitchen
								</div>
								<div className="photoSpace">
									<img src={this.state.photos.kitchen} className="uploadedPhoto"></img>
								</div>
								{/* <div className="photoMagicBar">
									Free photo magic
								</div> */}
								<br/>
								{
									this.state.photos.kitchen === undefined
									?
									<div>
										<label for="kitchen-upload" className="replacePhoto">
											Choose a photo
										</label>
										<input onChange={(e) => this.uploadPhoto(e, "kitchen")} className="fileUpload" id="kitchen-upload" type="file"/>
									</div>
									:
									<label onClick={() => this.deletePhoto("kitchen")} className="replacePhoto">Remove</label>
								}
							</div>

							<div className="photoUpload">
								<div className="photoHeading">
									Living room
								</div>
								<div className="photoSpace">
									<img src={this.state.photos.livingroom} className="uploadedPhoto"></img>
								</div>
								{/* <div className="photoMagicBar">
									Free photo magic
								</div> */}
								<br/>
								{
									this.state.photos.livingroom === undefined
									?
									<div>
										<label for="livingroom-upload" className="replacePhoto">
											Choose a photo
										</label>
										<input onChange={(e) => this.uploadPhoto(e, "livingroom")} className="fileUpload" id="livingroom-upload" type="file"/>
									</div>
									:
									<label onClick={() => this.deletePhoto("livingroom")} className="replacePhoto">Remove</label>
								}
							</div>

							<div className="photoUpload">
								<div className="photoHeading">
									Floorplan
								</div>
								<div className="photoSpace">
									<img src={this.state.photos.floorplan} className="uploadedPhoto"></img>
								</div>
								{/* <div className="photoMagicBar">
									Free photo magic
								</div> */}
								<br/>
								{
									this.state.photos.floorplan === undefined
									?
									<div>
										<label for="floorplan-upload" className="replacePhoto">
											Choose a photo
										</label>
										<input onChange={(e) => this.uploadPhoto(e, "floorplan")} className="fileUpload" id="floorplan-upload" type="file"/>
									</div>
									:
									<label onClick={() => this.deletePhoto("floorplan")} className="replacePhoto">Remove</label>
								}
							</div>

							<div className="photoUpload">
								<div className="photoHeading">
									Additional room
								</div>
								<div className="photoSpace">
									<img src={this.state.photos.additionalRoom} className="uploadedPhoto"></img>
								</div>
								{/* <div className="photoMagicBar">
									Free photo magic
								</div> */}
								<br/>
								{
									this.state.photos.additionalRoom === undefined
									?
									<div>
										<label for="additionalRoom-upload" className="replacePhoto">
											Choose a photo
										</label>
										<input onChange={(e) => this.uploadPhoto(e, "additionalRoom")} className="fileUpload" id="additionalRoom-upload" type="file"/>
									</div>
									:
									<label onClick={() => this.deletePhoto("additionalRoom")} className="replacePhoto">Remove</label>
								}
							</div>
						</div>
						<button disabled={this.state.preventBtn ? "disabled" : ""} className="capBtn" onClick={this.nextPage}>{this.state.preventBtn ? "Uploading photos..." : "View my dashboard"}</button>
					</div>
				break;

			case 3:
				displayPage =
					<div>
						<div className="pageHeading">
							Your advert is now live!
						</div>
						<div className="pageContent">
							X million people can now see your home.<br/>
							Check it out in all it's glory!
						</div>
						<a href="https://zoopla.com" target="_blank">
							<button className="adBtn">
								Zoopla <img className="rightArrow" src={require("../img/icon-arrow-forward.svg")}></img>
							</button>
						</a><br/>
						<a href="https://homeshare.com" target="_blank">
							<button className="adBtn">
								homeshare <img className="rightArrow" src={require("../img/icon-arrow-forward.svg")}></img>
							</button>
						</a><br/>
						<a href="https://primelocation.com" target="_blank">
							<button className="adBtn">
								PrimeLocation <img className="rightArrow" src={require("../img/icon-arrow-forward.svg")}></img>
							</button>
						</a><br/>
						<a href="https://gumtree.com" target="_blank">
							<button className="adBtn">
								Gumtree <img className="rightArrow" src={require("../img/icon-arrow-forward.svg")}></img>
							</button>
						</a><br/>
						<a href="https://rightmove.com" target="_blank">
							<button disabled="disabled" className="adBtn mB">
								Rightmove <img className="rightArrow" src={require("../img/icon-arrow-forward.svg")}></img>
							</button>
						</a>
						<div className="pageContent" style={{marginBottom: "0"}}>
							Book a <strong>free</strong> professional photo shoot to get your advert on Rightmove!
						</div>
						<button className="capBtn" onClick={this.nextPage}>View my dashboard</button>
					</div>
				break;
		}
		return (
			<div className="listing-flow">
				<div className="navbar">
					<button className="back" onClick={this.prevPage}>	
						<img className="backArrow" src={require("../img/icon-arrow-back.svg")}></img>	
						<div className="backText">		
							Back	
						</div>
					</button>
					<img className="logo" src={require("../img/logo.svg")}></img>
				</div>
				<div className="page">
					{displayPage}
				</div>
			</div>
		);
	}
}

export default PhotoUpload;