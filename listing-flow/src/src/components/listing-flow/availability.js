import React from 'react';

class Availability extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			page: 1,
			availableSetting: null,
			availability: {
				"mon": {"am": false,"pm": false,"eve": false},
				"tue": {"am": false,"pm": false,"eve": false},
				"wed": {"am": false,"pm": false,"eve": false},
				"thurs": {"am": false,"pm": false,"eve": false},
				"fri": {"am": false,"pm": false,"eve": false},
				"sat": {"am": false,"pm": false,"eve": false},
				"sun": {"am": false,"pm": false,"eve": false}
			},
			sheltering: null,
			photos: {
			},
			nBaths: 10,
			nBeds: 10,
			listing: null,
			token: null,
			slug: null
		};
	}

	componentDidMount() {
		let user = JSON.parse(localStorage.getItem("user"));
		if(user !== null && user !== undefined){
			let token = user.token;

			var url = new URL(window.location.href);
			var slug = url.searchParams.get("slug");

			this.setState({
				token: token,
				slug: slug
			});

			var myHeaders = new Headers();
			myHeaders.append("Content-Type", "application/json");
			myHeaders.append("X-Requested-With", "XMLHttpRequest");
			myHeaders.append("Authorization", "Token " + token);

			var requestOptions = {
				method: 'GET',
				headers: myHeaders,
				redirect: 'follow'
			};

			console.log(token);

			fetch("https://freeco-api.herokuapp.com/api/listings", requestOptions)
			.then(response => response.json())
			.then(result => {
				console.log(result);
				
				var myHeaders = new Headers();
				myHeaders.append("Content-Type", "application/json");
				myHeaders.append("X-Requested-With", "XMLHttpRequest");
				myHeaders.append("Authorization", "Token " + token);

				var requestOptions = {
					method: 'GET',
					headers: myHeaders,
					redirect: 'follow'
				};

				fetch("https://freeco-api.herokuapp.com/api/listings/" + slug, requestOptions)
				.then(response => response.json())
				.then(result2 => {
					console.log(result2);

					let arr = [];

					for(let i = 0; i < result.listings.length; i++){
						arr.push(result.listings[i].slug);
					}

					console.log(arr);

					if(!arr.includes(slug.toString())){
						window.location.href = "/listing";
					}

					let listing = result2;
			
					if(listing.listing !== undefined || listing.listing !== null !== listing.listing !== ""){
						let nBaths = listing.listing.propBaths;
						let nBeds = listing.listing.propBeds;
			
						this.setState({
							nBaths: nBaths,
							nBeds: nBeds,
							listing: listing,
							availability: listing.listing.viewingAvailability
						});
					}
				})
				.catch(error => console.log('error', error));
			})
			.catch(error => console.log('error', error));
		} else {
			window.location.href = "/login";
		}
	}

	nextPage = () => {
		let page = this.state.page;
		if(page === 1){
			page = 2;
		} else if(page === 2){
			this.pushAvailability();
		}

		this.setState({
			page: page
		});
	}

	prevPage = () => {
		let page = this.state.page;

		if(page === 2){
			page = 1;
		} else if(page === 1){
			document.getElementById("leaveModal").style.display = "block";
			page = 1;
		}

		this.setState({
			page: page
		});
	}

	updateAvailability = (e) => {
		let time = e.currentTarget.value;
		let page = 2;
		let availability = this.state.availability;

		if(time === "Anytime"){
			availability = {
				"mon": {"am": true,"pm": true,"eve": true},
				"tue": {"am": true,"pm": true,"eve": true},
				"wed": {"am": true,"pm": true,"eve": true},
				"thurs": {"am": true,"pm": true,"eve": true},
				"fri": {"am": true,"pm": true,"eve": true},
				"sat": {"am": true,"pm": true,"eve": true},
				"sun": {"am": true,"pm": true,"eve": true}
			};
		} else if(time === "Weekends"){
			availability = {
				"mon": {"am": false,"pm": false,"eve": false},
				"tue": {"am": false,"pm": false,"eve": false},
				"wed": {"am": false,"pm": false,"eve": false},
				"thurs": {"am": false,"pm": false,"eve": false},
				"fri": {"am": false,"pm": false,"eve": false},
				"sat": {"am": true,"pm": true,"eve": true},
				"sun": {"am": true,"pm": true,"eve": true}
			};
		} else if(time === "Evenings"){
			availability = {
				"mon": {"am": false,"pm": false,"eve": true},
				"tue": {"am": false,"pm": false,"eve": true},
				"wed": {"am": false,"pm": false,"eve": true},
				"thurs": {"am": false,"pm": false,"eve": true},
				"fri": {"am": false,"pm": false,"eve": true},
				"sat": {"am": false,"pm": false,"eve": true},
				"sun": {"am": false,"pm": false,"eve": true},
			};
		} else if(time === "Evenings-and-weekends"){
			availability = {
				"mon": {"am": false,"pm": false,"eve": true},
				"tue": {"am": false,"pm": false,"eve": true},
				"wed": {"am": false,"pm": false,"eve": true},
				"thurs": {"am": false,"pm": false,"eve": true},
				"fri": {"am": false,"pm": false,"eve": true},
				"sat": {"am": true,"pm": true,"eve": true},
				"sun": {"am": true,"pm": true,"eve": true},
			};
		} else if(time === "Specify"){
			page = 2;
		}

		this.setState({
			availability: availability,
			availableSetting: time
		}, () => {
			if(time !== "Specify"){
				this.pushAvailability();
			} else {
				this.setState({
					page: page
				});
			}
		});
	}

	updateSheltering = (e) => {
		let decision = e.currentTarget.value;
		this.setState({
			sheltering: decision,
			page: 3
		});
	}

	clickCheck = (e) => {
		let x = e.currentTarget.value;
		let av = this.state.availability;
		if(e.currentTarget.value.toString().slice(e.currentTarget.value.length - 4, e.currentTarget.value.length) === "-img"){
			document.getElementById(e.currentTarget.value).style.display = "none";
			document.getElementById(e.currentTarget.value.toString().slice(0,e.currentTarget.value.length - 4)).style.display = "block";
			if(x.slice(0,3) === "mon"){
				if(x.slice(4, x.length - 4) === "am"){av.mon.am = false;} 
				else if(x.slice(4, x.length - 4) === "pm"){av.mon.pm = false;} 
				else if(x.slice(4, x.length - 4) === "eve"){av.mon.eve = false;}
			} else if(x.slice(0,3) === "tue"){
				if(x.slice(4, x.length - 4) === "am"){av.tue.am = false;} 
				else if(x.slice(4, x.length - 4) === "pm"){av.tue.pm = false;} 
				else if(x.slice(4, x.length - 4) === "eve"){av.tue.eve = false;}
			} else if(x.slice(0,3) === "wed"){
				if(x.slice(4, x.length - 4) === "am"){av.wed.am = false;} 
				else if(x.slice(4, x.length - 4) === "pm"){av.wed.pm = false;} 
				else if(x.slice(4, x.length - 4) === "eve"){av.wed.eve = false;}
			} else if(x.slice(0,3) === "thu"){
				if(x.slice(4, x.length - 4) === "am"){av.thurs.am = false;} 
				else if(x.slice(4, x.length - 4) === "pm"){av.thurs.pm = false;} 
				else if(x.slice(4, x.length - 4) === "eve"){av.thurs.eve = false;}
			} else if(x.slice(0,3) === "fri"){
				if(x.slice(4, x.length - 4) === "am"){av.fri.am = false;} 
				else if(x.slice(4, x.length - 4) === "pm"){av.fri.pm = false;} 
				else if(x.slice(4, x.length - 4) === "eve"){av.fri.eve = false;}
			} else if(x.slice(0,3) === "sat"){
				if(x.slice(4, x.length - 4) === "am"){av.sat.am = false;} 
				else if(x.slice(4, x.length - 4) === "pm"){av.sat.pm = false;} 
				else if(x.slice(4, x.length - 4) === "eve"){av.sat.eve = false;}
			} else if(x.slice(0,3) === "sun"){
				if(x.slice(4, x.length - 4) === "am"){av.sun.am = false;} 
				else if(x.slice(4, x.length - 4) === "pm"){av.sun.pm = false;} 
				else if(x.slice(4, x.length - 4) === "eve"){av.sun.eve = false;}
			}
		} else {
			document.getElementById(e.currentTarget.value).style.display = "none";
			document.getElementById(e.currentTarget.value + "-img").style.display = "block";
			if(x.slice(0,3) === "mon"){
				if(x.slice(4, x.length) === "am"){av.mon.am = true;} 
				else if(x.slice(4, x.length) === "pm"){av.mon.pm = true;} 
				else if(x.slice(4, x.length) === "eve"){av.mon.eve = true;}
			} else if(x.slice(0,3) === "tue"){
				if(x.slice(4, x.length) === "am"){av.tue.am = true;} 
				else if(x.slice(4, x.length) === "pm"){av.tue.pm = true;} 
				else if(x.slice(4, x.length) === "eve"){av.tue.eve = true;}
			} else if(x.slice(0,3) === "wed"){
				if(x.slice(4, x.length) === "am"){av.wed.am = true;} 
				else if(x.slice(4, x.length) === "pm"){av.wed.pm = true;} 
				else if(x.slice(4, x.length) === "eve"){av.wed.eve = true;}
			} else if(x.slice(0,3) === "thu"){
				if(x.slice(4, x.length) === "am"){av.thurs.am = true;} 
				else if(x.slice(4, x.length) === "pm"){av.thurs.pm = true;} 
				else if(x.slice(4, x.length) === "eve"){av.thurs.eve = true;}
			} else if(x.slice(0,3) === "fri"){
				if(x.slice(4, x.length) === "am"){av.fri.am = true;} 
				else if(x.slice(4, x.length) === "pm"){av.fri.pm = true;} 
				else if(x.slice(4, x.length) === "eve"){av.fri.eve = true;}
			} else if(x.slice(0,3) === "sat"){
				if(x.slice(4, x.length) === "am"){av.sat.am = true;} 
				else if(x.slice(4, x.length) === "pm"){av.sat.pm = true;} 
				else if(x.slice(4, x.length) === "eve"){av.sat.eve = true;}
			} else if(x.slice(0,3) === "sun"){
				if(x.slice(4, x.length) === "am"){av.sun.am = true;} 
				else if(x.slice(4, x.length) === "pm"){av.sun.pm = true;} 
				else if(x.slice(4, x.length) === "eve"){av.sun.eve = true;}
			}
		}

		this.setState({
			availability: av
		});
	}

	pushAvailability = () => {
		var myHeaders = new Headers();
		myHeaders.append("Content-Type", "application/json");
		myHeaders.append("X-Requested-With", "XMLHttpRequest");
		myHeaders.append("Authorization", "Token " + this.state.token);

		var raw = JSON.stringify({"listing":{"addStreet":this.state.listing.addStreet,"addTown":this.state.listing.addTown,"propType":this.state.listing.propType,"description":this.state.listing.description,"viewingAvailability":this.state.availability}});

		var requestOptions = {
			method: 'PUT',
			headers: myHeaders,
			body: raw,
			redirect: 'follow'
		};

		fetch("https://freeco-api.herokuapp.com/api/listings/" + this.state.slug, requestOptions)
		.then(response => response.text())
		.then(result => {
			console.log(result);
			localStorage.setItem("listing", result);
			if(localStorage.getItem("previous") === "dashboard"){
				localStorage.setItem("previous", "availability");
				window.location.href = "/dashboard/?slug=" + this.state.slug;
			} else {
				localStorage.setItem("previous", "availability");
				window.location.href = "/photo-upload/?slug=" + this.state.slug;
			}
		})
		.catch(error => console.log('error', error));
	}

	leavePage = (e) => {
		let decision = e.currentTarget.value;
		if(decision === "yes"){
			if(localStorage.getItem("previous") === "dashboard"){
				window.location.href = "/dashboard/?slug=" + this.state.slug;
			} else {
				window.location.href = "/listing";
			}
		} else {
			document.getElementById("leaveModal").style.display = "none";
		}
	}

	goToPhotoshoot = () => {
		window.location.href = "/photoshoot";
	}

	render() {
		let displayPage = null;
		let previousPage = "listing page";
		if(localStorage.getItem("previous") === "dashboard"){
			previousPage = "dashboard";
		}
		
		switch(this.state.page){
			case 1:
				displayPage = 
					<div>	
						<div className="pageHeading">		
							When would you prefer to do property viewings?	
						</div>	
						<div className="pageContent">		
							You can always update your preferences, and we'll check with you before we confirm each booking.	
						</div>	
						<button className="radio" value="Anytime" onClick={this.updateAvailability}>Anytime <small>(Every day, 8am - 8pm)</small></button><br />	
						<button className="radio" value="Weekends" onClick={this.updateAvailability}>Weekends only <small>(8am - 6pm)</small></button><br />	
						<button className="radio" value="Evenings" onClick={this.updateAvailability}>Evenings only <small>(After 6pm)</small></button><br />	
						<button className="radio" value="Evenings-and-weekends" onClick={this.updateAvailability}>Evenings and weekends</button><br />	
						<button className="radio" value="Specify" onClick={this.updateAvailability}>Or specify your availability</button>
						<div id="leaveModal" className="modal">
							<div className="modalContent">
								<div className="pageHeading">
									Go back to the {previousPage}?
								</div>
								<button className="radio" value="yes" onClick={this.leavePage}>Yes</button><br/>
								<button className="radio" value="no" onClick={this.leavePage}>No</button>
							</div>
						</div>
					</div>
					
				break;

			case 2:
				displayPage =
					<div>
						<div className="pageHeading">
							Set your availability for property viewings
						</div>
						<div className="pageContent">
							We'll always check with you before we confirm each booking, and you can update your availability at any time.
						</div>
						<table>
							<thead>
								<td></td>
								<td><strong>6am-Noon</strong><br/>Morning</td>
								<td><strong>Noon-6pm</strong><br/>Afternoon</td>
								<td><strong>6pm-8pm</strong><br/>Evening</td>
							</thead>
							<tbody>
								<tr>
									<td>Mon</td>
									<td>
										<button style={{display: this.state.availability.mon.am === true ? "none" : "block"}} id="mon-am" value="mon-am" onClick={this.clickCheck} className="check">Morning</button>
										<button style={{display: this.state.availability.mon.am === true ? "block" : "none"}} id="mon-am-img" value="mon-am-img" onClick={this.clickCheck} className="check check-img">
											<img src={require("../img/icon-tick.svg")}></img>
										</button>
									</td>
									<td>
										<button style={{display: this.state.availability.mon.pm === true ? "none" : "block"}} id="mon-pm" value="mon-pm" onClick={this.clickCheck} className="check">Afternoon</button>
										<button style={{display: this.state.availability.mon.pm === true ? "block" : "none"}} id="mon-pm-img" value="mon-pm-img" onClick={this.clickCheck} className="check check-img">
											<img src={require("../img/icon-tick.svg")}></img>
										</button>
									</td>
									<td>
										<button style={{display: this.state.availability.mon.eve === true ? "none" : "block"}} id="mon-eve" value="mon-eve" onClick={this.clickCheck} className="check">Evening</button>
										<button style={{display: this.state.availability.mon.eve === true ? "block" : "none"}} id="mon-eve-img" value="mon-eve-img" onClick={this.clickCheck} className="check check-img">
											<img src={require("../img/icon-tick.svg")}></img>
										</button>
									</td>
								</tr>
								<tr>
									<td>Tue</td>
									<td>
										<button style={{display: this.state.availability.tue.am === true ? "none" : "block"}} id="tue-am" value="tue-am" onClick={this.clickCheck} className="check">Morning</button>
										<button style={{display: this.state.availability.tue.am === true ? "block" : "none"}} id="tue-am-img" value="tue-am-img" onClick={this.clickCheck} className="check check-img">
											<img src={require("../img/icon-tick.svg")}></img>
										</button>
									</td>
									<td>
										<button style={{display: this.state.availability.tue.pm === true ? "none" : "block"}} id="tue-pm" value="tue-pm" onClick={this.clickCheck} className="check">Afternoon</button>
										<button style={{display: this.state.availability.tue.pm === true ? "block" : "none"}} id="tue-pm-img" value="tue-pm-img" onClick={this.clickCheck} className="check check-img">
											<img src={require("../img/icon-tick.svg")}></img>
										</button>
									</td>
									<td>
										<button style={{display: this.state.availability.tue.eve === true ? "none" : "block"}} id="tue-eve" value="tue-eve" onClick={this.clickCheck} className="check">Evening</button>
										<button style={{display: this.state.availability.tue.eve === true ? "block" : "none"}} id="tue-eve-img" value="tue-eve-img" onClick={this.clickCheck} className="check check-img">
											<img src={require("../img/icon-tick.svg")}></img>
										</button>
									</td>
								</tr>
								<tr>
									<td>Wed</td>
									<td>
										<button style={{display: this.state.availability.wed.am === true ? "none" : "block"}} id="wed-am" value="wed-am" onClick={this.clickCheck} className="check">Morning</button>
										<button style={{display: this.state.availability.wed.am === true ? "block" : "none"}} id="wed-am-img" value="wed-am-img" onClick={this.clickCheck} className="check check-img">
											<img src={require("../img/icon-tick.svg")}></img>
										</button>
									</td>
									<td>
										<button style={{display: this.state.availability.wed.pm === true ? "none" : "block"}} id="wed-pm" value="wed-pm" onClick={this.clickCheck} className="check">Afternoon</button>
										<button style={{display: this.state.availability.wed.pm === true ? "block" : "none"}} id="wed-pm-img" value="wed-pm-img" onClick={this.clickCheck} className="check check-img">
											<img src={require("../img/icon-tick.svg")}></img>
										</button>
									</td>
									<td>
										<button style={{display: this.state.availability.wed.eve === true ? "none" : "block"}} id="wed-eve" value="wed-eve" onClick={this.clickCheck} className="check">Evening</button>
										<button style={{display: this.state.availability.wed.eve === true ? "block" : "none"}} id="wed-eve-img" value="wed-eve-img" onClick={this.clickCheck} className="check check-img">
											<img src={require("../img/icon-tick.svg")}></img>
										</button>
									</td>
								</tr>
								<tr>
									<td>Thu</td>
									<td>
										<button style={{display: this.state.availability.thurs.am === true ? "none" : "block"}} id="thu-am" value="thu-am" onClick={this.clickCheck} className="check">Morning</button>
										<button style={{display: this.state.availability.thurs.am === true ? "block" : "none"}} id="thu-am-img" value="thu-am-img" onClick={this.clickCheck} className="check check-img">
											<img src={require("../img/icon-tick.svg")}></img>
										</button>
									</td>
									<td>
										<button style={{display: this.state.availability.thurs.pm === true ? "none" : "block"}} id="thu-pm" value="thu-pm" onClick={this.clickCheck} className="check">Afternoon</button>
										<button style={{display: this.state.availability.thurs.pm === true ? "block" : "none"}} id="thu-pm-img" value="thu-pm-img" onClick={this.clickCheck} className="check check-img">
											<img src={require("../img/icon-tick.svg")}></img>
										</button>
									</td>
									<td>
										<button style={{display: this.state.availability.thurs.eve === true ? "none" : "block"}} id="thu-eve" value="thu-eve" onClick={this.clickCheck} className="check">Evening</button>
										<button style={{display: this.state.availability.thurs.eve === true ? "block" : "none"}} id="thu-eve-img" value="thu-eve-img" onClick={this.clickCheck} className="check check-img">
											<img src={require("../img/icon-tick.svg")}></img>
										</button>
									</td>
								</tr>
								<tr>
									<td>Fri</td>
									<td>
										<button style={{display: this.state.availability.fri.am === true ? "none" : "block"}} id="fri-am" value="fri-am" onClick={this.clickCheck} className="check">Morning</button>
										<button style={{display: this.state.availability.fri.am === true ? "block" : "none"}} id="fri-am-img" value="fri-am-img" onClick={this.clickCheck} className="check check-img">
											<img src={require("../img/icon-tick.svg")}></img>
										</button>
									</td>
									<td>
										<button style={{display: this.state.availability.fri.pm === true ? "none" : "block"}} id="fri-pm" value="fri-pm" onClick={this.clickCheck} className="check">Afternoon</button>
										<button style={{display: this.state.availability.fri.pm === true ? "block" : "none"}} id="fri-pm-img" value="fri-pm-img" onClick={this.clickCheck} className="check check-img">
											<img src={require("../img/icon-tick.svg")}></img>
										</button>
									</td>
									<td>
										<button style={{display: this.state.availability.fri.eve === true ? "none" : "block"}} id="fri-eve" value="fri-eve" onClick={this.clickCheck} className="check">Evening</button>
										<button style={{display: this.state.availability.fri.eve === true ? "block" : "none"}} id="fri-eve-img" value="fri-eve-img" onClick={this.clickCheck} className="check check-img">
											<img src={require("../img/icon-tick.svg")}></img>
										</button>
									</td>
								</tr>
								<tr>
									<td>Sat</td>
									<td>
										<button style={{display: this.state.availability.sat.am === true ? "none" : "block"}} id="sat-am" value="sat-am" onClick={this.clickCheck} className="check">Morning</button>
										<button style={{display: this.state.availability.sat.am === true ? "block" : "none"}} id="sat-am-img" value="sat-am-img" onClick={this.clickCheck} className="check check-img">
											<img src={require("../img/icon-tick.svg")}></img>
										</button>
									</td>
									<td>
										<button style={{display: this.state.availability.sat.pm === true ? "none" : "block"}} id="sat-pm" value="sat-pm" onClick={this.clickCheck} className="check">Afternoon</button>
										<button style={{display: this.state.availability.sat.pm === true ? "block" : "none"}} id="sat-pm-img" value="sat-pm-img" onClick={this.clickCheck} className="check check-img">
											<img src={require("../img/icon-tick.svg")}></img>
										</button>
									</td>
									<td>
										<button style={{display: this.state.availability.sat.eve === true ? "none" : "block"}} id="sat-eve" value="sat-eve" onClick={this.clickCheck} className="check">Evening</button>
										<button style={{display: this.state.availability.sat.eve === true ? "block" : "none"}} id="sat-eve-img" value="sat-eve-img" onClick={this.clickCheck} className="check check-img">
											<img src={require("../img/icon-tick.svg")}></img>
										</button>
									</td>
								</tr>
								<tr>
									<td>Sun</td>
									<td>
										<button style={{display: this.state.availability.sun.am === true ? "none" : "block"}} id="sun-am" value="sun-am" onClick={this.clickCheck} className="check">Morning</button>
										<button style={{display: this.state.availability.sun.am === true ? "block" : "none"}} id="sun-am-img" value="sun-am-img" onClick={this.clickCheck} className="check check-img">
											<img src={require("../img/icon-tick.svg")}></img>
										</button>
									</td>
									<td>
										<button style={{display: this.state.availability.sun.pm === true ? "none" : "block"}} id="sun-pm" value="sun-pm" onClick={this.clickCheck} className="check">Afternoon</button>
										<button style={{display: this.state.availability.sun.pm === true ? "block" : "none"}} id="sun-pm-img" value="sun-pm-img" onClick={this.clickCheck} className="check check-img">
											<img src={require("../img/icon-tick.svg")}></img>
										</button>
									</td>
									<td>
										<button style={{display: this.state.availability.sun.eve === true ? "none" : "block"}} id="sun-eve" value="sun-eve" onClick={this.clickCheck} className="check">Evening</button>
										<button style={{display: this.state.availability.sun.eve === true ? "block" : "none"}} id="sun-eve-img" value="sun-eve-img" onClick={this.clickCheck} className="check check-img">
											<img src={require("../img/icon-tick.svg")}></img>
										</button>
									</td>
								</tr>
							</tbody>
						</table>
						<button onClick={this.nextPage}>Set availability</button>
					</div>
				break;
		}

		return (
			<div className="listing-flow">
				<div className="navbar">
					<button className="back" onClick={this.prevPage}>	
						<img className="backArrow" src={require("../img/icon-arrow-back.svg")}></img>	
						<div className="backText">		
							Back	
						</div>
					</button>
					<img className="logo" src={require("../img/logo.svg")}></img>
				</div>
				<div className="page">
					{displayPage}
				</div>
			</div>
		);
	}
}

export default Availability;
