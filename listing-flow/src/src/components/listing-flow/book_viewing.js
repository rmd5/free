import React from 'react';
require("datejs");

class BookViewing extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			page: 1,
			listing: null,
			dayRequest: 1,
			card: null,
			chosenSlot: null,
			name: null,
			number: null,
			securityCode: null,
			slug: null,
			error: null,
			scroll: 0,
		};
	}

	componentDidMount() {
		var myHeaders = new Headers();
		myHeaders.append("Content-Type", "application/json");
		myHeaders.append("X-Requested-With", "XMLHttpRequest");
		// myHeaders.append("Authorization", "Token " + token);

		var requestOptions = {
			method: 'GET',
			headers: myHeaders,
			redirect: 'follow'
		};

		var url = new URL(window.location.href);
		var slug = url.searchParams.get("slug");
		console.log(slug);

		fetch("https://freeco-api.herokuapp.com/api/listings/" + slug, requestOptions)
			.then(response => response.json())
			.then(result => {
				console.log(result);
				this.setState({
					listing: result.listing,
					slug: slug
				}, () => {
					this.generateButtons("next");
				});
			})
			.catch(error => {
				this.setState({
					error: "Unknown Property",
					slug: slug
				});
				console.log('error', error);
			});
	}

	nextPage = () => {
		let page = this.state.page;
		page += 1;
		this.setState({
			page: page
		});
	}

	prevPage = () => {
		let page = this.state.page;
		page -= 1;
		if(page < 1){
			window.history.back();
		}

		this.setState({
			page: page
		});
	}

	generateButtons = (direction) => {
		if(this.state.listing !== null){
			let long = Date.today().addDays(this.state.dayRequest).toString("dddd dS MMMM");
			let abbv = Date.today().addDays(this.state.dayRequest).toString("ddd").toLowerCase();
			if(abbv === "thu"){
				abbv = "thurs";
			}

			let listing = this.state.listing;

			let availabilityOnDay = listing.viewingAvailability[abbv];

			if(availabilityOnDay.am === false && availabilityOnDay.pm === false && availabilityOnDay.eve === false){
				let next = parseInt(this.state.dayRequest) + 1;
				if(direction === "past"){
					next = parseInt(this.state.dayRequest) - 1;
				}
				this.setState({
					dayRequest: next
				}, () => {
					this.generateButtons(direction);
				});
			} else {
				let buttons = [];
				if(availabilityOnDay.am === true){
					for(let i = 8; i <= 12; i++){
						let btn = <button onClick={() => this.updateChosenSlot(i, 0)} className="viewingRadio" style={{backgroundColor: this.state.chosenSlot === Date.today().addDays(this.state.dayRequest).addHours(i + 1).toISOString() ? "#AAFF00" : "#F0F0F0"}}>{i}:00</button>
						buttons.push(btn);
						if(i !== 12){
							let btn2 = <button onClick={() => this.updateChosenSlot(i, 30)} className="viewingRadio" style={{backgroundColor: this.state.chosenSlot === Date.today().addDays(this.state.dayRequest).addHours(i + 1).addMinutes(30).toISOString() ? "#AAFF00" : "#F0F0F0"}}>{i}:30</button>
							buttons.push(btn2);
						}
					}
				}

				if(availabilityOnDay.pm === true){
					for(let i = 12; i <= 18; i++){
						if(i !== 12){
							let btn = <button onClick={() => this.updateChosenSlot(i, 0)} className="viewingRadio" style={{backgroundColor: this.state.chosenSlot === Date.today().addDays(this.state.dayRequest).addHours(i + 1).toISOString() ? "#AAFF00" : "#F0F0F0"}}>{i}:00</button>
							buttons.push(btn);
						}
						if(i !== 18){
							let btn2 = <button onClick={() => this.updateChosenSlot(i, 30)} className="viewingRadio" style={{backgroundColor: this.state.chosenSlot === Date.today().addDays(this.state.dayRequest).addHours(i + 1).addMinutes(30).toISOString() ? "#AAFF00" : "#F0F0F0"}}>{i}:30</button>
							buttons.push(btn2);
						}
					}
				}

				if(availabilityOnDay.eve === true){
					for(let i = 18; i <= 20; i++){
						if(i !== 18){
							let btn = <button onClick={() => this.updateChosenSlot(i, 0)} className="viewingRadio" style={{backgroundColor: this.state.chosenSlot === Date.today().addDays(this.state.dayRequest).addHours(i + 1).toISOString() ? "#AAFF00" : "#F0F0F0"}}>{i}:00</button>
							buttons.push(btn);
						}
						let btn2 = <button onClick={() => this.updateChosenSlot(i, 30)} className="viewingRadio" style={{backgroundColor: this.state.chosenSlot === Date.today().addDays(this.state.dayRequest).addHours(i + 1).addMinutes(30).toISOString() ? "#AAFF00" : "#F0F0F0"}}>{i}:30</button>
						buttons.push(btn2);
					}
				}

				let viewingCard = 
				<div className="viewingCard">
					<div className="dayDate">
						<div className="viewingArrows" onClick={this.prevDate}>
							<i className="fa fa-angle-left"></i>
						</div>
					 	{long}
						 <div className="viewingArrows" onClick={this.nextDate}>
						 	<i className="fa fa-angle-right"></i>
						</div>
					</div>
					<div className="viewingButtons">
						{buttons}
					</div>
				</div>;

				this.setState({
					card: viewingCard
				});
			}
		}
	}

	nextDate = () => {
		this.setState({
			dayRequest: parseInt(this.state.dayRequest) + 1
		}, () => {
			console.log(this.state.dayRequest);
		});
	}

	prevDate = () => {
		let next = parseInt(this.state.dayRequest) - 1;
		if(next < 1){
			next = 1;
		}
		this.setState({
			dayRequest: next
		}, () => {
			this.generateButtons("past");
		});
	}

	updateChosenSlot = (hours, mins) => {
		let slot = Date.today().addDays(this.state.dayRequest).addHours(hours + 1).addMinutes(mins).toISOString();
		
		this.setState({
			chosenSlot: slot
		}, () => {
			console.log(this.state.chosenSlot);
			this.generateButtons("neither");
		});
	}

	updateName = (e) => {
		this.setState({
			name: e.currentTarget.value
		});
	}

	updateNumber = (e) => {
		this.setState({
			number: e.currentTarget.value
		});
	}

	sendSMS = () => {
		if(document.getElementById("phoneModal")){
			document.getElementById("phoneModal").style.display = "none";
		}

		if(document.getElementById("authModal")){
			document.getElementById("authModal").style.display = "block";
		}

		let number = parseInt(this.state.number);
		number = number.toString();

		var myHeaders = new Headers();
		myHeaders.append("Content-Type", "application/json");
		myHeaders.append("X-Requested-With", "XMLHttpRequest");

		var raw = JSON.stringify({"user":{"mobile":"+44" + number}});

		var requestOptions = {
			method: 'POST',
			headers: myHeaders,
			body: raw,
			redirect: 'follow'
		};

		fetch("https://freeco-api.herokuapp.com/api/users/signup-sms", requestOptions)
			.then(response => response.text())
			.then(result => {
				result = JSON.parse(result);
				this.setState({codeId: result.codeId});
			})
			.catch(error => console.log('error', error));
	}

	confirmSMS = () => {
		let code = this.state.securityCode.toString();
		var myHeaders = new Headers();
		myHeaders.append("Content-Type", "application/json");
		myHeaders.append("X-Requested-With", "XMLHttpRequest");

		var raw = JSON.stringify({"user":{"code":code, "codeId": this.state.codeId}});

		var requestOptions = {
			method: 'POST',
			headers: myHeaders,
			body: raw,
			redirect: 'follow'
		};

		fetch("https://freeco-api.herokuapp.com/api/users/confirm-sms", requestOptions)
			.then(response => response.text())
			.then(result => {
				console.log(result);
				result = JSON.parse(result);
				if(result.user.token){
					var myHeaders = new Headers();
					myHeaders.append("Content-Type", "application/json");
					myHeaders.append("X-Requested-With", "XMLHttpRequest");

					var raw = JSON.stringify({"viewing":{"date":this.state.chosenSlot,"status":"pending","name":this.state.name,"mobile":"+44" + parseInt(this.state.number)}});

					var requestOptions = {
						method: 'POST',
						headers: myHeaders,
						body: raw,
						redirect: 'follow'
					};

					fetch("https://freeco-api.herokuapp.com/api/listings/" + this.state.slug + "/viewings", requestOptions)
					.then(response => response.text())
					.then(result => {
						console.log(result);
						this.setState({
							page: 3
						});
					})
					.catch(error => console.log('error', error));
				}
			})
			.catch(error => console.log('error', error));
	}

	openModal = () => {
		document.getElementById("authModal").style.display = "block";
		this.sendSMS();
	}

	updateAuth = (e) => {
		this.setState({
			securityCode: e.currentTarget.value
		});
	}

	updateSlug = (e) => {
		this.setState({
			slug: e.currentTarget.value
		});
	}

	searchCode = () => {
		window.location.href = "/book-viewing/?slug=" + this.state.slug;
	}

	calcAuth = (e) => {
		let input = e.currentTarget.value.replace(/,/g, "");

		let current = this.state.securityCode;
		if (input !== "." && input !== "backspace") {
			let value = input;
			if (current !== null) {
				value = current.toString() + input;
			}

			this.setState({
				securityCode: value
			});
		} else if (input === "backspace") {
			let value = current.toString().slice(0, current.toString().length - 1);

			this.setState({
				securityCode: value
			});
		} else if (input === ".") {
			if (current % 1 === 0) {
				let value = current.toString() + ".";
				this.setState({
					securityCode: value
				});
			}
		}
	}

	slideLeft = () => {
		let to = parseInt(this.state.scroll) - 1;
		if(to < 0){
			to = 0;
		}

		this.setState({
			scroll: to
		}, () => {
			if(document.getElementById("qualityImage" + this.state.scroll)){
				document.getElementById("qualityImage" + this.state.scroll).scrollIntoView({ behavior: "smooth", block: "nearest", inline: "nearest" });
			}
		})
	}

	slideRight = () => {
		let to = parseInt(this.state.scroll) + 1;
		if(to >= this.state.listing.images.length){
			to = this.state.listing.images.length - 1;
		}

		this.setState({
			scroll: to
		}, () => {
			if(document.getElementById("qualityImage" + this.state.scroll)){
				document.getElementById("qualityImage" + this.state.scroll).scrollIntoView({ behavior: "smooth", block: "nearest", inline: "nearest" });
			}
		})
	}

	login = () => {
		window.location.href = "/login";
	}

	render() {
		let displayPage = "";

		if(this.state.page === 3){
			document.body.style.backgroundColor = "black";
		} else {
			document.body.style.backgroundColor = "#f0f0f0";
		}

		if(this.state.listing !== null){
			let listing = this.state.listing;
			switch(this.state.page){
				case 1:
					let imageArr = [];
					let display = "none";
					if(this.state.listing !== null){
						for(let i = 0, size = this.state.listing.images.length; i < size; i++){
							imageArr.push(<img id={"qualityImage" + i} style={{width: (100 / size) + "%"}} className="qualityImage" src={this.state.listing.images[i].url}></img>);
							display = "block";
						}
					}

					displayPage =
						<div>
							<div className="pageHeading">
								{listing.description.slice(0,listing.description.indexOf(".") + 1)}
							</div>
							<div className="pageContent">
								{listing.addLine1}, {listing.addTown} {listing.addPostcode.slice(0,listing.addPostcode.length - 3)}
							</div>
							<div className="priceHeading">
								£{listing.propAskingPrice.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}
							</div>
							<div className="viewingPhoto" style={{margin: "20px"}}>
								<div className="scrollThrough" style={{marginBottom: "-4px"}}>
									{/* <img src={listing.images[0] !== undefined ? listing.images[0].url : null} style={{maxWidth: "100%"}}></img> */}
								
									<div style={{display: display}}>
										<div className="qualityImagesContainer">
											<div className="qualityImagesExtension" style={{width: imageArr.length + "00%"}}>
												{imageArr}
											</div>
										</div>

										<div style={{textAlign: "center", border: "1px solid black", borderTop: "none"}}>
											<button style={{float: "left", margin: "0", "borderRadius": "0", dislay: "inline-block", border: "none", borderRight: "1px solid black"}} onClick={this.slideLeft}><i class="fas fa-angle-left"></i></button>
											<button style={{float: "right", margin: "0", "borderRadius": "0", dislay: "inline-block", border: "none", borderLeft: "1px solid black"}} onClick={this.slideRight}><i class="fas fa-angle-right"></i></button>
											<button className="imageNumberBox" style={{cursor: "auto", fontSize: "15px", textTransform: "initial", margin: "0", border: "none", "borderRadius": "0", dislay: "inline-block"}}>{this.state.scroll + 1} out of {imageArr.length} photos</button>
										</div>
									</div>
								</div>
							</div>
							<div className="pageContent" style={{textAlign: "left"}}>
								{listing.description}
							</div>
							<div className="spaceForViewingContainer"></div>
							<div className="bookViewingContainer">
								<button className="capBtn" onClick={this.nextPage}>Book a viewing</button>
							</div>
						</div>
					break;
				
				case 2:
					let authCalculator =
					<div className="calculator">
						<button className="calc-btn" value="1" onClick={this.calcAuth}>1</button>
						<button className="calc-btn" value="2" onClick={this.calcAuth}>2</button>
						<button className="calc-btn" value="3" onClick={this.calcAuth}>3</button>
						<button className="calc-btn" value="4" onClick={this.calcAuth}>4</button>
						<button className="calc-btn" value="5" onClick={this.calcAuth}>5</button>
						<button className="calc-btn" value="6" onClick={this.calcAuth}>6</button>
						<button className="calc-btn" value="7" onClick={this.calcAuth}>7</button>
						<button className="calc-btn" value="8" onClick={this.calcAuth}>8</button>
						<button className="calc-btn" value="9" onClick={this.calcAuth}>9</button>
						<button disabled="disabled" className="calc-btn" value="." onClick={this.calcAuth}>.</button>
						<button className="calc-btn" value="0" onClick={this.calcAuth}>0</button>
						<button className="calc-btn" value="backspace" onClick={this.calcAuth}><img value="backspace" src={require("../img/icon-keypad-delete.svg")}></img></button>
					</div>;

					let dis = "disabled";
					if(this.state.name !== null && this.state.number !== null){
						if(this.state.number.length >= 10 && this.state.number.length < 12){
							dis = "";
						}
					}

					displayPage =
						<div>
							<div className="pageHeading">
								Select a viewing
							</div>
							<div className="pageContent">
								Select a timeslot to view the property
							</div>
							{this.state.card}
							<div className="pageContent">
								Please enter your name and mobile number - we'll confirm your booking via SMS.
							</div>
							<input type="text" placeholder="Your name" value={this.state.name} onChange={this.updateName}></input><br/>
							<input style={{marginTop: "-1px"}} type="number" placeholder="Mobile number" value={this.state.number} onChange={this.updateNumber}></input>
							<br/>
							<button className="capBtn" disabled={dis} onClick={this.openModal}>Book</button>
							<div id="authModal" className="mobileModal modal">
								<div className="modalContent">
									<div className="pageHeading">
										Please enter your 6-digit security code
									</div>
									<input type="password" onChange={this.updateAuth} className="leaseInput codeInput" value={this.state.securityCode} placeholder="••••••"></input>
									<br/>
									{authCalculator}
									<br/>
									<button className="capBtn" onClick={this.confirmSMS}>verify</button>
									<div className="pageContent light-grey">
										A 6-digit security code was sent via SMS to +44 (0){parseInt(this.state.number)}.
										<br/>
										<span className="link grey-link" onClick={this.sendSMS}>
											Resend message
										</span>
									</div>
								</div>
							</div>
						</div>
					break;

				case 3:
					displayPage = 
					<div className="blackPage">
						<div className="freeExperts whiteText">
							The <img className="logoWhite" src={require("../img/free-logo.svg")}></img> Experts
						</div>
						<div className="pageHeading whiteText">
							Your property viewing is booked!
						</div>
						<div className="pageContent whiteText">
							We'll send you an SMS reminder an hour before the appointment
						</div>
						{/* <div className="videoBox">
							Video
						</div>
						<div className="videoDescription whiteText">
							<strong>Ann Miura-Ko</strong><br/>
							Ann talks about the most important things to consider when choosing a mortgage
						</div> */}
						<div className="whatNext borderTop" style={{paddingTop: "0", marginTop: "20px", marginLeft: "-20px", marginRight: "-20px"}}>
							<div className="pageHeading whatNextHeading">
								What's next?
							</div>

							<div className="pageContent">
								<strong>Get a mortgage-in-principle in 5 minutes</strong><br />
								A no-commitment peek into what lenders are prepared to offer you, giving you a clear idea
								of a budget while looking for your new home.
							</div>

							<a href="https://beta.free.co.uk/mortgage/wizard" target="_blank">
								<button className="capBtn colorBtn">Find your budget</button>
							</a>
						</div>
					</div>
			}
		}

		if(this.state.slug === null || this.state.error !== null){
			displayPage =
				<div>
					<div className="pageHeading" style={{marginTop: "50px"}}>
						Enter the property code to find its booking page!
					</div>
					<input type="number" onChange={this.updateSlug} placeholder="Property code" value={this.state.slug}></input>
					<br/>
					<button className="capBtn" onClick={this.searchCode}>Search</button>
				</div>;
		}

		return (
			<div className="listing-flow">
				<div className="navbar" style={{display: this.state.page === 3 ? "none" : "block"}}>
					<button className="back" onClick={this.prevPage}>
						<img className="backArrow" src={require("../img/icon-arrow-back.svg")}></img>
						<div className="backText">
							Back
						</div>
					</button>
					<img className="logo" src={require("../img/logo.svg")}></img>
					<button className="back" onClick={this.login}>
						<div className="backText">
							Login
						</div>
					</button>
				</div>
				
				<div className="page">
					{displayPage}
				</div>
			</div>
		);
	}
}

export default BookViewing;
