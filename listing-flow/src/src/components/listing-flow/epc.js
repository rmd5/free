import React from 'react';

class EPC extends React.Component {
    render() {
        return (
            <div className="listing-flow">
                <div className="navbar">
					<button className="back" onClick={this.prevPage}>
						<img className="backArrow" src={require("../img/icon-arrow-back.svg")}></img>
						<div className="backText">
							Back
						</div>
					</button>
					<img className="logo" src={require("../img/logo.svg")}></img>
				</div>

                <div className="page">
                    
                </div>
            </div>
        );
    }
}

export default EPC;