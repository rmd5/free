import React from 'react';

class Login extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			phonenumber: "",
			securityCode: ""
		};
	}

	componentDidMount() {
		let user = localStorage.getItem("user");
		
		if(user !== null && user !== undefined){
			user = JSON.parse(user);
			let arr = user.listings;

			if(arr.length > 1){
				window.location.href = "/select-listing";
			} else if(arr.length === 1){
				window.location.href = "/dashboard/?slug=" + arr[0];
			}
		}
	}

	prevPage = () => {
		document.getElementById("leaveModal").style.display = "block";
	}

	leavePage = (e) => {
		if(e.currentTarget.value === "yes"){
			window.location.href = "https://free.co.uk";
		} else {
			document.getElementById("leaveModal").style.display = "none";
		}
	}

	updatePhone = (e) => {
		if(e.currentTarget.value >= 0){
			this.setState({
				phonenumber: e.currentTarget.value
			});
		}
	}

	calcPhone = (e) => {
		let input = e.currentTarget.value.replace(/,/g, "");

		let current = this.state.phonenumber;
		if (input !== "." && input !== "backspace") {
			let value = input;
			if (current !== null) {
				value = current.toString() + input;
			}

			this.setState({
				phonenumber: value
			});
		} else if (input === "backspace") {
			let value = current.toString().slice(0, current.toString().length - 1);

			this.setState({
				phonenumber: value
			});
		} else if (input === ".") {
			if (current % 1 === 0) {
				let value = current.toString() + ".";
				this.setState({
					phonenumber: value
				});
			}
		}
	}

	updateAuth = (e) => {
		this.setState({
			securityCode: e.currentTarget.value
		});
	}

	calcAuth = (e) => {
		let input = e.currentTarget.value.replace(/,/g, "");

		let current = this.state.securityCode;
		if (input !== "." && input !== "backspace") {
			let value = input;
			if (current !== null) {
				value = current.toString() + input;
			}

			this.setState({
				securityCode: value
			});
		} else if (input === "backspace") {
			let value = current.toString().slice(0, current.toString().length - 1);

			this.setState({
				securityCode: value
			});
		} else if (input === ".") {
			if (current % 1 === 0) {
				let value = current.toString() + ".";
				this.setState({
					securityCode: value
				});
			}
		}
	}

	sendSMS = () => {
		if(document.getElementById("phoneModal")){
			document.getElementById("phoneModal").style.display = "none";
		}

		if(document.getElementById("authModal")){
			document.getElementById("authModal").style.display = "block";
		}

		let number = parseInt(this.state.phonenumber);
		number = number.toString();

		var myHeaders = new Headers();
		myHeaders.append("Content-Type", "application/json");
		myHeaders.append("X-Requested-With", "XMLHttpRequest");

		var raw = JSON.stringify({"user":{"mobile":"+44" + number}});

		var requestOptions = {
			method: 'POST',
			headers: myHeaders,
			body: raw,
			redirect: 'follow'
		};

		fetch("https://freeco-api.herokuapp.com/api/users/signup-sms", requestOptions)
			.then(response => response.text())
			.then(result => {
				result = JSON.parse(result);
				this.setState({codeId: result.codeId});
			})
			.catch(error => console.log('error', error));
	}

	confirmSMS = () => {
		let code = this.state.securityCode.toString();
		var myHeaders = new Headers();
		myHeaders.append("Content-Type", "application/json");
		myHeaders.append("X-Requested-With", "XMLHttpRequest");

		var raw = JSON.stringify({"user":{"code":code, "codeId": this.state.codeId}});

		var requestOptions = {
			method: 'POST',
			headers: myHeaders,
			body: raw,
			redirect: 'follow'
		};

		fetch("https://freeco-api.herokuapp.com/api/users/confirm-sms", requestOptions)
			.then(response => response.text())
			.then(result => {
				console.log(result);
				result = JSON.parse(result);
				if(result.user.token){
					localStorage.setItem("user", JSON.stringify(result.user));
					if(result.user.listings.length > 1){
						window.location.href = "/select-listing";
					} else if(result.user.listings.length === 1){
						window.location.href = "/dashboard/?slug=" + result.user.listings[0];
					} else {
						window.location.href = "/listing";
					}
				}
			})
			.catch(error => console.log('error', error));
	}

	render() {
		return(
			<div className="listing-flow">
				<div className="navbar">
					<button className="back" onClick={this.prevPage}>
						<img className="backArrow" src={require("../img/icon-arrow-back.svg")}></img>
						<div className="backText">
							Back
						</div>
					</button>
					<img className="logo" src={require("../img/logo.svg")}></img>
				</div>

				<div className="page">
					<div className="pageHeading">
						Login to Free.co.uk
					</div>
					<div className="pageContent">
						We'll verify your phone number and take you to your dashboard<br/>
						Don't have a property listing? <a style={{textDecoration: "underline"}} href="/listing">Create one here!</a>
					</div>
					<span className="input-label-pound input-label">+44</span>
					<input onChange={this.updatePhone} className="leaseInput priceInput phoneInput" value={this.state.phonenumber} placeholder="Your phone number"></input><br/>
					<div className="calculator">
						<button className="calc-btn" value="1" onClick={this.calcPhone}>1</button>
						<button className="calc-btn" value="2" onClick={this.calcPhone}>2</button>
						<button className="calc-btn" value="3" onClick={this.calcPhone}>3</button>
						<button className="calc-btn" value="4" onClick={this.calcPhone}>4</button>
						<button className="calc-btn" value="5" onClick={this.calcPhone}>5</button>
						<button className="calc-btn" value="6" onClick={this.calcPhone}>6</button>
						<button className="calc-btn" value="7" onClick={this.calcPhone}>7</button>
						<button className="calc-btn" value="8" onClick={this.calcPhone}>8</button>
						<button className="calc-btn" value="9" onClick={this.calcPhone}>9</button>
						<button disabled="disabled" className="calc-btn" value="." onClick={this.calcPhone}>.</button>
						<button className="calc-btn" value="0" onClick={this.calcPhone}>0</button>
						<button className="calc-btn" value="backspace" onClick={this.calcPhone}><img value="backspace" src={require("../img/icon-keypad-delete.svg")}></img></button>
					</div>
					<br/>
					<button onClick={this.sendSMS} className="capBtn" disabled={this.state.phonenumber.length < 10 ? "disabled" : ""}>Log in</button>
					
					<div id="authModal" className="mobileModal modal">
						<div className="modalContent">
							<div className="pageHeading">
								Please enter your 6-digit security code
							</div>
							<input type="password" onChange={this.updateAuth} className="leaseInput codeInput" value={this.state.securityCode} placeholder="••••••"></input>
							<br/>
							<div className="calculator">
								<button className="calc-btn" value="1" onClick={this.calcAuth}>1</button>
								<button className="calc-btn" value="2" onClick={this.calcAuth}>2</button>
								<button className="calc-btn" value="3" onClick={this.calcAuth}>3</button>
								<button className="calc-btn" value="4" onClick={this.calcAuth}>4</button>
								<button className="calc-btn" value="5" onClick={this.calcAuth}>5</button>
								<button className="calc-btn" value="6" onClick={this.calcAuth}>6</button>
								<button className="calc-btn" value="7" onClick={this.calcAuth}>7</button>
								<button className="calc-btn" value="8" onClick={this.calcAuth}>8</button>
								<button className="calc-btn" value="9" onClick={this.calcAuth}>9</button>
								<button disabled="disabled" className="calc-btn" value="." onClick={this.calcAuth}>.</button>
								<button className="calc-btn" value="0" onClick={this.calcAuth}>0</button>
								<button className="calc-btn" value="backspace" onClick={this.calcAuth}><img value="backspace" src={require("../img/icon-keypad-delete.svg")}></img></button>
							</div>
							<br/>
							<button className="capBtn" onClick={this.confirmSMS}>verify</button>
							<div className="pageContent light-grey">
								A 6-digit security code was sent via SMS to +44 (0){parseInt(this.state.phonenumber)}.
								<br/>
								<span className="link grey-link" onClick={this.sendSMS}>
									Resend message
								</span>
							</div>
						</div>
					</div>

					<div id="leaveModal" className="modal">
						<div className="modalContent">
							<div className="pageHeading">
								Go back to the Free.co.uk?
							</div>
							<button className="radio" value="yes" onClick={this.leavePage}>Yes</button><br/>
							<button className="radio" value="no" onClick={this.leavePage}>No</button>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Login;