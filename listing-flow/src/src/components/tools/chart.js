import React from 'react';

class Chart extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            startingValue: 40000,
            endValue: 200000,
            stepSize: null,
            numberSteps: 13,
            bars: [],
            barsDisplay: [],
            sliderValue: 1,
        };
    }

    componentDidMount() {

        const $style = document.createElement("style");
        document.head.appendChild($style);
        const randBlue = ~~(Math.random() * 250);
        $style.innerHTML = `
        .App {
            height: auto;
        }

        input {
            background-color: #f0f0f0;
        }
        input[type=range] {
            height: 33px;
            -webkit-appearance: none;
            margin: 10px 0;
            width: 100%;
          }
          input[type=range]:focus {
            outline: none;
          }
          input[type=range]::-webkit-slider-runnable-track {
            width: 100%;
            height: 3px;
            cursor: pointer;
            animate: 0.2s;
            background: #999;
            border-radius: 1px;
            border: 0px solid #010101;
          }
          input[type=range]::-webkit-slider-thumb {
            border: 0px solid #00001E;
            height: 20px;
            width: 20px;
            border-radius: 15px;
            background: #AAFF00;
            cursor: pointer;
            -webkit-appearance: none;
            margin-top: -9px;
          }
          input[type=range]:focus::-webkit-slider-runnable-track {
            background: #999;
          }
          input[type=range]::-moz-range-track {
            width: 100%;
            height: 2px;
            cursor: pointer;
            animate: 0.2s;
            background: #999;
            border-radius: 1px;
            border: 0px solid #010101;
          }
          input[type=range]::-moz-range-thumb {
            border: 0px solid #00001E;
            height: 26px;
            width: 26px;
            border-radius: 15px;
            background: #AAFF00;
            cursor: pointer;
          }
          input[type=range]::-ms-track {
            width: 100%;
            height: 2px;
            cursor: pointer;
            animate: 0.2s;
            background: transparent;
            border-color: transparent;
            color: transparent;
          }
          input[type=range]::-ms-fill-lower {
            background: #999;
            border: 0px solid #010101;
            border-radius: 2px;
          }
          input[type=range]::-ms-fill-upper {
            background: #999;
            border: 0px solid #010101;
            border-radius: 2px;
          }
          input[type=range]::-ms-thumb {
            margin-top: 1px;
            border: 0px solid #00001E;
            height: 26px;
            width: 26px;
            border-radius: 15px;
            background: #AAFF00;
            cursor: pointer;
          }
          input[type=range]:focus::-ms-fill-lower {
            background: #999;
          }
          input[type=range]:focus::-ms-fill-upper {
            background: #999;
          }
        `;

        if (this.state.stepSize === null || this.state.stepSize === undefined) {
            this.createStepArray();
        } else {
            this.createArray();
        }
    }

    createArray() {
        let start = this.state.startingValue;
        let end = this.state.endValue;
        let steps = this.state.stepSize;

        let arr = [];

        for (let i = start; i <= end; i += steps) {
            arr.push(i);
        }

        this.setState({
            bars: arr
        }, () => {
            this.createBars();
        });
    }

    createStepArray() {
        let start = this.state.startingValue;
        let end = this.state.endValue;
        let steps = this.state.numberSteps - 1;

        let arr = [];

        for (let i = 0; i <= steps; i++) {
            let n = ((end - start) / steps * i) + start;
            arr.push(parseInt(n));
        }

        this.setState({
            bars: arr
        }, () => {
            this.createBars();
        });
    }

    createBars() {
        let bars = this.state.bars;

        bars = bars.sort(function (a, b) { return a - b });

        let max = bars[bars.length - 1];
        let barsDisplay = [];
        for (let i = 0, size = bars.length; i < size; i++) {
            let height = 100 / max * bars[i];
            let width = 100 / size;
            let percent = parseInt(100 / (size - 1) * i);
            let bar =
                <div id={i} style={{ position: "relative", marginRight: "2px", marginLeft: "2px", height: height + "%", width: "calc(" + width + "% - 4px)", backgroundColor: "#999", display: "inline-block" }}>
                    <div id={"bar" + i} className="chartInfo" style={{ backgroundColor: "#f0f0f0", zIndex: "2", display: "none", padding: "10px", position: "absolute", left: "50%", top: "-50px", transform: "translate(-50%, -50%)", border: "2px solid black" }}>
                        <strong>£{bars[i].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}</strong><br />
                        income
                        <div className="chartDownArrow" style={{ position: "absolute", height: "5px", width: "5px", bottom: "-9px", transform: "translate(-50%, -50%)rotate(-45deg)", left: "50%", borderLeft: "2px solid black", borderBottom: "2px solid black", backgroundColor: "#f0f0f0", }}></div>
                    </div>
                    <div id={"percent" + i} className="chartPercent" style={{ display: "none", zIndex: "2", padding: "10px", position: "absolute", left: "50%", bottom: "-60px", transform: "translate(-50%, -50%)" }}>
                        <strong>{percent}%</strong>
                    </div>
                </div>;

            barsDisplay.push(bar);
        }

        this.setState({
            barsDisplay: barsDisplay
        }, () => {
            this.hoverBar(this.state.sliderValue - 1);
        });
    }

    hoverBar(i) {
        document.getElementById("bar" + i).style.display = "block";
        document.getElementById("percent" + i).style.display = "block";
        document.getElementById(i).style.backgroundColor = "#AAFF00";
    }

    hoverOff(i) {
        document.getElementById("bar" + i).style.display = "none";
        document.getElementById("percent" + i).style.display = "none";
        document.getElementById(i).style.backgroundColor = "#999";
    }

    slider = (e) => {
        this.setState({
            sliderValue: e.currentTarget.value
        }, () => {
            for (let i = 0, size = this.state.bars.length; i < size; i++) {
                this.hoverOff(i);
            }
            this.hoverBar(this.state.sliderValue - 1);
        });
    }

    render() {
        let width = "calc(98% - " + (1 / this.state.bars.length * 100) + "%)";
        return (
            <div style={{width: "calc(100% - 90px)", marginLeft: "60px", marginRight: "30px"}}>
                <div className="chart" style={{width: "100%"}}>

                    <div id="chartBox" className="chartBox" style={{ paddingBottom: "5px", borderBottom: "3px solid #999", height: "300px", maxHeight: "calc(60vw - 60px)", width: "500px", maxWidth: "calc(100% - 25px)", position: "relative", margin: "15px", marginTop: "100px"}}>
                        <div className="chartScale" style={{position: "absolute", left: "-70px", top: "-40px", fontSize: "20px", color: "#999"}}>
                            <strong>Total offered</strong>
                        </div>

                        <div className="chartScale" style={{position: "absolute", left: "-70px", top: "0px", fontSize: "20px", color: "#999"}}>
                            <strong>£1.5m</strong>
                        </div>

                        <div className="chartScale" style={{position: "absolute", left: "-70px", top: "40%", fontSize: "20px", color: "#999"}}>
                            <strong>£1.0m</strong>
                        </div>

                        <div className="chartScale" style={{position: "absolute", left: "-70px", top: "80%", fontSize: "20px", color: "#999"}}>
                            <strong>£0.5m</strong>
                        </div>

                        {this.state.barsDisplay}
                    </div>

                    <div class="slidecontainer" style={{maxWidth: "100%"}}>
                        <input style={{ width: width, marginTop: "20px", border: "#AAFF00" }} id="chartSlider" onChange={this.slider} type="range" min="1" max={this.state.bars.length} value={this.state.sliderValue} class="slider" id="myRange"></input>
                    </div>
                </div>
            </div>
        )
    }
}

export default Chart;