import React from 'react';

class Listing extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			page: 1,
			postcode: null,
			firstName: null,
			lastName: null,
			propType: null,
			propFloor: "",
			propSize: null,
			propLastDecorated: null,
			propHasLift: null,
			propTenure: null,
			propLeaseYears: null,
			propBeds: null,
			propBaths: null,
			propBuilt: null,
			propOutdoorSpace: null,
			propParkingType: null,
			propAskingPrice: null,
			propHeatingType: null,
			propExtraCosts: null,
			propEPC: null,
			propExtras: [],
			generatedDescription: null,
			description: null,
			descriptionTone: 0,
			addressSelect: [],
			addressSummary: null,
			line1: null,
			line2: null,
			numberName: null,
			posttown: null,
			county: null,
			manualEdit: false,
			randomiser: null,
			phonenumber: null,
			securityCode: null,
			codeId: null
		};
	}

	componentDidMount() {
		localStorage.setItem("previous", "listing");
		let page = 1;
		let data = localStorage.getItem("listingData");
		if (data !== null) {
			data = JSON.parse(localStorage.getItem("listingData"));
			if (data.description !== null) {
				page = 18;
			} else if (data.propAskingPrice !== null) {
				page = 16;
			} else if (data.propExtras !== null) {
				page = 15;
			} else if (data.propParkingType !== null) {
				page = 14;
			} else if (data.propEPC !== null) {
				page = 13;
			} else if (data.propOutdoorSpace !== null) {
				page = 12;
			} else if (data.propHeatingType !== null) {
				page = 11;
			} else if (data.propLastDecorated !== null) {
				page = 10;
			} else if (data.propBuilt !== null) {
				page = 9;
			} else if (data.propSize !== null) {
				page = 8;
			} else if (data.propBaths !== null) {
				page = 7;
			} else if (data.propBeds !== null) {
				page = 6;
			} else if (data.propLeaseYears !== null && (data.propTenure === "leasehold" || data.propTenure === "share-of-lease")) {
				page = 5.3;
			} else if (data.propTenure !== null) {
				page = 5;
			} else if (data.propHasLift !== null && data.propFloor !== "ground-floor" && data.propType === "flat") {
				page = 4.6;
			} else if (data.propFloor !== null && data.propType === "flat") {
				page = 4.3;
			} else if (data.propType !== null) {
				page = 4;
			} else if (data.firstName !== null) {
				page = 3;
			} else if (data.postcode !== null) {
				page = 1;
			}

			let price = data.propAskingPrice;
			if (price === null) {
				price = "";
			}

			let size = data.propSize;
			if (size === null) {
				size = "";
			}

			let years = data.propLeaseYears;
			if (years === null) {
				years = "";
			}

			this.setState({
				page: page,
				firstName: data.firstName,
				lastName: data.lastName,
				postcode: data.postcode,
				posttown: data.posttown,
				county: data.county,
				line1: data.line1,
				propType: data.propType,
				propFloor: data.propFloor,
				propSize: parseInt(size),
				propLastDecorated: data.propLastDecorated,
				propHasLift: data.propHasLift,
				propTenure: data.propTenure,
				propLeaseYears: years,
				propBeds: parseInt(data.propBeds),
				propBaths: parseInt(data.propBaths),
				propBuilt: data.propBuilt,
				propOutdoorSpace: data.propOutdoorSpace,
				propParkingType: data.propParkingType,
				propAskingPrice: parseInt(price),
				propHeatingType: data.propHeatingType,
				propEPC: data.propEPC,
				propExtras: data.propExtras,
				generatedDescription: data.generatedDescription,
				description: data.description,
				descriptionTone: 0,
				addressSelect: data.addressSelect,
				propExtraCosts: parseInt(data.propExtraCosts),
				manualEdit: data.manualEdit
			}, () => {
				if (this.state.postcode !== "null" && this.state.postcode !== "" && this.state.postcode !== null) {
					this.postcodeSearch();
				}
			});
		}
	}

	nextPage = () => {
		let page = this.state.page;
		if (page === 2.5) {
			page = 3;
		} else if (page === 2.25) {
			page = 2.5;
		} else if (page === 5.3) {
			page = 5.6;
		} else if (page === 5.6) {
			page = 6
		} else {
			page += 1;
		}
		this.setState({
			page: page
		}, () => {
			this.store();

			if (this.state.page === 17) {
				// setTimeout(function(){window.location.href="https://beta.free.co.uk/description"}, 2000);
				this.generateDescription();
				setTimeout(this.nextPage, 4000);
			}
		});
	}

	store = () => {
		let data = {
			"postcode": this.state.postcode,
			"line1": this.state.line1,
			"posttown": this.state.posttown,
			"county": this.state.county,
			"firstName": this.state.firstName,
			"lastName": this.state.lastName,
			"propBuilt": this.state.propBuilt,
			"propBeds": this.state.propBeds,
			"propBaths": this.state.propBaths,
			"propType": this.state.propType,
			"propFloor": this.state.propFloor,
			"propSize": this.state.propSize,
			"propLastDecorated": this.state.propLastDecorated,
			"propHasLift": this.state.propHasLift,
			"propTenure": this.state.propTenure,
			"propLeaseYears": this.state.propLeaseYears,
			"propOutdoorSpace": this.state.propOutdoorSpace,
			"propParkingType": this.state.propParkingType,
			"propAskingPrice": this.state.propAskingPrice,
			"propHeatingType": this.state.propHeatingType,
			"propEPC": this.state.propEPC,
			"propExtras": this.state.propExtras,
			"generatedDescription": this.state.generatedDescription,
			"description": this.state.description,
			"descriptionTone": this.state.descriptionTone,
			"addressSelect": this.state.addressSelect,
			"propExtraCosts": this.state.propExtraCosts,
			"manualEdit": this.state.manualEdit
		}
		localStorage.clear();
		localStorage.setItem("listingData", JSON.stringify(data));
	}

	prevPage = () => {
		if (this.state.page === 1) {
			document.getElementById("leaveModal").style.display = "block";
			this.setState({
				page: parseInt(this.state.page)
			});
		} else if (this.state.page > 1) {
			let page = this.state.page;
			if (page === 2.5 && this.state.manualEdit === false) {
				page = 2;
			} else if (page === 2.5 && this.state.manualEdit === true) {
				page = 2.25;
			} else if (page === 2.25) {
				page = 2;
			} else if (page === 3) {
				page = 2.5;
			} else if (page === 5 && this.state.propHasLift !== null) {
				page = 4.6;
			} else if (page === 5 && this.state.propFloor !== null) {
				page = 4.3;
			} else if (page === 4.6) {
				page = 4.3;
			} else if (page === 4.3) {
				page = 4;
			} else if (page === 6 && (this.state.propTenure === "share-of-freehold" || this.state.propTenure === "leasehold")) {
				page = 5.3;
			} else if (page === 5.3) {
				page = 5;
			} else if (page === 11 && this.state.propBuilt === "brandNew") {
				page -= 2;
			} else if (page === 18) {
				page -= 2;
			} else if (page === 6 && this.state.propExtraCosts !== null) {
				page = 5.6;
			} else if (page === 5.6){
				page = 5.3;
			} else {
				page -= 1;
			}

			this.setState({
				page: page
			});
		}
	}

	updatePostcode = (e) => {
		let code = e.currentTarget.value.toUpperCase();

		this.setState({
			postcode: code
		});
	}

	updateFirstName = (e) => {
		this.setState({
			firstName: e.currentTarget.value
		});
	}

	updateLastName = (e) => {
		this.setState({
			lastName: e.currentTarget.value
		});
	}

	updatePropType = (e) => {
		let page = this.state.page;
		if (e.currentTarget.value !== "flat") {
			page = 5;
			this.setState({
				propHasLift: null,
				propFloor: null
			});
		} else {
			page = 4.3;
		}

		this.setState({
			propType: e.currentTarget.value,
			page: page
		}, () => {
			this.store();
		});
	}

	updatePropFloor = (e) => {
		let page = this.state.page;
		if (e.currentTarget.value === "ground-floor") {
			page = 5;
			this.setState({
				propHasLift: null
			});
		} else {
			page = 4.6;
		}

		this.setState({
			propFloor: e.currentTarget.value,
			page: page
		}, () => {
			this.store();
		});
	}

	updatePropHasLift = (e) => {
		this.setState({
			propHasLift: e.currentTarget.value,
			page: parseInt(this.state.page) + 1
		}, () => {
			this.store();
		});
	}

	updatePropTenure = (e) => {
		let page = this.state.page;
		if (e.currentTarget.value === "freehold" || e.currentTarget.value === "flying-freehold") {
			page = 6;
		} else {
			page = 5.3;
		}

		this.setState({
			propTenure: e.currentTarget.value,
			page: page
		}, () => {
			this.store();
		});
	}

	updatePropLeaseYears = (e) => {
		if (e.currentTarget.value > 0) {
			this.setState({
				propLeaseYears: e.currentTarget.value
			});
		} else if (e.currentTarget.value === "") {
			this.setState({
				propLeaseYears: null
			});
		}
	}

	calcPropLeaseYears = (e) => {
		let input = e.currentTarget.value;
		let current = this.state.propLeaseYears;
		if (input !== "." && input !== "backspace") {
			let value = input;
			if (current !== null) {
				value = current.toString() + input;
			}

			this.setState({
				propLeaseYears: value
			});
		} else if (input === "backspace") {
			let value = current.toString().slice(0, current.toString().length - 1);
			this.setState({
				propLeaseYears: value
			});
		} else if (input === ".") {
			if (current % 1 === 0) {
				let value = current.toString() + ".";
				this.setState({
					propLeaseYears: value
				});
			}
		}
	}

	updatePropBeds = (e) => {
		let beds = e.currentTarget.value;

		this.setState({
			propBeds: parseInt(beds),
			page: this.state.page + 1
		}, () => {
			this.store();
		});
	}

	updatePropBaths = (e) => {
		let baths = e.currentTarget.value;

		this.setState({
			propBaths: parseInt(baths),
			page: this.state.page + 1
		}, () => {
			this.store();
		});
	}

	updatePropBuilt = (e) => {
		let built = e.currentTarget.value;
		let increment = 1;
		if (built === "brandNew") {
			increment = 2;
		}

		this.setState({
			propBuilt: built,
			page: this.state.page + increment
		}, () => {
			this.store();
		});
	}

	updatePropLastDecorated = (e) => {
		let decorated = e.currentTarget.value;

		this.setState({
			propLastDecorated: decorated,
			page: this.state.page + 1
		}, () => {
			this.store();
		});
	}

	updatePropSize = (e) => {
		this.setState({
			propSize: e.currentTarget.value
		});
	}

	calcPropSize = (e) => {
		let input = e.currentTarget.value;
		let current = this.state.propSize;
		if (input !== "." && input !== "backspace") {
			let value = input;
			if (current !== null) {
				value = current.toString() + input;
			}

			this.setState({
				propSize: parseInt(value)
			});
		} else if (input === "backspace") {
			let value = "";
			if(current !== null){
				value = current.toString().slice(0, current.toString().length - 1);
			}

			this.setState({
				propSize: value
			});
		} else if (input === ".") {
			if (current % 1 === 0) {
				let value = current.toString() + ".";
				this.setState({
					propSize: parseInt(value)
				});
			}
		}
	}

	updatePropHeatingType = (e) => {
		this.setState({
			propHeatingType: e.currentTarget.value,
			page: parseInt(this.state.page) + 1
		}, () => {
			this.store();
		});
	}

	updatePropOutdoorSpace = (e) => {
		this.setState({
			propOutdoorSpace: e.currentTarget.value,
			page: parseInt(this.state.page) + 1
		}, () => {
			this.store();
		});
	}

	updatePropEPC = (e) => {
		this.setState({
			propEPC: e.currentTarget.value,
			page: parseInt(this.state.page) + 1
		}, () => {
			this.store();
		});
	}

	updatePropParkingType = (e) => {
		this.setState({
			propParkingType: e.currentTarget.value,
			page: parseInt(this.state.page) + 1
		}, () => {
			this.store();
		});
	}

	updatePropExtras = (e) => {
		let extras = this.state.propExtras.slice();
		if (extras.includes(e.currentTarget.value)) {
			for (var i = 0; i < extras.length; i++) {
				if (extras[i] === e.currentTarget.value) {
					extras.splice(i, 1);
				}
			}
		} else {
			extras.push(e.currentTarget.value);
		}

		this.setState({
			propExtras: extras
		});
	}

	updatePropAskingPrice = (e) => {
		this.setState({
			propAskingPrice: e.currentTarget.value.replace(/,/g, "")
		});
	}

	calcPropAskingPrice = (e) => {
		let input = e.currentTarget.value.replace(/,/g, "");

		let current = this.state.propAskingPrice;
		if (input !== "." && input !== "backspace") {
			let value = input;
			if (current !== null) {
				value = current.toString() + input;
			}

			this.setState({
				propAskingPrice: value
			});
		} else if (input === "backspace") {
			let value = current.toString().slice(0, current.toString().length - 1);

			this.setState({
				propAskingPrice: value
			});
		} else if (input === ".") {
			if (current % 1 === 0) {
				let value = current.toString() + ".";
				this.setState({
					propAskingPrice: parseInt(value)
				});
			}
		}
	}

	updatePropExtraCosts = (e) => {
		this.setState({
			propExtraCosts: e.currentTarget.value.replace(/,/g, "")
		});
	}

	calcPropExtraCosts = (e) => {
		let input = e.currentTarget.value.replace(/,/g, "");

		let current = this.state.propExtraCosts;
		if (input !== "." && input !== "backspace") {
			let value = input;
			if (current !== null) {
				value = current.toString() + input;
			}

			this.setState({
				propExtraCosts: parseInt(value)
			});
		} else if (input === "backspace") {
			let value = "";
			if(current !== null){
				value = current.toString().slice(0, current.toString().length - 1);
			}

			this.setState({
				propExtraCosts: value
			});
		} else if (input === ".") {
			if (current % 1 === 0) {
				let value = current.toString() + ".";
				this.setState({
					propExtraCosts: parseInt(value)
				});
			}
		}
	}

	accordion = (e) => {
		if (document.getElementById(e.currentTarget.value)) {
			if (document.getElementById(e.currentTarget.value).style.maxHeight === document.getElementById(e.currentTarget.value).scrollHeight + "px") {
				document.getElementById(e.currentTarget.value).style.maxHeight = "0px";
				if (document.getElementById("moreBedrooms")) {
					document.getElementById("moreBedrooms").innerHTML = "More options";
				} else if (document.getElementById("moreBathrooms")) {
					document.getElementById("moreBathrooms").innerHTML = "More options";
				} else if (document.getElementById("moreParking")) {
					document.getElementById("moreParking").innerHTML = "More options";
				}
			} else {
				document.getElementById(e.currentTarget.value).style.maxHeight = document.getElementById(e.currentTarget.value).scrollHeight + "px";
				if (document.getElementById("moreBedrooms")) {
					document.getElementById("moreBedrooms").innerHTML = "Less options";
				} else if (document.getElementById("moreBathrooms")) {
					document.getElementById("moreBathrooms").innerHTML = "Less options";
				} else if (document.getElementById("moreParking")) {
					document.getElementById("moreParking").innerHTML = "Less options";
				}
			}
		}
	}

	postcodeSearch = () => {
		let apiKey = "PCW6L-J7R44-GX59L-C5FTK";
		let country = "UK";
		let input = this.state.postcode;
		let identifier = "holmes-address-lookup";
		let array = [];

		fetch(
			`https://ws.postcoder.com/pcw/${apiKey}/address/${country}/${encodeURIComponent(
				input,
			)}?format=json&lines=2&identifier=${identifier}`,
		).then(response => {
			return response.json();
		}).then(data => {
			let options = array;
			for (let i = 0, size = data.length; i < size; i++) {
				let button = <button className="radio" value={JSON.stringify(data[i])} onClick={this.selectAddress}>{data[i].addressline1}</button>;
				options.push(button);
				options.push(<br />);
			}
			console.log(data);

			let page = this.state.page;
			if (this.state.page === 1) {
				page += 1;
			}
			this.setState({
				addressSelect: options,
				page: page
			});
		});
	}

	selectAddress = (e) => {
		let summary = JSON.parse(e.currentTarget.value);
		let line1 = summary.addressline1;
		let line2 = summary.addressline2;
		let posttown = summary.posttown;
		let number = summary.number;
		let county = summary.county;
		let postcode = summary.postcode;
		
		this.setState({
			line1: line1,
			line2: line2,
			numberName: number,
			posttown: posttown,
			postcode: postcode,
			county: county,
			page: 2.5,
			manualEdit: false
		}, () => {
			this.store();
		});
	}

	editAddressButton = () => {
		this.setState({
			page: 2.25,
			manualEdit: true
		});
	}

	updateLine1 = (e) => {
		this.setState({
			line1: e.currentTarget.value
		});
	}

	updatePosttown = (e) => {
		this.setState({
			posttown: e.currentTarget.value
		});
	}

	updateCounty = (e) => {
		this.setState({
			county: e.currentTarget.value
		});
	}

	updatePostcodeManual = (e) => {
		this.setState({
			postcode: e.currentTarget.value
		});
	}

	editDescription = () => {
		if (document.getElementById("description").style.display !== "none") {
			document.getElementById("description").style.display = "none";
			document.getElementById("editor").style.display = "block";
			document.getElementById("editLink").innerHTML = "Save edits";
			document.getElementById("cancelLink").style.display = "block";
		} else {
			document.getElementById("description").style.display = "block";
			document.getElementById("editor").style.display = "none";
			document.getElementById("editLink").innerHTML = "Edit your description";
			document.getElementById("cancelLink").style.display = "none";
			this.setState({
				generatedDescription: this.state.description,
				manualEdit: true
			}, () => {
				this.store();
			});
		}
	}

	updateDescription = (e) => {
		this.setState({
			description: e.currentTarget.value
		});
	}

	cancelEditDescription = () => {
		this.setState({
			description: this.state.generatedDescription
		}, () => {
			document.getElementById("description").style.display = "block";
			document.getElementById("editor").style.display = "none";
			document.getElementById("editLink").innerHTML = "Edit your description";
			document.getElementById("cancelLink").style.display = "none";
		});
	}

	changeTone = () => {
		let tone = 0;
		if(this.state.descriptionTone === 0){
			tone = 1;
		}

		this.setState({
			descriptionTone: tone
		}, () => {
			this.generateDescription();
		});
	}

	updateAuth = (e) => {
		this.setState({
			securityCode: e.currentTarget.value
		});
	}

	calcAuth = (e) => {
		let input = e.currentTarget.value.replace(/,/g, "");

		let current = this.state.securityCode;
		if (input !== "." && input !== "backspace") {
			let value = input;
			if (current !== null) {
				value = current.toString() + input;
			}

			this.setState({
				securityCode: value
			});
		} else if (input === "backspace") {
			let value = current.toString().slice(0, current.toString().length - 1);

			this.setState({
				securityCode: value
			});
		} else if (input === ".") {
			if (current % 1 === 0) {
				let value = current.toString() + ".";
				this.setState({
					securityCode: value
				});
			}
		}
	}

	updatePhone = (e) => {
		this.setState({
			phonenumber: e.currentTarget.value
		});
	}

	calcPhone = (e) => {
		let input = e.currentTarget.value.replace(/,/g, "");

		let current = this.state.phonenumber;
		if (input !== "." && input !== "backspace") {
			let value = input;
			if (current !== null) {
				value = current.toString() + input;
			}

			this.setState({
				phonenumber: value
			});
		} else if (input === "backspace") {
			let value = current.toString().slice(0, current.toString().length - 1);

			this.setState({
				phonenumber: value
			});
		} else if (input === ".") {
			if (current % 1 === 0) {
				let value = current.toString() + ".";
				this.setState({
					phonenumber: value
				});
			}
		}
	}

	finish = () => {
		if(document.getElementById("phoneModal")){
			document.getElementById("phoneModal").style.display = "block";
		}
	}

	sendSMS = () => {
		if(document.getElementById("phoneModal")){
			document.getElementById("phoneModal").style.display = "none";
		}

		if(document.getElementById("authModal")){
			document.getElementById("authModal").style.display = "block";
		}

		let number = parseInt(this.state.phonenumber);
		number = number.toString();

		var myHeaders = new Headers();
		myHeaders.append("Content-Type", "application/json");
		myHeaders.append("X-Requested-With", "XMLHttpRequest");

		var raw = JSON.stringify({"user":{"mobile":"+44" + number}});

		var requestOptions = {
			method: 'POST',
			headers: myHeaders,
			body: raw,
			redirect: 'follow'
		};

		fetch("https://freeco-api.herokuapp.com/api/users/signup-sms", requestOptions)
			.then(response => response.text())
			.then(result => {
				result = JSON.parse(result);
				this.setState({codeId: result.codeId});
			})
			.catch(error => console.log('error', error));
	}

	confirmSMS = () => {
		let code = this.state.securityCode.toString();
		var myHeaders = new Headers();
		myHeaders.append("Content-Type", "application/json");
		myHeaders.append("X-Requested-With", "XMLHttpRequest");

		var raw = JSON.stringify({"user":{"code":code, "codeId": this.state.codeId}});

		var requestOptions = {
			method: 'POST',
			headers: myHeaders,
			body: raw,
			redirect: 'follow'
		};

		fetch("https://freeco-api.herokuapp.com/api/users/confirm-sms", requestOptions)
			.then(response => response.text())
			.then(result => {
				console.log(result);
				result = JSON.parse(result);
				if(result.user.token){
					let user = result.user;

					let floor = "ground-floor";
					if(this.state.propFloor !== null){
						floor = this.state.propFloor
					}
					let lift = "no";
					if(this.state.propHasLift !== null){
						lift = this.state.propHasLift;
					}
					let size = "0";
					if(this.state.propSize !== null){
						size = this.state.propSize.toString();
					}
					let leaseYears = "0";
					if(this.state.propLeaseYears !== null && this.state.propLeaseYears !== ""){
						leaseYears = this.state.propLeaseYears;
					}
					let extraCosts = "0";
					if(this.state.propExtraCosts !== null){
						extraCosts = this.state.propExtraCosts.toString();
					}
					let tone = "plain";
					if(this.state.descriptionTone === 1){
						tone = "agent";
					}
					let line2 = "_";
					if(this.state.line2 !== null){
						line2 = this.state.line2;
					}
					let number = "_";
					if(this.state.numberName !== null){
						number = this.state.numberName;
					}

					let listing = { 
						"listing": {
							"addPostcode": this.state.postcode,
							"addLine1": this.state.line1,
							"addTown": this.state.posttown,
							"addCounty": this.state.county,
							"userFirstName": this.state.firstName,
							"userLastName": this.state.lastName,
							"propBuilt": this.state.propBuilt,
							"propBeds": this.state.propBeds,
							"propBaths": this.state.propBaths,
							"propType": this.state.propType,
							"propFloor": floor,
							"propSize": size,
							"propLastDecorated": this.state.propLastDecorated,
							"propHasLift": lift,
							"propTenure": this.state.propTenure,
							"propLeaseYears": leaseYears,
							"propOutdoorSpace": this.state.propOutdoorSpace,
							"propParkingType": this.state.propParkingType,
							"propAskingPrice": this.state.propAskingPrice.toString(),
							"propHeatingType": this.state.propHeatingType,
							"propHaveEpc": this.state.propEPC,
							"descriptionGenerated": this.state.generatedDescription,
							"description": this.state.description,
							"descriptionEdited": this.state.description,
							"descriptionModified": this.state.manualEdit,
							"descriptionTone": tone,
							"propExtraCosts": extraCosts,
							"addLine2": line2,
							"addNumberName": number,
							"isPublished": false,
							"viewingAvailability": {
								"mon": {"am": true,"pm": true,"eve": true},
								"tue": {"am": true,"pm": true,"eve": true},
								"wed": {"am": true,"pm": true,"eve": true},
								"thurs": {"am": true,"pm": true,"eve": true},
								"fri": {"am": true,"pm": true,"eve": true},
								"sat": {"am": true,"pm": true,"eve": true},
								"sun": {"am": true,"pm": true,"eve": true}
							}
						}
					}

					localStorage.setItem("listing", JSON.stringify(listing));

					var myHeaders = new Headers();
					myHeaders.append("Content-Type", "application/json");
					myHeaders.append("X-Requested-With", "XMLHttpRequest");
					myHeaders.append("Authorization", "Token " + result.user.token);

					var requestOptions = {
						method: 'POST',
						headers: myHeaders,
						body: JSON.stringify(listing),
						redirect: 'follow'
					};

					fetch("https://freeco-api.herokuapp.com/api/listings", requestOptions)
					.then(response => response.json())
					.then(result => {
						console.log(result);
						localStorage.setItem("slug", result.listing.slug);
						localStorage.setItem("user", JSON.stringify(user));
						window.location.href = "http://free.co.uk/availability/?slug=" + result.listing.slug;	
					})
					.catch(error => console.log('error', error));	
				}
			})
			.catch(error => console.log('error', error));
	}

	leavePage = (e) => {
		let decision = e.currentTarget.value;
		if(decision === "yes"){
			window.location.href = "/";
		} else {
			document.getElementById("leaveModal").style.display = "none";
		}
	}

	render() {
		let page = this.state.page;

		let leaseYears = null;
		if (this.state.propLeaseYears !== null && this.state.propLeaseYears !== undefined) {
			leaseYears = this.state.propLeaseYears.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
		}

		let houseorflat = "house";
		if(this.state.propType === "flat"){
			houseorflat = "flat";
		}

		let displayPage = null;
		switch (page) {
			case 1:
				let dis = "disabled";
				if (this.state.postcode !== null) {
					if (this.state.postcode.length >= 6) {
						dis = "";
					}
				}
				displayPage =
					<div className="page1">
						<div className="pageHeading">
							What's your postcode?
						</div>
						<div className="pageContent">
							The postcode of the property you're advertising to sell.
						</div>
						<input className="postcodeInput" type="text" value={this.state.postcode} placeholder="e.g. SW1A 1AA" onChange={this.updatePostcode}></input>
						<br />
						<button className="capBtn" disabled={dis} onClick={this.postcodeSearch}>Search</button>
						<div id="leaveModal" className="modal">
							<div className="modalContent">
								<div className="pageHeading">
									Go back to Free.co.uk?
								</div>
								<button className="radio" value="yes" onClick={this.leavePage}>Yes</button><br/>
								<button className="radio" value="no" onClick={this.leavePage}>No</button>
							</div>
						</div>
					</div>;
				break;

			case 2:
				displayPage =
					<div className="page2">
						<div className="pageHeading">
							Select the address
						</div>
						<div className="addressSelect">
							{this.state.addressSelect}
						</div>
						<div className="pageContent">
							Can't find your address?
							<br />
							<a className="linkText" onClick={this.editAddressButton}>Enter it manually</a>
						</div>
					</div>;
				break;

			case 2.25:
				displayPage =
					<div className="page2">
						<div className="pageHeading">
							Manually add the address
						</div>
						<div className="addressSelect">
							<input className="radio" onChange={this.updateLine1} value={this.state.line1} placeholder="Address line 1"></input><br />
							<input className="radio" onChange={this.updatePosttown} value={this.state.posttown} placeholder="Address line 2 (Optional)"></input><br />
							<input className="radio" onChange={this.updateCounty} value={this.state.county} placeholder="Town"></input><br />
							<input className="radio" onChange={this.updatePostcodeManual} value={this.state.postcode} placeholder="Postcode"></input><br />
						</div>
						<button className="capBtn" onClick={this.nextPage}>next</button>
					</div>;
				break;

			case 2.5:
				let br = <br />;
				if (this.state.posttown === null || this.state.posttown === "") {
					br = null;
				}
				displayPage =
					<div className="page2">
						<div className="pageHeading">
							Is this correct?
						</div>
						<div className="addressSelect">
							{this.state.line1}
							<br />
							{this.state.posttown}
							{br}
							{this.state.county}
							<br />
							{this.state.postcode}
						</div>
						<button className="capBtn" onClick={this.nextPage}>Yes!</button>
						<br />
						<a className="linkText" onClick={this.editAddressButton}>This address isn't right</a>
					</div>;
				break;

			case 3:
				let dis2 = "disabled";
				if (this.state.firstName !== null && this.state.firstName !== "" && this.state.lastName !== null && this.state.lastName !== "") {
					dis2 = "";
				}
				displayPage =
					<div className="page3">
						<div className="pageHeading">
							What's your name?
						</div>
						<div className="pageContent">
							This won't appear in your advert, and will only be used for the property viewings you accept.
						</div>
						<input className="firstNameInput" type="text" value={this.state.firstName} placeholder="First name" onChange={this.updateFirstName}></input>
						<br />
						<input className="lastNameInput" type="text" value={this.state.lastName} placeholder="Last name" onChange={this.updateLastName}></input>
						<br />
						<button className="capBtn" disabled={dis2} onClick={this.nextPage}>Next</button>
					</div>;
				break;

			case 4:
				displayPage =
					<div className="page4">
						<div className="pageHeading">
							What type of property are you selling?
						</div>
						<button className="radio" onClick={this.updatePropType} value="detached-house">Detached house<img style={{ visibility: this.state.propType === "detached-house" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" onClick={this.updatePropType} value="semi-detached-house">Semi-detached house<img style={{ visibility: this.state.propType === "semi-detached-house" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" onClick={this.updatePropType} value="terraced-house">Terrace house<img style={{ visibility: this.state.propType === "terraced-house" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" onClick={this.updatePropType} value="bungalow">Bungalow<img style={{ visibility: this.state.propType === "bungalow" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" onClick={this.updatePropType} value="flat">Flat<img style={{ visibility: this.state.propType === "flat" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button>
					</div>;
				break;

			case 4.3:
				displayPage =
					<div className="page5">
						<div className="pageHeading">
							What floor is the flat on?
						</div>
						<button className="radio" onClick={this.updatePropFloor} value="basement">Basement<img style={{ visibility: this.state.propFloor === "basement" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" onClick={this.updatePropFloor} value="ground-floor">Ground floor<img style={{ visibility: this.state.propFloor === "ground-floor" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" onClick={this.updatePropFloor} value="first-floor">1st floor<img style={{ visibility: this.state.propFloor === "first-floor" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" onClick={this.updatePropFloor} value="second-floor">2nd floor<img style={{ visibility: this.state.propFloor === "second-floor" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" onClick={this.updatePropFloor} value="higher">Higher<img style={{ visibility: this.state.propFloor === "higher" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button>
					</div>;
				break;

			case 4.6:
				let floor = "basement";
				if (this.state.propFloor === "first-floor") {
					floor = "first floor";
				} else if (this.state.propFloor === "second-floor") {
					floor = "second floor";
				} else if (this.state.propFloor === "higher") {
					floor = "upper floors";
				}

				displayPage =
					<div>
						<div className="pageHeading">
							Is there a lift to the {floor}?
						</div>
						<button className="radio" onClick={this.updatePropHasLift} value="higher-with-lift">Yes<img style={{ visibility: this.state.propHasLift === "higher-with-lift" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" onClick={this.updatePropHasLift} value="higher-no-lift">No<img style={{ visibility: this.state.propHasLift === "higher-no-lift" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button>
					</div>;
				break;

			case 5:
				displayPage =
					<div>
						<div className="pageHeading">
							What do you own of the {houseorflat}?
						</div>
						<button className="radio" onClick={this.updatePropTenure} value="freehold">Freehold<img style={{ visibility: this.state.propTenure === "freehold" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" onClick={this.updatePropTenure} value="flying-freehold">Flying freehold<img style={{ visibility: this.state.propTenure === "flying-freehold" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" onClick={this.updatePropTenure} value="share-of-freehold">Share of freehold<img style={{ visibility: this.state.propTenure === "share-of-freehold" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" onClick={this.updatePropTenure} value="leasehold">Leasehold<img style={{ visibility: this.state.propTenure === "leasehold" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button>
					</div>;
				break;

			case 5.3:
				let calculator =
					<div className="calculator">
						<button className="calc-btn" value="1" onClick={this.calcPropLeaseYears}>1</button>
						<button className="calc-btn" value="2" onClick={this.calcPropLeaseYears}>2</button>
						<button className="calc-btn" value="3" onClick={this.calcPropLeaseYears}>3</button>
						<button className="calc-btn" value="4" onClick={this.calcPropLeaseYears}>4</button>
						<button className="calc-btn" value="5" onClick={this.calcPropLeaseYears}>5</button>
						<button className="calc-btn" value="6" onClick={this.calcPropLeaseYears}>6</button>
						<button className="calc-btn" value="7" onClick={this.calcPropLeaseYears}>7</button>
						<button className="calc-btn" value="8" onClick={this.calcPropLeaseYears}>8</button>
						<button className="calc-btn" value="9" onClick={this.calcPropLeaseYears}>9</button>
						<button disabled="disabled" className="calc-btn" value="." onClick={this.calcPropLeaseYears}>.</button>
						<button className="calc-btn" value="0" onClick={this.calcPropLeaseYears}>0</button>
						<button className="calc-btn" value="backspace" onClick={this.calcPropLeaseYears}><img value="backspace" src={require("./img/icon-keypad-delete.svg")}></img></button>
					</div>;
				displayPage =
					<div>
						<div className="pageHeading">
							How many full years are left on the lease?
						</div>
						<input type="text" value={this.state.propLeaseYears} className="leaseInput" onChange={this.updatePropLeaseYears}></input>
						<span className="input-label">years</span><br />
						{calculator}
						<br />
						<button className="capBtn" onClick={this.nextPage}>next</button>
					</div>;
				break;

			case 5.6:
				let extraCalculator =
					<div className="calculator">
						<button className="calc-btn" value="1" onClick={this.calcPropExtraCosts}>1</button>
						<button className="calc-btn" value="2" onClick={this.calcPropExtraCosts}>2</button>
						<button className="calc-btn" value="3" onClick={this.calcPropExtraCosts}>3</button>
						<button className="calc-btn" value="4" onClick={this.calcPropExtraCosts}>4</button>
						<button className="calc-btn" value="5" onClick={this.calcPropExtraCosts}>5</button>
						<button className="calc-btn" value="6" onClick={this.calcPropExtraCosts}>6</button>
						<button className="calc-btn" value="7" onClick={this.calcPropExtraCosts}>7</button>
						<button className="calc-btn" value="8" onClick={this.calcPropExtraCosts}>8</button>
						<button className="calc-btn" value="9" onClick={this.calcPropExtraCosts}>9</button>
						<button disabled="disabled" className="calc-btn" value="." onClick={this.calcPropExtraCosts}>.</button>
						<button className="calc-btn" value="0" onClick={this.calcPropExtraCosts}>0</button>
						<button className="calc-btn" value="backspace" onClick={this.calcPropExtraCosts}><img value="backspace" src={require("./img/icon-keypad-delete.svg")}></img></button>
					</div>;

				let costs = "";
				if (this.state.propExtraCosts !== null && this.state.propExtraCosts !== "") {
					costs = this.state.propExtraCosts.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
				}

				displayPage =
					<div>
						<div className="pageHeading">
							Are there any associated additional costs?
						</div>
						<div className="pageContent">
							This includes ground rent and any other additional service charges.
						</div>
						<span className="input-label-pound input-label">£</span>
						<input type="text" value={costs} className="leaseInput priceInput" onChange={this.updatePropExtraCosts}></input>
						<br/>
						{extraCalculator}
						<br />
						<button className="capBtn" onClick={this.nextPage}>next</button>
					</div>;
				break;

			case 6:
				displayPage =
					<div>
						<div className="pageHeading">
							How many bedrooms are there?
						</div>
						<button className="radio" value="1" onClick={this.updatePropBeds}>1 bedroom<img style={{ visibility: this.state.propBeds === "1" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" value="2" onClick={this.updatePropBeds}>2 bedrooms<img style={{ visibility: this.state.propBeds === "2" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" value="3" onClick={this.updatePropBeds}>3 bedrooms<img style={{ visibility: this.state.propBeds === "3" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<div className="accordion" id="bedroomAccordion">
							<button className="radio" value="4" onClick={this.updatePropBeds}>4 bedrooms<img style={{ visibility: this.state.propBeds === "4" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
							<button className="radio" value="5" onClick={this.updatePropBeds}>5 bedrooms<img style={{ visibility: this.state.propBeds === "5" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
							<button className="radio" value="6" onClick={this.updatePropBeds}>6 bedrooms<img style={{ visibility: this.state.propBeds === "6" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
							<button className="radio" value="7" onClick={this.updatePropBeds}>7 bedrooms<img style={{ visibility: this.state.propBeds === "7" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
							<button className="radio" value="8" onClick={this.updatePropBeds}>8 bedrooms<img style={{ visibility: this.state.propBeds === "8" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
							<button className="radio" value="9" onClick={this.updatePropBeds}>9 bedrooms<img style={{ visibility: this.state.propBeds === "9" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
							<button className="radio" value="10" onClick={this.updatePropBeds}>10 or more bedrooms<img style={{ visibility: this.state.propBeds === "10" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button>
						</div>
						<button id="moreBedrooms" className="radio" value="bedroomAccordion" onClick={this.accordion}>More options</button>
					</div>;
				break;

			case 7:
				displayPage =
					<div>
						<div className="pageHeading">
							...and what about bathrooms?
						</div>
						<button className="radio" value="1" onClick={this.updatePropBaths}>1 bathroom<img style={{ visibility: this.state.propBaths === "1" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" value="2" onClick={this.updatePropBaths}>2 bathrooms<img style={{ visibility: this.state.propBaths === "2" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" value="3" onClick={this.updatePropBaths}>3 bathrooms<img style={{ visibility: this.state.propBaths === "3" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button>
						<div className="accordion" id="bathroomAccordion">
							<button className="radio" value="4" onClick={this.updatePropBaths}>4 bathrooms<img style={{ visibility: this.state.propBaths === "4" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
							<button className="radio" value="5" onClick={this.updatePropBaths}>5 bathrooms<img style={{ visibility: this.state.propBaths === "5" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
							<button className="radio" value="6" onClick={this.updatePropBaths}>6 bathrooms<img style={{ visibility: this.state.propBaths === "6" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
							<button className="radio" value="7" onClick={this.updatePropBaths}>7 bathrooms<img style={{ visibility: this.state.propBaths === "7" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
							<button className="radio" value="8" onClick={this.updatePropBaths}>8 bathrooms<img style={{ visibility: this.state.propBaths === "8" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
							<button className="radio" value="9" onClick={this.updatePropBaths}>9 bathrooms<img style={{ visibility: this.state.propBaths === "9" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
							<button className="radio" value="10" onClick={this.updatePropBaths}>10 or more bathrooms<img style={{ visibility: this.state.propBaths === "10" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button>
						</div>
						<button id="moreBathrooms" className="radio" value="bathroomAccordion" onClick={this.accordion}>More options</button>
					</div>;
				break;

			case 8:
				let sizeCalculator =
					<div className="calculator">
						<button className="calc-btn" value="1" onClick={this.calcPropSize}>1</button>
						<button className="calc-btn" value="2" onClick={this.calcPropSize}>2</button>
						<button className="calc-btn" value="3" onClick={this.calcPropSize}>3</button>
						<button className="calc-btn" value="4" onClick={this.calcPropSize}>4</button>
						<button className="calc-btn" value="5" onClick={this.calcPropSize}>5</button>
						<button className="calc-btn" value="6" onClick={this.calcPropSize}>6</button>
						<button className="calc-btn" value="7" onClick={this.calcPropSize}>7</button>
						<button className="calc-btn" value="8" onClick={this.calcPropSize}>8</button>
						<button className="calc-btn" value="9" onClick={this.calcPropSize}>9</button>
						<button disabled="disabled" className="calc-btn" value="." onClick={this.calcPropSize}>.</button>
						<button className="calc-btn" value="0" onClick={this.calcPropSize}>0</button>
						<button className="calc-btn" value="backspace" onClick={this.calcPropSize}><img value="backspace" src={require("./img/icon-keypad-delete.svg")}></img></button>
					</div>;

				displayPage =
					<div>
						<div className="pageHeading">
							How big is the floor space?
						</div>
						<div className="pageContent">
							The average for a two bedroom house with one bathroom is 500 square feet.
						</div>
						<input type="text" value={this.state.propSize} className="leaseInput" onChange={this.updatePropSize}></input>
						<span className="input-label">sq. ft</span><br />
						{sizeCalculator}
						<br />
						<button className="capBtn" onClick={this.nextPage}>next</button>
					</div>;
				break;

			case 9:
				displayPage =
					<div>
						<div className="pageHeading">
							When was the {houseorflat} built?
						</div>
						<div className="pageContent">
							This doesn't have to be exact - it's to give buyers a better sense of the property.
						</div>
						<button className="radio" value="brandNew" onClick={this.updatePropBuilt}>Brand new<img style={{ visibility: this.state.propBuilt === "brandNew" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" value="last20Years" onClick={this.updatePropBuilt}>2000 - 2019<img style={{ visibility: this.state.propBuilt === "last20Years" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" value="between1950and2000" onClick={this.updatePropBuilt}>1950 - 2000<img style={{ visibility: this.state.propBuilt === "between1950and2000" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" value="between1900and1950" onClick={this.updatePropBuilt}>1900 - 1950<img style={{ visibility: this.state.propBuilt === "between1900and1950" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" value="pre1900" onClick={this.updatePropBuilt}>1900 or earlier<img style={{ visibility: this.state.propBuilt === "pre1900" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button>
					</div>;
				break;

			case 10:
				displayPage =
					<div>
						<div className="pageHeading">
							When was the {houseorflat} last decorated?
						</div>
						<div className="pageContent">
							A good indicator is often when the kitchen and bathroom were last updated.
						</div>
						<button className="radio" value="thisYear" onClick={this.updatePropLastDecorated}>This year<img style={{ visibility: this.state.propLastDecorated === "thisYear" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" value="year2019" onClick={this.updatePropLastDecorated}>2019<img style={{ visibility: this.state.propLastDecorated === "year2019" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" value="year2018" onClick={this.updatePropLastDecorated}>2018<img style={{ visibility: this.state.propLastDecorated === "year2018" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" value="pre2018" onClick={this.updatePropLastDecorated}>2017 or earlier<img style={{ visibility: this.state.propLastDecorated === "pre2018" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button>
					</div>;
				break;

			case 11:
				displayPage =
					<div>
						<div className="pageHeading">
							How is the {houseorflat} heated?
						</div>
						<div className="pageContent">
							In most households in the UK the boiler is gas powered.
						</div>
						<button className="radio" value="electric" onClick={this.updatePropHeatingType}>Electric<img style={{ visibility: this.state.propHeatingType === "electric" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" value="gas" onClick={this.updatePropHeatingType}>Gas<img style={{ visibility: this.state.propHeatingType === "gas" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" value="oil" onClick={this.updatePropHeatingType}>Oil<img style={{ visibility: this.state.propHeatingType === "oil" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" value="exchanger" onClick={this.updatePropHeatingType}>Heat exchanger<img style={{ visibility: this.state.propHeatingType === "exchanger" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button>
					</div>;
				break;

			case 12:
				displayPage =
					<div>
						<div className="pageHeading">
							Is there any outside space?
						</div>
						<div className="pageContent">
							Any associated outside space can be an appealing selling point for a property.
						</div>
						<button className="radio" value="private-garden" onClick={this.updatePropOutdoorSpace}>Private garden<img style={{ visibility: this.state.propOutdoorSpace === "private-garden" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" value="patio" onClick={this.updatePropOutdoorSpace}>Patio<img style={{ visibility: this.state.propOutdoorSpace === "patio" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" value="terrace" onClick={this.updatePropOutdoorSpace}>Terrace<img style={{ visibility: this.state.propOutdoorSpace === "terrace" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" value="no-outside" onClick={this.updatePropOutdoorSpace}>No outside space<img style={{ visibility: this.state.propOutdoorSpace === "no-outside" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button>
					</div>;
				break;

			case 13:
				displayPage =
					<div>
						<div className="pageHeading">
							Do you have an EPC for the property?
						</div>
						<div className="pageContent">
							You're legally required to have an EPC (Energy Performance Certificate) to sell your property.
						</div>
						<button className="radio" value="yes" onClick={this.updatePropEPC}>Yes<img style={{ visibility: this.state.propEPC === "yes" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" value="no" onClick={this.updatePropEPC}>No<img style={{ visibility: this.state.propEPC === "no" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button>
					</div>;
				break;

			case 14:
				displayPage =
					<div>
						<div className="pageHeading">
							Where can you park a car?
						</div>
						<div className="pageContent">
							Any associated parking space can be an appealing selling point for a property.
						</div>
						<button className="radio" value="garage" onClick={this.updatePropParkingType}>Garage<img style={{ visibility: this.state.propParkingType === "garage" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" value="driveway" onClick={this.updatePropParkingType}>Driveway<img style={{ visibility: this.state.propParkingType === "driveway" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" value="permit" onClick={this.updatePropParkingType}>Permit required<img style={{ visibility: this.state.propParkingType === "permit" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button>
						<div className="accordion" id="parkingAccordion">
							<button className="radio" value="on-street" onClick={this.updatePropParkingType}>Free on street<img style={{ visibility: this.state.propParkingType === "on-street" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
							<button className="radio" value="off-street" onClick={this.updatePropParkingType}>Off street<img style={{ visibility: this.state.propParkingType === "off-street" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
							<button className="radio" value="garage-and-driveway" onClick={this.updatePropParkingType}>Garage and driveway<img style={{ visibility: this.state.propParkingType === "garage-and-driveway" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
							<button className="radio" value="allocated" onClick={this.updatePropParkingType}>Allocated space<img style={{ visibility: this.state.propParkingType === "allocated" ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button>
						</div>
						<button id="moreParking" className="radio" value="parkingAccordion" onClick={this.accordion}>More options</button>
					</div>;
				break;

			case 15:
				displayPage =
					<div>
						<div className="pageHeading">
							Does your home have any other features?
						</div>
						<div className="pageContent">
							If your home includes any of these features, make sure to include them (they'll be a magnet for enquiries!)
						</div>
						<button className="radio" value="central-heating" onClick={this.updatePropExtras}>Central heating<img style={{ visibility: this.state.propExtras.includes("central-heating") ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" value="double-glazing" onClick={this.updatePropExtras}>Double glazing<img style={{ visibility: this.state.propExtras.includes("double-glazing") ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" value="secure-doors-and-windows" onClick={this.updatePropExtras}>Secure doors & windows<img style={{ visibility: this.state.propExtras.includes("secure-doors-and-windows") ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" value="bath-tub" onClick={this.updatePropExtras}>Bathtub<img style={{ visibility: this.state.propExtras.includes("bath-tub") ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" value="hot-tub" onClick={this.updatePropExtras}>Hot tub<img style={{ visibility: this.state.propExtras.includes("hot-tub") ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" value="period-features" onClick={this.updatePropExtras}>Period features<img style={{ visibility: this.state.propExtras.includes("period-features") ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" value="swimming-pool" onClick={this.updatePropExtras}>Swimming pool<img style={{ visibility: this.state.propExtras.includes("swimming-pool") ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" value="loft" onClick={this.updatePropExtras}>Loft<img style={{ visibility: this.state.propExtras.includes("loft") ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" value="basement" onClick={this.updatePropExtras}>Basement<img style={{ visibility: this.state.propExtras.includes("basement") ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" value="conservatory" onClick={this.updatePropExtras}>Conservatory<img style={{ visibility: this.state.propExtras.includes("conservatory") ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" value="sauna" onClick={this.updatePropExtras}>Sauna<img style={{ visibility: this.state.propExtras.includes("sauna") ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" value="alarm-system" onClick={this.updatePropExtras}>Alarm system<img style={{ visibility: this.state.propExtras.includes("alarm-system") ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" value="fitted-wardrobes" onClick={this.updatePropExtras}>Fitted wardrobes<img style={{ visibility: this.state.propExtras.includes("fitted-wardrobes") ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" value="power-shower" onClick={this.updatePropExtras}>Power shower<img style={{ visibility: this.state.propExtras.includes("power-shower") ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="radio" value="fireplace" onClick={this.updatePropExtras}>Fireplace<img style={{ visibility: this.state.propExtras.includes("fireplace") ? "visible" : "hidden" }} className="tick" src={require("./img/icon-tick.svg")}></img></button><br />
						<button className="capBtn" onClick={this.nextPage}>next</button>
					</div>;
				break;

			case 16:
				let priceCalculator =
					<div className="calculator">
						<button className="calc-btn" value="1" onClick={this.calcPropAskingPrice}>1</button>
						<button className="calc-btn" value="2" onClick={this.calcPropAskingPrice}>2</button>
						<button className="calc-btn" value="3" onClick={this.calcPropAskingPrice}>3</button>
						<button className="calc-btn" value="4" onClick={this.calcPropAskingPrice}>4</button>
						<button className="calc-btn" value="5" onClick={this.calcPropAskingPrice}>5</button>
						<button className="calc-btn" value="6" onClick={this.calcPropAskingPrice}>6</button>
						<button className="calc-btn" value="7" onClick={this.calcPropAskingPrice}>7</button>
						<button className="calc-btn" value="8" onClick={this.calcPropAskingPrice}>8</button>
						<button className="calc-btn" value="9" onClick={this.calcPropAskingPrice}>9</button>
						<button disabled="disabled" className="calc-btn" value="." onClick={this.calcPropAskingPrice}>.</button>
						<button className="calc-btn" value="0" onClick={this.calcPropAskingPrice}>0</button>
						<button className="calc-btn" value="backspace" onClick={this.calcPropAskingPrice}><img value="backspace" src={require("./img/icon-keypad-delete.svg")}></img></button>
					</div>;

				let price = "";
				if (this.state.propAskingPrice !== null && this.state.propAskingPrice !== "") {
					price = this.state.propAskingPrice.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
				}

				displayPage =
					<div>
						<div className="pageHeading">
							How much are you selling for?
						</div>
						<div className="pageContent">
							You'll always be able to adjust your price, even once your advert has been published.
						</div>
						<span className="input-label-pound input-label">£</span>
						<input type="text" value={price} className="leaseInput priceInput" onChange={this.updatePropAskingPrice}></input><br />
						{priceCalculator}
						<br />
						<button className="capBtn" onClick={this.nextPage}>next</button>
					</div>;
				break;

			case 17:
				displayPage =
					<div className="encompass">
						<div className="float-middle">
							<div className="pageHeading">
								<img className="drum" src={require("./img/drum-framed.gif")}></img>
								<br />
								Drum roll please...
							</div>
						</div>
					</div>;
				break;

			case 18:
				let slideDisplay = "block";
				if(this.state.manualEdit === true){
					slideDisplay = "none";
				}

				let phoneCalculator =
					<div className="calculator">
						<button className="calc-btn" value="1" onClick={this.calcPhone}>1</button>
						<button className="calc-btn" value="2" onClick={this.calcPhone}>2</button>
						<button className="calc-btn" value="3" onClick={this.calcPhone}>3</button>
						<button className="calc-btn" value="4" onClick={this.calcPhone}>4</button>
						<button className="calc-btn" value="5" onClick={this.calcPhone}>5</button>
						<button className="calc-btn" value="6" onClick={this.calcPhone}>6</button>
						<button className="calc-btn" value="7" onClick={this.calcPhone}>7</button>
						<button className="calc-btn" value="8" onClick={this.calcPhone}>8</button>
						<button className="calc-btn" value="9" onClick={this.calcPhone}>9</button>
						<button disabled="disabled" className="calc-btn" value="." onClick={this.calcPhone}>.</button>
						<button className="calc-btn" value="0" onClick={this.calcPhone}>0</button>
						<button className="calc-btn" value="backspace" onClick={this.calcPhone}><img value="backspace" src={require("./img/icon-keypad-delete.svg")}></img></button>
					</div>;

				let authCalculator =
					<div className="calculator">
						<button className="calc-btn" value="1" onClick={this.calcAuth}>1</button>
						<button className="calc-btn" value="2" onClick={this.calcAuth}>2</button>
						<button className="calc-btn" value="3" onClick={this.calcAuth}>3</button>
						<button className="calc-btn" value="4" onClick={this.calcAuth}>4</button>
						<button className="calc-btn" value="5" onClick={this.calcAuth}>5</button>
						<button className="calc-btn" value="6" onClick={this.calcAuth}>6</button>
						<button className="calc-btn" value="7" onClick={this.calcAuth}>7</button>
						<button className="calc-btn" value="8" onClick={this.calcAuth}>8</button>
						<button className="calc-btn" value="9" onClick={this.calcAuth}>9</button>
						<button disabled="disabled" className="calc-btn" value="." onClick={this.calcAuth}>.</button>
						<button className="calc-btn" value="0" onClick={this.calcAuth}>0</button>
						<button className="calc-btn" value="backspace" onClick={this.calcAuth}><img value="backspace" src={require("./img/icon-keypad-delete.svg")}></img></button>
					</div>;

				let btnDisplay = "disabled";
				if(this.state.phonenumber !== null){
					if(this.state.phonenumber.length >= 10){
						btnDisplay = "";
					}
				}	

				displayPage =
					<div>
						<div className="pageHeading">
							We've generated your description!
						</div>
						<div className="pageContent">
							It's straight speaking, but we made you a filter if you miss the BS (what we call agent-speak!)
							<br />
							<div className="descriptionBox">
								<div className="boxHead">
									Agent-speaking filter
									<label class="switch" style={{display: slideDisplay}}>
										<input type="checkbox"></input>
										<span class="slider round" onClick={this.changeTone}></span>
									</label>
								</div>
								<div className="boxContent">
									<div id="description">
										{this.state.description}
									</div>
									<textarea id='editor' value={this.state.description} onChange={this.updateDescription}>{this.state.description}</textarea>
									<br />
									<span id="editLink" className="link" onClick={this.editDescription}>
										Edit your description
									</span>
									<span id="cancelLink" className="link" onClick={this.cancelEditDescription}>
										Cancel changes
									</span>
								</div>
							</div>
							<button className="capBtn" onClick={this.finish}>finish</button>
							{/* <textarea className="paraInput" value={this.state.description}></textarea> */}
						</div>
						{/* <button className="capBtn" onClick={this.nextPage}>next</button> */}
						<div id="phoneModal" className="mobileModal modal">
							<div className="modalContent">
								<div className="pageHeading">
									Your mobile number
								</div>
								<div className="pageContent">
									We'll send you a 6-digit security code to verify your account.
								</div>
								<span className="input-label-pound input-label">+44</span>
								<input onChange={this.updatePhone} className="leaseInput priceInput phoneInput" value={this.state.phonenumber} placeholder="Enter phone number"></input>
								<br/>
								{phoneCalculator}
								<button disabled={btnDisplay} className="capBtn" onClick={this.sendSMS}>next</button>
								<div className="pageContent light-grey">
									We won't call you, but we'll text to confirm your property viewings.
								</div>
							</div>
						</div>

						<div id="authModal" className="mobileModal modal">
							<div className="modalContent">
								<div className="pageHeading">
									Please enter your 6-digit security code
								</div>
								<input type="password" onChange={this.updateAuth} className="leaseInput codeInput" value={this.state.securityCode} placeholder="••••••"></input>
								<br/>
								{authCalculator}
								<br/>
								<button className="capBtn" onClick={this.confirmSMS}>verify</button>
								<div className="pageContent light-grey">
									A 6-digit security code was sent via SMS to +44 (0){parseInt(this.state.phonenumber)}.
									<br/>
									<span className="link grey-link" onClick={this.sendSMS}>
										Resend message
									</span>
								</div>
							</div>
						</div>
					</div>;
				break;
		}

		let progress = 100 / 17 * (this.state.page - 1);

		return (
			<div className="listing-flow">
				<div className="navbar">
					<button className="back" onClick={this.prevPage}>
						<img className="backArrow" src={require("./img/icon-arrow-back.svg")}></img>
						<div className="backText">
							Back
						</div>
					</button>
					<img className="logo" src={require("./img/logo.svg")}></img>
				</div>
				<div className="progress-bar" style={{display: this.state.page !== 1 && this.state.page !== 18 ? "inline-block" : "none"}}>
					<div className="progress" style={{ width: progress + "%" }}>

					</div>
				</div>
				<div className="page">
					{displayPage}
				</div>
			</div>
		)
	}



//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////DESCRIPTION GENERATOR///////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

	generateDescription() {
		function getRandom(min, max) {
			return Math.floor(Math.random() * (max - min)) + min;
		}

		let propDescription = "";

		let propTypeDescription = "";
		let propFloorDescription = "";
		let propLastDecoratedDescription = "";
		let propBuiltDescription = "";
		let propBedsDescription = "";
		let propBathsDescription = "";
		let propOutdoorSpaceDescription = "";
		let propParkingTypeDescription = "";
		let propSizeDescription = "";
		let propHasLiftDescription = "";
		let propHeatingTypeDescription = "";
		let propAskingPriceDescription = "";
		let propTenureDescription = "";
		let propLeaseYearsDescription = "";
		let propExtraCostsDescription = "";

		//----------------------------------------------------------------------------------------------//

		let propType = this.state.propType;
		let propFloor = this.state.propFloor;
		let propLastDecorated = this.state.propLastDecorated;
		let propBuilt = this.state.propBuilt;
		let propBeds = this.state.propBeds;
		let propBaths = this.state.propBaths;
		let propOutdoorSpace = this.state.propOutdoorSpace;
		let propParkingType = this.state.propParkingType;
		let propSize = this.state.propSize;
		let propHasLift = this.state.propHasLift;
		let propHeatingType = this.state.propHeatingType;
		let propAskingPrice = this.state.propAskingPrice;
		let propTenure = this.state.propTenure;
		let propLeaseYears = this.state.propLeaseYears;
		let propExtraCosts = this.state.propExtraCosts;
		let descriptionTone = this.state.descriptionTone;

		//----------------------------------------------------------------------------------------------//

		if (descriptionTone === 0) {
			console.log("Plain");
		} else if (descriptionTone === 1) {
			console.log("Agent");
		}

		let randomiser = getRandom(0, 3);
		if(this.state.randomiser !== null){
			randomiser = this.state.randomiser;
		} else {
			this.setState({
				randomiser: randomiser
			});
		}

		switch (descriptionTone) {

			// --------------------------------------------Plain---------------------------------------------------------






			case 0:

				if (randomiser === 0) {
					switch (propType) {
						case "detached-house":
							propTypeDescription = "detached house";
							break;
						case "semi-detached-house":
							propTypeDescription = "semi-detached house";
							break;
						case "terraced-house":
							propTypeDescription = "terraced house";
							break;
						case "bungalow":
							propTypeDescription = "bungalow";
							break;
						case "flat":
							propTypeDescription = "apartment";
							break;
					}

					propDescription = propTypeDescription;

					if (propType === "flat") {
						switch (propFloor) {
							case "basement":
								propFloorDescription = "basement";
								propDescription = propFloorDescription + " " + propTypeDescription;
								break;
							case "ground-floor":
								propFloorDescription = "on the ground floor";
								propDescription += " " + propFloorDescription;
								break;
							case "first-floor":
								propFloorDescription = "on the first floor";
								propDescription += " " + propFloorDescription;
								break;
							case "second-floor":
								propFloorDescription = "on the second floor";
								propDescription += " " + propFloorDescription;
								break;
							case "higher":
								propFloorDescription = "on an upper floor";
								propDescription += " " + propFloorDescription;
								break;
							default:
								propFloorDescription = "";
								propDescription += propFloorDescription;
								break;
						}
					}

					let aAn = "A ";
					if (propType === "flat" && propFloor != "basement") {
						aAn = "An ";
					}

					switch (propLastDecorated) {
						case "thisYear":
							propLastDecoratedDescription = ", decorated this year";
							propDescription = aAn + propDescription + propLastDecoratedDescription + " and ";
							break;
						case "year2019":
							propLastDecoratedDescription = ", decorated last year";
							propDescription = aAn + propDescription + propLastDecoratedDescription + " and ";
							break;
						case "year2018":
							propLastDecoratedDescription = ", last decorated in 2018";
							propDescription = aAn + propDescription + propLastDecoratedDescription + " and ";
							break;
						case "pre2018":
							propLastDecoratedDescription = ", last decorated before 2018";
							propDescription = aAn + propDescription + propLastDecoratedDescription + " and ";
							break;
					}

					switch (propBuilt) {
						case "brandNew":
							propBuiltDescription = "built this year. ";
							propDescription += propBuiltDescription;
							break;
						case "last20Years":
							propBuiltDescription = "built in the last 20 years. ";
							propDescription += propBuiltDescription;
							break;
						case "between1950and2000":
							propBuiltDescription = "built between 1950 and 2000. ";
							propDescription += propBuiltDescription;
							break;
						case "between1900and1950":
							propBuiltDescription = "built between 1900 and 1950. ";
							propDescription += propBuiltDescription;
							break;
						case "pre1900":
							propBuiltDescription = "built before 1900. ";
							propDescription += propBuiltDescription;
							break;
					}

					switch (propBeds) {
						case 0:
							propBedsDescription = "It has a studio bedroom"
							propDescription += propBedsDescription;
							break;
						case 1:
							propBedsDescription = "It has one bedroom"
							propDescription += propBedsDescription;
							break;
						case 2:
							propBedsDescription = "It has two bedrooms"
							propDescription += propBedsDescription;
							break;
						case 3:
							propBedsDescription = "It has three bedrooms"
							propDescription += propBedsDescription;
							break;
						case 4:
							propBedsDescription = "It has four bedrooms"
							propDescription += propBedsDescription;
							break;
						case 5:
							propBedsDescription = "It has five bedrooms"
							propDescription += propBedsDescription;
							break;
						case 6:
							propBedsDescription = "It has six bedrooms"
							propDescription += propBedsDescription;
							break;
						case 7:
							propBedsDescription = "It has seven bedrooms"
							propDescription += propBedsDescription;
							break;
						case 8:
							propBedsDescription = "It has eight bedrooms"
							propDescription += propBedsDescription;
							break;
						case 9:
							propBedsDescription = "It has nine bedrooms"
							propDescription += propBedsDescription;
							break;
						default:
							propBedsDescription = "It has many bedrooms"
							propDescription += propBedsDescription;
							break;
					}

					switch (propBaths) {
						case 1:
							propBathsDescription = " and one bathroom"
							propDescription += propBathsDescription;
							break;
						case 2:
							propBathsDescription = " and two bathrooms"
							propDescription += propBathsDescription;
							break;
						case 3:
							propBathsDescription = " and three bathrooms"
							propDescription += propBathsDescription;
							break;
						default:
							let numberBaths = "many";
							if (propBaths === 4) {
								numberBaths = "four";
							} else if (propBaths === 5) {
								numberBaths = "five";
							} else if (propBaths === 6) {
								numberBaths = "six";
							} else if (propBaths === 7) {
								numberBaths = "seven";
							} else if (propBaths === 8) {
								numberBaths = "eight";
							} else if (propBaths === 9) {
								numberBaths = "nine";
							}
							propBathsDescription = " and " + numberBaths + " bathrooms"
							propDescription += propBathsDescription;
							break;
					}

					propDescription += ".";

					switch (propOutdoorSpace) {
						case "private-garden":
							propOutdoorSpaceDescription = " It has a private garden"
							propDescription += propOutdoorSpaceDescription;
							break;
						case "patio":
							propOutdoorSpaceDescription = " It has a patio area"
							propDescription += propOutdoorSpaceDescription;
							break;
						case "terrace":
							propOutdoorSpaceDescription = " It has a terrace"
							propDescription += propOutdoorSpaceDescription;
							break;
						case "no-outside":
							propOutdoorSpaceDescription = " It is located near public parks and gardens"
							propDescription += propOutdoorSpaceDescription;
							break;
					}

					switch (propParkingType) {
						case "garage":
							propParkingTypeDescription = " and a private garage."
							propDescription += propParkingTypeDescription;
							break;
						case "driveway":
							propParkingTypeDescription = " and a driveway."
							if (propOutdoorSpace === "no-outside") {
								propParkingTypeDescription = " and has a driveway."
							}
							propDescription += propParkingTypeDescription;
							break;
						case "permit":
							propParkingTypeDescription = " and permit required street parking."
							if (propOutdoorSpace === "no-outside") {
								propParkingTypeDescription = " and has permit required street parking."
							}
							propDescription += propParkingTypeDescription;
							break;
						case "on-street":
							propParkingTypeDescription = " and free street parking."
							if (propOutdoorSpace === "no-outside") {
								propParkingTypeDescription = " and has free street parking."
							}
							propDescription += propParkingTypeDescription;
							break;
						case "off-street":
							propParkingTypeDescription = " and off street parking."
							if (propOutdoorSpace === "no-outside") {
								propParkingTypeDescription = " and has off street parking."
							}
							propDescription += propParkingTypeDescription;
							break;
						case "garage-and-driveway":
							propParkingTypeDescription = " and both a garage and a driveway."
							if (propOutdoorSpace === "no-outside") {
								propParkingTypeDescription = " and has both a garage and a driveway."
							}
							propDescription += propParkingTypeDescription;
							break;
						case "allocated":
							propParkingTypeDescription = " and allocated parking."
							if (propOutdoorSpace === "no-outside") {
								propParkingTypeDescription = " and has allocated parking."
							}
							propDescription += propParkingTypeDescription;
							break;
					}

					propSizeDescription = " Covering " + propSize + " square metres";
					propDescription += propSizeDescription;

					if (propHasLift === "higher-with-lift") {
						propHasLiftDescription = ", with a lift";
						propDescription += propHasLiftDescription;
					}

					switch (propHeatingType) {
						case "electric":
							propHeatingTypeDescription = " and electric heating"
							if (propHasLift === "higher-no-lift") {
								propHeatingTypeDescription = ", and with electric heating";
							}
							propDescription += propHeatingTypeDescription;
							break;
						case "gas":
							propHeatingTypeDescription = " and gas central heating";
							if (propHasLift === "higher-no-lift") {
								propHeatingTypeDescription = ", and with gas central heating";
							}
							propDescription += propHeatingTypeDescription;
							break;
						case "oil":
							propHeatingTypeDescription = " and oil fired central heating";
							if (propHasLift === "higher-with-lift") {
								propHeatingTypeDescription = " and with oil fired central heating";
							}
							propDescription += propHeatingTypeDescription;
							break;
						default:
							propHeatingTypeDescription = " and a heat exchanger";
							if (propHasLift === "higher-no-lift") {
								propHeatingTypeDescription = ", and with a heat exchanger";
							}
							propDescription += propHeatingTypeDescription;
							break;
					}

					propAskingPriceDescription = ", this house is on the market for a price of £" + propAskingPrice.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') + ".";
					propDescription += propAskingPriceDescription;

					switch (propTenure) {
						case "freehold":
							propTenureDescription = " It is a freehold property";
							propDescription += propTenureDescription;
							break;
						case "leasehold":
							propTenureDescription = " It is a leasehold property";
							propDescription += propTenureDescription;
							break;
						case "flying-freehold":
							propTenureDescription = " It is a flying freehold property";
							propDescription += propTenureDescription;
							break;
						case "share-of-freehold":
							propTenureDescription = " It is a share of freehold property";
							propDescription += propTenureDescription;
							break;
					}

					if (propTenure === "leasehold") {
						propLeaseYearsDescription = " with " + propLeaseYears + " years left on the lease";
						propDescription += propLeaseYearsDescription;
					}

					if (propExtraCosts > 0) {
						propExtraCostsDescription = " with additional costs of £" + propExtraCosts + " per month.";
						if (propTenure === "leasehold") {
							propExtraCostsDescription = " and additional costs of £" + propExtraCosts + " per month.";
						}
					} else {
						propExtraCostsDescription = " with no additional costs.";
						if (propTenure === "leasehold") {
							propExtraCostsDescription = " and no additional costs.";
						}
					}
					propDescription += propExtraCostsDescription;






				} else if (randomiser === 1) {

					switch (propBeds) {
						case 0:
							propBedsDescription = "A studio ";
							propDescription += propBedsDescription;
							break;
						case 1:
							propBedsDescription = "A one bedroom ";
							propDescription += propBedsDescription;
							break;
						case 2:
							propBedsDescription = "A two bedroom ";
							propDescription += propBedsDescription;
							break;
						case 3:
							propBedsDescription = "A three bedroom ";
							propDescription += propBedsDescription;
							break;
						case 4:
							propBedsDescription = "A four bedroom ";
							propDescription += propBedsDescription;
							break;
						case 5:
							propBedsDescription = "A five bedroom "
							propDescription += propBedsDescription;
							break;
						case 6:
							propBedsDescription = "A six bedroom "
							propDescription += propBedsDescription;
							break;
						case 7:
							propBedsDescription = "A seven bedroom "
							propDescription += propBedsDescription;
							break;
						case 8:
							propBedsDescription = "An eight bedroom "
							propDescription += propBedsDescription;
							break;
						case 9:
							propBedsDescription = "A nine bedroom "
							propDescription += propBedsDescription;
							break;
						default:
							propBedsDescription = "A multiple bedroom ";
							propDescription += propBedsDescription;
							break;
					}

					switch (propType) {
						case "detached-house":
							propTypeDescription = "detached house";
							break;
						case "semi-detached-house":
							propTypeDescription = "semi-detached house";
							break;
						case "terraced-house":
							propTypeDescription = "terraced house";
							break;
						case "bungalow":
							propTypeDescription = "bungalow";
							break;
						case "flat":
							propTypeDescription = "apartment";
							break;
					}

					propDescription = propBedsDescription + propTypeDescription;

					switch (propBaths) {
						case 1:
							propBathsDescription = " with one bathroom"
							propDescription += propBathsDescription;
							break;
						case 2:
							propBathsDescription = " with two bathrooms"
							propDescription += propBathsDescription;
							break;
						case 3:
							propBathsDescription = " with three bathrooms"
							propDescription += propBathsDescription;
							break;
						default:
							let numberBaths = "many";
							if (propBaths === 4) {
								numberBaths = "four";
							} else if (propBaths === 5) {
								numberBaths = "five";
							} else if (propBaths === 6) {
								numberBaths = "six";
							} else if (propBaths === 7) {
								numberBaths = "seven";
							} else if (propBaths === 8) {
								numberBaths = "eight";
							} else if (propBaths === 9) {
								numberBaths = "nine";
							}
							propBathsDescription = " with " + numberBaths + " bathrooms"
							propDescription += propBathsDescription;
							break;
					}

					if (propType === "flat") {
						switch (propFloor) {
							case "basement":
								propFloorDescription = "basement";
								propDescription = propBedsDescription + propFloorDescription + " " + propTypeDescription + propBathsDescription + ".";
								break;
							case "ground-floor":
								propFloorDescription = "on the ground floor.";
								propDescription += " " + propFloorDescription;
								break;
							case "first-floor":
								propFloorDescription = "on the first floor.";
								propDescription += " " + propFloorDescription;
								break;
							case "second-floor":
								propFloorDescription = "on the second floor.";
								propDescription += " " + propFloorDescription;
								break;
							case "higher":
								propFloorDescription = "on an upper floor.";
								propDescription += " " + propFloorDescription;
								break;
							default:
								propFloorDescription = ".";
								propDescription += propFloorDescription;
								break;
						}
					} else {
						propDescription += ".";
					}

					switch (propBuilt) {
						case "brandNew":
							propBuiltDescription = " Built this year ";
							propDescription += propBuiltDescription;
							break;
						case "last20Years":
							propBuiltDescription = " Built in the last 20 years ";
							propDescription += propBuiltDescription;
							break;
						case "between1950and2000":
							propBuiltDescription = " Built between 1950 and 2000 ";
							propDescription += propBuiltDescription;
							break;
						case "between1900and1950":
							propBuiltDescription = " Built between 1900 and 1950 ";
							propDescription += propBuiltDescription;
							break;
						case "pre1900":
							propBuiltDescription = " Built before 1900 ";
							propDescription += propBuiltDescription;
							break;
					}

					switch (propLastDecorated) {
						case "thisYear":
							propLastDecoratedDescription = "and decorated this year,";
							propDescription = propDescription + propLastDecoratedDescription;
							break;
						case "year2019":
							propLastDecoratedDescription = "and decorated last year,";
							propDescription = propDescription + propLastDecoratedDescription;
							break;
						case "year2018":
							propLastDecoratedDescription = "and last decorated in 2018,";
							propDescription = propDescription + propLastDecoratedDescription;
							break;
						case "pre2018":
							propLastDecoratedDescription = "and last decorated before 2018,";
							propDescription = propDescription + propLastDecoratedDescription;
							break;
					}

					switch (propOutdoorSpace) {
						case "private-garden":
							propOutdoorSpaceDescription = " it has a private garden"
							propDescription += propOutdoorSpaceDescription;
							break;
						case "patio":
							propOutdoorSpaceDescription = " it has a patio area"
							propDescription += propOutdoorSpaceDescription;
							break;
						case "terrace":
							propOutdoorSpaceDescription = " it has a terrace"
							propDescription += propOutdoorSpaceDescription;
							break;
						case "no-outside":
							propOutdoorSpaceDescription = " it is located near public parks and gardens"
							propDescription += propOutdoorSpaceDescription;
							break;
					}

					switch (propParkingType) {
						case "garage":
							propParkingTypeDescription = " and a private garage."
							propDescription += propParkingTypeDescription;
							break;
						case "driveway":
							propParkingTypeDescription = " and a driveway."
							if (propOutdoorSpace === "no-outside") {
								propParkingTypeDescription = " and has a driveway."
							}
							propDescription += propParkingTypeDescription;
							break;
						case "permit":
							propParkingTypeDescription = " and permit required street parking."
							if (propOutdoorSpace === "no-outside") {
								propParkingTypeDescription = " and has permit required street parking."
							}
							propDescription += propParkingTypeDescription;
							break;
						case "on-street":
							propParkingTypeDescription = " and free street parking."
							if (propOutdoorSpace === "no-outside") {
								propParkingTypeDescription = " and has free street parking."
							}
							propDescription += propParkingTypeDescription;
							break;
						case "off-street":
							propParkingTypeDescription = " and off street parking."
							if (propOutdoorSpace === "no-outside") {
								propParkingTypeDescription = " and has off street parking."
							}
							propDescription += propParkingTypeDescription;
							break;
						case "garage-and-driveway":
							propParkingTypeDescription = " and both a garage and a driveway."
							if (propOutdoorSpace === "no-outside") {
								propParkingTypeDescription = " and has both a garage and a driveway."
							}
							propDescription += propParkingTypeDescription;
							break;
						case "allocated":
							propParkingTypeDescription = " and allocated parking."
							if (propOutdoorSpace === "no-outside") {
								propParkingTypeDescription = " and has allocated parking."
							}
							propDescription += propParkingTypeDescription;
							break;
					}

					propAskingPriceDescription = " On the market for £" + propAskingPrice.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') + " ";
					propDescription += propAskingPriceDescription;

					if (propExtraCosts > 0) {
						propExtraCostsDescription = "and with additional costs of £" + propExtraCosts + " per month,";
					} else {
						propExtraCostsDescription = "and with no additional costs,";
					}
					propDescription += propExtraCostsDescription;

					propSizeDescription = " this property offers " + propSize + " square metres";
					propDescription += propSizeDescription;

					if (propHasLift === "higher-with-lift") {
						propHasLiftDescription = ", a lift";
						propDescription += propHasLiftDescription;
					}

					switch (propHeatingType) {
						case "electric":
							propHeatingTypeDescription = " and electric heating."
							propDescription += propHeatingTypeDescription;
							break;
						case "gas":
							propHeatingTypeDescription = " and gas central heating.";
							propDescription += propHeatingTypeDescription;
							break;
						case "oil":
							propHeatingTypeDescription = " and oil fired central heating.";
							propDescription += propHeatingTypeDescription;
							break;
						default:
							propHeatingTypeDescription = " and a heat exchanger.";
							propDescription += propHeatingTypeDescription;
							break;
					}

					switch (propTenure) {
						case "freehold":
							propTenureDescription = " It is a freehold property.";
							propDescription += propTenureDescription;
							break;
						case "leasehold":
							propTenureDescription = " It is a leasehold property";
							propDescription += propTenureDescription;
							break;
						case "flying-freehold":
							propTenureDescription = " It is a flying freehold property.";
							propDescription += propTenureDescription;
							break;
						case "share-of-freehold":
							propTenureDescription = " It is a share of freehold property.";
							propDescription += propTenureDescription;
							break;
					}

					if (propTenure === "leasehold") {
						propLeaseYearsDescription = " with " + propLeaseYears + " years left on the lease.";
						propDescription += propLeaseYearsDescription;
					}






				} else if (randomiser === 2) {
					switch (propBeds) {
						case 0:
							propBedsDescription = "A studio ";
							propDescription += propBedsDescription;
							break;
						case 1:
							propBedsDescription = "A one bedroom ";
							propDescription += propBedsDescription;
							break;
						case 2:
							propBedsDescription = "A two bedroom ";
							propDescription += propBedsDescription;
							break;
						case 3:
							propBedsDescription = "A three bedroom ";
							propDescription += propBedsDescription;
							break;
						case 4:
							propBedsDescription = "A four bedroom ";
							propDescription += propBedsDescription;
							break;
						case 5:
							propBedsDescription = "A five bedroom "
							propDescription += propBedsDescription;
							break;
						case 6:
							propBedsDescription = "A six bedroom "
							propDescription += propBedsDescription;
							break;
						case 7:
							propBedsDescription = "A seven bedroom "
							propDescription += propBedsDescription;
							break;
						case 8:
							propBedsDescription = "An eight bedroom "
							propDescription += propBedsDescription;
							break;
						case 9:
							propBedsDescription = "A nine bedroom "
							propDescription += propBedsDescription;
							break;
						default:
							propBedsDescription = "A multi-bedroom ";
							propDescription += propBedsDescription;
							break;
					}

					switch (propType) {
						case "detached-house":
							propTypeDescription = "detached house";
							break;
						case "semi-detached-house":
							propTypeDescription = "semi-detached house";
							break;
						case "terraced-house":
							propTypeDescription = "terraced house";
							break;
						case "bungalow":
							propTypeDescription = "bungalow";
							break;
						case "flat":
							propTypeDescription = "apartment";
							break;
					}

					propDescription = propBedsDescription + propTypeDescription;

					propAskingPriceDescription = " on the market for £" + propAskingPrice.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') + ".";
					propDescription += propAskingPriceDescription;

					if (propType === "flat") {
						switch (propFloor) {
							case "basement":
								propFloorDescription = "basement";
								propDescription = propBedsDescription + propFloorDescription + " " + propTypeDescription + propAskingPriceDescription;
								break;
							case "ground-floor":
								propFloorDescription = "On the ground floor";
								propDescription += " " + propFloorDescription;
								break;
							case "first-floor":
								propFloorDescription = "On the first floor";
								propDescription += " " + propFloorDescription;
								break;
							case "second-floor":
								propFloorDescription = "On the second floor";
								propDescription += " " + propFloorDescription;
								break;
							case "higher":
								propFloorDescription = "On an upper floor";
								propDescription += " " + propFloorDescription;
								break;
						}
					}

					if (propType === "flat" && propFloor === "basement") {
						switch (propBaths) {
							case 1:
								propBathsDescription = " With one bathroom, "
								propDescription += propBathsDescription;
								break;
							case 2:
								propBathsDescription = " With two bathrooms, "
								propDescription += propBathsDescription;
								break;
							case 3:
								propBathsDescription = " With three bathrooms, "
								propDescription += propBathsDescription;
								break;
							default:
								let numberBaths = "many";
								if (propBaths === 4) {
									numberBaths = "four";
								} else if (propBaths === 5) {
									numberBaths = "five";
								} else if (propBaths === 6) {
									numberBaths = "six";
								} else if (propBaths === 7) {
									numberBaths = "seven";
								} else if (propBaths === 8) {
									numberBaths = "eight";
								} else if (propBaths === 9) {
									numberBaths = "nine";
								}
								propBathsDescription = " With " + numberBaths + " bathrooms, "
								propDescription += propBathsDescription;
								break;
						}
					} else if (propType === "flat") {
						switch (propBaths) {
							case 1:
								propBathsDescription = " with one bathroom, "
								propDescription += propBathsDescription;
								break;
							case 2:
								propBathsDescription = " with two bathrooms, "
								propDescription += propBathsDescription;
								break;
							case 3:
								propBathsDescription = " with three bathrooms, "
								propDescription += propBathsDescription;
								break;
							default:
								let numberBaths = "many";
								if (propBaths === 4) {
									numberBaths = "four";
								} else if (propBaths === 5) {
									numberBaths = "five";
								} else if (propBaths === 6) {
									numberBaths = "six";
								} else if (propBaths === 7) {
									numberBaths = "seven";
								} else if (propBaths === 8) {
									numberBaths = "eight";
								} else if (propBaths === 9) {
									numberBaths = "nine";
								}
								propBathsDescription = " with " + numberBaths + " bathrooms, "
								propDescription += propBathsDescription;
								break;
						}
					} else {
						switch (propBaths) {
							case 1:
								propBathsDescription = " With one bathroom, ";
								propDescription += propBathsDescription;
								break;
							case 2:
								propBathsDescription = " With two bathrooms, ";
								propDescription += propBathsDescription;
								break;
							case 3:
								propBathsDescription = " With three bathrooms, ";
								propDescription += propBathsDescription;
								break;
							default:
								let numberBaths = "many";
								if (propBaths === 4) {
									numberBaths = "four";
								} else if (propBaths === 5) {
									numberBaths = "five";
								} else if (propBaths === 6) {
									numberBaths = "six";
								} else if (propBaths === 7) {
									numberBaths = "seven";
								} else if (propBaths === 8) {
									numberBaths = "eight";
								} else if (propBaths === 9) {
									numberBaths = "nine";
								}
								propBathsDescription = " With " + numberBaths + " bathrooms, ";
								propDescription += propBathsDescription;
								break;
						}
					}

					switch (propOutdoorSpace) {
						case "private-garden":
							propOutdoorSpaceDescription = "a private garden";
							propDescription += propOutdoorSpaceDescription;
							break;
						case "patio":
							propOutdoorSpaceDescription = "a patio area";
							propDescription += propOutdoorSpaceDescription;
							break;
						case "terrace":
							propOutdoorSpaceDescription = "a terrace";
							propDescription += propOutdoorSpaceDescription;
							break;
						case "no-outside":
							propOutdoorSpaceDescription = "nearby parks and gardens";
							propDescription += propOutdoorSpaceDescription;
							break;
					}

					propSizeDescription = " and " + propSize + " square metres";
					propDescription += propSizeDescription;

					switch (propLastDecorated) {
						case "thisYear":
							propLastDecoratedDescription = ", the property was last decorated this year.";
							propDescription = propDescription + propLastDecoratedDescription;
							break;
						case "year2019":
							propLastDecoratedDescription = ", the property was decorated last year.";
							propDescription = propDescription + propLastDecoratedDescription;
							break;
						case "year2018":
							propLastDecoratedDescription = ", the property was last decorated in 2018.";
							propDescription = propDescription + propLastDecoratedDescription;
							break;
						case "pre2018":
							propLastDecoratedDescription = ", the property was last decorated before 2018.";
							propDescription = propDescription + propLastDecoratedDescription;
							break;
					}

					switch (propBuilt) {
						case "brandNew":
							propBuiltDescription = " Built this year,";
							propDescription += propBuiltDescription;
							break;
						case "last20Years":
							propBuiltDescription = " Built in the last 20 years,";
							propDescription += propBuiltDescription;
							break;
						case "between1950and2000":
							propBuiltDescription = " Built between 1950 and 2000,";
							propDescription += propBuiltDescription;
							break;
						case "between1900and1950":
							propBuiltDescription = " Built between 1900 and 1950,";
							propDescription += propBuiltDescription;
							break;
						case "pre1900":
							propBuiltDescription = " Built before 1900,";
							propDescription += propBuiltDescription;
							break;
					}

					switch (propParkingType) {
						case "garage":
							propParkingTypeDescription = " it has a private garage";
							propDescription += propParkingTypeDescription;
							break;
						case "driveway":
							propParkingTypeDescription = " it has a driveway";
							propDescription += propParkingTypeDescription;
							break;
						case "permit":
							propParkingTypeDescription = " it has permit required street parking";
							propDescription += propParkingTypeDescription;
							break;
						case "on-street":
							propParkingTypeDescription = " it has free street parking";
							propDescription += propParkingTypeDescription;
							break;
						case "off-street":
							propParkingTypeDescription = " it has off street parking";
							propDescription += propParkingTypeDescription;
							break;
						case "garage-and-driveway":
							propParkingTypeDescription = " it has both a garage and a driveway";
							propDescription += propParkingTypeDescription;
							break;
						case "allocated":
							propParkingTypeDescription = " it has allocated parking";
							propDescription += propParkingTypeDescription;
							break;
					}

					if (propHasLift === "higher-with-lift") {
						propHasLiftDescription = ", a lift,";
						propDescription += propHasLiftDescription;
					}

					switch (propHeatingType) {
						case "electric":
							propHeatingTypeDescription = " and electric heating."
							propDescription += propHeatingTypeDescription;
							break;
						case "gas":
							propHeatingTypeDescription = " and gas central heating.";
							propDescription += propHeatingTypeDescription;
							break;
						case "oil":
							propHeatingTypeDescription = " and oil fired central heating.";
							propDescription += propHeatingTypeDescription;
							break;
						default:
							propHeatingTypeDescription = " and aheat exchanger.";
							propDescription += propHeatingTypeDescription;
							break;
					}

					if (propExtraCosts > 0) {
						propExtraCostsDescription = " It comes with additional costs of £" + propExtraCosts + " per month";
					} else {
						propExtraCostsDescription = " It comes with no additional costs";
					}
					propDescription += propExtraCostsDescription;

					switch (propTenure) {
						case "freehold":
							propTenureDescription = " and is a freehold property.";
							propDescription += propTenureDescription;
							break;
						case "leasehold":
							propTenureDescription = " and is a leasehold property";
							propDescription += propTenureDescription;
							break;
						case "flying-freehold":
							propTenureDescription = " and is a flying freehold property.";
							propDescription += propTenureDescription;
							break;
						case "share-of-freehold":
							propTenureDescription = " and is a share of freehold property.";
							propDescription += propTenureDescription;
							break;
					}

					if (propTenure === "leasehold") {
						propLeaseYearsDescription = " with " + propLeaseYears + " years left on the lease.";
						propDescription += propLeaseYearsDescription;
					}
				}

				break;

			// --------------------------------------------\ Plain---------------------------------------------------------







			case 1:

				// --------------------------------------------Agent---------------------------------------------------------

				if (randomiser === 0) {
					switch (propType) {
						case "detached-house":
							propTypeDescription = "detached house";
							break;
						case "semi-detached-house":
							propTypeDescription = "semi-detached house";
							break;
						case "terraced-house":
							propTypeDescription = "terraced house";
							break;
						case "bungalow":
							propTypeDescription = "bungalow";
							break;
						case "flat":
							propTypeDescription = "apartment";
							break;
					}

					propDescription = propTypeDescription;

					if (propType === "flat") {
						switch (propFloor) {
							case "basement":
								propFloorDescription = "basement";
								propDescription = propFloorDescription + " " + propTypeDescription;
								break;
							case "ground-floor":
								propFloorDescription = "on the ground floor";
								propDescription += " " + propFloorDescription;
								break;
							case "first-floor":
								propFloorDescription = "on the first floor";
								propDescription += " " + propFloorDescription;
								break;
							case "second-floor":
								propFloorDescription = "on the second floor";
								propDescription += " " + propFloorDescription;
								break;
							case "higher":
								propFloorDescription = "on an upper floor";
								propDescription += " " + propFloorDescription;
								break;
							default:
								propFloorDescription = "";
								propDescription += propFloorDescription;
								break;
						}
					}

					switch (propLastDecorated) {
						case "thisYear":
							propLastDecoratedDescription = "A newly and tastefully decorated";
							propDescription = propLastDecoratedDescription + " " + propDescription + ", ";
							break;
						case "year2019":
							propLastDecoratedDescription = "A recently and tastefully decorated";
							propDescription = propLastDecoratedDescription + " " + propDescription + ", ";
							break;
						case "year2018":
							propLastDecoratedDescription = ", last decorated in 2018 in a tasteful manner,";
							propDescription = "A beautiful " + propDescription + propLastDecoratedDescription + " and ";
							break;
						case "pre2018":
							propLastDecoratedDescription = ", last decorated before 2018 in a tasteful manner,";
							propDescription = "A beautiful " + propDescription + propLastDecoratedDescription + " and ";
							break;
						default:
							propLastDecoratedDescription = "A newly and tastefully decorated";
							propDescription = propLastDecoratedDescription + " " + propDescription + ", ";
							break;
					}

					switch (propBuilt) {
						case "brandNew":
							propBuiltDescription = "newly built in a modern and contemporary style. ";
							propDescription += propBuiltDescription;
							break;
						case "last20Years":
							propBuiltDescription = "built in the last 20 years with style and elegance in mind. ";
							propDescription += propBuiltDescription;
							break;
						case "between1950and2000":
							propBuiltDescription = "built between 1950 and 2000 with plenty of charm and character. ";
							propDescription += propBuiltDescription;
							break;
						case "between1900and1950":
							propBuiltDescription = "built between 1900 and 1950 with plenty of charm and character. ";
							propDescription += propBuiltDescription;
							break;
						case "pre1900":
							propBuiltDescription = "built before 1900 with stunning period craftsmanship. ";
							propDescription += propBuiltDescription;
							break;
					}

					if (propFloor != "basement" && propFloor != "groundFloor") {
						propHasLiftDescription = "Unfortunately the property doesn't have a lift, but the stairs will keep you fit and healthy! ";
						if (propHasLift === "higher-with-lift") {
							propHasLiftDescription = "Don't worry about the stairs though, the property comes with a lift! ";
							propDescription += propHasLiftDescription;
						}
					}

					switch (propBeds) {
						case 0:
							propBedsDescription = "With a brilliant studio bedroom"
							propDescription += propBedsDescription;
							break;
						case 1:
							propBedsDescription = "With a single, lovely bedroom, perfect for a couple,"
							propDescription += propBedsDescription;
							break;
						case 2:
							propBedsDescription = "With two lovely bedrooms, perfect if you're hoping to have guests to stay,"
							propDescription += propBedsDescription;
							break;
						case 3:
							propBedsDescription = "With three lovely bedrooms, perfect if you're looking for a family home,"
							propDescription += propBedsDescription;
							break;
						case 4:
							propBedsDescription = "With four lovely bedrooms, perfect for a large family,"
							propDescription += propBedsDescription;
							break;
						case 5:
							propBedsDescription = "With five lovely bedrooms, perfect for a large family,"
							propDescription += propBedsDescription;
							break;
						case 6:
							propBedsDescription = "With six lovely bedrooms, perfect for a large family,"
							propDescription += propBedsDescription;
							break;
						case 7:
							propBedsDescription = "With seven lovely bedrooms, perfect for a large family,"
							propDescription += propBedsDescription;
							break;
						case 8:
							propBedsDescription = "With eight lovely bedrooms, perfect for a large family,"
							propDescription += propBedsDescription;
							break;
						case 9:
							propBedsDescription = "With nine lovely bedrooms, perfect for a large family,"
							propDescription += propBedsDescription;
							break;
						default:
							propBedsDescription = "With many lovely bedrooms, perfect for a large family,"
							propDescription += propBedsDescription;
							break;
					}

					switch (propBaths) {
						case 1:
							propBathsDescription = " and one luxurious bathroom"
							propDescription += propBathsDescription;
							break;
						case 2:
							propBathsDescription = " and two luxurious bathrooms"
							propDescription += propBathsDescription;
							break;
						case 3:
							propBathsDescription = " and three luxurious bathrooms"
							propDescription += propBathsDescription;
							break;
						default:
							let numberBaths = "many";
							if (propBaths === 4) {
								numberBaths = "four";
							} else if (propBaths === 5) {
								numberBaths = "five";
							} else if (propBaths === 6) {
								numberBaths = "six";
							} else if (propBaths === 7) {
								numberBaths = "seven";
							} else if (propBaths === 8) {
								numberBaths = "eight";
							} else if (propBaths === 9) {
								numberBaths = "nine";
							}
							propBathsDescription = " and " + numberBaths + " luxurious bathrooms"
							propDescription += propBathsDescription;
							break;
					}

					propDescription += ", this could be the dream house for you.";

					switch (propOutdoorSpace) {
						case "private-garden":
							propOutdoorSpaceDescription = " It has a stunning private garden that would be great for a dog,"
							propDescription += propOutdoorSpaceDescription;
							break;
						case "patio":
							propOutdoorSpaceDescription = " It has a stunning patio area that's great for summer barbeques"
							propDescription += propOutdoorSpaceDescription;
							break;
						case "terrace":
							propOutdoorSpaceDescription = " It has a stunning terrace with an incredible view"
							propDescription += propOutdoorSpaceDescription;
							break;
						case "no-outside":
							propOutdoorSpaceDescription = " It is located near stunning parks and gardens"
							propDescription += propOutdoorSpaceDescription;
							break;
					}

					switch (propParkingType) {
						case "garage":
							propParkingTypeDescription = " and boasts a private garage where you can store your car or bike, or even convert into your own workshop or art studio!";
							propDescription += propParkingTypeDescription;
							break;
						case "driveway":
							propParkingTypeDescription = " and has a fully accessible, private driveway.";
							propDescription += propParkingTypeDescription;
							break;
						case "permit":
							propParkingTypeDescription = " and has permit required street parking with an abundance of spaces.";
							propDescription += propParkingTypeDescription;
							break;
						case "on-street":
							propParkingTypeDescription = " and has free street parking with an abundance of spaces.";
							propDescription += propParkingTypeDescription;
							break;
						case "off-street":
							propParkingTypeDescription = " and has off street parking with an abundance of spaces.";
							propDescription += propParkingTypeDescription;
							break;
						case "garage-and-driveway":
							propParkingTypeDescription = " and boasts both a driveway and a garage where you can store your car or bike, or even convert into your own workshop or art studio!";
							propDescription += propParkingTypeDescription;
							break;
						case "allocated":
							propParkingTypeDescription = " and has personal, allocated parking, so you won't have to search out a parking spot.";
							propDescription += propParkingTypeDescription;
							break;
					}

					if (propSize < 60) {
						let propSizeDescription = " An extremely cozy property with an overall footprint of " + propSize + " square metres";
						propDescription += propSizeDescription;
					} else {
						let propSizeDescription = " An extremely spacious property with an overall footprint of " + propSize + " square metres";
						propDescription += propSizeDescription;
					}

					switch (propHeatingType) {
						case "electric":
							propHeatingTypeDescription = " and efficient, warming electric heating";
							propDescription += propHeatingTypeDescription;
							break;
						case "gas":
							propHeatingTypeDescription = " and efficient, warming gas central heating";
							propDescription += propHeatingTypeDescription;
							break;
						case "oil":
							propHeatingTypeDescription = " and efficient, warming oil fired central heating";
							propDescription += propHeatingTypeDescription;
							break;
						default:
							propHeatingTypeDescription = " and a warm, maintainable heat exchanger";
							propDescription += propHeatingTypeDescription;
							break;
					}

					propAskingPriceDescription = ", this house is on the market for an absolute bargain of £" + propAskingPrice.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') + ", get it before it's gone!";
					propDescription += propAskingPriceDescription;

					switch (propTenure) {
						case "freehold":
							propTenureDescription = " This is a freehold property, so no need to worry about lease renewals,";
							propDescription += propTenureDescription;
							break;
						case "leasehold":
							propTenureDescription = " This is a leasehold property";
							propDescription += propTenureDescription;
							break;
						case "flying-freehold":
							propTenureDescription = " This is a flying freehold property";
							propDescription += propTenureDescription;
							break;
						case "share-of-freehold":
							propTenureDescription = " This is a share of freehold property";
							propDescription += propTenureDescription;
							break;
					}

					if (propTenure === "leasehold") {
						propLeaseYearsDescription = " with " + propLeaseYears + " years left on the lease, plenty of time before renewal is necessary,";
						propDescription += propLeaseYearsDescription;
					}

					if (propExtraCosts > 0) {
						propExtraCostsDescription = " with additional costs of only £" + propExtraCosts + " per month.";
						if (propTenure === "leasehold") {
							propExtraCostsDescription = " and additional costs of only £" + propExtraCosts + " per month.";
						}
					} else {
						propExtraCostsDescription = " with no additional costs!";
						if (propTenure === "leasehold") {
							propExtraCostsDescription = " and no additional costs!";
						}
					}
					propDescription += propExtraCostsDescription;

					propDescription += " Don't miss out, book a viewing today!"





				} else if (randomiser === 1) {
					switch (propBeds) {
						case 0:
							propBedsDescription = "A beautiful studio ";
							propDescription += propBedsDescription;
							break;
						case 1:
							propBedsDescription = "A beautiful, one bedroom ";
							propDescription += propBedsDescription;
							break;
						case 2:
							propBedsDescription = "A beautiful, two bedroom ";
							propDescription += propBedsDescription;
							break;
						case 3:
							propBedsDescription = "A beautiful, three bedroom ";
							propDescription += propBedsDescription;
							break;
						case 4:
							propBedsDescription = "A beautiful, four bedroom ";
							propDescription += propBedsDescription;
							break;
						case 5:
							propBedsDescription = "A beautiful, five bedroom "
							propDescription += propBedsDescription;
							break;
						case 6:
							propBedsDescription = "A beautiful, six bedroom "
							propDescription += propBedsDescription;
							break;
						case 7:
							propBedsDescription = "A beautiful, seven bedroom "
							propDescription += propBedsDescription;
							break;
						case 8:
							propBedsDescription = "A beautiful, eight bedroom "
							propDescription += propBedsDescription;
							break;
						case 9:
							propBedsDescription = "A beautiful, nine bedroom "
							propDescription += propBedsDescription;
							break;
						default:
							propBedsDescription = "A beautiful, multi-bedroom ";
							propDescription += propBedsDescription;
							break;
					}

					switch (propType) {
						case "detached-house":
							propTypeDescription = "detached house";
							break;
						case "semi-detached-house":
							propTypeDescription = "semi-detached house";
							break;
						case "terraced-house":
							propTypeDescription = "terraced house";
							break;
						case "bungalow":
							propTypeDescription = "bungalow";
							break;
						case "flat":
							propTypeDescription = "apartment";
							break;
					}

					propDescription = propBedsDescription + propTypeDescription;

					switch (propBaths) {
						case 1:
							propBathsDescription = " with a lovely bathroom"
							propDescription += propBathsDescription;
							break;
						case 2:
							propBathsDescription = " with two lovely bathrooms"
							propDescription += propBathsDescription;
							break;
						case 3:
							propBathsDescription = " with three lovely bathrooms"
							propDescription += propBathsDescription;
							break;
						default:
							let numberBaths = "many";
							if (propBaths === 4) {
								numberBaths = "four";
							} else if (propBaths === 5) {
								numberBaths = "five";
							} else if (propBaths === 6) {
								numberBaths = "six";
							} else if (propBaths === 7) {
								numberBaths = "seven";
							} else if (propBaths === 8) {
								numberBaths = "eight";
							} else if (propBaths === 9) {
								numberBaths = "nine";
							}
							propBathsDescription = " with " + numberBaths + " lovely bathrooms"
							propDescription += propBathsDescription;
							break;
					}

					if (propType === "flat") {
						switch (propFloor) {
							case "basement":
								propFloorDescription = "basement";
								propDescription = propBedsDescription + propFloorDescription + " " + propTypeDescription;
								break;
							case "ground-floor":
								propFloorDescription = "on the ground floor";
								propDescription += " " + propFloorDescription;
								break;
							case "first-floor":
								propFloorDescription = "on the first floor";
								propDescription += " " + propFloorDescription;
								break;
							case "second-floor":
								propFloorDescription = "on the second floor";
								propDescription += " " + propFloorDescription;
								break;
							case "higher":
								propFloorDescription = "on an upper floor";
								propDescription += " " + propFloorDescription;
								break;
							default:
								propFloorDescription = ".";
								propDescription += propFloorDescription;
								break;
						}
					}

					if (propBeds > 1) {
						propDescription += ", perfect for a family.";
					} else {
						propDescription += ", perfect as a first property or for a couple.";
					}

					switch (propBuilt) {
						case "brandNew":
							propBuiltDescription = " Built this year in a modern and contemporary style, ";
							propDescription += propBuiltDescription;
							break;
						case "last20Years":
							propBuiltDescription = " Built in the last 20 years with style and elegance in mind ";
							propDescription += propBuiltDescription;
							break;
						case "between1950and2000":
							propBuiltDescription = " Built between 1950 and 2000, with plenty of charm and character, ";
							propDescription += propBuiltDescription;
							break;
						case "between1900and1950":
							propBuiltDescription = " Built between 1900 and 1950, with plenty of charm and character, ";
							propDescription += propBuiltDescription;
							break;
						case "pre1900":
							propBuiltDescription = " Built before 1900 with stunning period craftsmanship, ";
							propDescription += propBuiltDescription;
							break;
					}

					switch (propLastDecorated) {
						case "thisYear":
							propLastDecoratedDescription = "and decorated this year in a tasteful manner,";
							propDescription = propDescription + propLastDecoratedDescription;
							break;
						case "year2019":
							propLastDecoratedDescription = "and decorated last year in a tasteful manner,";
							propDescription = propDescription + propLastDecoratedDescription;
							break;
						case "year2018":
							propLastDecoratedDescription = "and last decorated in 2018 in a tasteful manner,";
							propDescription = propDescription + propLastDecoratedDescription;
							break;
						case "pre2018":
							propLastDecoratedDescription = "and last decorated before 2018 in a tasteful manner,";
							propDescription = propDescription + propLastDecoratedDescription;
							break;
					}

					switch (propOutdoorSpace) {
						case "private-garden":
							propOutdoorSpaceDescription = " it has a stunning private garden that would be great for a dog,"
							propDescription += propOutdoorSpaceDescription;
							break;
						case "patio":
							propOutdoorSpaceDescription = " it has a stunning patio area that would be great for summer barbeques,"
							propDescription += propOutdoorSpaceDescription;
							break;
						case "terrace":
							propOutdoorSpaceDescription = " it has a stunning terrace with a great view,"
							propDescription += propOutdoorSpaceDescription;
							break;
						case "no-outside":
							propOutdoorSpaceDescription = " it is located near stunning public parks and gardens"
							propDescription += propOutdoorSpaceDescription;
							break;
					}

					switch (propParkingType) {
						case "garage":
							propParkingTypeDescription = " and boasts a private garage where you can store your car or bike, or even convert into your own workshop or art studio!";
							propDescription += propParkingTypeDescription;
							break;
						case "driveway":
							propParkingTypeDescription = " and a fully accessible, private driveway.";
							if (propOutdoorSpace === "no-outside") {
								propParkingTypeDescription = " and has a fully accessible, private driveway.";
							}
							propDescription += propParkingTypeDescription;
							break;
						case "permit":
							propParkingTypeDescription = " and permit required street parking with an abundance of spaces on offer.";
							if (propOutdoorSpace === "no-outside") {
								propParkingTypeDescription = " and has permit required street parking with an abundance of spaces on offer.";
							}
							propDescription += propParkingTypeDescription;
							break;
						case "on-street":
							propParkingTypeDescription = " and free street parking with an abundance of spaces on offer.";
							if (propOutdoorSpace === "no-outside") {
								propParkingTypeDescription = " and has free street parking with an abundance of spaces on offer.";
							}
							propDescription += propParkingTypeDescription;
							break;
						case "off-street":
							propParkingTypeDescription = " and off street parking with an abundance of space.";
							if (propOutdoorSpace === "no-outside") {
								propParkingTypeDescription = " and has off street parking with an abundance of space.";
							}
							propDescription += propParkingTypeDescription;
							break;
						case "garage-and-driveway":
							propParkingTypeDescription = " and boasts both a garage and a driveway where you can store your car or bike, or even convert into your own workshop or art studio!";
							propDescription += propParkingTypeDescription;
							break;
						case "allocated":
							propParkingTypeDescription = " and peronal, allocated parking, so no wasted time searching for a parking space.";
							if (propOutdoorSpace === "no-outside") {
								propParkingTypeDescription = " and has personal, allocated parking, so no wasted time searching for a parking space.";
							}
							propDescription += propParkingTypeDescription;
							break;
					}

					propAskingPriceDescription = " On the market for a bargain of £" + propAskingPrice.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') + " ";
					propDescription += propAskingPriceDescription;

					if (propExtraCosts > 0) {
						propExtraCostsDescription = "and with additional costs of only £" + propExtraCosts + " per month,";
					} else {
						propExtraCostsDescription = "and with no additional costs,";
					}
					propDescription += propExtraCostsDescription;

					if (propSize < 60) {
						let propSizeDescription = " this cozy property offers an overall footprint of " + propSize + " square metres";
						propDescription += propSizeDescription;
					} else {
						let propSizeDescription = " this spacious property offers an overall footprint of  " + propSize + " square metres";
						propDescription += propSizeDescription;
					}

					if (propHasLift === "higher-with-lift") {
						propHasLiftDescription = ", a functional lift";
						propDescription += propHasLiftDescription;
					}

					switch (propHeatingType) {
						case "electric":
							propHeatingTypeDescription = " and warm, comforting electric heating."
							propDescription += propHeatingTypeDescription;
							break;
						case "gas":
							propHeatingTypeDescription = " and warm, comforting gas central heating.";
							propDescription += propHeatingTypeDescription;
							break;
						case "oil":
							propHeatingTypeDescription = " and warm, comforting oil fired central heating.";
							propDescription += propHeatingTypeDescription;
							break;
						default:
							propHeatingTypeDescription = " and a warm, maintainable heat exchanger.";
							propDescription += propHeatingTypeDescription;
							break;
					}

					propDescription += " Get it before it's gone!";

					switch (propTenure) {
						case "freehold":
							propTenureDescription = " It is a freehold property, so no need to worry about lease renewals.";
							propDescription += propTenureDescription;
							break;
						case "leasehold":
							propTenureDescription = " It is a leasehold property";
							propDescription += propTenureDescription;
							break;
						case "flying-freehold":
							propTenureDescription = " It is a flying freehold property.";
							propDescription += propTenureDescription;
							break;
						case "share-of-freehold":
							propTenureDescription = " It is a share of freehold property.";
							propDescription += propTenureDescription;
							break;
					}

					if (propTenure === "leasehold") {
						propLeaseYearsDescription = " with " + propLeaseYears + " years left on the lease.";
						propDescription += propLeaseYearsDescription;
					}

					propDescription += " Don't miss out, book a viewing today!";





				} else if (randomiser === 2) {
					switch (propBeds) {
						case 0:
							propBedsDescription = "A beautiful studio ";
							propDescription += propBedsDescription;
							break;
						case 1:
							propBedsDescription = "A beautiful, one bedroom ";
							propDescription += propBedsDescription;
							break;
						case 2:
							propBedsDescription = "A beautiful, two bedroom ";
							propDescription += propBedsDescription;
							break;
						case 3:
							propBedsDescription = "A beautiful, three bedroom ";
							propDescription += propBedsDescription;
							break;
						case 4:
							propBedsDescription = "A beautiful, four bedroom ";
							propDescription += propBedsDescription;
							break;
						case 5:
							propBedsDescription = "A beautiful, five bedroom "
							propDescription += propBedsDescription;
							break;
						case 6:
							propBedsDescription = "A beautiful, six bedroom "
							propDescription += propBedsDescription;
							break;
						case 7:
							propBedsDescription = "A beautiful, seven bedroom "
							propDescription += propBedsDescription;
							break;
						case 8:
							propBedsDescription = "A beautiful, eight bedroom "
							propDescription += propBedsDescription;
							break;
						case 9:
							propBedsDescription = "A beautiful, nine bedroom "
							propDescription += propBedsDescription;
							break;
						default:
							propBedsDescription = "A beautiful multi-bedroom ";
							propDescription += propBedsDescription;
							break;
					}

					switch (propType) {
						case "detached-house":
							propTypeDescription = "detached house";
							break;
						case "semi-detached-house":
							propTypeDescription = "semi-detached house";
							break;
						case "terraced-house":
							propTypeDescription = "terraced house";
							break;
						case "bungalow":
							propTypeDescription = "bungalow";
							break;
						case "flat":
							propTypeDescription = "apartment";
							break;
					}

					propDescription = propBedsDescription + propTypeDescription;

					propAskingPriceDescription = " on the market for an absolute bargain of £" + propAskingPrice.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
					propDescription += propAskingPriceDescription;

					if (propBeds > 1) {
						propDescription += ", this property is perfect for a family, don't miss out!";
					} else {
						propDescription += ", this property is perfect as a first home or for a couple, don't miss out!";
					}

					if (propType === "flat") {
						switch (propFloor) {
							case "basement":
								propFloorDescription = "basement";
								propDescription = propBedsDescription + propFloorDescription + " " + propTypeDescription + ".";
								break;
							case "ground-floor":
								propFloorDescription = "On the ground floor";
								propDescription += " " + propFloorDescription;
								break;
							case "first-floor":
								propFloorDescription = "On the first floor";
								propDescription += " " + propFloorDescription;
								break;
							case "second-floor":
								propFloorDescription = "On the second floor";
								propDescription += " " + propFloorDescription;
								break;
							case "higher":
								propFloorDescription = "On an upper floor";
								propDescription += " " + propFloorDescription;
								break;
						}
					}

					if (propType === "flat" && propFloor === "basement") {
						switch (propBaths) {
							case 1:
								propBathsDescription = " with one luxurious bathroom, "
								propDescription += propBathsDescription;
								break;
							case 2:
								propBathsDescription = " with two luxurious bathrooms, "
								propDescription += propBathsDescription;
								break;
							case 3:
								propBathsDescription = " with three luxurious bathrooms, "
								propDescription += propBathsDescription;
								break;
							default:
								let numberBaths = "many";
								if (propBaths === 4) {
									numberBaths = "four";
								} else if (propBaths === 5) {
									numberBaths = "five";
								} else if (propBaths === 6) {
									numberBaths = "six";
								} else if (propBaths === 7) {
									numberBaths = "seven";
								} else if (propBaths === 8) {
									numberBaths = "eight";
								} else if (propBaths === 9) {
									numberBaths = "nine";
								}
								propBathsDescription = " with " + numberBaths + " luxurious bathrooms"
								propDescription += propBathsDescription;
								break;
						}
					} else if (propType === "flat") {
						switch (propBaths) {
							case 1:
								propBathsDescription = " with one luxurious bathroom, "
								propDescription += propBathsDescription;
								break;
							case 2:
								propBathsDescription = " with two luxurious bathrooms, "
								propDescription += propBathsDescription;
								break;
							case 3:
								propBathsDescription = " with three luxurious bathrooms, "
								propDescription += propBathsDescription;
								break;
							default:
								let numberBaths = "many";
								if (propBaths === 4) {
									numberBaths = "four";
								} else if (propBaths === 5) {
									numberBaths = "five";
								} else if (propBaths === 6) {
									numberBaths = "six";
								} else if (propBaths === 7) {
									numberBaths = "seven";
								} else if (propBaths === 8) {
									numberBaths = "eight";
								} else if (propBaths === 9) {
									numberBaths = "nine";
								}
								propBathsDescription = " with " + numberBaths + " luxurious bathrooms"
								propDescription += propBathsDescription;
								break;
						}
					} else {
						switch (propBaths) {
							case 1:
								propBathsDescription = " With one luxurious bathroom, "
								propDescription += propBathsDescription;
								break;
							case 2:
								propBathsDescription = " With two luxurious bathrooms, "
								propDescription += propBathsDescription;
								break;
							case 3:
								propBathsDescription = " With three luxurious bathrooms, "
								propDescription += propBathsDescription;
								break;
							default:
								let numberBaths = "many";
								if (propBaths === 4) {
									numberBaths = "four";
								} else if (propBaths === 5) {
									numberBaths = "five";
								} else if (propBaths === 6) {
									numberBaths = "six";
								} else if (propBaths === 7) {
									numberBaths = "seven";
								} else if (propBaths === 8) {
									numberBaths = "eight";
								} else if (propBaths === 9) {
									numberBaths = "nine";
								}
								propBathsDescription = " With " + numberBaths + " luxurious bathrooms"
								propDescription += propBathsDescription;
								break;
						}
					}

					switch (propOutdoorSpace) {
						case "private-garden":
							propOutdoorSpaceDescription = "a stunning private garden that would be perfect for a dog,"
							propDescription += propOutdoorSpaceDescription;
							break;
						case "patio":
							propOutdoorSpaceDescription = "a stunning patio area that would be perfect for summer barbeques,"
							propDescription += propOutdoorSpaceDescription;
							break;
						case "terrace":
							propOutdoorSpaceDescription = "a stunning terrace with an incredible view"
							propDescription += propOutdoorSpaceDescription;
							break;
						case "no-outside":
							propOutdoorSpaceDescription = "stunning nearby parks and gardens,"
							propDescription += propOutdoorSpaceDescription;
							break;
					}

					if (propSize < 60) {
						propSizeDescription = " and an extremely cozy footprint of " + propSize + " square metres";
						propDescription += propSizeDescription;
					} else {
						propSizeDescription = " and an extremely spacious footprint of " + propSize + " square metres";
						propDescription += propSizeDescription;
					}

					propDescription += ", this could be the house for you!"

					switch (propLastDecorated) {
						case "thisYear":
							propLastDecoratedDescription = " The property was last decorated this year in a modern and contemporary manner, ";
							propDescription = propDescription + propLastDecoratedDescription;
							break;
						case "year2019":
							propLastDecoratedDescription = " The property was decorated last year in a modern and contemporary manner, ";
							propDescription = propDescription + propLastDecoratedDescription;
							break;
						case "year2018":
							propLastDecoratedDescription = " The property was last decorated in 2018 in a stylish and tastefull manner, ";
							propDescription = propDescription + propLastDecoratedDescription;
							break;
						case "pre2018":
							propLastDecoratedDescription = " The property was last decorated before 2018 in a stylish and tastefull manner, ";
							propDescription = propDescription + propLastDecoratedDescription;
							break;
						default:
							propLastDecoratedDescription = " The property was last decorated this year in a modern and contemporary manner, ";
							propDescription = propDescription + propLastDecoratedDescription;
							break;
					}

					switch (propBuilt) {
						case "brandNew":
							propBuiltDescription = "and built this year with elegance in mind.";
							propDescription += propBuiltDescription;
							break;
						case "last20Years":
							propBuiltDescription = "and built in the last 20 years with elegance in mind.";
							propDescription += propBuiltDescription;
							break;
						case "between1950and2000":
							propBuiltDescription = "and built between 1950 and 2000 with elegance in mind.";
							propDescription += propBuiltDescription;
							break;
						case "between1900and1950":
							propBuiltDescription = "and built between 1900 and 1950 with elegance in mind.";
							propDescription += propBuiltDescription;
							break;
						case "pre1900":
							propBuiltDescription = "and built before 1900 with stunning period craftsmanship.";
							propDescription += propBuiltDescription;
							break;
					}

					switch (propParkingType) {
						case "garage":
							propParkingTypeDescription = " It boasts a private garage where you can store your car or bike, or even convert into your own workshop or art studio,";
							propDescription += propParkingTypeDescription;
							break;
						case "driveway":
							propParkingTypeDescription = " It has a fully accessible, private driveway";
							propDescription += propParkingTypeDescription;
							break;
						case "permit":
							propParkingTypeDescription = " It has permit required street parking with a huge choice of spaces";
							propDescription += propParkingTypeDescription;
							break;
						case "on-street":
							propParkingTypeDescription = " It has free street parking with a huge choice of spaces";
							propDescription += propParkingTypeDescription;
							break;
						case "off-street":
							propParkingTypeDescription = " It has off street parking with a huge choice of spaces";
							propDescription += propParkingTypeDescription;
							break;
						case "garage-and-driveway":
							propParkingTypeDescription = " It boasts both a garage and a driveway where you can store your car or bike, or even convert into your own workshop or art studio";
							propDescription += propParkingTypeDescription;
							break;
						case "allocated":
							propParkingTypeDescription = " It has personal, allocated parking, so there is no time wasted searching out a space";
							propDescription += propParkingTypeDescription;
							break;
					}

					if (propParkingType !== "garage") {
						propDescription += ",";
					}

					if (propHasLift === "higher-with-lift") {
						propHasLiftDescription = " a functional lift,";
						propDescription += propHasLiftDescription;
					}

					switch (propHeatingType) {
						case "electric":
							propHeatingTypeDescription = " and warm, comforting electric heating."
							propDescription += propHeatingTypeDescription;
							break;
						case "gas":
							propHeatingTypeDescription = " and warm, comforting gas central heating.";
							propDescription += propHeatingTypeDescription;
							break;
						case "oil":
							propHeatingTypeDescription = " and warm, comforting oil fired central heating.";
							propDescription += propHeatingTypeDescription;
							break;
						default:
							propHeatingTypeDescription = " and a warm, maintainable heat exchanger.";
							propDescription += propHeatingTypeDescription;
							break;
					}

					if (propExtraCosts > 0) {
						propExtraCostsDescription = " It comes with additional costs of only £" + propExtraCosts + " per month";
					} else {
						propExtraCostsDescription = " It comes with no additional costs";
					}
					propDescription += propExtraCostsDescription;

					switch (propTenure) {
						case "freehold":
							propTenureDescription = " and is a freehold property, so no need to worry about lease renewals.";
							propDescription += propTenureDescription;
							break;
						case "leasehold":
							propTenureDescription = " and is a leasehold property";
							propDescription += propTenureDescription;
							break;
						case "flying-freehold":
							propTenureDescription = " and is a flying freehold property.";
							propDescription += propTenureDescription;
							break;
						case "share-of-freehold":
							propTenureDescription = " and is a share of freehold property.";
							propDescription += propTenureDescription;
							break;
					}

					if (propTenure === "leasehold") {
						propLeaseYearsDescription = " with " + propLeaseYears + " years left on the lease.";
						propDescription += propLeaseYearsDescription;
					}

					propDescription += " Don't miss out, book a viewing today!";
				}


				break;

			default:

				console.log("Unknown Error");
				break;
		}
		
		
		let descriptionGenerator = propDescription;

this.setState({
	description: descriptionGenerator,
	generatedDescription: descriptionGenerator,
	manualEdit: false
});
	}
}

export default Listing;