import React from 'react';

class Publish extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            listing: null,
            slug: null,
            token: null
        };
    }

    componentDidMount() {
		var url = new URL(window.location.href);
		var slug = url.searchParams.get("slug");

		let user = JSON.parse(localStorage.getItem("user"));
		if(user !== undefined && user !== null){
			let token = user.token;

			this.setState({
				token: token,
				slug: slug
			}, () => {

				var myHeaders = new Headers();
				myHeaders.append("Content-Type", "application/json");
				myHeaders.append("X-Requested-With", "XMLHttpRequest");
				myHeaders.append("Authorization", "Token " + token);

				var requestOptions = {
					method: 'GET',
					headers: myHeaders,
					redirect: 'follow'
				};

				fetch("https://freeco-api.herokuapp.com/api/listings/" + slug, requestOptions)
					.then(response => response.json())
					.then(result => {
						this.setState({
							listing: result.listing
						}, () => {
							console.log(this.state.listing);
						});
					})
					.catch(error => {
						console.log('error', error);
						window.location.href = "/login";
					});
				});
			} else {
				window.location.href = "/login";
			}
    }
    
    publish = () => {
		if(this.state.listing !== null){
			var myHeaders = new Headers();
			console.log(this.state.token);
			myHeaders.append("Content-Type", "application/json");
			myHeaders.append("X-Requested-With", "XMLHttpRequest");
			myHeaders.append("Authorization", "Token " + this.state.token);

			var raw = JSON.stringify({"listing":{"isPublished":true}});
			if(this.state.listing.isPublished === true){
				raw = JSON.stringify({"listing":{"isPublished":false}});
			}

			var requestOptions = {
				method: 'PUT',
				headers: myHeaders,
				body: raw,
				redirect: 'follow'
			};

			fetch("https://freeco-api.herokuapp.com/api/listings/" + this.state.slug, requestOptions)
			.then(response => response.text())
			.then(result => {
				let data = JSON.parse(result);
				console.log(data);
				this.setState({
					listing: data.listing
				}, () => {
					console.log(this.state.listing);
				});
			})
			.catch(error => console.log('error', error));
		}
    }
    
    prevPage = () => {
        window.location.href = "/dashboard/?slug=" + this.state.slug;
    }

    render() {
		let checked = "";
		let header = "Your advert is not published";
		let can = "will be able to";
		let glory = "What are you waiting for?";
		let disabled = "disabled";
		if(this.state.listing !== null){
			if(this.state.listing.isPublished){
				checked = "checked";
				header = "Your advert is live!";
				can = "can now";
				glory = "Check it out in all it's glory!";
				disabled = "";
			}
        }
        
        return(
            <div className="listing-flow">
                <div className="navbar">
					<button className="back" onClick={this.prevPage}>
						<img className="backArrow" src={require("./img/icon-arrow-back.svg")}></img>
						<div className="backText">
							Back
						</div>
					</button>
					<img className="logo" src={require("./img/logo.svg")}></img>
				</div>
				
				<div className="pageHeading">
					{header}
				</div>
				<div className="pageContent">
					X million people {can} view your home.<br/>
					{glory}
				</div>
                <a href="https://zoopla.com" target="_blank">
                    <button disabled={disabled} className="adBtn">
                        Zoopla <img className="rightArrow" src={require("./img/icon-arrow-forward.svg")}></img>
                    </button>
                </a><br />
                <a href="https://homeshare.com" target="_blank">
                    <button disabled={disabled} className="adBtn">
                        homeshare <img className="rightArrow" src={require("./img/icon-arrow-forward.svg")}></img>
                    </button>
                </a><br />
                <a href="https://primelocation.com" target="_blank">
                    <button disabled={disabled} className="adBtn">
                        PrimeLocation <img className="rightArrow" src={require("./img/icon-arrow-forward.svg")}></img>
                    </button>
                </a><br />
                <a href="https://gumtree.com" target="_blank">
                    <button disabled={disabled} className="adBtn">
                        Gumtree <img className="rightArrow" src={require("./img/icon-arrow-forward.svg")}></img>
                    </button>
                </a><br />
                <a href="https://rightmove.com" target="_blank">
                    <button disabled="disabled" className="adBtn">
                        Rightmove <img className="rightArrow" src={require("./img/icon-arrow-forward.svg")}></img>
                    </button>
                </a>
                <button className="publishBtn adBtn mB">
                    Publish advert
                    <label className="switch">
                        <input type="checkbox" checked={checked}></input>
                        <span className="slider round sliderSwitch" onClick={this.publish}></span>
                    </label>
                </button>
            </div>
        );
    }
}

export default Publish;