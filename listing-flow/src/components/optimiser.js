import React from 'react';
import json from './outcodes.json';
import "scroll-behavior-polyfill";

class Optimiser extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            data: null,
            location: 1,
            numRooms: 2,
            propArray: [],
            greaterPropArray: [],
            lesserPropArray: [],
            withUserPrice: [],
            featured: [],
            pages: ["Loading..."],
            userPrice: 530000,
            index: 24,
            outcodes: json,
            postcode: "Loading...",
            postcodeNotFound: false,
            locationError: null,
            predictionArray: null,
            pageNumber: null,
            pagePosition: null,
            links: [],
            scroll: false,
            loadingUpdate: null,
            started: false,
            loading: <div className="middle-container" id="loading">
                        <div className="top loading"><div className="section-content">
                            <div className="section-info">
                            <img className="logo" src={require("./img/splashHome.svg")}></img>
                                <div id="loader" className="loader">
                                    <div id="loader2" className="loader2">
                                        <div id="loader3" className="loader3">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        };
    }

    async componentWillMount() {
        let begin = 0;
        let end = this.state.outcodes.length;
        let prediction = [];
        if(this.state.postcode !== undefined){
            for(var k = begin; k <= end; k++){
                if(this.state.outcodes[k] !== undefined){
                        let expr = (
                            <option value={this.state.outcodes[k].outcode}></option>
                        );
                        prediction.push(expr);
                }
            }
        }

        this.setState({
            predictionArray: prediction
        });
    }

    async componentDidMount() {
        // Using a proxy to work around the CORS issue:
        const proxy = "https://cors-anywhere.herokuapp.com/";
        
        // Without a proxy, used a chrome extension to circumvent the issue, which I think arose due to the fact that it was being run locally:
        const url = "https://optimiser-proxy.herokuapp.com/optimiser-proxy?location=" + this.state.location + "&numRooms=" + this.state.numRooms + "&index=" + this.state.index;

        const response = await fetch(url);
        const data = await response.json();
        this.setState({
            data: data
        });
        this.sortData();
    }

    sortData() {
        let numPropResponses = this.state.data.numReturnedResults;
        let totalResults = this.state.data.totalAvailableResults;
        let featuredPropertyNum = this.state.data.featuredProperties.length;
        for(let i = 0; i < featuredPropertyNum; i++){
            let link = this.state.data.featuredProperties[0].identifier;
            this.setState({
                featured: this.state.featured.concat(<a href={"https://www.rightmove.co.uk/property-for-sale/property-"+link+".html"} target="_blank" rel="noopener noreferrer">Featured</a>)
            });
        }

        for(let i = 0; i < numPropResponses; i++){
            let price = this.state.data.properties[i].price;
            let link = this.state.data.properties[i].identifier;
            this.setState({
                propArray: this.state.propArray.concat(price),
                links: this.state.links.concat(link)
            });
        }

        if(this.state.index >= totalResults){
            this.sortPropArray();
        }

        if(this.state.index < totalResults){
            this.state.index += 24;
            this.componentDidMount();
        }
    }

    sortPropArray() {
        let begin = 0;
        let end = this.state.outcodes.length;
        let prediction = [];
        if(this.state.postcode !== undefined){
            for(var k = begin; k <= end; k++){
                if(this.state.outcodes[k] !== undefined){
                        let expr = (
                            <option value={this.state.outcodes[k].outcode}></option>
                        );
                        prediction.push(expr);
                }
            }
        }

        this.setState({
            propArray: this.state.propArray.sort(function(a, b){return b-a})
        }, () => {
            this.filterPropArray();
        });
    }

    filterPropArray() {
        let greaterArray = [];
        let lesserArray = [];

        for(let i = 0; i < this.state.propArray.length; i++){
            if(this.state.propArray[i] > this.state.userPrice){
                greaterArray.push(this.state.propArray[i]);
            } else {
                lesserArray.push(this.state.propArray[i]);
            }
        }

        this.setState({
            greaterPropArray: greaterArray,
            lesserPropArray: lesserArray
        }, () => {
            this.findPostcode();
        });
    }

    findPostcode() {
        this.setState({
            postcode: this.state.outcodes[this.state.location - 1].outcode,
            postcodeNotFound: false
        }, () => {
            this.formatProps();
        });
    }

    formatProps(){
        let currentArray = [];

        let count = 1;

        for(let i = 0; i < this.state.greaterPropArray.length; i++){
            let link = this.state.links[i];
            let place = count % 24;
            if(place === 0){
                place = 24;
            }
            currentArray.push(<a href={"https://www.rightmove.co.uk/property-for-sale/property-"+link+".html"} target="_blank" rel="noopener noreferrer">{place}<br/></a>);
            count++;
        }

        let pageNumber = Math.floor(this.state.greaterPropArray.length / 24 + 1);
        let pagePosition = this.state.greaterPropArray.length % 24 + 1;
        let linkLocation = 1;
        if(this.state.featured.length >= pageNumber){
            pagePosition = this.state.greaterPropArray.length % 24 + 2;
            linkLocation = 2;
        }
        let place2 = count % 24;
        if(place2 === 0){
            place2 = 24;
        }
        currentArray.push(<div id="userPrice" className="optimiser-user-price">{place2}<br/></div>);

        count++;

        for(let i = 0; i < this.state.lesserPropArray.length; i++){
            let link = this.state.links[i + ((pageNumber-1)*24 + pagePosition - linkLocation)];
            let place = count % 24;
            if(place === 0){
                place = 24;
            }
            currentArray.push(<a href={"https://www.rightmove.co.uk/property-for-sale/property-"+link+".html"} target="_blank" rel="noopener noreferrer">{place}<br/></a>);
            count++;
        }

        this.setState({
            withUserPrice: currentArray,
            pageNumber: pageNumber,
            pagePosition: pagePosition
        }, () => {
            this.splitProps();
        });
    }

    splitProps() {
        const chunkSize = 24;
        const arr = this.state.withUserPrice;
        const groups = arr.map((e, i) => { 
            return i % chunkSize === 0 ? arr.slice(i, i + chunkSize) : null; 
        }).filter(e => { return e; });

        let defaultPages = [];
        let count = 1;

        for(let i = 0; i < groups.length; i++){
            let tableStructure = (
                <div className="optimiser-td" id={count}>
                    <span className="optimiser-page">Page {count}</span>
                    <ul>
                        <li>{this.state.featured[i]}</li>
                        {groups[i]}
                    </ul>
                </div>
            );

            defaultPages.push(tableStructure);
            count++;
        }

        this.setState({
            pages: defaultPages,
            loading: null,
            loadingUpdate: null
        }, () => {
            if(this.state.scroll === true){
                this.scrollToObject();
                this.setState({
                    scroll: false
                });
            }
        });
    }

    updatePrice = (event) => {
        this.setState({
            userPrice: event.target.value
        }, () => {
            this.sortPropArray();
        });
    }

    updateRooms = (event) => {
        this.setState({
            numRooms: event.target.value
        });
    }

    updatePostcode = (event) => {
        this.setState({
            postcode: event.target.value.trim().toUpperCase()
        }, () => {
            let postcode = this.state.postcode;
            let result = this.state.outcodes.filter(function(e){return e.outcode == postcode});
            if(result[0] !== undefined){
                this.setState({
                    postcodeNotFound: false,
                    location: result[0].locationIdent,
                });
            } else {
                this.setState({
                    postcodeNotFound: true
                });
            }
        });
    }

    runUpdate = () => {
        this.setState({
            propArray: [],
            featured: [],
            index: 24,
            locationError: null,
            links: [],
            scroll: true,
            loadingUpdate: <div className="middle-container" id="loading">
                        <div className="top loading"><div className="section-content">
                            <div className="section-info">
                            <img className="logo" src={require("./img/splashHome.svg")}></img>
                                <div className="loader">
                                    <div className="loader2">
                                        <div className="loader3">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        }, () => {
            this.componentDidMount();
        });
    }

    scrollToObject = () => {
        document.getElementById(this.state.pageNumber).scrollIntoView({inline: "center"});
    }

    scrollToHome = () => {
        document.getElementById("slide1").scrollIntoView({inline: "center"});
    }

    increasePrice = () => {
        this.setState({
            userPrice: this.state.greaterPropArray[this.state.greaterPropArray.length - 1],
            scroll: true
        }, () => {
            this.sortPropArray();
        });
    }

    decreasePrice = () => {
        this.setState({
            userPrice: this.state.lesserPropArray[0] - 50,
            scroll: true
        }, () => {
            this.sortPropArray();
        });
    }

    startSession = () => {
        this.setState({
            started: true
        });
    }

    plusPrice = (e) => {
        e.preventDefault();
        let newPrice = parseInt(this.state.userPrice) + 5000;
        this.setState({
            userPrice: newPrice,
            scroll: false
        }, () => {
            this.sortPropArray();
        });
    }

    minusPrice = (e) => {
        e.preventDefault();
        let newPrice = this.state.userPrice - 5000;
        if(newPrice < 0){
            newPrice = this.state.userPrice;
        }
        this.setState({
            userPrice: newPrice,
            scroll: false
        }, () => {
            this.sortPropArray();
        });
    }

    plusRooms = (e) => {
        e.preventDefault();
        this.setState({
            numRooms: parseInt(this.state.numRooms) + 1,
            scroll: false
        });
    }

    minusRooms = (e) => {
        e.preventDefault();
        if(this.state.numRooms !== 0){
            this.setState({
                numRooms: this.state.numRooms - 1,
                scroll: false
            });
        }
    }

    render() {
        let moveUp;
        if(this.state.greaterPropArray.length !== 0){
            moveUp = <span className="note2">To move up a position, increase your price to <span className="score">£{this.state.greaterPropArray[this.state.greaterPropArray.length - 1]}</span><br/></span>;
        }

        let display = "none";
        if(this.state.loading === null){
            display = "block";
        }

        let opacity = "0.2";
        let scroll = "none";
        if(this.state.loadingUpdate === null){
            opacity = "1";
            scroll = "block"
        }

        let greaterDisplay = <button style={{cursor: "pointer"}} className="positionBtn incrBtn" onClick={this.increasePrice}>Increase Position</button>;
        if(this.state.greaterPropArray.length === 0){
            greaterDisplay = <button style={{cursor: "auto"}} className="positionBtn incrBtn disableBtn">Increase Position</button>;
        }

        let lesserDisplay = <button style={{cursor: "pointer"}} className="positionBtn decrBtn" onClick={this.decreasePrice}>Decrease Position</button>;
        if(this.state.lesserPropArray.length === 0){
            lesserDisplay = <button style={{cursor: "auto"}} className="positionBtn decrBtn disableBtn">Decrease Position</button>;
        }

        let colour = "#02bc8320";
        if(this.state.postcodeNotFound === true){
            colour = "#bc020240";
        }

        let priceColour = "#bc020240";
        if(this.state.userPrice > 0){
            priceColour = "#02bc8320";
        }

        let roomColour = "#bc020240";
        if(this.state.numRooms >= 0){
            roomColour = "#02bc8320";
        }

        return (
            <div className="App">
            
            
            {this.state.loading}
            
                <div className="main" style={{display: display, opacity: opacity}}>
                <div valign="center" id="slide1">
                    <div className="formContent" id="formHouse">
                        {/* <img className="logo" src={require("./img/splashHome.svg")}></img> */}

                        {/* <br/> */}

                        <span id="comment">How visible is your property?</span>

                        <br/>

                        <span id="fillComment">Fill out your details to find out!</span>
                        
                        <br/><br/>

                        <div className="propertyForm">

                            <label>Price of Property<br/><span className="poundSign">£</span>
                                <span className="inputBar">
                                    <input 
                                        style={{backgroundColor: priceColour}}
                                        id="priceInput"
                                        type="number" 
                                        min="0" 
                                        value={this.state.userPrice} 
                                        placeholder={this.state.userPrice} 
                                        onChange={this.updatePrice}
                                        step="50">
                                    </input>
                                    <button onClick={this.minusPrice} className="negative"><i className="fa fa-minus" aria-hidden="true"></i></button>
                                    <button onClick={this.plusPrice} className="plus"><i className="fa fa-plus" aria-hidden="true"></i></button>
                                </span>
                            </label>
                            &nbsp;
                            <span className="tooltip">
                                <i className="fas fa-info-circle"></i>
                                <span className="tooltiptext">The price that you would like to sell your house at</span>
                            </span>

                            <br/>

                            <label>Location of Property<br/>
                                <input
                                    style={{backgroundColor: colour}}
                                    id="locationInput"
                                    type="text"
                                    list="postcodes" 
                                    name="myBrowser" 
                                    value={this.state.postcode}
                                    onChange={this.updatePostcode}
                                />
                            </label>
                            <datalist id="postcodes">
                                {this.state.predictionArray}
                            </datalist>
                            &nbsp;
                            <span className="tooltip">
                                <i className="fas fa-info-circle"></i>
                                <span className="tooltiptext">The postcode district of your location, e.g. SW2</span>
                            </span>

                            <br/>

                            <label>Number of Bedrooms <br/>
                                
                                <input 
                                    style={{backgroundColor: roomColour}}
                                    type="number" 
                                    min="0" 
                                    value={this.state.numRooms} 
                                    placeholder={this.state.numRooms} 
                                    onChange={this.updateRooms}>
                                </input>
                                <button onClick={this.minusRooms} className="negative"><i className="fa fa-minus" aria-hidden="true"></i></button>
                                <button onClick={this.plusRooms} className="plus"><i className="fa fa-plus" aria-hidden="true"></i></button>
                            </label>
                            &nbsp;
                            <span className="tooltip">
                                <i className="fas fa-info-circle"></i>
                                <span className="tooltiptext">The number of bedrooms in your house</span>
                            </span>

                        </div>

                        <br className="firstBreak"/>

                        <div className="notes">
                            <span className="note">You are number <span className="score">{this.state.pagePosition}</span> on page <span className="score">{this.state.pageNumber}</span></span><br/>
                            {/* {moveUp} */}
                        </div>

                        <br className="secondBreak"/>
                        
                        <button className="updateBtn" onClick={this.runUpdate}>UPDATE</button>

                        <button className="scrollBtn" onClick={this.scrollToObject}>Scroll to see listings <span className="fa fa-angle-down scrollToPages"></span></button>

                    </div>
                </div>

                <div valign="center" id="slide2" style={{display: scroll}}>

                    <div className="formContent">

                        <span className="alterPriceText">Page <span className="score">{this.state.pageNumber}</span>, number <span className="score">{this.state.pagePosition}</span></span>
                        
                        <br className="firstBreak"/>
                        <br className="firstBreak"/>

                        <span id="clickNote">
                            <small>Click on a listing to view the property!</small>
                        </span>

                        <span className="fadeLeft"></span>
                        <div className="scrollTable">
                            <span className="optimiser-table">
                                {this.state.pages}
                            </span>
                        </div>
                        <span className="fadeRight"></span>

                        <br className="secondBreak"/>

                        {/* {lesserDisplay}
                        {greaterDisplay} */}
                        <label><br/><span className="poundSign">£</span>
                            <span className="inputBar">
                                <input 
                                    style={{backgroundColor: priceColour}}
                                    id="priceInput"
                                    type="number" 
                                    min="0" 
                                    value={this.state.userPrice} 
                                    placeholder={this.state.userPrice} 
                                    onChange={this.updatePrice}
                                    step="50">
                                </input>
                                <button onClick={this.minusPrice} className="negative"><i className="fa fa-minus" aria-hidden="true"></i></button>
                                <button onClick={this.plusPrice} className="plus"><i className="fa fa-plus" aria-hidden="true"></i></button>
                            </span>
                        </label>

                    </div>
                </div>
            </div>
            {this.state.loadingUpdate}
        </div>
        );
    }
}

export default Optimiser;